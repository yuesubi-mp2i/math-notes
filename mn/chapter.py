import os

from . import fs


PART_PREFIX_LEN = len("part")
PART_EXT_LEN = len(".typ")


def get_chapter_path(chapter: int) -> str:
    two_digit_number = f"{chapter:0>2}"
    chapter_dir_name = f"c{two_digit_number}"
    return os.path.join(fs.CLASS, chapter_dir_name)


def is_part_file_name(part: str) -> bool:
    return len(part) >= 4 and part[0:4] == "part"


def get_part_number_from_file_name(part: str) -> int:
    assert len(part) >= PART_PREFIX_LEN + 1 + PART_EXT_LEN
    str_number = part[PART_PREFIX_LEN : -PART_EXT_LEN]
    return int(str_number)


def find_parts_of_chapter(chapter_path: str) -> list[str]:
    assert os.path.exists(chapter_path)

    return list(sorted(filter(
        is_part_file_name,
        os.listdir(chapter_path)
    )))