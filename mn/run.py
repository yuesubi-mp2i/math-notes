import os
import shutil

from . import fs
from .args import Args
from .apkg import apkg
from .chapter import *


def anki(args: Args.Anki):
    deck = os.path.join(fs.ROOT, "deck")
    media = os.path.join(deck, "media")
    csv = os.path.join(deck, "deck.csv")
    os.mkdir(deck)
    os.mkdir(media)

    out_deck = os.path.join(fs.OUT, "partial-deck")
    out_media = os.path.join(out_deck, "media")
    out_csv = os.path.join(out_deck, "deck.csv")

    chapter_path = get_chapter_path(args.chapter)
    chapter_name = os.path.basename(chapter_path)

    for part_name in find_parts_of_chapter(chapter_path):
        if os.path.exists(out_deck):
            shutil.rmtree(out_deck)

        part_path = os.path.join(chapter_path, part_name)

        part_number = get_part_number_from_file_name(part_name)
        name = f"{chapter_name}p{part_number}"

        cmd = (f"math2anki make {part_path} -n {name} -o {out_deck} " +
            "-i template/ytemplate/math_layout.typ")
        
        if args.chapter < 8:
            cmd += " -c"

        print(cmd)
        ret = os.system(cmd)
        assert ret == 0
    
        for file in os.listdir(out_media):
            shutil.copy(os.path.join(out_media, file), os.path.join(media, file))

        os.system(f"cat {out_csv} >> {csv}")


def class_(args: Args.Class):
    chap_dir = get_chapter_path(args.chapter)
    os.makedirs(fs.CLASS, exist_ok=True)
    chap = os.path.join(chap_dir, "chap.typ")

    if not os.path.exists(chap_dir):
        os.makedirs(chap_dir, exist_ok=True)
        shutil.copy(fs.SAMPLE_CHAP, chap)

        _append_to_file(fs.CLASS_COMPLETE,
            f"#include \"{os.path.join(os.path.basename(chap_dir), 'chap.typ')}\""
        )
    
    part = os.path.join(chap_dir, f"part{args.part}.typ")
    if not os.path.exists(part):
        shutil.copy(fs.SAMPLE_PART, part)

        _append_to_file(chap, f"#include \"{os.path.basename(part)}\"")
    
    if args.chapter >= 13:
        _add_gitlab_ci_job(args.chapter)
        _add_anki_to_index(args.chapter)
    
    _start_editing(part)


def complete(_args: Args.Complete):
    os.system(f"typst compile {fs.CLASS_COMPLETE} {fs.COMPLETE_PDF} --root .")
    

def draft(args: Args.Draft):
    os.makedirs(fs.DRAFT, exist_ok=True)
    
    draft = os.path.join(fs.DRAFT, args.name + ".typ")
    if not os.path.exists(draft):
        shutil.copy(fs.SAMPLE_DRAFT, draft)

    _start_editing(draft)


def td(args: Args.Td):
    chap_dir = os.path.join(fs.TD, args.chapter)
    os.makedirs(fs.TD, exist_ok=True)
    os.makedirs(chap_dir, exist_ok=True)
    
    exercise = os.path.join(chap_dir, args.exercise + ".typ")
    if not os.path.exists(exercise):
        shutil.copy(fs.SAMPLE_EX, exercise)

    _start_editing(exercise)


def _start_editing(file: str):
    os.makedirs(fs.OUT, exist_ok=True)
    shutil.copy(fs.SAMPLE_EDITING, fs.EDITING)

    to_file = os.path.relpath(
        os.path.normpath(file),
        os.path.normpath(fs.OUT)
    )
    _append_to_file(fs.EDITING, f"#include \"{to_file}\"")

    os.system(f"typst watch {fs.EDITING} {fs.EDITING_PDF} --root {fs.ROOT}")


def _append_to_file(file: str, text: str):
    with open(file, "a") as f:
        f.write("\n" + text)

def _add_gitlab_ci_job(chapter: int):
    ci_path = os.path.join(fs.ROOT, ".gitlab-ci.yml")

    is_chapter_there = False
    with open(ci_path, "r") as file:
        for line in file.readlines():
            is_chapter_there = is_chapter_there or f"anki-c{chapter}:" in line
        
    if is_chapter_there:
        return

    chapter_name = os.path.basename(get_chapter_path(chapter))
    
    ci_job = [
        "\n",
        "\n",
        f"anki-c{chapter}:\n",
        '  image: "123marvin123/typst"\n',
        "  stage: anki\n",
        "  variables:\n",
        "    GIT_SUBMODULE_STRATEGY: recursive\n",
        "  script:\n",
        "    - apt-get -y -qq update && apt-get -y -qq upgrade\n",
        "    - apt-get -y -qq install python3-pip\n",
        "    - pip install genanki\n",
        f"    - python3 mn.py apkg {chapter}\n",
        "  artifacts:\n",
        "    paths:\n",
        f'      - "math_c{chapter}.apkg"\n',
        # "  rules:\n",
        # "    - changes:\n",
        # f"      - class/{chapter_name}/*.typ\n"
    ]

    with open(ci_path, "a") as file:
        file.writelines(ci_job)


def _add_anki_to_index(chapter: int):
    index_path = os.path.join(fs.ROOT, "anki-index.md")

    is_chapter_there = False
    with open(index_path, "r") as file:
        for line in file.readlines():
            is_chapter_there = is_chapter_there or f"[C{chapter}]" in line
        
    if is_chapter_there:
        return

    chapter_name = os.path.basename(get_chapter_path(chapter))
    
    index_item = [
        "\n",
        f"- [C{chapter}](https://gitlab.com/api/v4/projects/51473655/jobs/artifacts/main/raw/math_c{chapter}.apkg?job=anki-c{chapter})"
    ]

    with open(index_path, "a") as file:
        file.writelines(index_item)