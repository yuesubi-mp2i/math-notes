import os


ROOT = os.path.join(os.path.dirname(__file__), "..")

CLASS = os.path.join(ROOT, "class")
DRAFT = os.path.join(ROOT, "draft")
TD = os.path.join(ROOT, "td")

CLASS_COMPLETE = os.path.join(CLASS, "complete.typ")
COMPLETE_PDF = os.path.join(ROOT, "complete.pdf")

TEMPLATE = os.path.join(ROOT, "template")
TEMPLATE_FILE = os.path.join(ROOT, TEMPLATE, "file")

SAMPLE_CHAP = os.path.join(TEMPLATE_FILE, "chap.typ")
SAMPLE_DRAFT = os.path.join(TEMPLATE_FILE, "draft.typ")
SAMPLE_EDITING = os.path.join(TEMPLATE_FILE, "editing.typ")
SAMPLE_EX = os.path.join(TEMPLATE_FILE, "ex.typ")
SAMPLE_PART = os.path.join(TEMPLATE_FILE, "part.typ")

OUT = os.path.join(ROOT, "out")
EDITING = os.path.join(OUT, "editing.typ")
EDITING_PDF = os.path.join(OUT, "editing.pdf")