import genanki
import os
import shutil

from .. import fs
from ..args import Args
from ..chapter import *

from .math2anki.src.cli_args import MakeArgs
from .math2anki.src.make.main import create_deck


class MyNote(genanki.Note):
    @property
    def guid(self):
        return genanki.guid_for(
            self.fields[0],
            compute_legacy_back_side_from_front(self.fields[0])
        )
    

LEGACY_LETTER_TO_MODIFY = len("f.svg width=100%>")
    
def compute_legacy_back_side_from_front(front_side: str) -> str:
    # Make "<img src=chapXpY_xxxx_b width=100%>" out of
    # "<img src=chapXpY_xxxx_f width=100%>"

    back_side = (front_side[:-LEGACY_LETTER_TO_MODIFY] + "b" +
        front_side[-LEGACY_LETTER_TO_MODIFY + 1:])
    
    return back_side


def get_first_line_of_file(file_path: str) -> str:
    with open(file_path, "r") as file:
        return file.readline().rstrip("\n")


def apkg(args: Args.Apkg):
    deck = os.path.join(fs.OUT, "deck")
    media = os.path.join(deck, "media")
    # csv = os.path.join(deck, "deck.csv")
    os.makedirs(media, exist_ok=True)

    partial_deck = os.path.join(fs.OUT, "partial-deck")
    partial_media = os.path.join(partial_deck, "media")
    # partial_csv = os.path.join(partial_deck, "deck.csv")

    cards = []
    media_files = []

    chapter_path = get_chapter_path(args.chapter)
    chapter_name = os.path.basename(chapter_path)

    if args.chapter <= 12:
        print("""  Attention aux MP2I 2023/2024 !
Si vous importez ce deck et que vous l'aviez téléchargé pendant cette année, la progression sur ce deck sera perdue !""")

    for part_name in find_parts_of_chapter(chapter_path):
        if os.path.exists(partial_deck):
            shutil.rmtree(partial_deck)

        part_path = os.path.join(chapter_path, part_name)

        part_number = get_part_number_from_file_name(part_name)
        name = f"{chapter_name}p{part_number}"

        make_args = MakeArgs()
        make_args.typ_source = part_path
        make_args.out_dir = partial_deck
        make_args.name = name
        make_args.include = "template/ytemplate/math_layout.typ"
        make_args.hash_with_kind = True

        deck = create_deck(make_args)
        cards.extend(deck.cards)
        
        print("Generating " + part_name)
    
        for file in os.listdir(partial_media):
            src = os.path.join(partial_media, file)
            dest = os.path.join(media, file)
            shutil.copy(src, dest)

            media_files.append(dest)
    
    deck_name = get_first_line_of_file(os.path.join(chapter_path, "chap.typ"))[3:]

    my_deck = genanki.Deck(
        2059400110 + args.chapter,
        f'PRÉPA::MP2I::MATHS::{chapter_name.capitalize()} {deck_name}'
    )

    for card in cards:
        note = MyNote(
            model=genanki.BASIC_MODEL,
            fields=[card.front, card.back]
        )

        my_deck.add_note(note)

    my_pkg = genanki.Package(my_deck)
    my_pkg.media_files = media_files
    my_pkg.write_to_file(f"math_{chapter_name}.apkg")