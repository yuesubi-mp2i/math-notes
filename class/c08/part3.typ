#import "../../template/ytemplate.typ": *


#ypart[ Relations binaires ]


#ydef(name: [Relation binaire])[
    Une *relation $cal(R)$ (binaire)* sur un ensemble $E$
    est un prédicat défini sur $E^2$.

    Pour $(x, y) in E^2$, on écrit $x cal(R) y$ si le prédicat
    est vrai évalué en $(x, y)$.
]

#yexample[
    + $E = NN$, $cal(R) = <=$
    + $E$ un ensemble, $=$ est une relation sur $E$.
    + Dans le DS2, il y avait aussi une relation.
]

#ydef(name: [Relation binaire H.-P.])[
    Une relation $cal(R)$ est une partie $G$ de $E times E$
    telle que
    $ x cal(R) y <=> (x,y) in G $
]


=== 3.1 Relations d'équivalences


#ybtw[
    + Ruban de möbius.
    + Carte de jeu vidéo réprésentée avec un tor.
]

#ydef(name: [Relation d'équivalence])[
    Soit $cal(R)$ une relation sur $E$.
    On dit que $cal(R)$ est une *relation d'équivalence* si
    + $forall x in E, x cal(R) x$ (réflexivité)
    + $forall (x,y) in E^2, (x cal(R) y => y cal(R) x)$ (symétrie)
    + $forall (x,y,z) in E^3, x cal(R) y and y cal(R) z => x cal(R) z$
        (transitivité)
]

#yexample[
    + $<=$ sur $NN$ n'est pas une relation déquivalence car non symétrique.

    + $=$ est une relation d'équivalence.

    + $subset$ sur $cal(P)(E)$ n'est pas une relation d'équivalence
        car non symétrique.

    + $equiv$ modulo 5 sur $ZZ$ est une relation d'équivalence.

    + $E$ l'ensemble des éléves du lycée, $x cal(R) y$ ssi (si et seulement si)
        $x$ et $y$ sont dans la même classe.

        $cal(R)$ est une relation d'équivalence.
]

#ydef(name: [Classe d'équivalence])[
    Soit $tilde$ une relation d'équivalence sur $E$.
    Soit $x in E$

    La *classe (d'équivalence)* de $x$ (*modulo $tilde$*) est
    $ overline(x) = "Cl"(x) = { y in E | x tilde y } $
]

#yexample[
    + Sur $ZZ$, $a tilde b <=> a equiv b [3]$.
        - $overline(0) = 3ZZ$
        - $overline(1) = { 3k + 1 | k in ZZ } = 3ZZ + 1$
        - $overline(2) = { 3k + 2 | k in ZZ } = 3ZZ + 2$
        - $overline(3) = overline(0)$, $overline(4) = overline(1)$
        Donc ${overline(0), overline(1), overline(2)} = ZZ/(3ZZ)$
 
        Et $overline(2) + overline(1) = overline(2+1) = overline(3) = overline(0)$

        $overline(2) times overline(2) = overline(4) = overline(1)$
    
    + Avec $ZZ/(6ZZ)$
        $ cases(overline(2) times overline(3) = overline(0),
            overline(2) != overline(0) and overline(3) != overline(0)) $
        $overline(z)^2 = overline(z)$ n'as pas de solutions
    
    + Construction de $ZZ$ à partir de $NN$.

        On veut définir $-2$ comme
        $ 0-2 = 4-6 $
        i.e. $-2 = overline((0,2))$

        On pose $E = NN^2$. On définit une relation sur $E$ par :
        $ (a,b) tilde (a', b') <=> a+b' = a'+b $

        On démontre que $tilde$ est une relation d'équivalence.
        $ ZZ = { overline((a, b)) | (a, b) in E } $

        Soit $(a,b) in E$.
        - Si $a >= b$, $overline((a,b)) = overline((a-b, 0))$
        - Si $a < b$, $overline((a,b)) = overline((0, b-a))$

        Donc, on pose
        $ Z = {overline((n,0)) | n in NN}
            ytt union {overline((0,n)) | n in NN}
            yt = ZZ^+ union ZZ^- $
        
        On pose
        $ i: &NN --> ZZ \
            &n |-> overline((n,0)) $
        
        On identifie $n$ et $overline((n,0))$.

        On pose $-n = overline((0, n))$

        $ overline((a,b)) + overline((c,d)) = overline((a+c, b+d)) $

        Par exemple :
        $ 5 - 7 &= overline((5,0)) - overline((0,7)) \
            &= overline((0,2)) = -2 $
    
    + On pose $E$ l'ensemble des polynômes à coefficients réels.

        $P tilde Q <=>
            yt P - Q "dérivable par" (x^2 + 1)$

        On pose
        $ CC = {overline(P) | P in E} = E/tilde $
        On définit $i = overline(x)$.
]

#ytheo(name: [Classes d'équivalence de deux éléments en relation d'équivalence])[
    Soient $E$ un ensemble muni d'une relation d'équivalence $tilde$,
    et $(x,y) in E^2$.
    $ x tilde y <=> overline(x) = overline(y) $
]

#yproof[
    - "$==>$" : On suppose $x tilde y$.

        - Soit $u in overline(x)$.

            Par définition, $u tilde x$.
            Et comme $tilde$ est transitive, $u tilde y$,
            et donc $u in overline(y)$.

            On a prouvé que $overline(x) subset overline(y)$.

        - Soit $u in overline(y)$.

            Donc $u tilde y$.
            Et comme $tilde$ est symétrique, $y tilde x$.
            Et comme $tilde$ est transitive, $u tilde x$, i.e. $u in overline(x)$.

            On a prouvé que $overline(y) subset overline(x)$.
        
        Ainsi $overline(x) = overline(y)$.
    
    - "$<==$" : On suppose $overline(x) = overline(y)$.

        Comme $tilde$ est réflexive, $x tilde y$, i.e. $x in overline(x)$,
        donc $x in overline(y)$, et donc $x tilde y$.

    $square$
]

#ytheo(name: [Réunion de représentants d'une relation d'équivalence])[
    Soit $E$ un ensemble, $tilde$ une relation d'équivalence,
    et $R$ un système de représentants
    (chaque classe d'équivalence contient un élément et un seul de $R$).

    Alors
    $ E = union.dot.big_(x in R) overline(x) $
    (où $union.dot$ est la réunion disjointe 2 à 2)
]

#yproof[
    - Soient $(x,y) in R^2$, avec $x != y$,
        et $z in overline(x) sect overline(y)$.

        Alors $z tilde x$ et $z tilde y$.
        Donc $x tilde y$, donc $overline(x) = overline(y)$.

        Donc $x$ et $y$ représentent la même classe :
        une contradiction.

        Ainsi $overline(x) sect overline(y) = emptyset$.

        La réunion est bien disjointe.
    
    - Soit $u in E$.

        Par définition de $R$, il y a un $x$ dans $R$
        qui représente $overline(u)$ : $overline(x) = overline(u)$.
        Donc
        $ u in overline(u) = overline(x) $
        et donc
        $ u in union.big_(x in R) overline(x) $

        Les éléments de $E$ sont inclus dans la réunion.
    
    - Soient $overline(u) in union.big_(x in R) overline(x)$,
        et $x in R$ tel que $u in overline(x)$.

        $overline(x) subset E$ donc $u in E$

        Les éléments de la réunion sont inclus dans $E$.

    $square$
]


#ydate(2023, 11, 23)
=== 3.2 Relations d'ordre


#ydef(name: [Relation d'ordre])[
    Soit $E$ un ensemble et $cal(R)$ une relation.
    On dit que $cal(R)$ est une *relation d'ordre* ou que $(E, cal(R))$
    est un *ensemble ordonné* si
    + $cal(R)$ est *réflexive* : $forall x in E, x cal(R) x$
    + $cal(R)$ est *anti symétrique* :
        $ forall (x, y) in E^2,
            yt x cal(R) y and y cal(R) x ==> x = y $
    + $cal(R)$ est *transitive* :
        $ forall (x,y,z) in E^3,
            yt x cal(R) y and y cal(R) z ==> x cal(R) z $
]

#yexample[
    + $(RR, <=)$ est un ensemble ordonné

    + $(RR, <)$ n'est pas un ensemble ordonné

    + $(cal(P)(E), subset)$ est un ensemble ordonné

    + $(RR^RR, o_(x->0))$ n'est pas une relation d'ordre

    + $(RR^RR, O_(x->0))$ n'est pas une relation d'ordre

        $f(x) =_(x->a) O(g(x))$ si et seulement si

        $exists M in RR, forall x in I, |f(x)| <= M|g(x)|$
        où $I$ est un voisinage de $a$.
    
    + Ordre lexicographique
]

#ydef(name: [Ordre lexicographique])[
    Soient $(E, prec)$ et $(F, subset.sq)$ deux ensembles ordonnés.

    L'*ordre lexicographique* sur $E times F$ est donné par la formule.

    $ (x,y) <= (a,b)
        yt <=> cases(x prec a, x != a) or cases(x = a, y subset.sq b) $
]

#yexample[
    Exercice :
    Montrer que $<=$ est une relation d'ordre.
]

#ydef(name: [Comparable])[
    Soient $(E, prec)$ un ensemble ordonné, et $(x, y) in E^2$.

    On dit que $x$ et $y$ sont *comparable* si $x prec y$ ou $y prec x$.
]

#yexample[
    + 2 et 5 sont comparables dans $(RR, <=)$

    + ${0}$ et ${1}$ ne sont pas comparables dans $(cal(P)({0,1}), subset)$

    + $(NN, |)$ est un enemble ordonné 2 et 5
        ne sont pas comparables dans $(NN, |)$
]

#ydef(name: [Ordre total / partiel])[
    Soit $(E, prec)$ un ensemble ordonné.

    On dit que $prec$ est un *ordre total*
    (ou que $(E, prec)$ est *totalement ordonné*)
    si, pour tout $(x,y) in E^2$, $x$ et $y$ sont comparables.

    Sinon, on dit que l'ordre est *partiel*.
]

#ydef(name: [Majoration et majorant])[
    Soit $(E, prec)$ un ensemble ordonné et $A in cal(P)(E)$.

    On dit que $A$ est *majorée* si
    $ exists M in E, forall a in A, a prec M $
    un tel $M$ est appelé *majorant* de $A$.
]

#ydef(name: [Minoration et minorant])[
    Soit $(E, prec)$ un ensemble ordonné et $A in cal(P)(E)$.

    On dit que $A$ est *minorée* si
    $ exists m in E, forall a in A, m prec a $
    un tel $m$ est appelé *minorant* de $A$.
]

#yexample[
    + Pour l'ensemble ordonnée $(RR, <=)$.

        On pose $A = {1/n | n in NN^*}$

        Pour tout $n in NN^*$, $1/n <= 1$ donc 1 est un majorant de $A$,
        42 aussi est un majorant de $A$.

        $ forall x in NN^*, 1/n >= -1/12 $
        donc $-1/12$ est une minorant de $A$.
    
    + Soit l'ensemble $(NN, |)$.

        On pose $A = {3,5,6}$.

        - 30 majore $A$
        - 0 majore $A$
        - 1 minore $A$
        - 2 ne minore pas $A$
    
    + Soit l'ensemble $E = cal(P)(NN)$ muni de $subset$.

        On pose $A = {{n} | n in NN}$.

        $emptyset$ minore $A$ car
        $ forall n in NN, emptyset subset {n} $
        $NN$ minore $A$ car
        $ forall n in NN, {n} subset NN $
]

#ydef(name: [Maximum/minimum d'un ensemble])[
    Soit $(E, prec)$ un ensemble ordonné, $A in cal(P)(E)$.

    - *Le plus grand élément de $A$*, appelé aussi *maximum* de $A$,
        s'il existe, est un élément $M$ de $A$ tel que
        $ forall a in A, a prec M $
        c'est donc une majorant de $A$ appartenant lui-même à $A$.

        On le note $max(A)$.

    - *Le plus petit élément de $A$*, appelé aussi *minimum* de $A$,
        s'il existe, est un élément $m$ de $A$ tel que
        $ forall a in A, m prec a $
        c'est donc une minorant de $A$ appartenant lui-même à $A$.

        On le note $min(A)$.
]

#yexample[
    + $max(A) = 1$, $min(A)$ n'existe pas.
    + $max(A)$ et $min(A)$ n'existent pas.
    + idem
]

#ydef(name: [Élément maximal/minimal d'un ensemble])[
    Soient $(E, prec)$ un ensemble ordonné, $A in cal(P)(E)$.

    - On dit que $M in A$ est un *élément maximal de $A$* si
        $ exists.not a in A, cases(M prec a, M != a) $

    - On dit que $m in A$ est un *élément minimal de $A$* si
        $ exists.not a in A, cases(a prec m, m != a) $

    // illustration : Arbre avec deux racines non comparables
]

#yexample[
    On reprend les exemples précédents.

    + 1 est maximal. Il n'y a pas d'éléments minimal.

    + 5 et 6 sont des éléments maximaux de $A$.

        5 et 5 sont des éléments minimaux de $A$.
    
    + Pour tout $n in NN$, ${n}$ est à la fois un élément maximal et un élément
        minimal.
    
    + $ZZ union { hexa } = E$
        où
        $ x <= y
            yt <=> cases((x\,y) in ZZ^2 or x=y=hexa, x <=y) $
        
        $hexa$ est le seul élément maximal mais n'est pas maximum.
]

#ydef(name: [Borne supérieure / inférieure])[
    Soit $(E, prec)$ un *ensemble ordonné* et $A in cal(P)(E)$.

    - La *borne supérieure de $A$*, si elle existe,
        est le *plus petit majorant* de $A$,
        elle est notée $sup(A)$ :
        $ cases(forall a in A\, a prec sup(A),
            forall M in E\, ( (forall a in A\, a prec M)
                yt ==> sup(A) prec M) ) $

    - La *borne inférieure de $A$*, si elle existe,
        est le *plus grand minorant* de $A$,
        elle est notée $inf(A)$ :
        $ cases(forall a in A\, inf(A) prec a,
            forall m in E\, ( (forall a in A\, m prec a)
                yt ==> m prec inf(A)) ) $
]

#yexample[
    + $(RR, <=)$ et $A = ]-1, 2]$
        - $sup(A) = 2 = max(A)$
        - $min(A)$ n'existe pas
        - Les minorants de $A$ sont les réels $<= -1$ donc $inf(A) = -1$.
    
    + $(RR, <=)$ et $A = {1/n | n in NN^*}$
        - $1 = sup(A) = max(A)$
        - $0 = inf(A) in.not A$
    
    + $(NN, |)$ et $A = {a,b}$
        - $sup(A) = "PPCM"(a, b) = a union b$
        - $inf(A) = "PGCD"(a, b) = a sect b$
    
    + $(cal(P)(E))$ et $A = {X, Y}$
        - $sup(A) = X union Y$
        - $inf(A) = X sect Y$
]

#ydate(2023, 11, 24)

#ydef(name: [Majoration et majorant d'une fonction])[
    Soit $f in F^E$ où $(F, prec)$ est un ensemble ordonné.

    On dit que $f$ est *majorée* si
    $ exists M in F, forall x in E, f(x) prec M $
    un tel $M$ est appelé majorant de $f$.
]

#ydef(name: [Minoration et minorant d'une fonction])[
    Soit $f in F^E$ où $(F, prec)$ est un ensemble ordonné.

    On dit que $f$ est *minorée* si
    $ exists m in F, forall x in E, m prec f(x) $
    un tel $m$ est appelé minorant de $f$.
]

#ydef(name: [Maximum/minimum d'une fonction])[
    Le *maximum* de $f$, s'il existe, est le maximum de $f(E)$
    et il est noté $max(f)$ ou $max_(x in E) (f(x))$.

    Le *minimum* de $f$, s'il existe, est le minimum de $f(E)$
    et il est noté $min(f)$ ou $min_(x in E) (f(x))$.
]

#ydef(name: [Borne supérieure/inférieure d'une fonction])[
    Soit $f in F^E$ où $(F, prec)$ est un ensemble ordonné.

    - La *borne supérieure* de $f$ (resp. borne inférieure)
        si elle existe, est la borne supérieur de $f(E)$
        et elle est notée $sup(f)$ ou $sup_(x in E) (f(x))$

    - La *borne inférieure* de $f$
        si elle existe, est la borne inférieure de $f(E)$
        et elle est notée $inf(f)$ ou $inf_(x in E) (f(x))$
]

#yexample[
    On pose
    $ f: &[0, 1[ --> RR \
        &x |-> x^2 $
    $f$ est majorée ($forall x in [0, 1[, x^2 <= 3$)

    $sup_(0<=x<1) (f(x)) = 1$

    $max(f(x))$ n'existe pas.

    $min_(0<=x<1) (f(x)) = inf_(0<=x<1) (f(x)) = 0$
]

#yexample[
    - $sup_(x in NN^*) (sqrt(n))$ n'existe pas.
    - $inf_(n in NN^*) (sqrt(n)) = 1 = min_(n in NN^*) (sqrt(n))$
    - $ sup_(n in NN^*) ((-1)^n) = 1
        yt = max_(n in NN^*) ((-1)^n) $
    - $sup_(n in NN^*) (1/n^2) = 1 = max_(n in NN^*) (1/n^2)$
    - $inf_(n in NN^*) (1/n^2) = 0$
]

#yexample[
    $ f: &E --> cal(P)(E) \
        &x |-> {x} $
    avec $subset$ comme ordre sur $cal(P)(E)$.

    $ sup_(x in E) (f(x)) = E $
    ($= max_(x in E) f(x)$ si $E$ est un singleton)

    Si $E$ a au moins 2 éléments,
    $ inf_(x in E) (f(x)) = emptyset $
    Si $E$ a un unique élément,
    $ inf_(x in E) (f(x)) = E $
]

#ydef(name: [Fonction croissante, décroissante, monotone])[
    Soit $f in F^E$, avec $(E, prec)$ et $(F, prec.double)$ sont ordonnés.

    On dit que $f$ est *croissante* si, pour tout $(x,y) in E^2$,
    $ x prec y ==> f(x) prec.double f(y) $

    On dit que $f$ est *décroissante* si, pour tout $(x,y) in E^2$,
    $ x prec y ==> f(y) prec.double f(x) $

    On dit que $f$ est *monotone* si $f$ est croissante ou décroissante.
]

#yexample[
    + Soit
        $ f: &(NN^*, |) --> (NN^*, <=) \
            &n |-> n $
        $f$ est croissante.
    
    + Soit
        $ g: &(NN^*, <=) --> (NN^*, |) \
            &n |-> n $
        $g$ n'est ni croissante, ni décroissante.
    
    + On pose $E = ZZ union { triangle.t }$.
        Soit
        $ f: &E --> {0, 1} \
            &x |-> cases(0 "si" x in ZZ,
                    1 "si" x = triangle.t) $
        $f$ est croissante et décroissante, mais non constante.
]
