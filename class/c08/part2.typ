#import "../../template/ytemplate.typ": *


#ypart[ Applications ]


#ydef(name: [Application (version nulle)])[
    Soient $E$ et $F$ deux ensembles.

    Une *application* de $E$ dans $F$ associe à tout élément $x$ de $E$
    un unique élément de $F$ noté $f(x)$.

    Cet élément $f(x)$ est appelé *image* de $x$ par $f$.
]

#ydef(name: [Application (version H.-P.)])[
    Une *application* $f$ de $E$ dans $F$
    est un ensemble $G subset E times F$ tel que
    $ forall x in E, exists! y in F, (x,y) in G $

    Pour $x in E$, on note $f(x)$ le seul élément de $F$
    tel que $(x, f(x)) in G$.

    (N.B. L'application est définie sur $E$ tout entier,
    et chaque élément de $E$ à une unique image. Une application est une focn)
]

#yexample[
    $id_E : &E --> E \
        &x |-> x $
    
    (_graphe de id_E_)
    $ G = { (x, x) | x in E } $
    est la diagonale de $E times E$.
]

#ydef(name: [Fonction (version nulle)])[
    Soient $E$ et $F$ deux ensembles.

    Une *fonction* de $E$ dans $F$ est une application de $D$
    dans $F$ où $D subset E$.
]

#ydef(name: [Fonction (version H.-P.)])[
    Une *fonction* de $E$ dans $F$ est une partie $G$ de $E times F$ telle que
    $ forall x in E, forall (y, y') in F^2,
        yt #math.cases(reverse: true, $(x, y) in G$, $(x, y') in G$) => y = y' $
    (N.B. Si la fonction est définie pour un élément de $E$,
    elle a une unique image associée à cet élément.)
]

#yexample[
    + On pose $E = cal(F)(RR, RR)$, l'ensemble des fonctions de $RR$ dans $RR$.

        La dérivation est un fonction de $E$ dans $E$, mais ce n'est pas une application
    
    + Soit $I$ un intervalle de $RR$.

        On pose $E = cal(C)^1(I, CC)$, l'ensemble des fonctions
        de classe $cal(C)^1$ sur $I$, à valeurs dans $CC$.
    
        On pose $F = cal(C)^0(I, CC)$ l'ensemble des fonctions continues sur $I$
        à valeurs complexes

        On pose
        $ d: E &--> F \
            f &|-> f' $
        $d$ est une application de $E$ dans $F$.
    
    + On pose
        $ i: &cal(C)^0([0, 1], CC) --> cal(C)^1 ([0, 1], CC) \
            &f |-> (x |-> integral_0^x f(t) dif t) $
        $i$ est une application.

        $ forall f in cal(C)^0 ([0, 1], CC), i(f) (0) = 0 $
    
    + On pose I = [0, 1].
        $ d compose i = id_(cal(C)^0 (I, CC)) $
        $ i compose d = f |-> (x |-> f(x) - f(0)) $
    
    + // exemple 5

    + Soit $E$ un ensemble, $(A, B) in cal(P)(E)^2$.
        $ f: &cal(P)(E) --> cal(P)(A) times cal(P)(B) \
            &X |-> (X sect A, X sect B) $
        $f$ est une application
]

#ydef(name: [Injectivité])[
    Soit $f$ une application de $E$ dans $F$.

    On dit que $f$ est *injective* ou que $f$ est une *injection* si,
    pour tout $y in F$, l'équation
    $ cases(f(x) = y, x in E) $
    a au plus une solution.
]

#ydef(name: [Surjectivité])[
    Soit $f$ une application de $E$ dans $F$.

    On dit que $f$ est *surjective* ou que $f$ est une *surjection* si,
    pour tout $y in F$, l'équation
    $ cases(f(x) = y, x in E) $
    a au moins une solution.
]

#ydef(name: [Bijectivité])[
    Soit $f$ une application de $E$ dans $F$.

    On dit que $f$ est *bijective* ou que $f$ est une *bijection* si,
    pour tout $y in F$, l'équation
    $ cases(f(x) = y, x in E) $
    a exactement une solution.
]

#ynot(name: [Ensemble des applications d'un ensemble dans un autre])[
    On note $F^E$, l'ensemble des applications de $E$ dans $F$.
]

#ydef(name: [Composition])[
    Soient $f in F^E$ et $g in G^F$.

    On définit la *composée*
    $ g compose f: &E --> G \
        &x |-> g(f(x)) $
    
    Ou autrement dit
    $ compose: G^F times F^E --> G^E $
    i.e.
    $ compose in (G^E)^(G^F times F^E) $
]

#ydef(name: [Réciproque])[
    Soit $f in F^E$ *bijective*.

    La *réciproque* de $f$ est $f^(-1) in E^F$ telle que, pour tout $y in F$,
    $f^(-1) (y)$ est l'unique solution de
    $ cases(f(x) = y, x in E) $
]

#yprop(name: [Composée d'une fonction et de sa réciproque])[
    Soit $f in F^E$ une *bijection*.
    $ f compose f^(-1) = id_F and f^(-1) compose f = id_E $
]

#yproof[
    On sait que $f compose f^(-1) in F^E$ et que $f^(-1) compose f in E^F$.

    Soit $y in F$.
    $f^(-1) (y)$ est l'unique solution de 
    $ cases(f(x) = y, x in E) $

    donc $f compose f^(-1) (y) = y = id_F (y)$

    Soit $x in E$.
    $f^(-1) (x)$ est l'unique solution de l'équation
    $ cases(f(a) = x, a in E) $

    Or $x$ est une solution de cette équation,
    c'est donc la seule.
    Donc $f^(-1) compose f (x) = x = id_E (x)$
    $square$
]

#yprop(name: [
    Conséquence de deux fonctions qui composées l'une avec l'autre
    donnent des identités
])[
    Soit $f in F^E$ et $g in E^F$.

    telles que 
    $ cases(f compose g = id_F, g compose f = id_E) $

    Alors $f$ est *bijective* et $g = f^(-1)$.
]

#yproof[
    Soit $y in F$, $x in E$.
    $ f(x) = y &=> g compose f (x) = g(y) \
        &=> x = g(y) $
    
    L'équation $f(x) = y$ à donc au plus une solution $x = g(y)$.

    Or $f compose g (y) = id_F (y) = y$

    donc $g(y)$ est bien une solution.

    Donc $f$ est bijective, et $f^(-1) (y) = g(y)$ pour $y in F$.
    $square$
]

#yprop(name: [Conséquence d'une composée injective / surjective])[
    Soient $f in F^E$ et $g in G^F$.

    + Si $g compose f$ est injective, alors $f$ est injective.
    + Si $g compose f$ est sujective, alors $g$ est surjective.
]

#yproof[
    + On suppose $g compose f$ injective.

        Soit $y in F$. Soit $(x, x') in E^2$ tels que
        $ cases(f(x) = y, f(x') = y)
            yt <=> f(x) = f(x') $
        donc $g compose f (x) = g compose f (x')$
        et $ cases(g compose f (x) = g(y), g compose f (x') = g(y)) $

        Comme $g compose f$ est injective, $x = x'$.
    
    + On suppose $g compose f$ surjective.

        Soit $y in G$, Soit $x in E$ tel que
        $ g compose f (x) = y $

        (C'est possible car $g compose f$ est surjective)

        $f(x) in F$ et $g compose f(x) = y$
        donc $g$ est surjective.
        $square$
]

// Schemas

#ydef(name: [Image directe / réciproque d'une fonction par un ensemble])[
    Soit $f in F^E$ une *application*, $A in cal(P)(E)$, $B in cal(P)(F)$.

    + L'*image (directe)* de $A$ par $f$ est
        $ f(A) 
            yt = { f(x) | x in A }
            yt = { y in F | exists x in A, y = f(x) } $
        
        // schema 5
    
    + L'*image réciproque* de B est
        $ f^(-1) (B) = { x in E | f(x) in B } $
]

#yexample[
    Soit
    $ f: &RR --> RR \
        &x |-> x^2 $
    
    - $f([1, +oo[) = [1, +oo[$
    - $f^(-1) ([1, +oo[) = ]-oo, -1] union [1, +oo[$
    - $f^(-1) ({-1}) = emptyset$
    - $f^(-1) ({2}) = {-sqrt(2), sqrt(2)}$
]

#ybtw[
    *Attention !*

    Avec les notation précédentes,
    - $f^(-1) compose f (A) != A$ en général
    - $f compose f^(-1) (B) != B$ en général
]

#yprop(name: [Image directe d'une image réciproque, et inversement])[
    $A subset f^(-1) compose f (A)$ et $f compose f^(-1) (B) subset B$
]

#yproof[
    Soit $x in A$.

    $ x in f^(-1) compose f (A)
        yt <=> f(x) in f(A)
        yt <=> exists a in A, f(x) = f(a) $
    
    Et comme $x in A$ et $f(x) = f(x)$,
    on a $x in f^(-1) compose f (A)$

    Soit $y in f compose f^(-1) (B)$.
    Soit $x in f^(-1) (B)$ tel que $y = f(x)$.

    Comme $x in f^(-1) (B)$, $f(x) in B$.
    Donc $y in B$.
    $square$
]

#ybtw[
    Il faut être convaincu que
    - $x in f^(-1) (B) <=> f(x) in B$
    - $y in f(A) <=> exists a in A, y = f(a)$
]

#yprop(name: [Méthode pour montrer qu'une fonction est, ou n'est pas, injective])[
    Soit $f in F^E$.

    - Pour montrer que $f$ n'est pas injective,
        on peut exhiber 2 éléments distrincts de $E$
        qui ont la même image par $f$.
    
    - Pour montrer que $f$ est injective,
        on considère 2 éléments de $E$, on suppose qu'ils ont la mêm image
        et on montre que ces 2 éléments sont égaux.
]

#yprop(name: [Méthode pour montrer qu'une fonction est, ou n'est pas, surjective])[
    Soit $f in F^E$.

    - Pour montrer que $f$ n'est pas surjective,
        on exhive un élément de $F$ qui n'a pas d'antécédent.
    
    - Pour montrer que $f$  est surjective,
        on considère un élément quelconque de F
        et on doit lui trouver au moins un antécédent.
]

#yexample[
    + $NN$ et $2NN$ sont en bijection :
        $ f: &NN --> 2NN \
            &n |-> 2n $
    
    + $NN$ et $NN^*$ sont en bijection :
        $ f: &NN --> NN^* \
            &n |-> n+1 $
        
    + $ZZ$ et $NN$ sont en bijection :
        $ f: &ZZ --> NN \
            &n |->
                cases(2n "si" n >= 0,
                    -2n + 1 "si" n < 0) $
    
    + $[0, 1]$ et $NN$ ne sont pas en bijection :

        Argument diagonal de Cantor.

        Soit $f: NN --> [0, 1]$ une bijection.

        Pour toutes les valeurs de $f$ on associe un nombre de $[0, 1]$
        avec un nombre à virgules infini.
        En créant un nombre qui a comme $n$-ième décimale,
        la $n$-ième décimale de $f(n) + 1$ modulo $10$.

        Ce nombre n'est pas dans $f(NN)$, contradiction.
    
    + $[0, 1[$ sont en bijection :

        On pose $D = { 1/2^k | k in NN^* }$.
        $ f: &[0, 1[ --> [0,1] \
            &x |-> cases(
                x "si" x in.not D,
                2x "si" x in D
            ) $
        
        Preuve :

        Soit $(x,x') in [0,1\[^2$

        On suppose que $f(x) = f(x')$.
        - Si $(x, x') in D^2$, alors
            $ 2x = f(x) = f(x') = 2x' $
            et alors $x = x'$.
        - Si $x in.not D$ et $x' in.not D$, alors
            $ x = f(x) = f(x') = x' $
        - Si $x in D$ et $x' in.not D$,
            $ 2x = f(x) = f(x') = x' $

            On pose $x = 1/2^k$ avec $k in NN^*$.
            
            Si $k >= 2$, alors $x' = 1/2^(k-1) in D$, une contradiction.
            Si $k = 1$ alors $x' = 2 1/2 = 1$ une contradiction.
        - De même avec $x in.not D$ et  $x' in D$.
    
    + $]0,1[$ et $[0, 1[$ sont en bijection.

    + Pour tout $(a, b) in RR^2$, avec $a < b$, $]a,b[$ et $]0,1[$
        sont en bijection
        $ f: &]a, b[ --> ]0,1[ \
            &x |-> (x-a)/(b-a) $
    
    + $RR$ est en bijection avec $]-1,1[$
        $ f: &RR --> ]-1,1[ \
            x |-> "th"(x) $
    
    + $RR$ est $RR^2$ sont en bijection.

    + $cal(P)(RR)$ et $RR$ ne sont pas en bijection.

        (C.F. exercice 1 du TD)

        On peut faire une injection entre les deux :
        $ f: &RR --> cal(P)(RR) \
            & x |-> {x} $
    
    + $cal(P)(NN)$ et $RR$ sont en bijection.

    + $cal(P)(NN)$ et ${0,1}^NN$ sont en bijection :

        $ f: &cal(P)(NN) --> {0,1}^NN \
            &A |-> 1_A $
        où
        $ 1_A: &NN --> {0,1} \
            &x |-> cases(1 "si" n in A, 0 "si" n in.not A) $
        
        Sa réciproque est
        $ g: &{0,1}^NN --> cal(P)(NN) \
            &h |-> h^(-1)({1}) $
]

#ydef(name: [Image d'une fonction])[
    Soit $f in F^E$.

    L'*image de $f$* est
    $ f(E) &= { f(x) | x in E } \
        &= { y in F | exists x in E, f(x) = y } $
]

#yprop(name: [Conséquence que l'image d'une fonction de $E$ dans $F$ soit $F$])[
    Soit $f in F^E$ une *application*.
    $ f "surjective" <=> F = f(E) $
    $square$
]