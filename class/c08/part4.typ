#import "../../template/ytemplate.typ": *


#ypart[ Lois de composition interne ]


#ydef(name: [Loi de composition interne])[
    Une *loi de composition interne* sur un ensemble $E$
    est une application de $E times E$ dans $E$.

    Si la loi est notée $hexa$, on écrira $x hexa y$
    à la place de $hexa(x, y)$.
]

#ydef(name: [Associativité et commutativité])[
    Soit $hexa$ une loi interne sur $E$.

    + On dit que $hexa$ est *associative* si, 
        pour tout $(x,y,z) in E^3$,
        $ x hexa (y hexa z) = (x hexa y) hexa z $

    + On dit que $hexa$ est *commutative* si, 
        pour tout $(x,y) in E^2$,
        $ x hexa y = y hexa x $
]

#ydef(name: [Élément neutre/absorbant])[
    Soit $hexa$ une loi interne sur $E$.

    + On dit que $e in E$ est un *élément neutre* de $hexa$ si,
        pour tout $x in E$,
        $ x hexa e = e hexa x = x $

    + On dit que $a in E$ est un *élément absorbant* de $hexa$ si,
        pour tout $x in E$,
        $ x hexa a = a hexa x = a $
]

#yexample[
    + On pose $E = NN$ et $hexa = +$.

        $hexa$ est associative, commutative, d'élément neutre $0$.
    
    + On pose $E = NN$ et $hexa = -$.

        $hexa$ n'est pas associative, pas commutative, pas d'élément neutre.
    
    + On pose $E = RR$ et $hexa = max$.

        $ 5 hexa 7 = 7$

        $hexa$ est associative, commutative, mais n'a pas de neutre.
    
    + On pose $E = cal(P)(E)$ et $hexa = union$.

        $hexa$ est associative, commutative, d'élément neutre $emptyset$,
        $E$ est absorbant.
]

#yexample[
    Exercice : essayer de déterminer les propriétés de la différence symétrique
    $triangle.t$
]

#yprop(name: [Nombre d'éléments neutres d'une loi de composition])[
    Toute loi de composition interne a au plus un élément neutre.
]

#yproof[
    Soient $e$ et $e'$ deux éléments neutres.
    - $e hexa e' = e'$ car $e$ est neutre
    - $e hexa e' = e$ car $e'$ est neutre
    Donc $e = e'$.
    $square$
]

#ydef(name: [Symétrique dans une loi interne])[
    Soient $hexa$ une loi interne d'élément neutre $e$, et $(x,y) in E^2$.

    On dit que $y$ est un *symétrique* de $x$ si, $x hexa y = y hexa x = e$.

    Si un tel $y$ existe, on dit que $x$ est *symétrisable*.
]

#yprop(name: [Nombre d'éléments symétriques d'une loi associative])[
    Si $hexa$ est associative, alors tout élément a au plus un symétrique.
]

#ydate(2023, 11, 25)

#yproof[
    On suppose $hexa$ associative. On note $e$ son neutre.

    Soit $x in E$. Supposons que $x$ ait 2 symétriques, disons $y$ et $y'$.

    $ y &= y hexa e \
        &= y hexa (x hexa y') \
        &= (y hexa x) hexa y' \
        &= e hexa y' \
        &= y' $
    $square$
]

#ynot(name: [Multiplicative pour les lois de composition])[
    Souvent, on note une loi interne par $times$ ou $dot$.
    Dans ce cas, le neutre est souvent noté $1$, ou $1_E$.
    (Donc notation pour des loi associatives).
    Au lieu de dire symétrisable on dit *inversible*,
    le symétrique de $x$ devient l'*inverse* de $x$ et il est noté $x^(-1)$.

    Pour $n in NN^*$,
    $ cases(x^n = underbrace(x x x ... x, n "fois"),
        x^0 = 1_E) $
    
    Si $(x_i) in E^I$, $I$ finie, $product_(i in I) x_i$
    est le produit des $x_i$ où $i in I$.
]

#ynot(name: [Additive pour les lois de composition])[
    Si la loi est commutative, on la note alors souvent par $+$.
    Le neutre est noté $0$ ou $0_E$;
    on parle d'élément *opposable*, d'*opposé* au lieu de symétrique.
    L'opposé de $x$ est noté $-x$.

    Pour $n in NN^*$,
    $ cases(sum_(i=1)^n x = n x = underbrace(x + x + ... + x, n "fois"),
        0 x = 0_E) $
]