// Théorie des ensembles naïve
#import "../../template/ytemplate.typ": *

#ychap[Théorie naïve des ensembles ]

#include "part1.typ"
#include "part2.typ"
#include "part3.typ"
#include "part4.typ"