#import "../../template/ytemplate.typ": *


#ypart[ Ensemble et appartenance ]


#ydef(name: [Ensemble])[
    Un *ensemble* est un collection finie ou infinie d'objets.
]


#ydef(name: [Appartenance et non-appartenance])[
    Si un objet $x$ est dans un ensemble $E$,
    on dit que $x$ *appartient* à $E$,
    sinon, on dit que $x$ n'*appartient pas* à $E$.
]

#ynot(name: [Appartenance et non-appartenance])[
    Soient un objet $x$ et un ensemble $E$.

    Si $x$ *appartient* à $E$, on écrit :
    $ x in E "ou bien" E in.rev x $
    
    Si $x$ n'*appartient pas* à $E$, on écrit :
    $ x in.not E "ou bien" E in.rev.not x $
]

#yexample[
    - $NN$ est l'ensemble des entiers naturels
        $ 0 in NN, pi in.not NN, NN in.not NN $

    - On pose ${NN, RR} = E$
        $ NN in E, RR in E, CC in.not E, ZZ in.not E, \
            0 in.not E $

    - On pose
        $ E = { t in RR |-> A cos(t) + B sin(t)
            yt | (A,B) in RR^2} $
        $ cos in E $
        $ cos - sin in E $
        $ exp in.not E $
]

#ybtw[
    Cette définition introduit un *paradoxe* :

    On appelle *ensemble ordinaire* tout ensemble $E$ tel que $E in.not E$
    et on appelle *ensemble extraordinaire* tout ensemble $E$ tel que $E in E$.

    (N.B. termes non conventionnels)

    Soit $O$, l'ensemble de tous les ensembles ordinaires :
    $ forall E, cases(E in O <=> E in.not E, E in.not O <=> E in E) $

    - Si $O$ est ordinaire, alors $O in O$ et donc il n'est pas ordinaire.
    - Si $O$ n'est pas ordinaire, alors $O in.not O$ et donc il est ordinaire.
]

#ydef(name: [Inclusion et non-inclusion])[
    Soit $E$ et $F$ deux ensembles.

    On dit que
    - $E$ est *inclus* dans $F$;
    - ou $E$ est un *sous-ensemble* de $F$;
    - ou $E$ est une *partie* de $F$
    si $ forall x in E, x in F $

    Sinon, on dit que $E$ n'est *pas inclus* dans $F$.
]

#ynot(name: [Inclusion et non-inclusion])[
    Soit $E$ et $F$ deux ensembles.
    
    Si $E$ est *inclus* dans $F$, on écrit :
    $ E subset F "ou bien" F supset E $

    Si $E$ n'est *pas inclus* dans $F$, on écrit :
    $ E subset.not F "ou bien" F supset.not E $
]

#ynot(name: [Ensemble vide])[
    On note $emptyset$ l'*ensemble vide*, i.e. celui qui n'a aucun élément.
]

#ybtw[
    + $F = NN$
        $ 2NN subset F $
        $ emptyset in NN $
        "$3 subset NN$" n'a pas de sens (pour l'instant)

    + $E = { emptyset, {emptyset} }$
        $ emptyset subset E $
        $ emptyset in E <=> {emptyset} subset E $
        $ {emptyset} in E <=> {{emptyset}} subset E $
        $ {{emptyset}} in.not E <=> {{{emptyset}}} subset.not E $
        $ {{{emptyset}}} in.not E $
]

#ynot(name: [Ensemble des parties d'un ensemble])[
    On note $cal(P)(E)$ l'*ensemble des parties* d'un ensemble $E$.
]

#yexample[
    $E = {0,1}$
    $ cal(P)(E) = {emptyset, {0}, {1}, E} $
]

#yexample[
    $ cal(P)(cal(P)(E)) = {
        yt emptyset,
        yt {emptyset}, {{0}}, {{1}}, {E},
        yt {emptyset, {0}}, {emptyset, {1}}, {emptyset, E},
            ytt {{0}, {1}}, {{0}, E},
            ytt {{1}, E},
        yt {emptyset, {0}, {1}}, {emptyset, {0}, E},
            ytt {emptyset, {1}, E}, {{0}, {1}, E}
        yt {emptyset, {0}, {1}, E}
        \ } $
]

#ydef(name: [Égalité d'ensembles])[
    On dit que deux ensembles sont *égaux*
    s'ils ont exactement les mêmes éléments.
]

#yprop(name: [Double inclusion])[
    Soient $E$ et $F$ deux ensembles.
    $ E = F <=> (E subset F and F subset E) $
    $square$
]

#ydef(name: [Intersection et union])[
    Soit $E$ un ensemble, $A$ et $B$ deux parties de E.
    + L'*intersection* de $A$ et $B$ est
        $ A sect B = { x in E | x in A and x in B } $
    + La *réunion* de $A$ et $B$ est
        $ A union B = { x in E | x in A or x in B } $
]

#ydef(name: [Différence d'ensembles])[
    Soit $E$ un ensemble, $A$ et $B$ deux parties de E.

    La *différence* de $A$ et $B$ est
    $ A without B = { x in E | x in A and x in.not B } $
]

#ydef(name: [Le complémentaire])[
    Soit $E$ un ensemble, $A$ une partie de $E$.

    Le *complémentaire* de $A$ dans $E$.
    $ E without A = overline(A) = A^C = { x in E | x in.not A } $
]

#ydef(name: [La différence symétrique])[
    $ A triangle.t B =
        yt { x in E | (cases(x in A, x in.not B) or cases(x in B, x in.not A))}
        yt (A union B) without (A sect B) $
    (N.B. correspond au XOR $xor$ en informatique)
]

#yprop(name: [Manipulations d'intersections])[
    Soient $A, B, C$ trois parties de $E$.

    + $A sect A = A$ (idempotence)
    + $B sect A = A sect B$ (commutativité)
    + $A sect ( B sect C) = (A sect B) sect C$ (associativité)
    + $A sect emptyset = emptyset$ (élément absorbant)
    + $A sect E = A$ (élément neutre)
    $square$
]

#yprop(name: [Manipulations de réunions])[
    Soient $A, B, C$ trois parties de $E$.

    + $A union A = A$ (idempotence)
    + $B union A = A union B$ (commutativité)
    + $A union (B union C) = (A union B) union C$ (associativité)
    + $A union emptyset = A$ (élément neutre)
    + $A union E = E$ (élément absorbant)
    $square$
]

#yprop(name: [Manipulations de différences d'ensembles])[
    Soit $A$ une partie de $E$.

    + $A without A = emptyset$
    + $A without emptyset = A$ (élément neutre)
    + $E without (E without A) = A$
    $square$
]

#yprop(name: [Relations de distributivité d'ensembles])[
    Soient $A, B, C$ trois parties de $E$.

    + $A sect (B union C) = (A sect B) union (A sect C)$
    + $A union (B sect C) = (A union B) sect (A union C)$
    + $E without (A sect B) = (E without A) union (E without B)$
    + $E without (A union B) = (E without A) sect (E without B)$
    $square$
]
    
#ydef(name: [Produit cartésien])[
    Soient $E$ et $F$ deux ensembles $E times F$,
    le *produit cartésient* de $E$ et $F$,
    est l'ensemble des couples $(x, y)$ avec $x in E$ et $y in F$.
]

#ydef(name: [Couple (par Kuratowsky)])[
    $ (x, y) = { {x}, {x, y} } $
]

#yprop(name: [Égalité de couples])[
    Soit $(x, y)$ et $(x', y')$.
    $ (x, y) = (x', y') <=> cases(x = x', y = y') $
    $square$
]

#ydef(name: [$n$-uplet])[
    Soit $(E_i)_(1<=i<=n)$ une famille d'ensembles où $n in NN$.

    $E_1 times ... times E_n = product_(i=1)^n E_i$
    est l'ensemble des $n$-uplets $(x_1, ..., x_n)$
    où pour tout $i in [|1, n|]$, $x_i in E_i$.
]

#ydef(name: [Ensembles des famille indexées sur un ensemble])[
    Soit $(E_i)_(i in I)$ une famille d'ensemble indexée par un ensemble $I$.

    $product_(i in I) E_i$
    est l'ensemble des familles $(x_i)_(i in I)$ indexées sur $I$
    où pour tout $i in I$, $x_i in E_i$
]

#yprop(name: [Passage au complémentaire d'une inclusion])[
    Soit $E$ un ensemble $(A, B) in cal(P)(E)^2$.
    $ A subset B <=> (E without A) supset (E without B) $
]