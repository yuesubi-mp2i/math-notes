#import "../../template/ytemplate.typ": *


#ypart[ Développement limité ]


#yprop(name: [Développement limité de $1/(1+x)$])[
    Pour tout $n in NN$,
    $ 1/(1 +x) = sum_(k=0)^n (-x)^k + o_(x -> 0) (x^n) $
    $ 1/(1 +x) =& 1 - x  + x^2 - x^3 + dots \
        &+ (-1)^n x^n + o_(x -> 0)(x^n) $
]

#yproof[
    Soit $x > -1$ réel.

    $ sum_(k=0)^n (-1)^k x^k &= sum_(k=0)^n (-x)^n \
        &= (1 - (-x)^(n+1))/(1 - (-x)) \
        &= 1/(1+x) - (-x)^(n+1)/(1 + x) $
    
    D'où,
    $ 1/(1+ x) = sum_(k=0)^n (-1)^k x^k + x^n ((-1)^(n+1) x)/(1 + x) $

    On pose $epsilon(x) = (-1)^(n+1) x/(1 + x)$ et on a pour $x > -1$,
    $ 1/(1 +x) = sum_(k=0)^n (-1)^k x^k + x^n epsilon(x) $
    et,
    $ abs(epsilon(x)) = abs(x)/(1+x) -->_(x -> 0) 0 $
    $square$
]

#yexample[
    Avec les notations de la preuve,

    $ 1/(1 + x^2) &= sum_(k=0)^n (-1)^k (x^2)^k + (x^2)^n epsilon(x^2) \
        &= sum_(k=0)^n (-1)^k x^(2k) + x^(2n) epsilon(x^2) \
        &= sum_(k=0)^n (-1)^k x^(2k) + x^(2n) epsilon_1(x) $
    
    où $epsilon_1(x) = epsilon(x^2)$
    
    et $lim_(x -> 0) epsilon_1(x) = lim_(x -> 0) epsilon(x^2) = 0$
]

#yexample[
    Développement limité à l'ordre 2 au voisinage de 2 de $1/x^2$.

    On pose $u = x - 2$.

    $ 1/x^2 &= 1/(u + 2)^2 \
        &= 1/4 1/(1+ u/2)^2 \
        &= 1/4 (1/(1+ u/2))^2 $
    
    $ 1/(1+ u/2) &= 1 - u/2 + underbrace(u/2 epsilon(u/2), = o(u)) $
    où $epsilon(y) -->_(y -> 0) = 0$.

    $ &(1/(1+ u/2))^2 \
        &= (1 - u/2 + u/2 epsilon(u/2))^2 \
        &= 1 + u^2/4 + u^2/4 epsilon(u/2)^2 - u \
            &space + u epsilon(u/2) - u^2/2 epsilon(u/2) \
        &= 1 - u + u^2/4 + (u^2/4 epsilon(u/2)^2 \
            &space + u epsilon(u/2) - u^2/2 epsilon(u/2)) $
    
    Si on factorise par $u^2$ le terme entre parenthèses, on fait apparaître
    une forme indéterminée quand $u$ tend vers $0$ ; ce n'est pas bon

    On reprends :
    $ 1/(1 + (u/2)) = 1 - u/2 + u^2/4 + u^2/4 epsilon_1(u/2) $
    $ &(1/(1 + (u/2)))^2 \
        &= 1 + u^2/4 + u^4/16 + u^4/16 epsilon_1(u/2)^2 \
            &space -u + u^2/2 + u^2/2 epsilon_1(u/2) - u^3/4 \
            &space -u^3/8 epsilon_1(u/2) + u^4/8 epsilon_1(u/2) \
        &= 1 - u + 3/4 u^2 + u^2 (dots) \
        &= 1 - u + 3/4 u^2 + u^2 epsilon_2(u) $
    où $epsilon_2(u) = ... -->_(u -> 0) 0$

    $ 1/x^2 =& 1/4 (1 - (x -2) + 3/4 (x - 2)^2) \
        &+ o_(x->2) ((x -2)^2) $
]

#yexample[
    On reprends les calculs mais avec moins de détails.
    $ &(1/(1 + u/2))^2 \
        &= (1 - u/2 + u^2/4 + o_(u -> 0) (u^2))^2 \
        &= 1 + u^2/4 - u + u^2/2 + o_0 (u^2) \
        &= 1 - u + 3/4 u^2 + o_0 (u^2) $
    
    (N.B. quand on développe on skip les termes de plus haut degré que $u^2$)
]

#ybtw[
    $ 1/x^2 &= underbrace(1/4 - 1/4(x - 2),
            "équation de la tangeante en" x = 2) \
        &space + underbrace(3/16 (x - 2)^2 + o_2 ((x -2)^2),
            "écart entre la fonction et la tangeante") $
]

#ydef(name: [Voisinage de $a$])[
    Un *voisinage* de $a$, est un *intervalle $I$
    ouvert* (d'une certaine magnière) qui *contiendrerait* $a$,
    auquel on enleve $a$ ou pas.
]

#ydef(name: [Développement limité])[
    Soit $f$ une fonction définie au *voisinage*, noté $I$, de $a in RR$.

    On dit que $f$ a un *développement limité d'ordre $n$ au voisinage de $a$*
    s'il existe $(alpha_k) in RR^[|0,n|]$
    tels que pour $x in I$,
    $ f(x) =_0& sum_(k=0)^n a_k (x-a)^k + o((x-a)^n) $
    $ f(x) =_0 & alpha_0 + alpha_1(x - a) + dots + \
            &+ alpha_n (x-a)^n + o((x-a)^n) $
]

#yprop(name: [Développement limité de $(1+x)^alpha$])[
    Pour $alpha in RR$,
    $ (1+x)^alpha =_0& sum_(k=0)^n (product_(p=0)^(k-1) (alpha - p))/k! x^k 
        &+ o(x^n) $
     $ (1+x)^alpha =_0& 1 + alpha x + alpha(alpha -1)/2 x^2 \
        &+ (alpha(alpha -1)(alpha-2))/6 x^3 \
        &+ (alpha(alpha -1)(alpha-2)(alpha-3))/24 x^4 \
        &dots.v \
        &+ (alpha dot ... dot (alpha - n + 1))/n! x^n \
        &+ o(x^n) $
]

#yprop(name: [Développement limité de $e^x$])[
    $ e^x =_0 sum_(k = 0)^n x^k/k! + o(x^n) $
    $ e^x =_0& 1 + x + x^2/2 + x^3/6 + x^4/24 + x^5/120 \
        &+ ... + x^n/n! + o(x^n) $
]

#yprop(name: [Développement limité de $ln(1+x)$])[
    $ ln(1 + x) =_0 sum_(k = 1)^n (-1)^(k+1) x^k/k + o(x^n) $
    $ ln(1 + x) =_0& x - x^2/2 + x^3/3 - x^4/4 \
        &+ ... + (-1)^(n+1) x^n/n \
        &+ o(x^n) $
]

#yprop(name: [Développement limité de $cos(x)$])[
    $ cos(x) =_0 sum_(k=0)^n (-1)^k x^(2k)/(2k)! + o(x^(2n + 1)) $
    $ cos(x) =_0& 1 - x^2/2 + x^4/24 - x^6/720 \
        &+ ... (-1)^n x^(2n)/(2n)! + o(x^(2n)) $

    (N.B. on peut aussi mettre une puissace $2n + 1$ pour le petit $o$)
]

#yprop(name: [Développement limité de $sin(x)$])[
    $ sin(x) =_0& sum_(k=0)^n (-1)^k x^(2k+1)/(2k+1)! \
        &+ o(x^(2n+2)) $
    $ sin(x) =_0& x - x^3/6 + x^5/120 + ... \
        &+ (-1)^n x^(2n + 1)/(2n+1)! 
        &+ o(x^(2n + 1)) $
]

#yprop(name: [Développement limité de $tan(x)$ à l'ordre 3])[
    $ tan(x) = x + x^3/3 + o_(x->0) (x^3) $
]

#yprop(name: [Développement limité de $"ch"(x)$])[
    $ "ch"(x) =_0 sum_(k=0)^n x^(2k)/(2k)! + o(x^(2n + 1)) $
    $ "ch"(x) =& 1 + x^2/2 + x^4/24 + dots \
        &+ x^(2n)/(2n)! + o(x^(2n)) $
]

#yprop(name: [Développement limité de $"sh"(x)$])[
    $ "sh"(x) =_0& sum_(k=0)^n x^(2k+1)/(2k+1)! + o(x^(2n+2)) $
    $ "sh"(x) =_0& x + x^3/6 + x^5/120 + dots \
        &+ x^(2n+1)/(2n+1)! + o(x^(2n+1)) $
]

#yprop(name: [Développement limité de $"th"(x)$ à l'ordre 1])[
    $ "th"(x) = x + o_(x->0)(x) $
]

#yprop(name: [Développement limité de $"Arctan"(x)$])[
    $ "Arctan"(x) =_0& sum_(k=0)^n (-1)^k x^(2k+1)/(2k+1) \
        &+ o(x^(2n+2)) $
    $ "Arctan"(x) =_0& x - x^3/3 + x^5/5 - x^7/7 \
        &+ dots + (-1)^n x^(2n+1)/(2n+1) \
        &+ o(x^(2n+1)) $
]