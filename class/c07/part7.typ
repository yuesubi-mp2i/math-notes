#import "../../template/ytemplate.typ": *


#ypart[ Application à la recherche d'équivalents ]


#yexample[
    Equivalent quand $x$ tend vers $0$ de $ x cos(x) - sin(x) $

    $ sin(x) =_a x - x^3/6 + o(x^3) $
    $ cos(x) =_a 1 - x^2/2 + o(x^2) $

    $ x cos(x) - sin(x)
        yt =_0 x -x^3/2 - x + x^3/6 + o(x^3)
        yt =_0 -1/3 x^3 + o(x^3)
        yt tilde_0 - x^3/3 $
]

#yexample[
    Equivalent de
    $ (1 + 1/x)^x - e $
    au voisinage de $+oo$.

    $ (1 + u)^(1/u) = e^(1/u ln(1 + u)) $

    $ ln(1 + u) =_0 u -u^2/2 + o(u^2) $
    $ 1/u ln(1 + u) =_0 1 -u/2 + o(u) $

    $ e^(1/u ln(1 + u))
        yt =_0 e^(1 -u/2 + o(u))
        yt =_0 e times e^(-u/2 + o(u))
        yt =_0 e (1 + (-u/2 + o(u)) + o(u)) $

    $ (1 + 1/x)^x - e
        yt =_(+oo) e (-1/2 x) + o(1/x)
        yt tilde_(+oo) -e / 2x $
]

#yprop(name: [ Équivalent en un point d'une fonction qui admet un DL en ce point ])[
    Soit $f: I --> RR$, $(alpha_k) in RR^[|p, n|]$ avec $alpha_p != 0$
    tels que, pour tout $x in I$,
    $ f(x) =_a sum_(k=p)^n alpha_k (x-a)^k + o((x-a)^n) $

    Alors
    $ f(x) tilde_a alpha_p (x-a)^p $
    $square$
]