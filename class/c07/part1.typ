#import "../../template/ytemplate.typ": *


#ypart[ Rappels ]


#ybtw[
    $f(x) = o_(x -> a) (g)$
    fignifie que, pout tout $x$ assez proche de $a$,
    $ f(x) = g(x) epsilon(x) $
    où $epsilon$ est une fonction telle que
    $epsilon(x) -->_a 0$

    Si $g$ ne s'annule pas au voisinage de $a$ sauf peut-être en $a$,
    alors
    $ f(x) = o_a (g(x)) <=> f/g -->_a 0$
]