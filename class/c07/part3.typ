#import "../../template/ytemplate.typ": *


#ypart[Opérations]


#let DL(order, point) = { $"DL"_order (point)$ }
#let DA(order, point) = { $"DA"_order (point)$ }


#yexample[
    $DL(5,0)$ de $cos(x) - sin(x)$

    $ cos(x) =& 1 - x^2/2 + x^4/24 - x^6/720 \
        &+ x^6 epsilon_1(x) $
    où $epsilon_1 -->_(x->0) 0$

    $ sin(x) = x - x^3/6 + x^5/120 + x^5 epsilon_2(x) $
    où $epsilon_2 -->_(x->0) 0$

    $ cos(x)& - sin(x) \
        =& 1 - x - x^2/2 + x^3/6 + x^4/24 \
            &- x^5/120 - x^6/720 \
            &+ x^6 epsilon_1(x) - x^5 epsilon_2(x) \
        =_0& 1 - x - x^2/2 + x^3/6 + x^4/24 \
            &- x^5/120 + o(x^5) $
]

#yexample[
    $DL(5,0)$ de $cos(x) sin(x)$

    $ cos(x)& sin(x) \
        =_0& (1 - x^2/2 + x^4/24 + o(x^4)) \
            &times (x - x^3/6 + x^5/120 + o(x^5)) \
        =_0& x - x^3/6  + x^5/120 + o(x^5) -x^3/2 \
            &+ x^5/12 + x^5/24 \
        =_0& x -2/3 x^3 + 2/15 x^5 + o(x^5) $
]

#yexample[
    $DL(5,0)$ de $tan$

    $ cos(x) =_(x->0) 1 - x^2/2 + x^4/24 + o(x^5) $

    $ 1/cos(x) &=_(x->0) 1 /(1 - x^2/2 + x^4/24 + o(x^5)) \
        &=_(x->0) 1/(1+u) $
    où $u =_(x->0) - x^2/2 + x^4/24 + o(x^5) -->_(x->0) 0$

    $ 1/(1 + u) =_0 & 1 - u + u^2 - u^3 + u^4 \
            &- u^5 o(u^5) \
        =_0 & 1 - (- x^2/2 + x^4/24 + o(x^5)) \
            &+ x^4/4 \
        =_0 & 1 + x^2/2 + 5x^4/24 + o(x^5) $
    
    $ tan(x) =& sin(x)/cos(x) \
        =_0 & (x - x^3/6 + x^5/120 + o(x^5)) \
            &times(1 + x^2/2 + 5x^4/24 + o(x^5)) \
        =_0 & x + x^3/2 + 5x^5/24 - x^3/6 \
            &- x^5/12 + x^5/120 + o(x^5) \
        =_0 & x + x^3/3 + 2/15 x^5 + o(x^5) $
]

#yexample[
    $DL(5,0)$ de $(e^x sin(x))/ln(1+ x)$

    Il n'y a pas de développement limité de $1/ln(1+x)$, on fait le
    développement asymptotique.
    $ &1/ln(1 +x) \
        =_0 & 1/(x - x^2/2 + x^3/3 - x^4/4 + x^5/5 - x^6/6 + o(x^5)) \
        =_0 & 1/x 1/(1 - x/2 + x^2/3 - x^3/4 + x^4/5 - x^5/6 + o(x^4)) $
    
    $ u &=_0 - x/2 + x^2/3 - x^3/4 + x^4/5 -x^5/6 + o(x^5) \
        &-->_0 0 $
    
    $ 1/(1+u)& \
        =_0 & 1 - u + u^2 - u^3 + u^4 - u^5 \
            &+ o(x^5) \
        =_0 & 1 \
            &- (- x/2 + x^2/3 - x^3/4 + x^4/5 - x^5/6) \
            &+ (x^2/4 + x^4/9 - x^3/3 + x^4/4 - x^5/5 \
                &- x^5/6) \
            &- (-x^3/8 + x^4/6 + x^4/12 -x^5/16 - x^5/9 \
                &-x^5/18 -x^5/8) \
            &+ (x^4/16 - x^5/12 - x^5/24 - x^5/24) \
            &- (-x^5/32) + o(x^5) \
        =_0 & 1 + x/2 - x^2/12 + x^3/24 - 19/720 x^4 \
            &+ 3/160 x^5 + o(x^5) $
    
    Non utilisé :
    $ 1/ln(1+x) =_0& 1/x + 1/2 - x/12 + x^2/24 \
        &- 19/720 x^3 + o(x^3) $
    
    $ e^x =_0& 1 + x + x^2/2 + x^3/6 + x^4/24 \
            &+ x^5/120 + o(x^6) $
    
    $ sin(x) =_0 x - x^3/6 + x^5/120 = o(x^5) $

    $ sin(x)/ln(1+x) =_0& cancel(x) (1 - x^2/6 + x^4/120 + o(x^5)) \
            & 1/cancel(x) (1 + x^2 -x x^2/12 + x^3/24 \
            &- 19/720 x^4 - 3/160 x^5 + o(x^5)) \
        =_0& ... \
        =_0& 1 + x/2 - x^2/4 -x^3/24 -x^4/240 \
            &+ 23/1440 x^5 + o(x^5) $
    
    $ (e^x sin(x))/ln(1+x) =_0& ... \
        =_0& 1 + 3/2 x + 3/4 x^2 + 1/8 x^3 \
            &-11/240 x^4 - 31/1440 x^5 + o(x^5) $
]

#yexample[
    $DA(3,0)$ de $ln compose sin (x)$

    $ sin(x) =_0 x -x^3/6 + o(x^4) $

    $ ln compose sin (x) \
        =_0& ln(x -x^3/6 + o(x^4)) \
        =_0& ln(x(1 - x^2/6 + o(x^3))) \
        =_0& ln(x) + ln(1 - x^2/6 + o(x^3)) $

    $ u =_0 -x^2/6 + o(x^3) -->_0 0 $

    $ ln(1 + x) =_0& u - u^2/2 + o(u^2) \
        =_0 & -x^2/6 + o(x^3) $
    
    $ ln compose sin (x) =_0 ln(x) - x^2/6 + o(x^3) $

    Donc
    $ ln compose sin(x) tilde.op_0 ln(x) $
    $ ln compose sin(x) - ln(x) tilde.op_0 -x^2/6 $
]

#yexample[
    $ 1/(1+x) =_0 1 - x + x^2 - x^3 + o(x^3) $
    par primitivation
    $ ln(1+x) =& ln(1-0) + x - x^2/2 \
        &+ x^3/3 - x^4/4 $
]

#yexample[
    $ 1/(1 + x^2) =_0 1 -x^2 + x^4 + o(x^4) $
    par primitivation
    $ "Arctan"(x) =_0 & "Arctan"(0) + x - x^3/3 \
        &+ x^5/5 + o(x^5) $
]

#yexample[
    $DL(7,0)$ de $tan$

    $ tan x =_0 x + o(x) $
    $ tan'x =_0& 1 + tan^2 x \
        =_0& 1 + (x + o(x))^2 \
        =_0& 1 + x^2 + o(x^2) $
    
    $ tan x =_0& tan(0) + x + x^3/3 + o(x^2) \
        =_0& x + x^3/3 + o(x^2) $
    $ tan'x =_0 1 + (x + x^3/3 + o(x^2))^2 $

    $ tan x =_0 x + x^3/3 + 2/3 x^5/5 + o(x^5) $
    ...
]

#yprop(name: [Troncature d'un développement limité])[
    Si $f$ admet un $DL(n,a)$,
    alors *pour tout $p <= n$*, $f$ admet aussi un $DL(p,a)$ obtenu en
    négligeant les termes de degré *supérieurs strictement* à $p$.
]

#yproof[
    Soit $(a_i)_(0<=i<=n) in RR^[|0,n|]$ et $epsilon : I --> RR$
    tels que pout tout $x in I$,
    $ f(x) = sum_(k=0)^n alpha_k (x-a)^k + (x-a) epsilon(x) $
    et $epsilon(x) -->_a 0$
    
    D'où, pour tout $x in I$,
    $ f(x) =& sum_(k=0)^p alpha_k (x-a)^k \
        &+ (x-a)^p (sum_(k=0)^(n-p) alpha_(p+k) (x-a)^k \
        &space + (x-a)^(n-p) epsilon(x)) $
    
    On pose, pour tout $x in I$,
    $ epsilon_1(x) =& sum_(k=0)^(n-p) alpha_(p+k) (x-a)^k \
        &+ (x-a)^(n-p) epsilon(x) \
        -->_a& 0 $
    $square$
]

#yprop(name : [Somme de fonction admettant un DL])[
    Soient $f$ et $g$ deux fonctions admettant un $DL(n,a)$.

    Alors $f+g$ admet un $DL(n,a)$.
]

#yproof[
    Soit $(alpha_i) in RR^[|0,n|]$ et $(beta_i) in RR^[|0,n|]$,
    $epsilon_1$ et $epsilon_2$ deux fonctions définies sur $I$ telles que
    pour tout $x in I$,
    $ f(x) =& sum_(k=0)^n alpha_k (x-a)^k \
        &+ (x-a)^n epsilon_1(x) $
    $ g(x) =& sum_(k=0)^n beta_k (x-a)^k\
        &+ (x-a)^n epsilon_1(x) $
    $epsilon_1 -->_a 0$ et $epsilon_2 -->_a 0$.

    D'où, pour tout $x in I$,
    $ f(x) +& g(x) \
        =& sum_(k=0)^n (alpha_k + beta_k) (x-a)^k \
            &+ (x-a)^n (epsilon_1(x) + epsilon_2(x)) $
    
    Or $epsilon_1 + epsilon_2 -->_a 0$.
    $square$
]

#yprop(name: [Produit de fonctions admettant un DL])[
    Soient $f$ et $g$ admettant un #DL($n$, $a$).
    Alors $f g$ aussi.
]

#yproof[
    Soit $(alpha_i) in RR^[|0,n|]$ et $(beta_i) in RR^[|0,n|]$,
    $epsilon_1$ et $epsilon_2$ deux fonctions définies sur $I$.

    Pour tout $x in I$,
    $ f(&x)g(x) \
        =& (sum_(k=0)^n alpha_k (x-a)^k + (x-a)^n epsilon_1(x)) \
            &times (sum_(k=0)^n beta_k (x-a)^k + (x-a)^n epsilon_1(x)) \
        =& sum_(k=0)^n sum_(l=0)^n alpha_k beta_l (x-a)^(k+l) \
            &+ (x-a)^n epsilon_1(x) sum_(l=0)^n beta_l (x-a)^l \
            &+ (x-a)^n epsilon_2(x) sum_(k=0)^n alpha_k (x-a)^k \
            &+ (x-a)^(2n) epsilon_1(x) epsilon_2(x) $
    
    Et comme, pour tout $x in I$,
    $ sum_(k=0)^n sum_(l=0)^n& alpha_k beta_l (x-a)^(k+l) \
        =& sum_(k=0)^n sum_(j=k)^(k+n) alpha_k beta_(j-k) (x-a)^j \
        // j = k+l et l =j-k
        =& sum_(j=0)^(2n) (sum_(k= max(0, j-n))^min(j,n) alpha_k beta_(j-k)) \
            &times (x-a)^j \
        =& sum_(j=0)^(2n) gamma_j (x-a)^j $
    où $gamma_j = sum_(k= max(0, j-n))^min(j,n) alpha_k beta_(j-k) $
    pour tout $j$.

    D'où, pour tout $x in I$,
    $ f(x&)g(x) \
        =& sum_(j=0)^n gamma_j (x-a)^j + (x-a)^n \
            &times (sum_(j=0)^(2n) gamma_j (x-a)^j \
                &space + (x-a)^n epsilon_1(x) sum_(l=0)^n beta_l (x-a)^l \
                &space + (x-a)^n epsilon_2(x) sum_(k=0)^n alpha_k (x-a)^k \
                &space + (x-a)^(2n) epsilon_1(x) epsilon_2(x)) $
    
    On pose, pour $x in I$,
    $ epsilon(x) =& sum_(j=0)^(2n) gamma_j (x-a)^j \
        &+ (x-a)^n epsilon_1(x) sum_(l=0)^n beta_l (x-a)^l \
        &+ (x-a)^n epsilon_2(x) sum_(k=0)^n alpha_k (x-a)^k \
        &+ (x-a)^(2n) epsilon_1(x) epsilon_2(x) $
    
    On a bien $epsilon -->_a 0$.
    $square$
]

#yprop(name : [Primitivation d'un DL])[
    Soit $f$ admettant un $DL(n,a)$ : pour tout $x in I$,
    $ f(x) =_a sum_(k=0)^n alpha_k (x-a)^k + o((x-a)^n) $
    et $F$ une primitive de $f$. Alors, pour tout $x in I$,
    $ F(x) =_a F(a) + sum_(k=0)^n alpha_k (x-a)^(k+1)/(k+1)
        yt + o((x-a)^(n+1)) $
    $square$
]