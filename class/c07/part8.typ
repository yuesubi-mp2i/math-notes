#import "../../template/ytemplate.typ": *


#ypart[ Étude locale ]


#yprop(name: [
    Tangente en un point pour lequel une fonction admet un DL ;
    et position relative de la tangente et de la courbe de la fonction.
])[
    Soient $f: I --> RR$, $cal(C)$ sa courbe représentative,
    et $(alpha_0, alpha_1, alpha_p) in RR^3$ avec $p >= 2$,
    tel que, pout tout $x in I$,
    $ f(x) =_a alpha_0 + alpha_1 (x - a)
        yt + alpha_p (x - a)^p + o((x-a)^p) $
    et que $alpha_p != 0$.

    - $cal(C)$ présente une tangente $(T)$, au point $A$ de coordonnées
        $(a, f(a))$ d'équation
        $ y = alpha_0 + alpha_1 (x-a)$

    - La position de $cal(C)$ par rapport à $(T)$
        au voisinage de $A$, est donnée par le signe de $alpha_p (x-a)^p$.
]

#yproof[
    Archi-simple, laissée en exercice.
    $square$
]