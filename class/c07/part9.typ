#import "../../template/ytemplate.typ": *


#ypart[ Application à la recherche d'asymptote ]


#yprop(name: [Méthode pour trouver une asymptote avec un développement asymptotique])[
    Soit $f : [M, +oo[ --> RR$, $cal(C)$ la courbe de $f$ et
    $D: y = alpha x + beta$.

    $ D "asymptote de" cal(C) "en" +oo
        yt <=> lim_(x->+oo) (f(x) - (alpha x + beta)) = 0
        yt <=> f(x) - (alpha x + beta) =_(+oo) o(1)
        yt <=> f(x) =_(+oo) alpha x + beta + o(1)
        yt <=> f(1/u) =_(0^+) alpha/u + beta + o(1)
        yt <=> u f(1/u) =_(0^+) alpha + beta u + o(u) $
]

#yexample[
    $f: x --> sqrt(1 + x^2)$

    Pour tout $u > 0$,
    $ u f(1/u) &= u sqrt(1 + 1/u^2) \
        &= sqrt(1 + u^2) \
        &= (1 + u^2)^(1/2) \
        &=_0 1 + 1/2 u^2 + o(u^2) $
    
    Donc
    $ 1/x f(x) =_(+oo) 1 + 1/(2x^2) + o(1/x^2) $
    et donc
    $ f(x) =_(+oo) x + 1/(2x) + o(1/x) $

    Donc la droit d'équation $y = x$ est asymptote en $+oo$ et $cal(C)$ est
    au-dessus.
]