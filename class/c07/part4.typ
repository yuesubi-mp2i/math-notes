#import "../../template/ytemplate.typ": *


#ypart[ Développement limité d'ordre $0$ ]


#let DL(order, point) = { $"DL"_order (point)$ }


#yprop(name: [Condition nécessaire est suffisante pour avoir un DL d'ordre 0])[
    $f$ admet un développement limité d'*ordre 0* au voisinage de $a$,
    si et seulement si, $f$ est *continue en $a$*.
]

#yproof[
    - "$==>$"
        
        Soit $alpha_0 in RR$ et $epsilon: I --> RR$, tels que
        $ cases(
            forall x in I\, f(x) = alpha_0 + epsilon(x),
            epsilon -->_a 0) $
        
        $f(a) = alpha(0) + epsilon(a) = alpha_0$
        et $lim_a f = alpha_0$
        ($epsilon$ est continue en $a$ comme elle y admet une limite)

        Donc $f(x) -->_a f(a)$.

    - "$<==$"

        On suppose que $f$ est continue en $a$.

        On pose, pour $x in I$,
        $ epsilon(x) = f(x) - f(a) $

        D'où,
        $ cases(forall x in I\, f(x) = f(a) + epsilon(x),
            epsilon -->_a 0) $

    $square$
]

#yprop(name: [ Prolongement par continuité d'un fonction qui admet un DL ])[
    Soit $f$ définite sur $I without {a}$ (où $a in I$)
    telle que, pour tout $x in I without {a}$,
    $ f(x) = sum_(k=0)^n alpha_k (x-a)^k + o((x-a)^n) $
    où $(a_i)_(0<=i<=n) in RR^[|0,n|]$.

    Alors on peut prolonger $f$ par continuité en posant $f(a) = alpha_0$ et
    on a alors, pour tout $x in I$,
    $ f(x) =_a sum_(k=0)^n alpha_k (x-a)^k + o((x-a)^n) $
]

#yproof[
    Soit $epsilon: I without {a} --> RR$ telle que,
    pour tout $x in I without {a}$,
    $ f(x) = sum_(k=0)^n alpha_k (x-a)^k
        yt + epsilon(a) (x-a)^n $
    et $epsilon -->_a 0$.

    D'où $f -->_a alpha_0 in RR$.

    On pose $f(a) = alpha_0$ et $epsilon(a) = 0$
    (deux prolongements par continuité)
    et on a bien #footnote[On considère que $0^0 = 1$],
    $ f(a) = alpha_0 = sum_(k=0)^n alpha_k (x-a)^k
        yt + epsilon(a) (x-a)^n $
    $square$
]