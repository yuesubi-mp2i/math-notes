#import "../../template/ytemplate.typ": *


#ypart[ Développement limité d'ordre $1$ ]


#let DL(order, point) = { $"DL"_order (point)$ }


#yprop(name: [Condition nécessaire est suffisante pour avoir un DL d'ordre 1])[
    $f$ admet un développement limité d'*ordre 1* au voisinage de $a$,
    si et seulement si, $f$ est *dérivable en $a$* ;
    et dans ce cas, pour tout $x in I$,
    $ f(x) =_a f(a) + (x-a) f'(a)
        yt + o(x-a) $
]

#yproof[
    - "$==>$"
        
        Soient $(alpha_0, alpha_1) in RR^2$ et
        $epsilon : I --> RR$, tels que,
        pour tout $x in I$,
        $ f(x) = alpha_0 + alpha_1 (x-a)
            yt + (x-a) epsilon(x) $
        et $epsilon -->_a 0$.

        On a,
        $ f(a) = alpha_0 + alpha_1 (a - a)
            ytt + (a-a) epsilon(a)
            yt = alpha_0 $
        
        Pour tout $x in I without {a}$,
        $ &(f(x) - f(a))/(x - a) \
            &= (alpha_1 (x-a) + (x-a) epsilon(x))/(x - a)\
            &= alpha_1 + epsilon(a) \
            &-->_a alpha_1 $
        ($epsilon$ est continue en $a$ comme elle y admet une limite)
        
        Donc $f$ est dérivable en $a$ et $f'(a) = alpha_1$
    
    - "$<==$"

        On suppose $f$ dérivable en $a$.

        On pose, pour $x in I$,
        $ epsilon(a) = cases((f(x) - f(a))/(x - a) &"si" x != a,
            0 &"si" x = a) $
        
        On a alors, pour tout $x in I$,
        $ f(x) =& f(a) + (x-a) f'(a) \
            &+ (x-a) epsilon(x) $
        et $epsilon -->_a 0$.

    $square$
]