#import "../../template/ytemplate.typ": *


#ypart[ Développement limité d'ordre supérieur ]


#ybtw[
    *Attention*

    $ f: x --> x^3 cos(1/x) $
    $ forall x != 0, f(x) = x^2 epsilon(x) =_(+oo) o(x^2) $

    où $epsilon(x) = x cos(1/x) -->_0 0$
    
    car $abs(epsilon(x)) <= abs(x)$

    $f$ a un DL_2(0). $f$ a un DL_0(0) donc on pronge par continuité en posant
    $f(0) = 0$.

    on a encore $f(x) =_(+oo) o(x^2)$ pour tour $x in RR$
    donc $f$ a un DL_1(0) donc $f$ est dérivable en 0 et $f'(0) = 0$

    $f$ est dérivable sur $RR^*$

    $forall x != 0, f'(x) = 3x^2 cos(1/x) - x sin(1/x)$
    et donc
    $ (f'(x) - f'(0))/(x - 0) =
        yt underbrace(3x cos(1/x), -->_0 0)
        - underbrace(sin(1/x), "pas de limite") $
    
    $f$ n'est pas dérivable 2 fois en $0$ alors qu'elle a un $"DL"_2 (0)$
]

#ytheo(name: [ Taylor-Young ])[
    Soit $f: I --> RR$ de classe $C^n$ en $a$.

    Alors $f$ a un $"DL"_n (a)$ et pour tout $x in I$,
    $ f(x) =_a& sum_(k=0)^n (f^((k))(a))/k! (x-a)^k \
        &+ o((x-a)^n) $

    $ f(x) =_a& f(a) + f'(a) + (f''(a))/2 \
        &+ (f'''(a))/6 + (f''''(a))/24 \
        &dots.v \
        &+ (f^((n))(a))/n! (x-a)^n \
        &+ o((x-a)^n) $
]

#yproof[
    Peuve par récurrence sur $n$.

    - Soit $f: I --> RR$ continue en $a$. On a vu au paragraphe 4 que,
        pour tout $x in I$,
        $ f(x) =_a f(a) + o(1) $
    
    - On suppose la formule vraie pour les fonctions de classes $C^n$ en $a$.

        Soit $f$ de classe $C^(n+1)$ en $a$.
        Donc $f'$ est de classe $C^n$ en $a$.

        Donc, pout tout $x in I$,
        $ f'(x) =_a& sum_(k=0)^n ((f')^((k)) (a))/k! (x-a)^k \
            &+ o((x-a)^n) $
        
        D'où, pour tout $x in I$,
        $ f(x) =_a f(a)
            yt+ sum_(k=0)^n ((f')^((k+1)) (a))/k! (x-a)^(k+1)/(k+1)
            yt+ o((x-a)^(n+1)) $
        
        $square$
]

#yexample[
    Exponencielle est de classe $cal(C)^oo$ au voisinage de $0$.

    $ forall n in NN, forall x in RR,
        yt e^x =_0 sum_(k=0)^n (exp^((k)) (0))/k! x^k + o(x^n)
        ytt =_0 sum_(k=0)^n x^k/k! + o(x^n) $
]

#yexample[
    _Exercice_: On obtient de la même façon les DL de cosinus et sinus.
]

#ytheo(name: [Unicité du développement limité])[
    Soit $f: I --> RR$ une fonction telle que, pour tout $x in I$,
    $ cases(
        f(x) =_a sum_(k=0)^n alpha_k (x - a)^k
            yt + o((x-a)^n),
        f(x) =_a sum_(k=0)^n beta_k (x - a)^k
            yt + o((x-a)^n)) $
    où $(alpha_0, dots, alpha_(n+1), beta_0, dots, beta_(n+1)) in RR^(2n + 2)$.

    Alors,
    $ forall k in [|0, n|], alpha_k = beta_k $
]

#yproof[
    Par récurrence sur $n$.

    - Si, pour tout $x in I$,
        $ f(x) =_a alpha_0 + o(1) = beta_0 + o(1) $
        alors $alpha_0 = lim_(x->a) f(x) = beta_0$
    
    - On suppose la propriété vraie pour les DL d'ordre $n$.

        Soit $(alpha_k) in RR^[|0, n+1|]$ et $(beta_k) in RR^[|0, n+1|]$

        On suppose que, pour $x in I$,
        $ f(x) =_a sum_(k=0)^(n+1)  alpha_k (x-a)^k
                ytt + o((x-a)^(n+1))
            yt =_a sum_(k=0)^(n+1) beta_k (x-a)^k
                ytt + o((x-a)^(n+1)) $
        
        Par troncature, pour $x in I$,
        $ f(x) =_a sum_(k=0)^n  alpha_k (x-a)^k
                ytt + o((x-a)^n))
            yt =_a sum_(k=0)^n beta_k (x-a)^k
                ytt + o((x-a)^n) $
        
        D'après l'hypothèse de récurrence,
        $ forall k in [|0,n|], alpha_k = beta_k $

        D'où, pour tout $x in I$,
        $ sum_(k=0)^(n+1)  alpha_k (x-a)^k + o((x-a)^(n+1)
            yt =_a sum_(k=0)^n  alpha_k (x-a)^k
                ytt + beta_(n+1) (x-a)^(n+1)
                ytt + o((x-a)^(n+1)) $
        et finalement : pour tout $x in I$,
        $ alpha_(n+1) (x-a)^(n+1) + o((x-a)^(n+1))
            yt = beta_(n+1) (x-a)^(n+1)
                ytt + o((x-a)^(n+1)) $
        
        Soient $epsilon_1$ et $epsilon_2$ deux fonctions définies sur $I$
        telles que, pour $x in I$,
        $ alpha_(n+1) (x-a)^(n+1) + (x-a)^(n+1) epsilon_1(x)
            yt = beta_(n+1) (x-a)^(n+1)
                ytt + (x-a)^(n+1) epsilon_2(x) $
        , $epsilon_1 -->_a 0$ et $epsilon_2 -->_a 0$.
    
        D'où, pour tout $x in I without {a}$,
        $ alpha_(n+1) + epsilon_1(x) = beta_(n+1) + epsilon_2(x) $

        On fait tendre $x$ vers $a$ et on obtient
        $ alpha_(n+1) = beta_(n+1) $
        $square$
]