#import "../../template/ytemplate.typ": *


#ypart[Permutations particulières]


#ynot(name: [Permutation])[
    Soit $sigma in S_n$.

    On représente $sigma$ par
    $ mat(1, 2, 3, ..., n;
        sigma(1), sigma(2), sigma(3), ..., sigma(n)) $
]

#ydef(name: [Support d'une permutation])[
    Soit $sigma in S_n$.

    Le _support_ de $sigma$ est
    $ "Supp"(sigma) = {i in [|1,n|] | sigma(i) != i} $
]

#ydef(name: [Cycle])[
    Soit $(a_1, ..., a_k) in [|1,n|]^2$ tels que $forall i!=j, a_i != a_j$
    où $k>=2$.

    L'application $gamma $ définie, pour $ell in [|1, n|]$, par
    $ gamma(ell) = cases(ell &"si" ell in.not {a_1, ..., a_k},
        a_(i+1) &"si" ell = a_i "avec" i < k,
        a_1 &"si" ell = a_k) $
    est appelée _$k$-cycle_, et elle est notée
    $ mat( a_1, a_2, ..., a_k ) $

    N.B. l'identitée n'est pas un cycle.
] 

#yprop(name: [Support d'un cycle])[
    $ "Supp"(mat(a_1, ..., a_n)) = { a_1, ..., a_n } $
]

#yprop(name: [Particularité de deux cycles ayant des supports disjoints])[
    Deux cycles à support disjoints commutent (au sens de $compose$).
]

#yproof[
    Soient $gamma_1 = mat(a_1, ..., a_p)$
    et $gamma_2 = mat(b_1, ..., b_q)$
    tels que leurs supports
    $ &A = "Supp"(gamma_1) = {a_1, ..., a_p} \
    &B = "Supp"(gamma_2) = {b_1, ..., b_q} $
    soient disjoints.

    On pose $a_(p+1) = a_1$ et $b_(q+1) = b_1$.

    Soit $ell in [|1, n|]$,

    - Cas 1 : On suppose $ell in.not A union B$. 
    
        Ainsi, d'une part
        $ gamma_1 (gamma_2 (ell)) = gamma_1 (ell) = ell $
        et d'autre part
        $ gamma_2 (gamma_1 (ell)) = gamma_2 (ell) = ell $
    
    - Cas 2 : On suppose $ell in A$.

        Soit $i in [|1, p|]$ tel que $a_i = ell$.
    
        Alors $ell in.not B$ car $A$ et $B$ sont disjoints.

        D'une part
        $ gamma_1 (gamma_2 (a_i)) = gamma_1 (a_i) = a_(i+1) $
        d'autre part
        $ gamma_2 (gamma_1 (a_i)) = gamma_2 (a_(i+1)) = a_(i+1) $
        (car $a_(i+1) in.not B$).
    
    - Cas 3 : De même que le cas précédent en supposant $ell in B$.
]

#ydef(name: [Orbite])[
    Soit $sigma in S_n$ et $ell in [|1,n|]$.

    L'_orbite_ de $ell$ par $sigma$ est
    $ { sigma^k (l) | k in NN }$.
]

#yprop(name: [Particularité des cycles qui forment une permutation])[
    Soit $sigma in S_n$.
    
    On suppose que
    $ sigma = gamma_1 compose gamma_2 compose ... compose gamma_n $
    où $gamma_1, ..., gamma_p$ sont des cycles à supports deux-à-deux disjoints.

    Les supports des $gamma_i$ correspondent aux orbites de $sigma$
    ayant au moins deux éléments.
]

#yproof[
    On note $gamma_1 = mat(a_1, ..., a_k)$.

    L'orbite de $a_1$ par $sigma$ est ${a_1, sigma(a_i), ...}$.

    $ sigma(a_1) &= gamma_1 compose ...  compose gamma_p (a_1) \
        &= gamma_2 compose ...  compose gamma_p compose gamma_1 (a_1) \
        &= gamma_2 compose ...  compose gamma_p (a_2) \
        &= a_2 $

    $ sigma^2 (a_1) &= sigma(a_2) \
        &= gamma_2 compose ...  compose gamma_p compose gamma_1 (a_2) \
        &= a_3 $
    
    $ dots.v $

    $ sigma^(k-1) (a_1) = a_k $ 
    $ sigma^k (a_1) = a_1 $ 

    L'orbite de $a_1$ par $sigma$ est
    $ {a_1, a_2, ..., a_k} = "Supp"(gamma_1) $
]

#ytheo(name: [Décomposition d'une permutation])[
    Toute permutation se décompose de magnière unique à l'ordre
    des facteurs près en un produit de cycles à supports
    deux-à-deux disjoints.
]

#yproof[
    Soit $sigma in S_n$.
    
    On note $O_1, ..., O_k$ les orbites de $sigma$
    $ cases(forall i != j\, O_i sect O_j = emptyset,
        union.big_(i=1)^k O_i = [|1, n|]) $

    En effet, les orbites sont les classes d'équivalence
    pour la relation d'équivalence
    $ i tilde j <=> exists k in ZZ, sigma^k (i) = j $

    Soit $i in [|1, k|]$ tel que $abs(O_i) >= 2$.

    Soit $a in O_i$, on pose $ell = abs(O_i)$.

    On forme $gamma_i = mat(a, sigma(a), ..., sigma^(ell -1) (a))$.
    $ "Supp"(gamma_i) = O_i $

    On vérifie alors que $sigma = gamma_1 compose ... compose gamma_k$.
]

#ydef(name: [Transposition (contexte du groupe symétrique)])[
    Une _transposition_ est un $2$-cycle.
]

#yprop(name: [Ordre d'un cycle])[
    Soit $gamma$ un $k$-cycle.

    L'_ordre_ de $gamma$ est $k$, _i.e_
    $ cases(gamma^k = id, forall ell [|1, k-1|]\, gamma^ell != id) $

    On note $abs(gamma)$ l'ordre de $gamma$.
]