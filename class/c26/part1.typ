#import "../../template/ytemplate.typ": *


#ypart[Rappels]


// Link for diagram https://discord.com/channels/1054443721975922748/1245011078413422653


#ydef(name: [Groupe symétrique])[
    On appelle _groupe symétrique_ $S_n$
    l'ensemble des permutations de $[|1,n|]$ muni
    de $compose$.
]

#yprop(name: [Cardinal du groupe symétrique])[
    $ abs(S_n) = n! $

    N.B. $n! tilde_(n->+oo) sqrt(2 pi n) (n/e)^n$
]