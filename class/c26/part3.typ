#import "../../template/ytemplate.typ": *


#ypart[Décomposition en produit de transposition]


#yprop(name: [Décomposition d'un cycle en transpositions])[
    Soit $gamma = mat(a_1, ..., a_k)$ un $k$-cycle.
    Alors
    $ gamma = mat(a_1, a_2) mat(a_2, a_3) ... mat(a_(k-1), a_k) $
]

#yproof[
    On pose, pour $i in [|1, k-1|]$, $ tau_i = mat(a_i, a_(i+1)) $

    Pour $i in [|1, k-1|]$,
    $ tau_1 ... tau_(k-1) (a_i) &= tau_1 ... tau_i (a_i) \
        &= tau_1 ... tau_(i-1) (a_(i+1)) \
        &= a_(i+1) \
        &= gamma(a_i) $

    $ tau_1 ... tau_(k-1) (a_k) &= tau_1 ... tau_(k-2) (a_(k-1)) \
        &= tau_1 ... tau_(k-3) (a_(k-2)) \
        &= tau_1 (a_2) \
        &= a_1 \
        &= gamma(a_k) $
    
    Si $ell in.not {a_1, ..., a_(k-1)}$, alors
    $ tau_1 ... tau_(k-1) (ell) = ell = gamma(ell) $
]

#ycor(name: [Décomposition d'une permutation en transpositions])[
    Toute permutation est un produit de transpositions.

    N.B. la décomposition n'est pas unique.
]

#yproof[
    C'est un corollaire de la décomposition des cycles en transpositions.

    On décompose la permutation en cycles, et on décompose chaqu'un
    des cycles en transpositions.
]

#ydef(name: [Inversion d'une permutation])[
    Soit $sigma in S_n$.

    Une _inversion_ de $sigma$ est un couple $(i,j)$ qui vérifie
    $ cases(i < j, sigma(i) > sigma(j)) $
]

#ydef(name: [Signature d'une permutation])[
    La signature d'une permutation $sigma in S_n$
    est
    $ epsilon(sigma) = (-1)^k $
    où $k$ est le nombre d'inversions de $sigma$.

    On dit que $sigma$ est _paire_ si $epsilon(sigma) = 1$,
    _impaire_ si $epsilon(sigma) = -1$.
]

#ylemme(name: [Ériture de la signature d'une permutation avec un produit])[
    Soit $sigma in S_n$.
    $ epsilon(sigma) = product_(1<=i<j<=n) (sigma(i) - sigma(j))/(i -j) $
]

#yproof[
    On détermine la valeur absolue du membre de droite :
    $ (product_(i<j) (sigma(i)-sigma(j))/(i-j))^2
        yt = (product_(i<j) (sigma(i)-sigma(j))/(i-j))
            ytt (product_(j<i) (sigma(i)-sigma(j))/(i-j))
        yt = product_(i!=j) (sigma(i) - sigma(j))/(i-j)
        yt = (product_(i!=j) (sigma(i) - sigma(j)))/(product_(i!=j) (i-j))
        yt = (product_(k!=ell) (k - ell))/(product_(i!=j) (i-j))
        yt = 1 $
    avec le changement de variable $sigma(i) = k$ et $sigma(j) = ell$.

    On détermine maintenant le signe du membre de droite :

    Pour tout $i < j$,
    $ (sigma(i) - sigma(j))/(i - j) $
    est négatif si $(i,j)$ est une inversion,
    et positif sinon.

    $ product_(i<j) (sigma(i) - sigma(j))/(i - j) $
    est positif s'il y a un nombre pair d'inversions,
    négatif sinon.
]

#ytheo(name: [Structure de la signature])[
    $epsilon: S_n -> {-1, 1}$ est un morphisme de groupes :
    pour tout $(sigma, sigma') in (S_n)^2$,
    $ epsilon(sigma compose sigma') = epsilon(sigma) epsilon(sigma') $
]

#yproof[
    $ epsilon(sigma compose sigma')
        yt = product_(i<j) (sigma(sigma'(i)) - sigma(sigma'(j)))/(i - j)
        yt = product_(i<j) (sigma(sigma'(i)) - sigma(sigma'(j)))/(sigma'(i) - sigma'(j))
            yttt times (sigma'(i) - sigma'(j))/(i - j)
        yt = product_(i<j) (sigma(sigma'(i)) - sigma(sigma'(j)))/(sigma'(i) - sigma'(j))
            epsilon(sigma') $
    
    On réalise un changement de variables :

    Pour $i < j$, on pose
    $ cases((k, ell) = (sigma'(i), sigma'(j)) \
        wide "si" sigma'(i) < sigma'(j),
        (k, ell) = (sigma'(j), sigma'(i)) "sinon") $
    
    $(i, j) |-> (k, ell)$ est une bijection
    de ${(i,j) in [|1, n|] | i < j}$ dans lui-même.

    D'où
    $ epsilon(sigma compose sigma') 
        yt = product_(i<j) (sigma(sigma'(i)) - sigma(sigma'(j)))/(sigma'(i) - sigma'(j))
            epsilon(sigma')
        yt = epsilon(sigma) epsilon(sigma') $
]

#yprop(name: [Signature d'une transposition])[
    La signature d'une transposition est $-1$.
]

#yproof[
    Soit $tau = mat(a, b)$ où $a < b$.

    $ epsilon(tau)
        yt= product_(i<j) (tau(i) - tau(j))/(i - j) \

        yt= (b-a)/(a-b) product_(j > a \ j!= b) (tau(a) - tau(j))/(a - j)
            ytt times product_(j > b) (tau(b) - tau(j))/(b - j)
            ytt times product_(i < a) (tau(i) - tau(a))/(i - a)
            ytt times product_(i < b \ i!=a) (tau(i) - tau(b))/(i - b) \

        yt= -1 times product_(j > a \ j!= b) (b - j)/(a - j)
            times product_(j > b) (a - j)/(b - j)
            ytt times product_(i < a) (i - b)/(i - a)
            times product_(i < b \ i!=a) (i - a)/(i - b) \

        yt= -1 times product_(j > a \ j!= b) (b - j)/(a - j)
            times product_(j > b) (a - j)/(b - j)
            ytt times product_(i < a) (i - b)/(i - a)
            times product_(i < b \ i!=a) (i - a)/(i - b) \

        yt= - product_(a < j < b) (b - j)/(a - j)
            times product_(a < i < b) (i - a)/(i - b) \

        yt= - product_(a < j < b) (b - j)/(a - j) (j - a)/(j - b) 
        yt= -1 $
]

#ycor(name: [Lien entre la parité d'une permutation et de sa décomposition en transpositions])[
    Toute permutation paire est le produit
    d'un nombre pair de transpositions et réciproquement.
]

#yproof[
    Découle du fait que la signature d'une transposition est $-1$.
]

#ycor(name: [Structure du groupe alterné])[
    L'ensemble des permutations paires et un sous-groupe de $S_n$.

    On l'appelle le _groupe alterné_ et on le note $A_n$.
]

#yproof[
    $ A_n = ker(epsilon) $
]

#yprop(name: [Signature d'un cycle])[
    La signature d'un $k$-cycle est 
    $(-1)^(k-1)$.
]

#yproof[
    $ mat(a_1, ..., a_k) = tau_1 ... tau_(k-1) $
    où $tau_i = mat(a_i a_(i+1))$.
]


Oublis :

#yprop(name: [Application d'une permutation sur les éléments d'un cycle])[
    Soit $sigma in S_n$.
    $ sigma mat(a_1, ..., a_k) sigma^(-1) = mat(sigma(a_1), ..., sigma(a_n)) $
]

#yproof[
    On pose $a_(k+1) = a_n$.>

    Soit $i in [|1,n|]$.

    - Si $i = sigma(a_j)$ alors
        $ &sigma mat(a_1, ..., a_k) sigma^(-1) (i) \
            &= sigma mat(a_1, ..., a_k) (a_j) \
            &= sigma(a_(j+1)) $
    
    - Si $i in.not { sigma(a_1), ..., sigma(a_k) }$ alors
        $ &sigma mat(a_1, ..., a_k) sigma^(-1) (i)\
            &= sigma (sigma^(-1) (i)) \
            &= i $
]