#import "../../template/ytemplate.typ": *


#ypart[
    Equations différencielles linéaires du seconde ordre à coefficients constants
]


#let let_ab() = {[
    $(a, b) in CC^2$
]}
#let let_fd() = {[
    $f$ continue sur un domaine $D$
]}

#let let_e() = {[
    $ E: y''(t) + a y'(t) + b y(t) = f(t) $
]}
#let let_h() = {[
    $ H: y''(t) + a y'(t) + b y(t) = 0 $
]}
#let let_c() = {[
    $ C: z^2 + a z + b = 0 $
]}


#ydef(name: [équation carctéristique du second ordre])[
    Soient #let_ab(), #let_fd() et #let_e()
    L'*équation caractéristique* associée à $E$ est
    #let_c()
]

#yprop(name: [
    solutions de l'équation différentielle linéaire résolue
    homogène du second ordre à coefficients constants
])[
    Soient #let_ab(), #let_h() et #let_c()

    + On suppose que $C$ a deux racines distinctes
        $r_1 != r_2$.
        Les solutions à valeurs complexes de $H$ sont les
        fonctions de la forme
        $t |-> A e^(r_1 t) + B e^(r_2 t)$ où
        $(A, B) in CC^2$.
    + On suppose que $C$ a un racine double $r$.
        Les solutions à valeurs complexes de $H$ sont
        $t |-> (A + B t) e^(r t)$ où $(A, B) in CC^2$.
]

#yexample[...]

#yproof[
    + Soit $y: RR --> CC$ dérivable 2 fois.

        On pose
        $ x: &RR --> CC \
            &t |-> y(t) e^(-r_1 t) $
        
        $ &y "solution de" H \
            &<=> forall t in RR, \
            &space y''(t) + a y'(t) + b y(t) = 0 $
        
        Ainsi pour $t in RR$,
        $ cases(
                y(t) = x(t) e^(r_1 t),
                y'(t) = (x'(t) + r_1 x(t)) e^(r_1 t),
                y''(t) = (x''(t) + 2 r_1 x'(t) \
                space + r^2_1 x(t)) e^(r_1 t)
            ) $
        
        D'où,
        $ &y "solution de" H \
            &<=> forall t in RR,\
                &space e^(r_1 t) \[x''(t) + (2r_1 + a) x'(t) \
                &space underbrace((r_1^2 + a r_1 + b), = 0) x(t)\] = 0 \
            &<=> forall t in RR,\
                &space x''(t) + (2 r_1 + a) x'(t) = 0 \
            &<=> exists lambda in CC, forall in RR, \
                &space x'(t) = lambda e^(-(2r_1 + a)t) $
        
        Or,
        $ 2r_1 + a &= 2 r_1 - r_1 - r_2 \
            &= r_1 - r_2 != 0 $

        $ y &"solution de" H \
            <=> &exists (lambda, mu) in CC^2, forall t in RR, \
                &x(t) = (lambda e^(-(2r_1 + a)t))/(-(2r_1 + a)) + mu \
            <=> &exists (lambda, mu) in CC^2, forall t in RR, \
                &y(t) = mu e^(r_1 t) - lambda/(2r_1 + a) e^((-a - r_1)t) \
            <=> &exists (lambda, mu) in CC^2, forall t in RR, \
                &y(t) = mu e^(r_1 t) - lambda/(2r_1 + a) e^(r_2 t) \
            <=> &exists (A, B) in CC^2, forall t in RR, \
                &y(t) = A e^(r_1 t) + B e^(r_2 t) $
    
    + Soit $y: RR --> CC$ dérivable 2 fois et

        $ x: &RR --> CC \
            &t |-> y(t) e^(-r t) $
        
        $ &y "solution de" H \
            &<=> forall t in RR, \
                &space e^(r t) [x''(t) + (2r + a) x'(t) \
                &space + (r^2 + a r + b) x(t)] = 0 \
            &<=> forall t in RR, x''(t) = 0 \
            &<=> exists (A, B) in CC^2, forall t in RR, \
                &space x(t) = A + B t \
            &<=> exists (a, B) in CC^2, forall t in RR, \
                &space y(t) = (A + B t) e^(r t) $
]

#yprop(name: [
    solutions de l'équation différentielle linéaire résolue
    homogène du second ordre à coefficients réels constants
])[
    Soient $(a, b) in RR^2$, #let_h() et #let_c()
    
    + Si $C$ a deux racines simples réelles $r_1 != r_2$,
        les sollutions à valeurs réelles de $H$ sont
        $t |-> A e^(r_1 t) + B e^(r_2 t)$ où
        $(A, B) in RR^2$.
    
    + Si $C$ a une racine double réelle $r$ alors les
        solutions à valeurs rélles de $H$ sont
        $t |-> (A + B t) e^(r t)$ où $(A, B) in RR^2$.
    
    + Si $C$ a 2 racines simples non réelles
        $lambda plus.minus i omega$, les solutions à valeurs dans $RR$ de $H$ sont
        $t |-> e^(lambda t) (A cos(omega t) + B sin(omega t))$
        où $(A, B) in RR^2$.
]

#yproof[
    Soit $y: RR --> RR$ dérivable 2 fois.

    $ (*) : &y "solution de" H \
        &<=> cases(y "solution complexe de" H,
                forall t in RR\, y(t) = overline(y(t))) $
    
    + $ &(*) \
        &<=> exists (A,B) in CC^2, \
            &space cases(
                forall t in RR\, y(t) = A e^(r_1t) + B e^(r_2 t),
                forall t in RR\, overline(A) e^(r_1 t) + overline(B) e^(r_2 t) \
                space = A e^(r_1 t) + B e^(r_2 t)
            ) \
        &=> exists (A, B) in CC^2, \
            &space cases(
                forall t in RR\, y(t) = A e^(r_1 t) + B e^(r_2 t),
                overline(A) + overline(B) = A + B,
                overline(A) e^(r_1) + overline(B) e^(r_2) = A e^(r_1) + B e^(r_2)
            ) \
        &=> exists (A, B) in CC^2, \
            &space cases(
                forall t in RR\, y(t) = A e^(r_1) + B e^(r_2),
                A + B in RR,
                overline(B) (e^(r_2) - e^(r_1)) = B (e^(r_2) - e^(r_1))
            ) \
        &=> exists (A,B) in CC^2, \
            &space cases(
                forall t in RR\, y(t) = A e^(r_1 t) + B e^(r_2 t),
                A + B in RR,
                B in RR
            ) $
        
        Réciproquement, si
        $ forall t in RR, y(t) = A e^(r_1 t) + B e^(r_2 t) $
        avec $(A,B) in RR^2$, on a bien, pour tout $t in RR$,
        $ cases(
                y(t) in RR,
                y''(t) + a y'(t) + b y(t) = 0
            ) $
        
    + Même méthode :
        $ (*) <=>
            yt exists (A,B) in CC^2,
            yt cases(
                forall t in RR\, y(t) = (A + B t) e^(r t),
                A = overline(A) "(avec" t = 0 ")",
                B = overline(B) "(avec" t = 1 ")") $
        
        La réciproque est facile.
    
    + $ &(*) \
            &<=> exists (A, B) in CC^2, \
                &space cases(
                    (*') space forall t in RR\, y(t),
                        space = A e^((lambda + i omega) t) +
                        B e^((lambda - i omega) t),
                    A + B = overline(A) + overline(B),
                    forall t in RR\, overline(A) e^((lambda - i omega) t) +
                        overline(B) e^((lambda + i omega) t),
                        space = A e^((lambda - i omega) t) +
                        B e^((lambda + i omega) t),
                ) \
            &<=> exists (A, B) in CC^2, \
                &space cases(
                    (*'),
                    A + B = overline(A) + overline(B),
                    forall t in RR\, e^(i omega t) (overline(B) - A),
                    space = e^(- i omega t) (overline(A) - B),
                    space = e^(- i omega t) (overline(B) - A)
                ) \
            &<=> exists (A, B) in CC^2, \
                &space cases(
                    (*'),
                    forall t in RR,
                    space (e^(i omega t) + e^(-i omega t)) (overline(B) - A) = 0
                ) \
            &<=> exists A in CC, forall t in RR, y(t) \
                &space = e^(lambda t) (A e^(i omega t) + overline(A) e^(-i omega t)) \
                &space = 2 e^(lambda t) ("Re"(A) cos(omega t) \
                &space - "Im"(A) sin(omega t)) $
]

#yexample[...]

#yexample[...]

#yprop(name: [
    Particularité d'une solution de
    $E: y''(t) + a y'(t) + b y(t) = P(t) e^(lambda t)$
    où $(a, b, lambda) in CC^3$, $P$ un polynôme à
    coefficients complexes.
])[
    Soit
    $ E: y''(t) + a y'(t) + b y(t) = P(t) e^(lambda t) $
    où $(a, b, lambda) in CC^3$, $P$ un polynôme à
    coefficients complexes.

    Il existe une solution de $E$ de la forme
    $t |-> Q(t) e^(lambda t)$ où $Q$ est une polynôme de
    degré :
    - $"deg"(P)$, si $lambda$ n'est pas racine de
        l'équation caractéristique;
    - $"deg"(P) + 1$, si $lambda$ est racine simple de 
        l'équation caractéristique;
    - $"deg"(P) + 2$, si $lambda$ est racine double de l'
        équation caractéristique.
]

#yexample[...]

#yprop(name: [
    forme des solutions d'une équation différentielle
    linéaire résolue d'ordre 2
])[
    Soit #let_ab(), $f: I --> CC$ *continue* sur un
    intervalle $I$, #let_e() et $y_0$ une solution
    particulière de $E$.

    Les solutions de $E$ sont les fonctions de la forme
    $h + y_0$ où $h$ est solution de #let_h()
]

#yproof[
    Soit $y: I --> CC$ dérivable deux fois.

    On pose $h = y - y_0$.

    $ y "solution de" E
        yt <=> forall t in I, y''(t) + a y'(t)
            ytt + b y(t) = f(t)
        yt <=> forall t in I, y''(t) + a y'(t) + b y(t)
            ytt = y''_0(t) + a y'_0(t) + b y_0(t)
        yt <=> forall t in I, h''(t) + a h'(t)
            ytt + b h(t) = 0
        yt <=> h "est solution de" H $
]