#import "../../template/ytemplate.typ": *


#ypart[Définitions]


#ydef(name: [équation différentielle])[
    Une *équation différentielle d'ordre $n$* est une
    équalité qui fait intervenir une fonction inconnue,
    disons $y$, et ses dérivées succéssives
    $y', y'', dots, y^((n))$.

    *Résoudre sur un domaine $D$*, une équation
    différentielle d'ordre $n$, c'est trouver toutes les
    fonctions dérivables $n$ fois sur $D$ qui satisfont l'égalité
    quand on remplace la fonction inconnue $y$ par $f$.
]

#ynot(name: [dérivée $n$-ième])[
    On note $y^((n))$, la dérivée $n$-ième de la fonction
    $y$. (Donc $y^((0)) = y$.)
]

#yexample[
    + $y' + 2y + 1$ est une équation différentielle d'
        ordre 1.
    + $x'' + underbrace(x, "fonction") =
        3 underbrace(f, "variable") $
]

#ybtw[
    Pour $y' + 2y = (x |-> e^(x^2))$, on s'autorise l'
        abus de notation (pas en DS) :
        $ y' + 2y = e^(x^2) $
        voire
        $ y'(x) + 2y(x) = e^(x^2) $
]

#ydef(name: [équation différentielle linéaire])[
    Une équation différentielle d'ordre $n$ est *linéaire*
    si elle est de la forme
    $ sum_(k = 0)^n a_k y^((k)) = b $
    où $a_0, a_1, dots, a_n, b$ sont des fonctions
    (indépendantes de $y$).
]

#yexample[
    + $x^2y' + x = e^x$ est liinéaire
    + $y'' + omega^2 sin(y) = 0$ n'est pas linéaire
    + $y'' + omega^2 y = 0$ est linéaire
    + $y' = -2 sqrt(y)$ n'est pas linéaire
]

#ytheo(name: [Cauchy-Lipshitz])[
    Soit $(E)$ une équation différentielle linéaire d'ordre
    $n$ *résolue* (i.e. il n'y a pas de $a_n$)
    $ y^((n)) + sum_(k = 0)^(n - 1) a_k y^((k)) = b $
    où $a_(n - 1), dots, a_0, b$ sont *continues* sur un
    intervalle $I$.

    Soit $t_0 in I$, $(u_k) in CC^[|0, n-1|]$.

    Le système
    $ cases(
        y^((n)) + sum_(k = 0)^(n - 1) a_k y^((k)) = b,
        forall k in [|0, n-1|]\, y^((k)) (t_0) = u_k) $
    a une unique solution.
    $square$
]

#let ycn(n, domain, codomain) = {
    $cal(C)^#n (#domain, #codomain)$
}

#ynot(name: [ensemble des fonctions de classe $cal(C)^n$])[
    On note $ycn(n, D, E)$, l'ensemble des fonctions de
    classe $cal(C)^n$, définies sur l'ensemble $D$, à
    valeurs dans l'ensemble $E$.
]

#yprop(name : [principe de superposition])[
    Soient $(a_0, dots, a_n)$ et $(b_1, b_2)$ des fonctions quelconques.

    $ E_1: sum_(k=0)^n a_k y^((k)) = b_1 $
    $ E_2: sum_(k=0)^n a_k y^((k)) = b_2 $

    Soient $(lambda_1, lambda_2) in CC^2$,
    $ E: sum_(k=0)^n a_k y^((k)) = lambda_1 b_1 + lambda_2 b_2 $

    Si $y_1$ et $y_2$ sont solutions de $E_1$ et $E_2$ respectivement.
    Alors, les fonctions de la forme $lambda_1 y_1 + lambda_2 y_2$
    sont *des* solutions de $E$.
]

#yproof[
    On pose $y = lambda_1 y_1 + lambda_2 y_2$,
    dérivable $n$ fois (car $y_1$ et $y_2$ le sont).

    Donc, pour tout $k in [|0, n|]$,
    $ y^((k)) = lambda_1 y_1^((k)) + lambda_2 y_2^((k)) $
    d'où,
    $ sum_(k=0)^n& a_k y^((k)) \
        =& sum_(k=0)^n a_k (lambda_1 y_1^((k)) + lambda_2 y_2^((k))) \
        =& lambda_1 sum_(k=0)^n a_k y_1^((k)) + lambda_2 sum_(k=0)^n a_k y_2^((k)) \
        =& lambda_1 b_1 + lambda_2 b_2 $
    $square$
]

#yprop(name: [structure des solutions])[
    Soit une domaine $D$, $(a_k) in (CC^D)^[|0,k|]$, $b in CC^D$,
    et
    $ E: sum_(k=0)^n a_k y^((k)) = b $

    On forme l'*équation homogène associée*,
    $ H: sum_(k=0)^n a_k y^((k)) = 0 $

    Soit $y_0$ une solutions de $E$.

    Les solutions de $E$ sur $D$ sont les fonctions $y_0 + h$ où $h$
    est une solutions de $H$.
]

#yproof[
    Soit  et $y_0$ une solution particulière de $E$.

    On pose, pour toute solution $y$ de $E$, $h_y = y - y_0$.

    D'après le principe de superposition, $h_y$ est une solution de $H$.

    Réciproquement, si $h_y$ est une solution de $H$ et
    $y_0$ une solution de $E$
    alors $h_y + y_0$ est aussi solution de $E$.
]

#yexample[
    $ E: y' = f(t) $
    $ H: y' = 0 $
    Les solutions de $H$ sur un intervalle $I$ sont les
    fonctions constantes.
]