// Équations différentielles linéaires
#import "../../template/ytemplate.typ": *

#ychap[Equations différencielles linéaires]

#include "part1.typ"
#include "part2.typ"
#include "part3.typ"