#import "../../template/ytemplate.typ": *


#ypart[Equations différencielles linéaires d'ordre 1]


#ytheo(name: [
    solutions d'une équation différentielle linéaire
    résolue homogène d'ordre 1
])[
    Soit $a: I --> CC$ *continue* sur l'*intervalle* $I$ et
    l'équation différentielle *linéaire* *résolue*
    *homogène* d'*ordre 1*
    $ H: y' + a(t) y = 0 $
    Les solutions de $H$ sont les fonctions de la forme
    $t |-> lambda e^(-A(t))$ où $lambda in CC$ et $A$ est l'une
    des primitives de $a$ sur l'intervalle $I$.
]

#yproof[
    Soit $y : I --> CC$ dérivable.
    
    On pose, pour $t in I$,
    $ z(t) = y(t) e^A(t) $

    $ y "solution de" H
        yt <=> forall t in I, y'(t) + a(t) y(t) = 0
        yt <=> forall t in I, z'(t) e^(-A(t))
            ytt - cancel(a(t) z(t) e^(-A(t)))
            ytt cancel(+ a(t) z(t) e^(-A(t))) = 0
        yt <=> forall t in I, z'(t) = 0
        yt <=> exists lambda in CC, forall t in I, z(t) = lambda
        yt <=> exists lambda in CC, forall t in I,
            ytt y(t) = lambda e^(-A(t)) $
]

#ybtw(name: ["à la physicienne"])[
    Moyen de retenir:
    $ (d y)/(d x) &+ a(t) y = 0 \
        &<=> d y + a(t) y d t = 0 \
        &<=> (d y)/y = -a(t) d t \
        &<=> integral (d y)/y = integral -a(t) d t \
        &<=> ln(y) = -A(t) + C \
        &<=> e^y = e^(-A(t) + C) \
        &<=> e^y = lambda e^(-A(t)) $
]

#yexample[
    + Résoudre sur $RR_+^*$, $t y'(t) + 2y(t) = 0$.

        Résoudre cette équation revient à résoudre sur $R_+^*$,
        $ H: y'(t) + 2/t y(t) = 0 $

        Les solutions de $H$ sont les fonctions,
        $ t |-> lambda e^(-2ln(t)) $
        où $lambda in CC$.
    
    + Les solutions de $H$ sour $RR_-^*$ sont,
        $ t|-> lambda e^(-2ln(t))
            = lambda/(-t)^2
            = lambda/t^2 $

    + Les solutions de $H$ sour $RR^*$ sont,
        $ t|-> cases(
            lambda/(t)^2 "si" t > 0,
            mu/(t)^2 "si" t < 0) $
        où $(mu, lambda) in CC^2$.
]

#yprop(name: [variation de la constante])[
    Soient $a$ et $b$ deux fonctions continues sur un
    intervalle $I$ et
    $ E: y'(t) + a(t)y(t) = b(t) $

    Il existe une solution de $E$ de la forme
    $ y_0: t |-> lambda(t) e^(-A(t)) $
    où $A$ est une primitive de $a$ et $lambda$ une fonction
    dérivable sur $I$.

    (N.B. : $lambda$ est une des primitives de
    $t |-> b(t) e^A(t)$)
]

#yproof[
    Soit $lambda$ dérivable et
    $ y_0 = lambda(t) e^(-A(t)) $

    $y_0$ est dérivable et pour $t in I$,
    $ y'_0(t)& + a(t) y_0(t) \
        =& lambda'(t) e^(-A(t)) - A'(t) lambda(t) e^(-A(t))\
        &+ a(t) lambda(t) e^(-A(t)) \
        =& lambda'(t) e^(-A(t)) $
    
    Ainsi,
    $ y_0 &"solution de" E_1 \
        <=>& forall t in I, lambda'(t) e^(-A(t)) = b(t) \
        <=>& forall t in I, lambda'(t) = b(t) e^A(t) $
    
    Comme $b$ et $A$ sont continues, $t |-> b(t) e^A(t)$ est
    continue, et admet donc au moins une primitive.

    En chosissant $lambda$ égale à l'une de ces primitives,
    on trouve bien une solution de $(E)$ de la forme
    proposée.
]

#yprop(name: [
    solutions d'une équation différentielle linéaire résolue
    d'ordre 1
])[
    Soient $a$ et $b$ deux fonctions *continues* sur un
    intervalle $I$,
    $ E: y'(t) + a(t)y(t) = b(t) $,
    $y_0$ une solution de $E$ sur $I$
    et $A$ une primitive de $a$.

    Les solutions de $E$ sur $I$ sont les fonctions
    $ t |-> lambda e^(-A(t)) + y_0(t) $
    pour $lambda in CC$.
]

#yproof[
    Soit $y$ un fonction dérivable sur I.

    $ y "solution de" E
        yt <=> forall t in I,
            ytt y'(t) + a(t) y(t) = b(t)
        yt <=> forall t in I,
            ytt y'(t) + a(t) y(t)
                yttt = y'_0(t) + a(t) y_0(t)
        yt <=> forall t in I, (y - y_0)'(t)
                ytt + a(t)(y - y_0)(t) = 0
        yt <=> y - y_0 "est une solution "
            ytt "de" H: z'(t) + a(t) z(t) = 0
        yt <=> exists lambda in CC, forall t in I,
            ytt y(t) - y_0(t) = lambda e^(-A(t))
        yt <=> exists lambda in CC, forall t in I,
            ytt y(t) = lambda e^(-A(t)) + y_0(t) $
]

#ytheo(name: [problème de Cauchy d'ordre 1])[
    Soient $a$ et $b$ *continues* sur un intervalle $I$,
    $t_0 in I$ et $u in CC$.

    Soit $P$ le problème de Cauchy,
    $ cases(
        y' + a(t)y = b(t),
        y(t_0) = u) $
    $P$ a une unique solution.
]

#yproof[
    Soit $y$ un fonction dérivable sur $I$,
    $A$ une primitive de $a$ et
    $y_0$ un solution particulière de $E$.

    $ y "solution de" P
        yt cases(
            exists lambda in CC\, forall t in I,
                yt y(t) = lambda e^(-A(t)) + y_0(t),
            y(t_0) = u
        ) \
        yt cases(
            exists lambda in CC\, forall t in I,
                yt y(t) = lambda e^(-A(t)) + y_0(t),
            u = lambda underbrace(e^(-A(t_0)), != 0) +
                y_0(t_0)
        )
        yt forall t in I, y(t)
            ytt = (u - y_0(t_0)) e^A(t_0) e^(-A(t))
            yttt + y_0(t) $
]

#yprop(name: [principe de superposition au premier ordre])[
    Soient $a_0, a_1, b_1, b_2$ définies sur un domaine $D$.
    $ &E_1: a_1 y' + a_0 y = b_1 \
        &E_2: a_1 y' + a_0 y = b_2 \
        &E: a_1 y' + a_0 y = b_1 + b_2 $
    Les fonctions de la forme $y_1 + y_2$,
    où $y_1$ et $y_2$ sont des solution de $E_1$ et $E_2$
    respectivement,
    sont des solutions de $E$.
]

#yproof[
    Soient $y_1$ et $y_2$ solutions de $E_1$ et $E_2$
    respectivement.

    On pose $y = y_1 + y_2$.

    Pour $t in D$,
    $ &cases(
            a_1(t) y'_1(t) + a_0(t) y_1(t) = b_1(t),
            a_1(t) y'_2(t) + a_0(t) y_2(t) = b_2(t)
        ) \
        &==> a_1(t) (y'_1 + y'_2)(t) \
            &space + a_0(t) (y_1 + y_2)(t) \
            &space = b_1(t) + b_2(t) \
        &==> a_1(t) y'(t) + a_0(t) y(t) \
            &space = b_1(t) + b_2(t) $
]