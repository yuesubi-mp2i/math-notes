#import "../../template/ytemplate.typ": *


#ypart[Définitions]


Dans ce chapitre, $KK$ désigne un corps quelconque.


#ydef(name: [Relation pour les fractions rationnelles])[
    On définit la relation $tilde$ sur
    $E = KK[X] times KK[X] without{0}$ par
    $ (A, B) tilde (P, Q) <=> A Q = B P $
]

#yprop(name: [La relation pour les fractions rationelles est une relation d'équivalence])[
    $tilde$ est une relation d'équivalence sur $E$.
]

#yproof[
    Soient $(A, B)$, $(P, Q)$, $(R, S)$ trois éléments de $E$.

    - $A B = B A$ donc $(A, B) tilde (A, B)$.

    - On suppose $(A, B) tilde (P, Q)$.

        Donc $A Q = B P$.

        Donc $P B = Q A$, donc $(P,Q) tilde (A,B)$.

    - On suppose
        $ cases((A,B) tilde (P, Q),
            (P, Q) tilde (R,S)) $
        
        D'où,
        $ cases(A Q = B P, P S = Q R) $
        donc
        $ (A Q) S = B (P S) = B Q R $
        d'où,
        $ Q (A S - B R) = 0 $
        
        $KK[X]$ est intègre, $Q != 0$ donc
        $A S - B R = 0$
        _i.e._ $A S = B R$ _i.e._ $(A, B) tilde (R,S)$.
        $qed$
]

#ydef(name: [Fraction rationnelle])[
    On appelle _fraction rationnelle_ toute classe
    d'équivalence de $KK[X] times KK[X] without {0}$.

    La classe $(P, Q)$ est notée $P/Q$.

    On note $KK(X)$ l'ensemble des fractions rationnelles.

    Par définition,
    $ P/Q = A/B &<=> (P,Q) tilde (A,B) \
        &<=> P B = Q A $
]


#yprop(name: [Simplification d'une fraction rationnelle])[
Soient $P , Q , K$ trois polynômes, $Q$ et $K$ non nuls. Alors
$ (P K)/(Q K) = P/Q $
]

#yproof[
    $ (P K)/(Q K) = P/Q <=> P K Q = P Q K $
]

#yprop(name: [Stabilité de l'addition pour la relation d'équivalence des fractions rationelles])[
Soient $(P_1 , Q_1)$ , $(P_2 , Q_2)$ , $(A_1 , A_2)$ , $(B_1 , B_2)$ des
éléments de $KK[X] times KK[X] without {0}$
tels que
$ cases((P_1 , Q_1) tilde.op (A_1 , B_1),
    (P_2 , Q_2) tilde.op (A_2 , B_2)) $
Alors
$ &(P_1 Q_2 + P_2 Q_1 , Q_1 Q_2) \
    &tilde.op (A_1 B_2 + A_2 B_1 , B_1 B_2) $

]

#yproof[
  $ &(P_1 Q_2 + P_2 Q_1) times B_1 B_2 \
  &= (P_1 B_1) (Q_2 B_2) + (P_2 B_2) (Q_1 B_1)\
    &= (A_1 Q_1)(Q_2 B_2) + (A_2 Q_2) (Q_1 B_1)\
    &= (A_1 B_2 + A_2 B_1) Q_1 Q_2. $
]

#ydef(name: [Addition de fractions rationnelles])[
    Soit $(A, B), (P, Q)$ des éléments de $KK[X] times KK[X] without{0}$.
    $ A/B + P/Q = (A Q + B P)/(B Q) $

]

#yprop(name: [Stabilité de la multiplication pour la relation d'équivalence des fractions rationelles])[
Soient $(P_1 , Q_1)$ , $(P_2 , Q_2)$ ,$( A_1 , A_2)$ , $(B_1 , B_2)$ des polynômes
tels que
$ cases((P_1 , Q_1) tilde.op (A_1 , B_1),
   (P_2 , Q_2) tilde.op (A_2 , B_2) ) $
Alors $(P_1 P_2 , Q_1 Q_2) tilde.op (A_1 A_2 , B_1 B_2)$.

]

#yproof[
  $ (P_1 P_2)(B_1 B_2) &= (P_1 B_1)(P_2 B_2) \
    &= (A_1 Q_1)(A_2 Q_2) \
    &= (A_1 A_2)(Q_1 Q_2). $
]

#ydef(name: [Multiplication de fractions rationnelles])[
    Soit $(A, B), (P, Q)$ des éléments de $KK[X] times KK[X] without {0}$.
    $ A/B times P/Q = (A P)/(B Q) $
]

#ytheo(name: [Corps des fractions rationnelles])[
    $(bb(K) (X) , + , times)$ est un corps, appelé #emph[corps des
    fractions rationnelles].

]

#yproof[
    Soient $P/Q, A/B$ et $C/D$ trois fractions rationnelles.

    -   Associativité de l'addition :
        $ &P/Q + (A/B + C/D) \
        &= P/Q + (A D + C B) / (B D)\
        &= (P B D + Q(A D + C B)) / (Q B D)\
        &= ((P B + Q A) D + C B Q) / (Q B D)\
        &= (P B + Q A) / (Q B) + C/D \
        &= (P/Q + A/B) + C/D. $

    - Commutativité de l'addition :
        $ P/Q + A/B &= (P B + A Q)/(Q B) \
        &= (A Q + P B)/(B Q) \
        &= A/B + P/Q. $

    - Neutre additif :
        $ P/Q + 0/1 &= (P times 1 + 0 times Q) / (Q times 1) \
        &= P/Q. $
    - Symétrique additif
        $ P/Q + (-P)/Q &= (P Q - P Q) / (Q^2)  \
            &= (0 times Q^2) / (1 times Q^2) \
            &= 0/1. $

    - Commutativité de la multiplication
        $ P/Q times A/B &= (P A) / (Q B) \
            &= (A P) / (B Q) \
            &= A/B times P/Q. $

    - Associativité de la multiplication
        $ &P/Q times (A/B times C/D) \ &= P/Q times (A C)/(B D) \
        &= (P A C)/(Q B D) \
        &= (P A)/(Q B) times C/D \
        &= (P/Q times A/B) times C/D. $

    - Neutre multiplicatif
        $ P/Q times 1/1 = (P times 1) / (Q times 1) = P/Q. $

    - Symétrique multiplicatif
        
        On suppose $P != 0$. Alors $ P/Q times Q/P = (P Q)/(Q P) = 1/1 $

    - Distributivité
        $ &P/Q times (A/B + C/D) \
            &= P/Q times (A D + B C)/(B D)\
            &= (P (A D + B C))/(Q B D)\
            &= (P A D + P B C)/(Q B D)\
            &= (P A)/(Q B) + (P C)/(Q D)\
            &= (P/Q times A/B) + (P/Q times C/D). $
]

#yprop(name: [Addition de fractions rationelles de même dénominateur])[
    $ A_1/B + A_2/B = (A_1 + A_2)/B $
]

#yproof[
    $ A_1/B + A_2/B &= (A_1 B + B A_2)/B^2 \
        &= (A_1 + A_2)/B $
]

#yprop(name: [Relation entre les polynômes et les fractions rationelles])[
L'application $i : P arrow.r.bar P / 1$ est un morphisme d'anneaux injectif de $bb(K) [X]$
dans $bb(K) (X)$.

On peut donc identifier un polynôme $P$ avec la fraction rationnelle
$P / 1$.

Suite à cette identification, $bb(K) [X]$ apparaît comme un
sous-anneau de $bb(K) (X)$.

]

#yproof[La fraction $i(1)=1/1$ est bien le neutre multiplicatif de $KK(X)$ et pour tous polynômes $P$ et $Q$,
$ i(P)+i(Q)&=P/1 + Q/1 \
    &= (P times 1+ Q times 1)/(1 times 1) \
    &= (P+Q)/1 \
    &= i(P+Q) $ 
$ i(P) times i(Q) &= P/1 times Q/1 \
    &= (P Q)/(1 times 1) \
    &= i(P Q) $
donc $i$ est un morphisme d'anneaux. 

Soit $P in "Ker"(i)$.

Alors $P/1 = 0/1$ et donc, par définition d'une fraction rationnelle, $P times 1 = 0 times 1$, donc $P=0$.

Ainsi $i$ est injective.
]


#yprop(name: [Fraction irréductible])[
Soit $F in bb(K) (X)$ non nulle.

Il existe un unique couple
$(P , Q)$ de $bb(K) [X] times (bb(K) [X] without {0})$
//à constante multitplicative près
tel que $F = P / Q$ avec $P and Q = 1$ et $"dom" (Q) = 1$.

On dit
que $P / Q$ est une fraction #emph[irréductible];.

]

#yproof[Soient $A$ et $B$ deux polynômes tels que $F = A/B$.

Soit $D = A and B$, $A'$ et $B'$ les polynômes tels que $A = D A'$ et $B = D B'$.

On remarque que $F = A/B = (D A')/(D B') = A'/B'$ et $A' and B' = 1$.

D'où l'existence d'un représentant irréductible.

Quitte à diviser $A'$ et $B'$ par le coefficient dominant de $B'$, on peut supposer $B'$ unitaire.

Soit $P/Q$ un autre représentant irréductible de $F$.

Ainsi $P/Q = A'/B'$ et par définition d'une fraction rationnelle, $P B' = Q A'$.

Ainsi $P$ divise $Q A'$, et comme $P$ est premier avec $Q$, $P$ divise $A'$.

On pose $A''$ le polynôme tel que $P A''=A'$.

On obtient alors $P B' = Q P A''$ et comme $KK[X]$ est intègre, $B' = Q A''$.

Ainsi $A''$ divise à la fois $A'$ et $B'$, donc $A''$ est une constante.

Comme $Q$ et $B'$ sont tous les deux unitaires, $A''=1$, et donc $P=A'$ et $Q=B'$, d'où l'unicité du représentant irréductible.
]


#ydef(name: [Degré d'une fraction rationnelle])[
    On peut donc définir le #emph[degré] de la fraction rationnelle $P / Q$ par
$ deg (P / Q) &= deg (P) - deg (Q) \
    &in ZZ union {-oo} $

]

#yprop(name: [Degré du produit de fractions rationnelles])[
Soient $F$ et $G$ deux fractions rationnelles.

Alors
$deg (F G) = deg (F) + deg (G)$.

]

#yproof[
    On pose $F = P/Q$ et $G = A/B$. 
$ &deg(F G) \ &= deg((P A)/(Q B)) \
    &= deg(P A) - deg(Q B)\ 
  &= deg(P) + deg(A) \
    &wide - deg(Q) - deg(B)\
  &= deg(P/Q) + deg(A/B). $]

#yprop(name: [Degré de deux polynômes égaux])[
Soient $P/Q$ et $A/B$ deux fractions rationelles.

Si $P / Q = A / B$, alors
$ deg P - deg Q = deg A - deg B $]

#yproof[Si $P/Q = A/B$, alors $P B = A Q$ et donc
$ deg P +deg B =deg A+deg Q $
]


#yprop(name: [Degré d'une somme de fraction rationnelles])[
Soient $F$ et $G$ deux fractions rationnelles. Alors
$ deg (F + G) lt.eq max (deg F , deg G) $

]

#yproof[On pose $F = P/Q$ et $G = A/B$, et donc $F+G = (P B + Q A)/(Q B)$.

Ainsi 
$ &deg(F + G) \
    &= deg(P B + Q A) - deg Q B \
  &<= max(deg P B , deg Q A ) \
    &wide - deg Q B  \
  &<= max(deg P B -deg Q B , \
    &wide deg Q A -deg Q B ) \
  &<= max(deg P/Q , deg A/B ) $

]


#yprop(name: [Condition de l'égalité du degré d'une somme de fraction rationnelles])[
Soient $F$ et $G$ deux fractions rationnelles.

Si $deg F != deg G$, alors
$ deg (F + G) = max (deg F , deg G) $

]

#yproof[On pose $F = P/Q$ et $G = A/B$, et donc $F+G = (P B + Q A)/(Q B)$.

On remarque que
$ &deg F != deg G \
    &=> deg P - deg Q != deg A - deg B \
    &=> deg P + deg B != deg A + deg Q \
    &=> deg P B != deg A Q $

Ainsi 
$ &deg(F + G) \
    &= deg(P B + Q A) - deg Q B \
  &= max(deg P B , deg Q A ) \
    &wide - deg Q B  \
  &= max(deg P B -deg Q B , \
    &wide deg Q A -deg Q B ) \
  &= max(deg P/Q , deg A/B ) $

]