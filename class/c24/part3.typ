#import "../../template/ytemplate.typ": *


#ypart[Décomposition en éléments simples]


#ydef(name: [Élément simple])[
    Un _éléments simple_ de $KK(X)$ est un polynôme de $KK[X]$,
    ou bien une fraction rationnelle de la forme $A/B^k$ où
    $B$ est irréductible, $k in NN^*$ et $deg A < deg B$.
]

#yprop(name: [Décomposition d'une fraction rationnelle avec la partie entière])[
Soient $N$ et $D$ deux polynômes avec $D$ non nul.

Il existe un unique
couple $(E , F) in bb(K) [X] times bb(K) (X)$ tel que
$ N / D = E + F $
avec $deg (F) < 0$.

Le polynôme $E$ est appelé #emph[partie entière de $N / D$.]

De plus $E$ est le quotient dans la division de $N$
par $D$.
]

#yproof[
  - Existence.
    
    On pose $E$ le quotient de la division de $N$ par $D$ et $R$ le reste :
    $ N = E D + R $
    avec $deg(R) < deg(D)$, et donc
    $ N/D = E + F $
    avec $F = R/D$, et donc $deg(F)<0$.
  
  - Unicité.
    
    Soit $E_1$ un polynôme et $F_1$ une fraction rationnelle de degré strictement négatif tels que
    $ N/D = E_1 + F_1 $
  
    Ainsi $E-E_1 = F_1-F$.
    
    Or $E-E_1$ est un polynôme donc nul ou de degré positif.
    
    Or, si $F_1-F$ est non nulle, alors elle est de degré négatif car c'est le cas de $F$ et de $F_1$.
    
    On en déduit que $E=E_1$ et $F=F_1$.
]

#ylemme(name: [Décomposition en éléments simple d'une fraction rationnelle de degré négative avec un produit au dénominateur])[
Soit $(A , B) in bb(K) [X]^2$ tel que $A and B = 1$.

Pour tout polynôme $C$ tel que $deg(C / (A B)) < 0$,
il existe un unique couple
$(P , Q) in bb(K) [X]^2$ tel que
$ cases(deg (P) < deg (A),
  deg (Q) < deg (B), frac(C, A B) = P / A + Q / B) $

]

#yproof[
  Idée de la preuve :
  - analyse
    - Bézout pour décomposer la fraction
    - division euclidienne pour avoir des
      numérateur de degrés faibles
  - synthèse
    - on considère un autre couple
    - utilisation de Gauss

  Preuve

  - Existence.
  
    Comme $A$ et $B$ sont premiers entre eux, il existe d'après le théorème de Bézout deux polynômes $U$ et $V$ tels que $A U + B V = C$, et donc
    $ V/A + U/B = (V B + U A)/(A B) = C/(A B) $

    On note $P$ le reste de la division de $V$ par $A$, et $Q_A$ le quotient.
    
    Ainsi, en posant $Q = U + B Q_A$, on a
    $ B P &= B(V-A Q_A) \
      &= C-A U - A B Q_A \
      &= C - A Q $
    et donc
    $ P/A + Q/B = C/(A B) $
    et $deg(P) < deg(A)$. 

    Enfin,
    $ deg(Q/B) &= deg(1/(A B) - deg(P/A)) \
      &<= deg(P/A) \
      &<0 $
    donc $deg(Q)<deg(B)$.

  - Unicité.
    
    Soit $P_1$ et $Q_1$ deux polynômes tels que
    $ C/(A B) = P_1/A + Q_1/B $ avec $deg(P_1)<deg(A)$ et $deg(Q_1)<deg(B)$.
    
    Ainsi
    $ P/A + Q/B = P_1/A + Q_1/B $
    et donc $(P-P_1)B = (Q_1-Q)A$.
    
    On en déduit que $A$ divise $B(P-P_1)$ et comme $A$ est premier avec $B$, $A$ divise $P-P_1$.
    
    Or $deg(A)>deg(P-P_1)$, donc $P-P_1=0$.
    
    Par suite $P=P_1$ et $Q=Q_1$.
]

#ycor(name: [Décomposition en éléments simple d'une fraction rationnelle de degré négative])[
Soient $A_1 , dots.h , A_n$ des polynômes premiers deux à deux premiers
entre eux, et $N in bb(K) [X]$ tel que $deg (N) < deg (A_1 dots.h A_n)$.

Alors il existe un unique $n$-uplet
$(N_1 , dots.h , N_n) in bb(K) [X]^n$ tel que
$ cases(forall i in [| 1 , n |]\, deg (N_i) < deg (A_i),
  frac(N, A_1 dots.h A_n) = sum_(i = 1)^n N_i / A_i ) $

]

#yproof[Par récurrence sur $n$.]

#yprop(name: [Décomposition de $P / (X - a)^n$])[
Soient $a in bb(K)$, $n in bb(N)$ et $P in bb(K)_(n - 1) [X]$.

Alors il
existe un unique $(a_1 , dots.h , a_n) in bb(K)^n$ tel que
$ P / (X - a)^n = sum_(i = 1)^n a_i / (X - a)^i $

]

#yproof[
  - Analyse.
    
    Soit $(a_1,...,a_n) in KK^n$ tel que $P/((X-a)^n) = sum_(i=1)^n a_i/((X-a)^i)$.
    
    En mettant sur le même dénominateur, on obtient $P = sum_(i=1)^n a_i (X-a)^(n-i)$.
    
    Comme la famille $((X-a)^(n-i))_(1 <= i <= n)$ est de degrés échelonnés, elle est libre, et donc les $a_i$ sont uniques.

  - Synthèse.
  
    D'après la formule de Taylor,
    $ P = sum_(j=0)^(n-1) (P^((j))(a))/(j!) (X-a)^j $
    et donc 
    $ &P/((X-a)^n) \
      &= sum_(j=0)^(n-1) (P^((j))(a) slash j!) / ((X-a)^(n-j)) \
      &= sum_(i=1)^n (P^((n-i))(a) slash (n-i)!) / ((X-a)^i) $
]

#ydef(name: [Zéros et pôles d'une fraction rationnelle])[
Soit $F = P / Q$ une fraction rationnelle écrite sous forme
irréductible.

Les racines de $P$ sont appelées #emph[zéros] de $F$ et
celles de $Q$ sont appelées #emph[pôles] de $F$.

]

#ydef(name: [Zéros et pôles multiples d'une fraction rationnelle])[
Soit $F = P / Q$ une fraction rationnelle écrite sous forme irréductible
et $a$ un pôle de $F$.

Si $a$ est une racine de multiplicité $m$ de $Q$,
alors on dit que $a$ est un pôle de #emph[multiplicité] $m$ de $F$.

De
même, si $b$ est une racine de multiplicité $mu$ de $P$, alors on dit
que $b$ est un zéro de #emph[multiplicité] $m$ de $F$.

]

#ytheo(name: [Décomposition en éléments simples sur $CC$])[
Soit $F$ une fraction rationnelle de $bb(C) (X)$ écrite sous
forme irréductible.

On note $z_1 , dots.h , z_n$ les pôles de $F$ et
$m_1 , dots.h , m_n$ leurs multiplicités respectives.

Alors
il existe un unique uplet de

- $E in CC[X]$, vérifiant $deg(F - E) < 0$

- pour chaque $i in [|1,n|]$,
  $ (a_(i,1), ..., a_(i, m_i)) in CC^(m_i) $ 

tel que
$ F = E + sum_(k = 1)^n sum_(i = 1)^(m_k) a_(k , i) / (X - z_k)^i $

On dit alors qu'on a #emph[décomposé $F$ en éléments simples sur
$bb(C)$.]

]

#yproof[
  On applique la partie entiere, puis le decomposition du fraction rationnelle de 
  degré négatif et enfin la decomposition des fractions rationnelles de la
  forme $P/(X-a)^m$.
]

#ytheo(name: [Décomposition en éléments simples sur $RR$])[
Soit $F = P / Q$ une fraction rationnelle de $bb(R) (X)$ écrite sous
forme irréductible. Soit
$ Q = d &product_(i = 1)^p (X - r_i)^(m_i) times \
  &product_(i = 1)^q (X^2 + a_i X + b_i)^(mu_i) $
la décomposition en facteurs irréductibles de $Q$ sur $bb(R) [X]$.

Alors il existe un unique uplet de

- $E in RR[X]$, vérifiant $deg(F - E)< 0$

- pour chaque $i in [|1,p|]$,
  $ (c_(i,1), ..., c_(i, m_i)) in RR^(m_i) $ 

- pour chaque $i in [|1,q|]$,
  $ (C_(i,1), ..., C_(i, m_i)) in (RR_1 [X])^(mu_i) $ 

tel que
$ F = E &+ sum_(k = 1)^p sum_(i = 1)^(m_k) c_(k , i) / (X - r_k)^i \
  &+ sum_(k = 1)^q sum_(i = 1)^(mu_k) C_(k , i) / (X^2 + a_k X + b_k)^i $

On dit alors qu'on a #emph[décomposé $F$ en éléments simples sur
$bb(R)$.]

]

#yproof[Il suffit de décomposer sur $CC(X)$ puis de regrouper les éléments simples non réels deux par deux pour produire des éléments simples réels.]

#yprop(name: [Décomposition en éléments simples $P'/P$])[
Soit $P in bb(C) [X]$, $z_1 , dots.h , z_n$ ses racines distinctes de
multiplicités respectives $m_1 , dots.h , m_n$.

La décomposition en
éléments simples de $frac(P prime, P)$ est
$ frac(P prime, P) = sum_(k = 1)^n frac(m_k, X - z_k) . $

N.B. Tout se passe comme si on avait dérivé le logarithme de $P$,
même si #ymega_emph[ça ne veut rien dire !]
]

#yproof[
  On note $a$ le coefficient dominant de $P$ :
  $ P = a product_(k=1)^n (X-z_k)^(m_k) $

  Lemme : pour toute famille $(A_k)_(1<=k<=n)$ de polynômes,
  $ (product_(k=1)^n A_k)' = sum_(k=1)^n A'_k product_(j != k) A_j $

  Ainsi
  $ P' =& a sum_(k=1)^n m_k (X-z_k)^(m_k - 1) \
      &times product_(j != k) (X-z_j)^(m_j) $

On en déduit que $ (P')/P = sum_(k=1)^n m_k/(X-z_k) $

On remarque que c'est bien une décomposition en éléments simples.
]


#ybtw[
Dans la formule ci-dessus, tout se passe comme si on a dérivé le
logarithme de $P$; bien sûr cette phrase n'a pas de sens (un polynôme
n'est pas une fonction, pas de logarithme complexe ni de dérivée de
fonctions complexes) mais ça aide à retenir la formule.

]

#yprop(name: [Décomposition en éléments simples d'une fraction rationnelle de $CC[X]$ à pôles simples])[
Soit $P / Q in bb(C) (X)$ une fraction irréductible à pôles simples de
degré strictement négatif.
On note $z_1 , dots.h , z_n$ les racines de
$Q$.

Alors sa décomposition en éléments simples est
$ P / Q = sum_(i = 1)^n frac(c_i, X - z_i) $ où pour tout $i in [|1, n|]$,
$c_i = frac(P (z_i), Q prime (z_i))$]

#yproof[
  Idée de la preuve :
  - Décomposer en éléments simples
  - Écrire $Q$, sa dérivée et sa valeur en $z_i$
  - Multiplier $P/Q$ par $X - z_i$ pour sortir $c_i$
    de la somme
  
  D'après le théorème de décomposition en éléments simples, il existe des coefficients $c_1,...,c_n$ tels que
$ P/Q = sum_(i=1)^n c_i/(X-z_i) $

On note $a$ le coefficient dominant de $Q$.

Soit $i in bracket.l.double 1,n bracket.r.double$.

En multipliant $P/Q$ par $X-z_i$, on obtient 
$ &P/(a product_(j != i) (X-z_j)) \
  &= c_i + sum_(j != i) (X-z_i)/(X-z_j). $ 

En évaluant en $z_i$, on obtient
$ P(z_i)/(a product_(j != i) (z_i-z_j)) = c_i $

Comme $Q = a product_(k=1)^n (X-z_k)$,
$ Q'(z_i) &= a sum_(k=1)^n product_(j != k) (z_i - z_j) \
  &= a product_(j != i) (z_i-z_j) $
et donc $c_i = P(z_i)/(Q'(z_i))$.
]

#yprop(name: [Décomposition en éléments simples d'une fraction rationnelle de $KK$])[
  On n'a pas de formule générale car les éléments simples dépendent
  du corps $KK$ !
]

#ymetho(name: [Trouver les coefficents d'une décomposition par une multiplication])[
  Si on a par exemple
  $ a/(X + 1) + b/(X + 1)^2 = (X + 3)/(X+1)^2 $
  on peut multiplier par l'un des dénominateurs :
  $ a (X + 1) + b = (X + 3) $
  puis substituer par la racine pour avoir
  $ a (-1 + 1) + b = (-1 + 3) $
  donc on trouve $ b = 2$ !
]

#ymetho(name: [Trouver des contraintes sur les coefficents d'une décomposition par un passage à la limite])[
  On se place dans $CC$.

  Si on a par exemple
  $ a/(X + 1) + b/(X + 2) = (X + 3)/((X+1)(X+2)) $
  on peut multiplier par l'un des dénominateurs :
  $ (a (X + 2))/(X+1) + b = (X + 3)/(X+1) $
  puis en considérant la fonction polynômiale associée,
  puis en passant à la limite on obtient
  $ a + b = 1 $
]