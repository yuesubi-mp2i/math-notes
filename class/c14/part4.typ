#import "../../template/ytemplate.typ": *


#ypart[Extension aux fontions à valeurs complexes]


#ydef(name: [Fonction complexe bornée et limite])[
    Soit $f: D --> CC$ une application, où $D$ est une
    partie de $RR$. Soit $a in overline(D)$.

    + On dit que *$f$ est bornée* s'il existe $M > 0$
        tel que pour tout $t in I$, $abs(f(t)) <= M$.
    
    + On dit que *$f$ admet pour limite $ell$ en $a$*
        si $abs(f - l)$ tend vers $0$ en $a$.
        Cette limite est alors unique.
] 

#yprop(name: [Résultats élémentaires sur les limites de fonctions complexes])[
    Soit $f: D --> CC$ une application, où $D$ est une
    partie de $RR$. Soit $a in overline(D)$.

    Si $f$ tend vers $ell$ en $a$ et que $g$ tend vers $ell'$,
    + $f + g$ tend vers $ell + ell'$ ,
    + $f g$ tend vers $ell ell'$
    + si $ell' != 0$, $f/g$ tend tend vers $ell/ell'$
    $qed$
]

#yprop(name: [Exprimer la limite d'une fonction complexe en fonction de celles des parties réelles et imaginaires])[
    Soit $f: D --> CC$ une application, où $D$ est une
    partie de $RR$. Soit $a in overline(D)$.

    + $f$ admet une limite en $a$ si et seulement
        $"Re"(f)$ et $"Im"(f)$ en admettent une.
        On a alors
        $ lim_a f = lim_a "Re"(f) + i lim_a "Im"(f) $
    $qed$
]

#yprop(name: [Conditions pour borner une fonction complexe])[
    Soit $f: D --> CC$ une application, où $D$ est une
    partie de $RR$. Soit $a in overline(D)$.

    + $f$ est bornée si et seulement si $"Re"(f)$ et
        $"Im"(f)$ le sont, si et seulement $abs(f)$
        l'est.
    
    + Si $f$ admet une limite en $a$, alors elle est
        bornée au voisinage de $a$.
    $qed$
]

#ydef(name: [Continuité d'une fonction complexe])[
    Soit $f: D --> CC$ une application, où $D$ est une
    partie de $RR$. Soit $a in overline(D) sect RR$.

    Si $f$ admet une limite réelle $ell$ en $a$, alors $ell = f(a)$.
    On dit que *$f$ est continue en $a$*.

    On dit que *$f: I --> CC$ est continue sur $I$*
    si elle est continue en chaque point de $I$.
]

#yprop(name: [Résultats élémentaires sur les limites de fonctions complexes])[
    Soit $f: D --> CC$ une application, où $D$ est une
    partie de $RR$. Soit $a in overline(D)$.

    + La somme et le produit de deux fontions continues
        sont continus.
        Le quotient d'une fonction continue par une une
        fonction continue ne s'annulant pas est continue.

    + $f$ est donc continue sur $I$ si et seulement si
        $"Re"(f)$ et $"Im"(f)$ le sont.
    
    + Si $f$ est continue, $abs(f)$ aussi
    $qed$
]

#ytheo(name: [Condition pour que le module d'une fonction complexe admet un minimum et un maximum])[
    Si $f$ est continue sur un segment $I$,
    $abs(f)$ admet un minimum et un maximum.
    $qed$
]