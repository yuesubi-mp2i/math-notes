#import "../../template/ytemplate.typ": *


#ypart[Continuité uniforme]


#ydef(name: [Continuité uniforme])[
    Soit $f$ une fonction définie sur une partie $D$
    de $RR$ à valeurs réelles.

    On dit que $f$ est *uniformément continue* sur $D$
    si
    $ forall epsilon > 0, exists eta > 0, forall (x, y) in D^2,
        yt abs(x - y) <= eta => abs(f(x) - f(y)) <= epsilon $
]

#ydef(name: [Fonction lipschitzienne])[
    On dit qu'une fontion est *$k$-lipschitzienne sur $D$* s'il
    existe $k in RR^+$ tel que
    $ forall (x, y) in D^2,
        yt abs(f(x) - f(y)) <= k abs(x - y) $
]

#yprop(name: [Uniforme continuité d'une fonction lipschitzienne])[
    Toute fonction lipschitzienne sur $D$ est uniformément
    continue sur $D$.
]

#yproof[
    Soit $k in RR$ tel que pour tout couple $(x, y) in D^2$,
    $ abs(f(x) - f(y)) <= k abs(x - y) $

    On remarque que si $k$ est nul, alors $f$ est constante,
    et donc uniformément continue.

    On suppose à présent $k > 0$.

    Soit $epsilon > 0$. On pose $eta = k/epsilon$.
    Soit $(x, y) in D^2$. On suppose $abs(x - y) <= eta$.

    Alors
    $ abs(f(x) - f(y)) <= k abs(x - y) = k eta = epsilon $

    Donc $f$ est uniformément continue.
]

#ytheo(name: [Heine])[
    Toute fonction continue sur un segment est
    uniformément continue sur ce segment.
]

#yproof[
    $qed$
]