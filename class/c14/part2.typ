#import "../../template/ytemplate.typ": *


#ypart[Fonctions continues sur un intervalle]


=== 1. Premières propriétés

#ydef(name: [Continuité d'une fonction en un point])[
    Soit $f$ une fontion définie sur un ensemble D.
    Soit $a in DD$.

    On dit que $f$ est *continue* en $a$ si $f$ a
    une limite en $a$.
]

#ydef(name: [Continuité d'une fontion sur un ensemble])[
    On dit que $f: D --> RR$ est continue sur $D$ si $f$
    est continue en chaque point de $D$, i.e.
    $lim_a f = f(a)$ pour tout point $a in D$.
]

#yprop(name: [Continuité de la somme, quotient, composée, maximum et minimum])[
    + La somme de deux fonctions $f$ et $g$ continue sur $D$
        sont continue sur $D$.
    + Le quotient d'une fonction $f$ continue sur $D$ par une
        fonction $g$ continue sur $D$ ne s'annulant pas sur
        $D$ est continue sur $D$.
    + Si $f$ est continue sur $D$ et que $g$ est continue sur $E$
        avec $f(D) subset E$, alors $g compose f$ est continue
        sur $D$.
    + Si $f$ et $g$ sont continues sur $D$, $max(f, g)$ et
        $min(f, g)$ sont continues sur $D$.
]

#yproof[
    $qed$
]

#yprop(name: [Continuité d'une fonction sur un sous ensemble])[
    Si $f$ est continue sur $D$, alors toute partie
    $E subset D$, $f_(|E) $ est continue sur $E$.
]


=== 2. Image d'un segment par une application continue

#yprop(name: [Propriété d'une fonction continue sur un segment dont l'image des extrémités ont des signe opposé])[
    Soit $f: [a, b] --> RR$ continue telle que $f(a) f(b) < 0$.

    Alors il existe $c in ]a, b[$ tel que $f(c)$ = 0
]

#yproof[
    $qed$
]

#ytheo(name: [Théorème des valeurs intermédiaires pour les fonctions])[
    Soit $f: I --> RR$ continue, si on a $a < b in I$,
    $alpha = f(a)$ et $beta = f(b)$, alors pour tout
    $gamma in ]alpha, beta[ union ]beta, alpha[$, il
    existe $c in ]a, b[$ tel que $f(c) = gamma$.
]

#yproof[
    Avec la fonction $x |-> f(x) - gamma$.
    $qed$
]

#yprop(name: [Image d'un intervalle par une fonction continue])[
    (Corollaire du TVI)

    L'image d'un intervalle par une fonction continue est
    un intervalle.
]

#yproof[
    Le théorème des valeurs intermédiaires prouve que
    l'image d'un intervalle par une fonction continue
    est convexe. Or les seules parties convexes de $RR$
    sont les intervalles.
    $qed$
]

#ytheo(name: [Image d'un segment par une fontion continue])[
    L'image par une fonction continue d'un segment est un segment.
]

#yproof[
    $qed$
]

#yprop(name: [Propriété d'une fontions continue sur un intervalle injective])[
    Soit $f$ une fontion continue *sur un intervalle $I$*,
    à valeurs réelles et injective.

    Alors $f$ est strictement monotone sur $I$
]

#yproof[
    $qed$
]


=== 3. Continuité de la fonction réciproque


#ylemme(name: [Relation entre la continuité et l'image directe est un intervalle, quand une application est monotone])[
    Soient $I$ un intervalle et $f: I --> RR$ une
    application monotone.

    Alors $f$ est continue si et seulement si $f(I)$
    est un intervalle.
]

#yproof[
    $qed$
]

#yprop(name: [Particularité d'une bijection monotone entre deux intervalles])[
    (Corollaire de quand monotone, continue ssi l'image directe est un intervalle)

    Soient $I$ et $J$ deux intervalles et $f: I --> J$
    une application bijective monotone.

    Alors $f^(-1)$ est continue.
]

#yproof[
    L'application $f^(-1)$ est bien définie puisque $f$
    est bijective, monotone (de même monotonie que $f$)
    et l'image de l'intervalle $J$ est un intervalle
    (c'est $I$), donc $f^(-1)$ est continue.
    $qed$
]

#ytheo(name: [Bijection continue])[
    Soient $I$ un intervalle et $f: I --> RR$ une
    application continue strictement monotone.

    Alors $f$ établit une bijection de $I$ sur
    l'intervalle $J = f(I)$ dont la réciproque est
    continue sur $J$.
    
    N.B. $f$ est donc un homéomorphisme.
]

#yproof[
    D'après le théorème de valeurs intermédiaires, $f(I)$
    est bien un intervalle, on le note $J$.
    L'application $I -> J, x |-> f(x)$ est surjective.
    Comme elle est strictement monotone, cette application
    est injective.
    Il s'agit donc d'une bjection monotone entre deux
    intervalles, sa réciproque est donc continue.
]