#import "../../template/ytemplate.typ": *

#ydate(2024, 1, 11)

#ypart[Limites]


=== 1. Définitions


#ydef(name: [Voisinage normal, à droite, et à gauche d'un nombre])[
    Soit $a in RR$.
    - On appelle *voisinage de $a$* toute partie de $RR$
        qui contient un intervalle de la forme
        $]a - h, a + h[$, avec $h > 0$.
    - On appelle *voisinage à gauche de $a$* toute partie
        de $RR$ qui contient un intervalle du type
        $]a - h, a]$, $h > 0$.
    - On appelle *voisinage à droite de $a$* toute partie
        de $RR$ qui contient un intervalle du type
        $[a, a + h[$, $h > 0$.
]

#ydef(name: [Prédicat vrai au voisinage d'un nombre])[
    Soit $P$ un prédicat défini sur une partie de $RR$.

    On dit que $P$ est vrai au voisinage de $a$ s'il
    existe un voisinage $V$ de $a$ tel que pour tout
    $x in V$, $P(x)$ est vraie.
]

#ydef(name: [Voisinage des infinis])[
    - On appelle *voisinage de $+oo$* toute partie de $RR$
        contenant un intervalle de la forme $[A, +oo[$
        où $A in RR$.
    - On appelle *voisinage de $-oo$* toute partie de $RR$
        contenant un intervalle de la forme $]-oo, A]$
        où $A in RR$.
]

#yprop(name: [Intersection de voisinages])[
    Soit $a in overline(RR)$.
    
    Une intersection finie de voisinages de $a$
    est un voisinage de $a$.
]

#yproof[
    Soient $(V_i)_(1 <= i <= n)$ une famille finie
    de voisinages de $a$.

    - On suppose $a in RR$.
    
        Chaque partie $V_i$ contient
        donc un intervalle ouvert centré en $a$, soit
        $]a - h_i, a + h_i[$ un tel intervalle
        
        On pose alors $h = min_(1 <= i <= n) (h_i) > 0$.
        
        On vérifie sans difficulté que
        $ ]a - h, a + h[ subset sect.big_(i=1)^n V_i $
        et donc $sect.big_(i=1)^n V_i$ est un
        voisinage de $a$.

    - On suppose à présent $a = +oo$.

        Chaque $V_i$ contient cette fois un intervalle de
        la forme $[A_i, +oo[$.
        
        Il suffit alors de poser
        $ A = max_(1 <= i <= n) (A_i) $
        ainsi
        $ [A, +oo[ subset sect.big_(i=1)^n V_i $
        et donc $sect.big_(i=1)^n V_i$ est bien un voisinage
        de $+oo$.

    - On procède de manière similaire dans le cas où $a = -oo$.
    $qed$
]

#ydef(name: [Point adhérent])[
    Soit $D$ une partie réelle.

    On dit qu'un élément $a in overline(RR)$ est
    *adhérent à $D$* si tout voisinage $V$ de $a$
    rencontre $D$ (i.e. $V sect D != emptyset$).
]

#ynot(name: [Ensemble des points adhérents])[
    Soit $D$ une partie de $RR$.

    On note $overline(D)$ l'ensemble des points
    adhérents à $D$.
]

#let fdal() = {[
    Soit $f$ définie sur $D subset RR$, $a in overline(D)$,
    et $ell in overline(RR)$.
]}

#ydef(name: [Limite d'une fonction])[
    #fdal()

    On dit que $f$ tend vers $ell$ en $a$ si pour tout
    voisinage $V$ de $ell$, il existe un voisinage $W$
    de $a$ tel que pour tout $x in W sect D$,
    $f(x) in V$.
]

#ydate(2024, 1, 12)

#ydef(name: [Limite d'une fonction (version horrible)])[
    #fdal()

    Une définition équivalente de $f$ tend vers $ell$
    en $a$ est :
    $ forall frak(A), exists frak(B), forall x in I, frak(C) => frak(D) $

    où on remplace $frak(A)$, $frak(B)$, $frak(C)$ et $frak(D)$
    avec ce joli tableau :
    #table(
        columns: 3,
        inset: 0.5em,
        align: horizon,

        [], [Condition], [Remplacement],

        $frak(A)$, [
            - $ell = plus.minus oo$
            - $ell in RR$
        ], [
            - $forall A in RR$
            - $forall epsilon > 0$
        ],

        $frak(B)$, [
            - $a = plus.minus oo$
            - $a in RR$
        ], [
            - $exists M in RR$
            - $exists eta > 0$
        ],

        $frak(C)$, [
            - $a = +oo$
            - $a = -oo$
            - $a in RR$
            - à gauche
            - à droite 
        ], [
            - $x >= M$
            - $x <= M$
            - $abs(x - a) <= eta$
            - $a - eta <= x <= a$
            - $a <= x <= a + eta$
        ],

        $frak(D)$, [
            - $ell = +oo$
            - $ell = -oo$
            - $ell in RR$
        ], [
            - $f(x) >= A$
            - $f(x) <= A$
            - $abs(f(x) - ell) <= epsilon$
        ]
    )
]

#ydef(name: [Limite d'une fonction à gauche/droite])[
    #fdal()

    - On dit que *$f$ tend vers $ell$ en $a$ à gauche*
        (resp. droite) (ou *par valeurs inférieures*
        (resp. supérieures)) si pour tout voisinage
        $V$ de $ell$, il existe un voisinage à gauche
        (resp. droite) $W$ de $a$ tel que pour tout
        $x in W sect D$, $f(x) in V$.

    - On dit que *$f$ tend vers $ell$ en $a$ à gauche
        strictement* (ou *par valeurs strictement
        inférieures*) si pour tout voisinage $V$ de
        $ell$, il existe un voisinage à gauche $W$ de
        $a$ tel que pour tout
        $x in W sect D without {a}$, $f(x) in V$.
]

#yprop(name: [Unicité de la limite d'une fonction])[
    #fdal()

    Si $f$ admet une limite $ell$ en $a$, alors $ell$
    est unique.

    On la note $lim_(x->a) f(x)$.
]

#yproof[
    Soient $ell_1$ et $ell_2$ deux limites distinctes de $f$.

    Nous allons construire $V_1$ un voisinage de $ell_1$
    et $V_2$ un voisinage de $ell_2$ tels que $V_1 sect V_2 = emptyset$.

    - On suppose $ell_1$ et $ell_2$ réels.

        Sans perte de généralité, on suppose $ell_1 < ell_2$.

        On pose $epsilon = (ell_2 - ell_1)/42$.

        Ainsi $V_1 = ]ell_1 - epsilon, ell_1 + epsilon[$ et
        $V_2 = ]ell_2 - epsilon, ell_2 + epsilon[$ conviennent
        puisque $ell_1 + epsilon < (ell_2 + ell_1)/2 < ell_2 - epsilon$.
    
    - On suppose l'une des deux limites réelles et l'autre infinie,
        par exemple $l_1 in RR$ et $l_2 = +oo$.

        Alors $V_1 = ]ell_1 - 1, ell_1 + 1[$ et $V_2 = ]ell_1 + 42, +oo[$
        conviennent.

        De même avec $ell_1 in RR$ et $ell_2 = -oo$.
    
    - Enfin si $ell = -oo$ et $ell = +oo$,
        $V_1 = ]-oo, 0[$ et $V_2 = ]42, +oo[$.
    
    Comme $f$ a pour limite $ell_1$, il existe un voisinage $W_1$ de $a$
    tel que $f(W_1 sect D) subset V_1$, et de même, on dispose d'un
    voisinage $W_2$ de $a$ tel que $f(W_2 sect D) subset V_2$.

    Par suite, pour tout $x in W_1 sect W_2 sect D$,
    $f(x) in V_1 sect V_2 = emptyset$.

    Or, $W_1 sect W_2$ est un voisinage de $a$.

    Comme $a in overline(D)$, $W_1 sect W_2 sect D != emptyset$ :
    une contradiction.

    Donc $ell_1 = ell_2$.
    $qed$
]

#yprop(name: [Unicité de la limite à gauche/droite d'une fonction])[
    #fdal()

    Si $f$ admet une limite à gauche/droite $ell$ en $a$, alors $ell$
    est unique.

    On la note $lim_(x->a^-) f(x)$ ou $lim_(x->a \ x <= a) f(x)$.
    $qed$
]

#yprop(name: [Existance de limite ssi celles à gauche et droite existent])[
    #fdal()

    $f$ admet une limite $ell$ en $a in RR$ si et
    seulement si elle admet $ell$ pour limite à
    gauche et à droite en $a$.
]

#yproof[
    - On suppose que $ell = lim_a f$.

        Soit $V$ un voisinage de $ell$.

        On considère un voisinage $W$ de $a$ tel que $f(W sect D) subset V$.

        On remarque que $W$ est aussi un voisinage à gauche de $a$,
        et un voisinage à droite de $a$.

        Donc $ lim_(x->a \ x>=a) f(x) = ell = lim_(x->a \ x<=a) f(x) $
    
    - Réciproquement, on suppose que les limites de $f$ en $a$ à
        gauche et à droite sont toutes les deux égales à $ell$.

        Soit $V$ un voisinage de $ell$.

        On considère $W_1$ un voisinage à gauche de $a$ tel que
        pour tout $x in D sect W_1$, $f(x) in V$.
        On considère également un voisinage à droite $W_2$ tel que
        pour tout $x in D sect W_2$, $f(x) in V$.

        On remarque que $W_1 union W_2$ est un voisinage de $a$,
        et pour tout $x in (W_1 union W_2) sect D$, $f(x) in V$.

        Donc $ell$ est la limite de $f$ en $a$.

    $qed$
]

#yprop(name: [Valeur d'une fonction en un point de limite connue])[
    #fdal()
    
    Si $a in D$ et que $f$ admet une limite $ell$ en $a$.

    Alors $ell = f(a)$.
]

#yproof[
    Soit $V$ un voisinage quelconque de $ell$.

    On considère $W$ un voisinage de $a$ tel que pour tout
    $x in D sect W$, $f(x) in V$.

    Comme $a in overline(D)$, et bien sûr $a in W$, $f(a) in V$.

    Ainsi $f(a)$ appartient à tous les voisinage de $ell$.

    Si $ell$ n'est pas réel, l'intersection de tous les voisinages
    de $ell$ est vide.
    Par conséquent $ell in RR$.

    L'intersection de tous les voisinages d'un réel ne contenant
    que ce réel, donc $ell = f(a)$.
    $qed$
]

#yprop(name: [Prolongement par continuité d'une fonction])[
    #fdal()

    Si $f$ admet une limite $ell$ en $a in RR$ et n'est
    pas définie en $a$, on peut la prolonger en une
    fonction $g$ définie sur $D union {a}$ en posant
    $g(a) = ell$.

    Dans ce cas, $lim_a g = ell$.
]

#yproof[
    Soit $V$ un voisinage de $ell$.

    On considère un voisinage $W$ de $a$ tel que $f(W sect D) subset V$.

    L'application $g$ est définie sur $D union {a}$, donc $a$ est bien
    adhérent au domaine de définition de $g$.

    De plus, pour tout $x in W sect (D union {a})$,
    $ g(x) = cases(f(x) &"si" x!=a, ell &"si" x=a) in V $

    Donc $lim_a g = ell$.
    $qed$
]

#ydef(name: [Limite par valeurs différentes d'une fonction])[
    #fdal()

    On dit que *$f$ tend vers $ell$ quand $x$ tend vers
    $a$ par valeurs différentes de $a$* si pour tout
    voisinage $V$ de $ell$, il existe un voisinage $W$
    de $a$ tel que pour tout $x in (D union W) without {a}$,
    $f(x) in V$.
]

#yprop(name: [Unicité de la limite par valeurs différentes d'une fonction])[
    La limite de $f$ en $a$, si elle existe, par valeurs
    différentes de $a$, est unique.

    On la note $lim_(x->a \ x!=a) f$.
    $qed$
]

#yprop(name: [Limite selon celle par valeurs différentes et l'image de la fonction])[
    #fdal()

    On suppose $a in D$.
    
    Alors $ell$ est la limite de $f$ en $a$ si et seulement
    si $ell = lim_(x->a \ x!=a) f(x)$ et $ell = f(a)$.
]

#yproof[
    - On suppose que $ell = lim_a f$.

        On a déjà vu que $ell = f(a)$.

        Soit $V$ un voisinage de $ell$.

        On considère un voisinage $W$ de $a$ tel que $f(D sect W) subset V$.

        Donc $f(D sect W without {a}) subset V$.

        Ainsi $lim_(x->a \ x!=a) f(x) = ell$.
    
    - Réciproquement, on suppose que $ell = f(a) = lim_(x->a \ x!=a) f(x)$.

        Soit $V$ un voisinage de $ell$.

        On considère $W$ un voisinage de $a$ tel que
        $f(W sect D without {a}) subset V$.
        
        Comme $f(a) = ell in V$, $f(D sect V) subset V$.

        Donc $ell = lim_(x->a) f(x)$.
]

#yprop(name: [Limite selon celles par valeurs différentes inférieures/supérieures et l'image de la fonction])[
    #fdal()

    On suppose $a in D$.

    Alors $ell = lim_a f$ si et seulement si
    $ lim_(x->a \ x<a) f(x) = lim_(x->a \ x>a) f(x) = f(a) = ell $
    $qed$
]


=== 2. Propriétés élémentaires


#yprop(name: [Borner une fonction au voisinage d'un point])[
    #fdal()

    Si $f$ admet une limite finie $ell in RR$ en $a$,
    alors $f$ est bornée au voisinage de $a$.
]

#yproof[
    L'intervalle $V = ]ell - 1, ell + 1[$ est un voisinage de $ell$.

    On considère $W$ un voisinage de $a$ tel que
    $f(D sect W) subset V$.

    La fonction $f$ est donc minorée par $l - 1$ et majorée par $ell + 1$
    sur ce voisinage $W$ de $a$.
    $qed$
]

#yprop(name: [Signe d'une fonction au voisinage d'un point])[
    + Une fonction $f$ qui admet une limite finie $ell > 0$
        en $a$ est strictement positive au voisinage de $a$.

    + Une fonction $f$ qui tend vers $+oo$ en $a$ est
        strictement positive au voisinage de $a$.
    
    Idem pour les limites strictement négatives.
]

#yproof[
    + L'intervalle $V = ]ell - ell/2, ell + ell/2[$ est un voisinage de $ell$.

        Soit $W$ un voisinage de $a$ tel que $f(D sect W) subset V$.

        Pour tout $x in D sect W$,
        $ f(x) > ell - ell/2 = ell/2 > 0 $
    
    + L'intervalle $[1, +oo[$ est un voisinage de $+oo$.

        Soit $W$ un voisinage de $a$ tel que $f(D sect W) subset [1, +oo[$.

        Pour tout $x in D sect W$,
        $ f(x) >= 1 > 0 $
    $qed$
]

#yprop(name: [Limite de deux fonction qui coïncident])[
    Soient $f$ et $g$ deux fonctions qui coïncident
    sur un voisinage de $a in overline(RR)$.

    On suppose que $f$ admet une limite $ell in overline(RR)$
    en $a$.

    Alors $g$ admet pour limite $ell$ en $a$.

    N.B. donc on se fiche de ce qui se passe loin de $a$.
]

#yproof[
    On note $D$ l'intersection des domaines de défintion de $f$ et de $g$.

    Soit $W$ un voisinage de $a$ tel que sur $D sect W$, $f = g$.

    Soit $V$ un voisinage de $ell$.

    On considère $W'$ un voisinage de $a$ tel que $f(D sect W') subset V$.

    On pose $W'' = W sect W'$ : c'est un voisinage de $a$ et
    pour tout $x in D sect W''$, $g(x) = f(x) in V$

    Donc $lim_a g = ell$.
    $qed$
]

#ytheo(name: [Caractérisation séquencielle de la limite d'une fonction])[
    #fdal()

    $f$ admet pour limite $ell$ en $a$ si et seulement si
    pour toute suite $(x_n)_(n in NN) in D^NN$ tendant
    vers $a$, $(f(x_n))_(n in NN)$ tend vers $ell$.
]

#yproof[
    $qed$
]


=== 3. Opérations sur les limites


#yprop(name: [Limite de la somme, du produit et du quotient de deux fontions])[
    Soient $f$ et $g$ deux fonctions tendant respectivement
    vers $ell in RR$ et $ell' in RR$ en $a$.

    Alors $f + g$ tend vers $ell + ell'$, et $f g$ tend
    vers $ell ell'$.

    Si de plus $ell' != 0$, alors $f/g$ tend vers $ell / ell'$.
]

#yproof[
    $qed$
]

#yprop(name: [Limite de somme/produit de fonctions dont l'une tend vers un infini])[
    Si $f$ tend vers $+oo$ en $a$ et $g$ est bornée au
    voisinage de $a$, alors $f + g$ tend vers $+oo$ en $a$.

    Si $f$ tend vers $+oo$ en $a$ et $g$ est minorée par
    un réel strictement positif au voisinage de $a$, alors
    $f g$ tend vers $+oo$ en $a$.

    Il existe des énoncés équivalents en $-oo$.
]

#yproof[
    Avec la caractérisation séquencielle.
    $qed$
]

#yprop(name: [Limite de l'inverse d'une fonction qui tend vers $0$])[
    Si une fontion $f$ tend vers $0$ en $a in overline(RR)$,
    - si $f$ reste strictement positive sur un voisinage
        de $a$ privé de $a$, alors $1/f$ tend vers $+oo$
        en $a$ ;
    - si $f$ reste strictement négative sur un voisinage de
        $a$ privé de $a$, alors $1/f$ tend vers $-oo$ en
        $a$ ;
    - si $f$ ne s'annule pas sur un voisinage de $a$ privé de $a$,
        alors $1/abs(f)$ tend vers $+oo$ en $a$.
]

#yprop(name: [Composition de limites de fonctions])[
    Soit $f: D --> RR$ et $g: E --> RR$ tel que $f(D) subset E$.
    Soient $a in overline(D)$ et $b in overline(E)$.

    Si $f$ tend vers $b$ en $a$ et $g$ tend vers $ell$ en $b$,
    alors $g compose f$ tend vers $ell$ en $a$.
]

#yproof[
    Avec la caractérisation séquencielle.
    $qed$
]


=== 4. Limites et inégalités


#ytheo(name: [Limite monotone pour les fonctions])[
    + Soit $f: ]a, b[ --> RR$ croissante
        - Si $f$ est majorée, alors $f$ admet $sup(f)$ pour
            limite à gauche en $b$. Sinon $f$ tend vers $+oo$
            à gauche en $b$.
        - Si $f$ est minorée, alors $f$ admet $inf(f)$ pour
            limite à droite en $a$. Sinon $f$ tend vers $-oo$
            à droite en $a$.

    + Soit $f: ]a, b[ --> RR$ décroissante
        - Si $f$ est majorée, alors $f$ admet $sup(f)$ pour
            limite à droite en $a$. Sinon $f$ tend vers $+oo$
            à droite en $a$.
        - Si $f$ est minorée, alors $f$ admet $inf(f)$ pour
            limite à gauche en $b$. Sinon $f$ tend vers $-oo$
            à gauche en $b$.
    
    + Une application monotone définie sur un intervalle admet
        des limites par valeurs inférieures strictes et par
        valeurs supérieures strictes en tout point $c$ de cet
        intervalle qui n'est pas une extrêmité, et on a, par
        exemple si $f$ est croissante,
        $ lim_(c^-) f <= f(c) <= lim_(c^+) f $
]

#yproof[
    $qed$
]

#yprop(name: [Limites de croissances comparées de fonctions])[
    Supposons que $f <= g$ au voisinage de $a$.

    - Si $f$ tend vers $+oo$ en $a$, $g$ aussi.
    - Si $g$ tend vers $-oo$ en $a$, $f$ aussi.
]

#yproof[
    Avec la caractérisation séquencielle.
    $qed$
]

#ytheo(name: [Théorème d'existance de la limite d'une fonction par encadrement (dit des gendarmes)])[
    Supposons que $f <= g <= h$ au voisinage de $a$.

    Alors si $g$ et $h$ tendent vers $ell in RR$ en $a$,
    $g$ tend aussi vers $ell$ en $a$.

    (C'est aussi vrai pour $ell = plus.minus oo$, mais
    on l'a déjà énoncé avec les croissances comparées)
]

#yproof[
    Avec la caractérisation séquencielle.
    $qed$
]