// Limites de fonctions et continuité
#import "../../template/ytemplate.typ": *

#ychap[ Limites de fonctions et continuité ]

#include "part1.typ"
#include "part2.typ"
#include "part3.typ"
#include "part4.typ"