#import "../../template/ytemplate.typ": *


#ydate(2023, 12, 5)

#ypart[Inégalités de convexités]


=== 6.1 Barycentres

Dans cette section, on se place dans le plan euclidien $cal(P)$
muni d'un repère orthonormé $cal(R) = (0, arrow(i), arrow(j))$.

#ydef(name: [Barycentre H.-P.])[
    Soit $(A_1, ... A_n) in cal(P)^n$, $(lambda_1, ..., lambda_n) in RR^n$
    tel que que $sum_(i = 1)^n lambda_i != 0$.

    Le *barycentre* du système pondéré $((A_1, lambda_1), ... (A_n, lambda_n))$
    est l'unique point $G$ vérifiant
    $ sum_(i=1)^n lambda_i arrow(A_i G) = arrow(0) $
]

#ybtw[
    $G$ existe bien et il est unique d'après le raisonnement suivant :

    Soit $G(x,y)$, on note $(x_i, y_i)$ les coordonnées de $A_i$
    pour $i in [|1, n|]$.

    $ sum_(i=1)^n lambda_i arrow(A_i G) = arrow(0)
        yt <=> cases(sum_(i=1)^n lambda_i (x - x_i) = 0,
                sum_(i=1)^n lambda_i (y - y_i) = 0)
        yt <=> cases(sum_(i=1)^n lambda_i x = sum_(i=1)^n lambda_i x_i,
                sum_(i=1)^n lambda_i y = sum_(i=1)^n lambda_i y_i)
        yt <=> cases(x = 1/(sum_(i=1)^n lambda_i) sum_(i=1)^n lambda_i x_i,
                y = 1/(sum_(i=1)^n lambda_i) sum_(i=1)^n lambda_i y_i) $
]

#ybtw[
    Soit $(lambda_1, ..., lambda_n) in RR^n$
    tel que $sum_(i=1)^n lambda_i != 0$, $(A_1, ..., A_n) in cal(P)^n$.

    Soit $mu in RR^*$.

    Le barycentre de $((A_1, lambda_1), ... (A_n, lambda_n))$ est le même que
    celui dans $((A_1, mu lambda_1), ... (A_n, mu lambda_n))$

    En effet, pour tout $G in cal(P)$,
    $ sum_(i=1)^n lambda_i arrow(A_i G) = arrow(0)
        <=> sum_(i=1)^n (mu lambda_i) arrow(A_i G) = arrow(0) $
    
    En posant $mu = 1/(sum_(i=1)^n lambda_i)$, on se ramène à la situation
    où la somme des poids vaut 1.
]

#yexample[
    Soit $(A,B) in cal(P)^2$, $lambda in RR$ et $G$ le barycentre de
    $((A, lambda), (B, 1-lambda))$.
    $ lambda arrow(A G) + (1-lambda) arrow(B G) = arrow(0) $

    Donc
    $ lambda arrow(A G) + (1-lambda) (arrow(B A) + arrow(A G)) = arrow(0) $
    donc
    $ arrow(A G) = (1-lambda) arrow(A B) $
    donc $G in (A B)$

    // drawing
    - Soit $lambda = 1$, alors $G = A$
    - Soit $lambda = 0$, alors $G = B$

    Si $lambda in [0, 1]$ alors $1 - lambda in [0, 1]$ et alors $G in [A B]$

    Réciproquement, on considère $G in [A B]$.

    On pose $lambda = norm(arrow(M B))/norm(arrow(A B))$.

    On vérifie que
    $ lambda arrow(A M) + (1 - lambda) arrow(B M) = arrow(0) $

    $[A B]$ est l'ensemble des barycentres de
    $((A, lambda), (B, 1 - lambda))$ avec $lambda in [0, 1]$.
]

#yprop(name: [Simplification de la fonction de Leibniz quand les points ont un barycentre H.-P.])[
    Soit $G$ le barycentre de
    $ ((A_1, lambda_1), ..., (A_n, lambda_n)) in (cal(P) times RR)^n $

    Alors, pout tout $M in cal(P)$,
    $ sum_(i=1)^n lambda_i arrow(A_i M) = (sum_(i=1)^n lambda_i) arrow(G M) $

    ($M |-> sum_(i=1)^n lambda_i arrow(A_i M)$ est appelée fonction
    vectorielle de leibniz)
]

#yproof[
    Soit $M in cal(P)$.

    $ sum_(i=1)^n lambda_i arrow(A_i M)
        yt = sum_(i=1)^n lambda_i (arrow(A_i G) + arrow(G M))
        yt = underbrace(sum_(i=1)^n lambda_i arrow(A_i G), = arrow(0))
            + sum_(i=1)^n lambda_i arrow(G M) $
    $square$
]

#yprop(name: [Associativité des barycentres H.-P.])[
    Soit $(A_i) in cal(P)^[|1, n|]$, $(lambda_i) in RR^[|1, n|]$
    tel que $sum_(i=0)^n lambda_i != 0$, $n >= 2$.

    Soit $p < n$ tel que $sum_(i=1)^p lambda_i != 0$
    et $sum_(i = p+1)^n lambda_i != 0$.

    On note
    - $G$ le barycentre de $ ((A_1, lambda_1), ..., (A_n, lambda_n)) $
    - $G_1$ le barycentre de $ ((A_1, lambda_1), ..., (A_p, lambda_p)) $
    - $G_2$ le barycentre de $ ((A_(p+1), lambda_(p+1)), ..., (A_n, lambda_n)) $

    alors $G$ est le barycentre de
    $ ((G_1, sum_(i=1)^p lambda_i), (G_2, sum_(i=p+1)^n lambda_i)) $
]

#yproof[
    Soit $M in cal(P)$.

    $ M = G
        yt <=> sum_(i=1)^n lambda_i arrow(A_i M) = arrow(0)
        yt <=> sum_(i=1)^p lambda_i arrow(A_i M)
            + sum_(i=p+1)^n lambda_i arrow(A_i M)
            ytt = arrow(0)
        yt <=> sum_(i=1)^p (lambda_i) arrow(G_1 M)
            ytt + sum_(i=p+1)^n (lambda_i) arrow(G_2 M) = arrow(0) $
    $square$
]

#yexample[
    $(A, lambda), (B, mu), (C, 1 - lambda - mu)$ où $(lambda, mu) in RR^2$

    // dessin d'un triangle A B C
    Tout point du plan est un barycentre de $(A,B,C)$
]


=== 6.2 Fonction convexes

#ydef(name: [Convexité d'une fonction])[
    Soit $f: I --> RR$ définie sur l'*intervalle* $I$.

    // graphe

    On dit que $f$ est *convexe* si
    $ forall (a, b) in I^2, forall lambda in [0, 1],
        yt f(lambda a + (1 - lambda) b)
        ytt <= lambda f(a) + (1 - lambda) f(b) $
]

#ybtw[
    $f$ est convexe si sa courbe est en desous de ses cordes.
]

#ylemme(name: [Lemme des pentes])[
    (Aussi appelé lemme des cordes, inégalité des cordes, inégalités des pentes,
    lemme des 3 pentes, etc...)

    Soit $f: I --> RR$, I un *intervalle*.
    Soit $(a,b,c) in I^3$ avec $a < b < c$.

    Si $f$ est convexe, alors

    $(f(b) - f(a))/(b-a) <= (f(c) - f(a))/(c-a)
       <= (f(c) - f(b))/(c-b)$
    // dessin

    Si l'une des 3 inégalités est vraie pour tous $a < b < c$,
    alors $f$ est convexe.
]

#yproof[
    - On suppose $f$ convexe.

        On pose $lambda = (b - a)/(c - a) in [0, 1]$.

        $ lambda a + (1 - lambda) c
            yt = a (c - b)/(c - a) + (b - a)/(c - a) c
            yt = b (c - a)/(c - a) = b $

        $f$ est convexe donc
        $ overbrace(f(lambda a + (1 - lambda) a), = f(b))
            yt <= lambda f(a) + (1 - lambda) f(c) $
        
        d'où,
        $ (f(b) - f(a))/(b - a)
            yt <= underbrace((1 - lambda) (f(c) - f(a))/(b - a),
                = (b-a)/(c-a) (f(c) - f(a))/(b - a))
            yt <= (f(c) - f(a))/(c - a) $
        
        On a aussi
        $ (f(c) - f(b))/(c - b)
            yt >= (f(c) - lambda f(a) - (1 - lambda) f(c))/(c - b)
            yt >= lambda (f(c) - f(a))/(c - b) = (f(c) - f(a))/(c - a) $
    
    Réciproque :

    -  Cas 1 : On suppose, pour tout $a < b < c$
        $ (f(b) - f(a))/(b - a) <= (f(c) - f(a))/(c - a) $

        Soit $(alpha, beta) in I^2$ avec $alpha < beta$, $lambda in [0, 1]$.

        On suppose $lambda in ]0, 1[$.

        On pose
        $ cases(a = alpha,
            c = beta,
            b = lambda alpha + (1 - lambda) beta) $
        
        Comme $lambda in ]0, 1[$, $a < b < c$.
        
        Donc
        $ (f(b) - f(a))/(b - a) <= (f(c) - f(a))/(c - a) $

        D'où,
        $ f(b) <= (b - a)/(c - a) (f(c) - f(a)) + f(a) $

        Or, $f(b) = f(lambda a + (1 - lambda) beta)$

        et $lambda = (b - beta)/(alpha + beta) = (b - c)/(a - c)$

        $ lambda f(alpha) + (1 - lambda) f(beta)
            yt = (b - c)/(a - c) f(a) + (a - b)/(a - c) f(c)
            yt = (b - a + a - c)/(a - c) f(a)
                ytt + (a - b)/(a - c) f(c)
            yt = f(a) + (a - b)/(a - c) (f(c) - f(a)) $
        
        On a bien
        $ f(lambda alpha + (1 - lambda) beta)
            yt <= lambda f(alpha) + (1 - lambda) f(beta) $

        Cette inégalité est envore vraie si $lambda in {0, 1}$.

        $overbrace(f(lambda a + (1 - lambda) f(c)), = f(b))
            yt <= lambda f(a) + (1 - lambda) f(c)$
        
        D'où,
        $ (f(b) - f(a))/(b - a)
            yt <= underbrace((1 - lambda) (f(c) -f(a))/(b - a),
                = (b - a)/(c - a) (f(c) - f(a))/(b - a))
            yt <= (f(c) - f(a))/(c - a) $
    
    - Cas 2 : On suppose que, pour tout $a < b < c$,
        $ (f(c) - f(a))/(c - a) <= (f(c) - f(b))/(c - b) $

        Soient $(alpha, beta) in I^2$ avec $alpha < beta$,
        et $lambda in ]0, 1[$.

        On pose
        $ cases(a = alpha,
            c = beta,
            b = lambda alpha + (1 - lambda) beta) $
        
        On a donc
        $ f(b)
            yt <= - (f(c) - f(a))/(c - a) (c - b) (c - b)
                ytt + f(c)
            yt <= ((b - c)(f(c) - f(a)))/(c - a)
                ytt (+ f(c) (c - a))/() $

        D'autre part
        $ lambda f(alpha) + (1 - lambda) f(beta)
            yt = lambda f(a) + (a - lambda) f(c)
            yt = (b - c)/(a - c) f(a) + (a - b)/(a - c) f(c)
            yt = ((c - b) f(a) + (b - a) f(c))/(c - a)
            yt >= f(b) = f(lambda alpha + (a - lambda) beta) $
    
    - Cas 3 : On suppose, pourt out $a < b < c$,
        $ (f(b) - f(a))/(b - a) <= (f(c) - f(b))/(c - b) $

        On fait un peu comme les deux autres cas...

    $square$
]

#ydate(2023, 12, 6)

#yprop(name: [Relation entre convexité et croissance du taux de variation])[
    Soit $f: I --> RR$, $I$ un *intervalle*.

    Pour $a in I$, on pose
    $ tau_a: &I without {a} --> RR \
        &x |-> (f(x) - f(a))/(x - a) $
    
    $f "convexe" <=> forall a in I, tau_a "est croissante"$
]

#yproof[
    "$==>$"
    On suppose $f$ convexe.

    Soit $a in I$, et $(x,y) in (I without {a})^2$.

    On suppose $x < y$.

    - Cas 1 : $a < x < y$
        $ tau_a (x) = (f(x) - f(a))/(x - a) $
        $ tau_a (y) = (f(y) - f(a))/(y - a) $
        // ptit dessin

        D'après le lemme des pentes,
        $ (f(x) - f(a))/(x - a) <= (f(y) - f(a))/(y - a) $

    - Cas 2 : $x < a < y$

        Comme $f$ est convexe,

        $underbrace((f(a) - f(x))/(a - x), = tau_a (x)) <= (f(y) - f(x))/(y - x)
            <= underbrace((f(y) - f(a))/(y - a), = tau_a (y))$
        
        On a bien $tau_a (x) <= tau_a (y)$.
    
    - Cas 3 : $x < y < a$

        On sait que,
        $ underbrace((f(a) - f(x))/(a - x), = tau_a (x))
            <= underbrace((f(a) - f(y))/(a - y), = tau_a (y)) $
    

    "$<==$"
    On suppose désormais que, pour tou $a in I$,
    $tau_a$ est croissante sur $I without {a}$.

    Soient $a, b, c$, 3 éléments de $I$ avec $a < b < c$.
    $ (b,c) in (I without {a})^2 $
    (à condition qu $a != sup(I)$, sinon, on prend $b < c < a$.)

    Comme $tau_a$ est croissante, $tau_a(b) <= tau_a(c)$.

    D'où
    $ (f(b) - f(a))/(b - a) <= (f(c) - f(a))/(c - a) $
    Donc $f$ est convexe.
    $square$
]

#yprop(name: [Dérivabilité d'une fonction convexe])[
    (Corollaire de l'équivalence, convexité, croissance du taux de variation.)

    Soit $f: I --> RR$ convexe.

    Soit $a in circle(I)$
    ($exists epsilon > 0, ]a - epsilon, a + epsilon[ subset I$).

    $f$ est dérivable à gauche et à droite de $a$, et
    $ f'_g (a) <= f'_d (a) $
]

#yproof[
    $ tau_a: &I without {a} --> RR \
        &x |-> (f(x) - f(a))/(x - a) $
    
    $tau_a$ est croissante sur $I without {a}$.
    // dessin

    $tau_a$ est minorée sur $]a, a + epsilon[$ (par $tau_a (a - epsilon/2)$)
    donc $lim_(x -> a^+) tau_a (x)$ existe et est réelle,
    donc $f$ est dérivable à droite et
    $ forall x < a, tau_a (x) <= f'_d (a) $

    $tau_a$ est majorée sur $]a - epsilon, a[$ (par $tau_a (a + epsilon/2)$)
    donc $f$ est dérivable à gauche et
    $ forall x > a, f'_g (a) <= tau_a (x) $

    En passant à la limite quand $x --> a^+$
    dans la dernière inégalitée on obtient
    $ f'_g (a) <= lim_(x -> a^+) tau_a (x) = f'_d (a) $
    $square$
]

#ytheo(name: [Equivalence, convexité, croissance de la dérivée, position de la tangente])[
    Soient $f: I --> RR$ dérivable, et $I$ un intervalle.
    Alors,
    $ f "convexe"
        yt <=> f' "croissante"
        yt <=> forall (x, a) in I^2,
            ytt f(x) >= f(a) + f'(a) (x - a) $
    // dessin avec les tangentes en dessous
]

#yproof[
    On suppose $f$ convexe.

    Soit $(x, y) in I^2$.

    On suppose $x < y$.

    $ f'(x) &= lim_(a -> x) (f(x) - f(a))/(x - a) \
        &= lim_(a->x) tau_a (x) $
    de même $f'(y) = lim_(a->y) tau_a (y)$.

    Soit $a in I without {x, y}$. $tau_a$ est croissante donc
    $ tau_x (a) = tau_a (x) <= tau_a (y) = tau_y (a) $

    $tau_x$ est $tau_y$ sont aussi croissantes
    sur leur domaines de définition donc
    $ forall a < y, tau_y (a) <= f' (y) $
    _dessin de $tau_y$_
    $ forall a > x, tau_x (a) >= f'(x) $
    _dessin de $tau_x$_

    D'où, pour tout $a in ]x, y[$,
    $ f'(x) <= tau_x (a) <= tau_y (a) <= f'(y) $
    donc $f'(x) <= f'(y)$ (car $]x, y[ != emptyset$).


    On suppose $f'$ croissante sur $I$. Soit $a in I$.

    On pose $ g_a: x |-> f(x) - f(a) - f'(a) (x - a) $
    $g_a$ est dérivable sur $I$ et
    $ forall  x in I, g'_a (x) = f'(x) - f'(a) $

    _Inséere tableu de variations: $g'_a$ est négative avant
    $a$ puis positive après, et $g_a (a) = 0$_

    Donc
    $ forall x in I, g_a (x) >= 0 $
    Donc
    $ forall x in I, f(x) >= f(a) + f'(a) (x - a) $


    On suppose que, pour tout $(x, a) in I^2$,
    $ f(x) >= f(a) + f'(a) (x - a) $
    Alors,
    $ f(a) >= f(x) + f'(x) (a - x) $
    car $a$ et $x$ jouent des rôles symétriques.

    Soit $a in I$.

    Pour tout $x in I without {a}$,
    $ tau'_a (x)
        yt = (f'(x) (x - a) - (f(x) - f(a)))/(x - a)^2
        yt = (f(a) - f(x) - f'(x) (a - x))/(x - a)^2
        yt >= 0 $
    
    $tau'_a$ est positive sur $I$ sauf en un nombre
    fini de points $tau_a$ est continue sur $I$
    $tau_a$ est croissant.

    D'où $f$ est convexe.
    $square$
]

#ytheo(name: [Équivalence convexité, signe de la dérivée seconde])[
    (Corollaire de l'équivalence, convexité, croissance de la dérivée,
    position de la tangente.)

    Soit $f$ dérivable 2 fois sur $I$.
    $ f "est convexe" <=> forall x in I, f''(x) >= 0 $
]

#yprop(name: [Convexité de l'exponencielle])[
    $exp$ est convexe sur $RR$, car
    $ exp'' = exp > 0 $

    D'où
    $ forall x in RR, e^x >= x + 1 $
]

#yprop(name: [Convexité du logarithme népérien])[
    $-ln$ est convexe sur $RR^*_+$

    (car $forall x > 0, (-ln)'' (x) = 1/x^2 > 0$)

    $forall x > 0, ln(x) <= x - 1$
]

#yprop(name: [Convexité de sinus])[
    
    $forall in [0, pi/2], sin(x) <= x$
]

#ydate(2023, 12, 7)

#yprop(name: [Inégalité de Jensen])[
    Soient $f: I --> RR$ convexe sur un intervalle $I$, et $n in NN^*$.

    Soient $(a_i) in I^[|1, n|]$, et $(lambda_i) in [0, 1]^[|1, n|]$
    tel que $sum_(i=1)^n lambda_i = 1$.

    Alors
    $ f(sum_(i=1)^n lambda_i a_i) <= sum_(i=1)^n lambda_i f(a_i) $
]

#yproof[
    Pour $n in NN^*$, on définit la propriété $P(n)$ :
    $ forall (a_i) in I^[|1, n|], forall (lambda_i) in S_n,
        yt f(sum_(i=1)^n lambda_i a_i) <= sum_(i=1)^n lambda_i f(a_i) $
    où
    $ S_n =
        yt { (lambda_i) in [0,1]^[|1, n|] | sum_(i=1)^n lambda_i = 1 } $
    
    - Initialisation :

        Soit $a_1 in I$ et $(lambda_i) in S_1$.

        Alors $lambda_1 = 1$ et donc
        $ f(lambda_1 a_1) = f(a_1) <= lambda_1 f(a_1) $

        $P(1)$ est vraie.

    - Hérédité : Soit $n in NN^*$. On suppose $P(n)$ vraie.

        Soient $(a_i) in I^[|1, n+1|]$
        et $(lambda_i) in S$.

        - Cas 1 : On suppose $underbrace(sum_(i=1)^n lambda_i, = lambda) != 0$.

            On pose
            $ a = (sum_(i=1)^n lambda_i a_i)/lambda in I $

            On a alors
            $ sum_(i=1)^(n+1) lambda_i a_i
                yt = sum_(i=1)^n lambda_i a_i + lambda_(n+1) a_(n+1)
                yt = lambda a + (1 - lambda) a_(n+1) $ 
            
            $f$ est convexe, et $lambda in [0, 1]$ donc
            $ f(lambda a + (1 - lambda) a_(n+1))
                yt <= lambda f(a) + (1 - lambda) f(a_(n+1)) $
            $ f(a) = f(sum_(i=1)^n lambda_i/lambda a_i)$

            On sait que $(a_i) in I^[|1, n|]$ et
            $ sum_(i=1)^n lambda_i/lambda = 1/lambda sum_(i=1)^n lambda_i = 1 $

            Comme $P(n)$ est vraie,
            $ f(a) <= sum_(i=1)^n lambda_i/lambda f(a_i) $

            D'où,
            $ f(sum_(i=1)^(n+1) lambda_i a_i)
                yt = f(lambda a + (1 - lambda) a_(n+1))
                yt <= lambda f(a) + (1 - lambda) f(a_n + 1)
                yt <= lambda sum_(i=1)^n lambda_i/lambda f(a_i)
                    ytt + (1 - lambda) f(a_(n+1))
                yt <= sum_(i=1)^n lambda_i f(a_i)
                    ytt + lambda_(n+1) f(a_(n+1))
                yt <= sum_(i=1)^(n+1) lambda_i f(a_i) $

        - Cas 2 : On suppose $sum_(i=1)^n lambda_i = 0$

            Donc
            $ forall i in [|1, n|], lambda_i = 0 $
            et $lambda_(n+1) = 1$.

            D'où,
            $ f(sum_(i=1)^(n+1) lambda_i a_i)
                yt = f(a_(n+1))
                yt <= sum_(i=1)^(n+1) lambda_i f(a_i) $
    
    Par récurrence, pour tout $n in NN^*$, $P(n)$.
    $square$
]

#yexample(name: [Inégalité arithmético-géométrique])[
    $exp$ est convexe sur $RR$.

    $forall n in NN^*, forall (a_i) in RR^[|1, n|],
        yt exp(sum_(i=1)^n 1/n a_i) <= sum_(i=1)^n 1/n exp(a_i)$

    D'où,
    $ product e^(1/n a_i) <= 1/n sum_(i=1)^n a_i $

    Soit $x_i = e^(a_i)$ pour $i in [|1, n|]$,
    $ product_(i=1)^n e^(1/n ln(x_i)) <= 1/n sum_(i=1)^n x_i $
    $ root(n, x_1 dot ... dot x_n) <= 1/n sum_(i=1)^n x_i $

    C'est l'inégalité arithmético-géométrique.
]

#ybtw[
    On mesure plusieurs fois un même objet,
    mais les résultats des mesures sont légérement différentes.
    Estimer _au mieux_ la "vraie mesure".
    
    Soit $(x_1, ..., x_n) in RR^n$.
    On cherche $x in RR$ qui minimise l'erreur commise
    en remplaçant la vraie mesure par l'estimation $x$.
    On estime cette erreur à l'aide des $x_i$.

    _Stratégie 1_ :
    On estime l'erreur par la formule
    $ epsilon(x) = sum_(i=1)^n (x_i - x)^2 $
    $epsilon$ est dérivable sur $RR$.

    Pour $x in RR$,
    $ epsilon'(x) &= 2 sum_(i = 1)^n (x - x_i) \
        &= 2 (n x - sum_(i=1)^n x_i) $

    $epsilon'$ est négative s'annule en $1/n sum_(i=1)^n x_i$,
    est négative avant, puis positive après.
    Ainsi $epsilon$ est donc décroissante avant $1/n sum_(i=1)^n x_i$,
    puis croissante après.

    $x = 1/n sum_(i=1)^n x_i$ est la meilleure estimation
    avec e calcul d'erreur.

    _Stratégie 2_ :
    On estime l'erreur par
    $ forall x in RR, epsilon(x) = sum_(i=1)^n abs(x - x_i) $

    On suppose $x_1 < x_2, < ... < x_n$.

    $epsilon$ est dérivable sur
    $ RR without {x_i | i in [|1, n|]} = union.big_(i=0)^n ]x_i, x_n[ $

    On pose $x_0 = -oo$ et $x_(n+1) = +oo$.
    
    $x < x_1$ :
    $ epsilon(x) = sum_(i=1)^n (x_i - x) = sum_(i=1)^n x_i = n x $

    $x_1 < x < x_2$ :
    $ epsilon(x) &= (x - x_1) + sum_(i=1)^n (x_i - x) \
      &= (-n + 2)x - x_1  + sum_(i=1)^n x_i $

    Cette fois, la meilleure estimation est la médiane.
]
