#import "../../template/ytemplate.typ": *


#ypart[L'ensemble des réels]


#ydef(name: [Définition axiomatique des réels])[
    L'ensemble des réels, noté $RR$,
    est muni de 2 lois internes, notées $+$ et $times$,
    et d'une relation d'ordre $<=$, vérifiant :

    + $+$ est asociative, commutative, 0 est son élément neutre,
        tout réels a un opposé ;

    + $times$ est associative, commutative, $1$ est sont élément neutre,
        tout réel non nul a un inverse ;
    
    + $<=$ est un ordre total ;

    + $QQ subset RR$ et l'addition, la multiplication et la comparaison de deux
        rationnels se comportent de la même manière en tant que réel
        et en tant que rationnels ;

    + distributivité
        $ forall (a,b,c) in RR^3,
            yt a(b+c) = a b + a c $
    
    + compatibilité de $<=$ avec $+$
        $ forall a in RR, forall (x, y) in RR^2,
            yt x <= y => a + x <= a + y $
    
    + $forall a in RR^+, forall (x,y) in RR^2$,
        $ x <= y => a x <= a y $
    
    + Axiome de la borne supérieure :

        Toute partie non vide majorée de $RR$ admet une borne supérieure.
    
    L'ensemble des réels est le plus petit ensemble qui vérifie ces axiomes.
]

#yexample[
    $QQ$ ne vérifie pas l'axiome 8.

    Soit $A = { x in QQ | x^2 <= 2 }$.

    $A != emptyset$ car $0 in A$

    $A$ est majorée (par 5).

    Supposons que $A$ ait une borne supérieure rationnelle disons $r = p/q$
    avec $(p,q) in ZZ times NN^*$ et $p/q$ est irréductible.

    - *Lemme* : $forall (r_1, r_2) in (QQ^+)^2$
        $ r_1 < r_2 => (exists r in QQ, r_1 < r^2 < r_2) $
        $square$
    
    - Cas 1 :
    
        Si $r^2 < 2$, alors il existe $r' in QQ$
        tel que $r^2 < r'^2 < 2 $ et donc
        $ cases(
            r' "majore" A,
            r < r') $
        contradiction.

    - Cas 2 :
        $ r^2 = 2
            yt => p^2 = 2 q^2
            yt => p^2 "est pair"
            yt => p "est pair"
            yt => p = 2k "avec" k in ZZ
            yt => 4k^2 = 2q^2
            yt => q^2 = 2k^2
            yt => q^2 "est pair"
            yt => q "est pair" $
        Donc $p/q$ n'est pas irréductible.
        Contradiction !
]

#yprop(name: [Somme d'inégalités])[
    $forall (a, b, c, d) in RR^4$,
    $ cases(a <= b, c <= d) => a + c <= b + d $
]

#yproof[
    On suppose
    $ cases(a<=b, c<=d) $

    - $a <= b$ donc $a + c <= b + c$
    - $c <= d$ donc $b + c <= b + d$
    Donc $a+c <= b+d$.
    $square$
]

#yprop(name: [Multiplication de termes d'une inégalités])[
    $forall (a,b,c,d) in RR^4$
    $ cases(0 <= a <= b, 0 <= c <= d) => a c <= b d $
]

#yproof[
    Soit $(a,b,c,d) in RR^4$.
    On suppose
    $ cases(0<=a<=b, 0<=c<=d) $

    - $a <= b$ donc $a c <= b c$
    - $c <= d$ donc $b c <= b d$
    Donc $a c <= b d$.
    $square$
]

#yprop(name: [Relation entre opposé et multiplication par $-1$])[
    $forall x in RR$,
    $ -x = (-1)x $
]

#yproof[
    Soit $x in RR$.
    $ x + (-1)x &=^2 1x + (-1)x \
        &=^5 (1 + (-1)) x \
        &= 0x $
    
    - *Lemme* :
        $ forall x in RR, 0x = 0 $

        *Preuve* :
        $ 0x = (0+0)x =^5 0x + 0x $
        Donc
        $ 0x - 0x &= (0x + 0x) - 0x \
            &=^1 0x + (0x - 0x) $
        donc $0 = 0x$.
        $square$
    
    D'où, $x + (-x) = 0$,

    donc $(-1)x$ est l'opposé de $x$,

    donc $(-1)x = -x$.
    $square$
]

#ydate(2023, 12, 1)

#yprop(name: [Inégalité sur les opposés])[
    Soit $(x,y) in RR^2$.
    $ x <= y => -x >= -y $
]

#yproof[
    $ x <= y
        yt => (-y + (-x)) + x
            ytt <= (-y + (-x)) + y
        yt => -y + ((-x) + x)
            ytt<= -x + ((-y) + y)
        yt => -y + 0 <= -x + 0
        yt => -x >= -y $
    $square$
]

#yprop(name: [Multiplication d'une inégalité par un négatif])[
    Soient $a in RR^*_-$, $(x, y) in RR^2$.
    $ x <= y => a x >= a y $
]

#yproof[
    $-a in RR^*_+$ donc

    $ x <= y
        yt => (-a) x <= (-a) y
        yt => -((-a) x) >= -((-a) y)
        yt => (-1) (-a) x >= (-1) (-a) y
        yt => a x >= a y $
    $square$
]

#ybtw[
    On ne peut pas, en général, soustraire des inégalités.
    $ a <= b and c <= d cancel(=>) a-c <= b-d $

    Contre-exemple : [Insert text here]

    Par contre,
    $ cases(a <= b, c <= d)
        &=> cases(a <= b, -d <= -c) \
        &=> a - d <= b - c $
]

#ylemme(name: [Signe de l'inverse d'un positif])[
    $ forall x in RR^*_+, 1/x > 0 $
]

#yproof[
    Soit $x in RR^*_+$.
    Alors $1/x != 0$, car $1 = x 1/x != 0$.

    Si $1/x cancel(>=) 0$ alors $x 1/x < 0$
    et donc $1 < 0$.

    Donc $1/x > 0$.
    $square$
]

#yprop(name: [Passage à l'inverse d'une inégalité de nombre positifs])[
    Soit $(x, y) in (RR^*_+)^2$.
    $ x <= y => 1/x >= 1/y $
]

#yproof[
    $ x <= y &=> 1/y 1/x x <= 1/y 1/x y \
        &=> 1/y <= 1/x $
    $square$
]

#yprop(name: [Passage à l'inverse d'une inégalité de nombre négatifs])[
    Soit $(x,y) in (RR^*_-)^2$.
    $ x <= y => 1/x >= 1/y $
]

#yproof[
    $ x <= y &=> 1/y 1/x x <= 1/x 1/y y \
        &=> 1/y <= 1/x $
    $square$
]

#ybtw[
    Si $x < 0 < y$
    alors $1/x < 0 < 1/y$
]