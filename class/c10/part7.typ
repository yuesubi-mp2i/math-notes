#import "../../template/ytemplate.typ": *


#ydate(2023, 12, 8)


#ypart[Intervalles]


#ydef(name: [Intervalle])[
    Un *intervalle* est une partie de $RR$ de la forme

    - $ ]a, b[ = { x in RR | a < x < b } $
        avec $a in RR union {-oo}$

        et $b in RR union {+oo}$.

    - $ ]a, b] = { x in RR | a < x <= b } $
        avec $a in RR union {-oo}$ et $b in RR$.

    - $ [a, b[ = { x in RR | a <= x < b } $
        avec $a in RR$ et $b in RR union {+oo}$.

    - $ [a, b] = { x in RR | a <= x <= b } $
        avec $a in RR$ et $b in RR$.
]

#ybtw[
    - $emptyset = ]0, 0[$ est un intervalle 
    - $forall x in RR, {x} = [x, x]$ est un intervalle
]

#ydef(name: [Ensemble convexe])[
    Soit $A in cal(P)(RR)$.

    On dit que $A$ est *convexe* si
    $ forall (x,y) in A^2, forall z in [x, y], z in A $
]

#ybtw(name: [En dimention 2])[
    _Dessins d'ensembles convexes en 2D, non représentables numériquement._

    _En compensation, voici un joli dessin qui n'a rien a voir :_
    #align(center)[
        #cetz.canvas(
            length: 2em,
            {
            import cetz.draw: *
    
            circle((0, 0), name: "c")
    
            circle("c.left", radius: 0.3)
        })
    ]
]

#ytheo(name: [Lien entre intervalle et ensemble convexe])[
    Soit $I in cal(P)(RR)$.
    $ I "est un intervalle" <=> I "est convexe" $
]

#yproof[
    "$==>$"
    On suppose que $I$ est un intervalle.

    Soit $(x, y) in I^2$.
    On suppose que $x < y$.
    Soit $z in [x, y]$.

    - Cas 1 : $I = ]a, b[$ avec $a in RR union {-oo}$
        et $b in RR union {+oo}$.

        $ a underbrace(<, x in I) x <= z <= y underbrace(<, y in I) b $
    
    - Cas 2 : $I = ]a, b]$ avec $a in RR union {-oo}$ et $b in RR$.

        $ a < x <= z <= y underbrace(<=, y in I) b$
    
    De même pour les cas où $I = [a, b[$ et $I = [a, b]$.

    "$<==$"
    On suppose que $I$ est convexe.

    On pose
    $ a = cases(inf(I) &"si" I "est minorée",
        -oo &"sinon") $
    et
    $ b = cases(sup(I) &"si" I "est majorée",
        +oo &"sinon") $
    
    Si $a = -oo$, on convient que $[a, b]$ désigne
    en fait $]a, b]$ si $b in RR$, et $]a, b[$ si $b = +oo$.
    Si $a = RR$ et $b = +oo$, $[a, b]$ désigne $[a, b[$.

    Montrons que $]a, b[ subset I subset [a, b]$.

    Soit $z in ]a, b[$.

    - $z$ ne minore pas $I$ (sinon $z <= inf(I)$) :
        soit $x in I$ tel que $x <= z$.

    - $z$ ne majore pas $I$ (sinon $sup(I) <= z$) :
        soit $y in I$ tel que $y >= z$.
    
    Donc
    $ cases(
        z in [x, y],
        x in I,
        y in I) $
    
    $I$ est convexe, donc $z in I$.

    D'où $]a, b[ subset I$.

    Soit $z in I$
    - Si $a = -oo$, alors $z >= a$.
    - Si $a in RR$, $a = inf(I)$ minore $I$ donc $z >= a$.
    - Si $b = +oo$, alors $z <= b$.
    - Si $b in RR$, $b = sup(I)$ majore $I$ donc $z <= b$.
    Dans tous les cas $z in [a, b]$.
    $square$
]

#yprop(name: [Particularité d'une intersection d'intervalles])[
    Une intersection quelconque d'intervalles est un intervalle.
]

#yproof[
    Soit $(I_k)_(k in K)$ une famille d'intervalles,
    avec $K$ un ensemble.

    On pose $I = sect.big_(k in K) I_k$.

    Soient $(x,y) in I^2$, $z in [x, y]$.

    Pour $k in K$, $cases(x in I_k, y in I_k)$ et $I_k$ est convexe,
    donc $z in I_k$.

    Donc $I$ est convexe, donc $I$ est un intervalle.
    $square$
]

#yprop(name: [Particularité du barycentre de nombres d'un intervalle])[
    Soit $I$ un intervalle. Soit $n in NN^*$.

    Alors,
    $ forall (a_i) in I^[|1, n|], forall (lambda_i) in S_n,
        yt sum_(i=1)^n lambda_i a_i in I $
    où $S_n = { (lambda_i) in [0, 1]^[|1, n|] | sum_(i=1)^n lambda_i = 1 }$
]

#yproof[
    Pour $n in NN^*$, on définit
    $ P(n) : forall (a_i) in I I^[|1, n|], forall (lambda_i) in S_n,
        yt sum_(i=1)^n lambda_i a_i in I $
    
    - Initialisation :
        
        Soit $a_1 in I$ et $(lambda_1) in S_1$.
        Alors $lambda_1 = 1$ et donc $lambda_1 a_1 = a_1 = I$.

        $P(1)$ est vraie.
    
    - Hérédité : Soit $n in NN^*$. On suppose $P(n)$.

        Soient $(a_i) in I^[|1, n+1|]$ et $(lambda_i) in S_n$.

        - Cas 1 : On suppose que $sum_(i=1)^n lambda_i != 0$.

            On pose $lambda = sum_(i=1)^n lambda_i = 1 - lambda_(n+1)$.

            On pose, pour $i in [|1, n|]$, $lambda'_i = lambda_i/lambda$.

            $ sum_(i=1)^(n+1) lambda_i a_i
                yt = sum_(i=1)^n lambda_i a_i + lambda_(n+1) a_(n+1)
                yt = lambda sum_(i=1)^n lambda'_i a_i + (1 - lambda) a_(n+1) $
            
            On pose $a = sum_(i=1)^n lambda'_i a_i$.

            Pour tout $i in [|1, n|]$,
            $ lambda'_i = lambda_i/lambda in [0, 1] $
            $ sum_(i=1)^n lambda'_i = (sum_(i=1)^n lambda_i)/lambda = 1 $

            Donc $(lambda'_i) in S_n$.
            D'après $P(n)$, $a in I$.

            Comme $lambda in [0, 1]$,
            - si $a <= a_(n+1)$, alors
                $ lambda a + (1 - lambda) a_(n+1) in [a, a_(n+1)] $
            - sinon
                $ lambda a + (1 - lambda) a_(n+1) in [a_(n+1), a] $
            
            Comme $I$ est convexe,
            $ lambda a (1 - lambda) a_(n+1) in I $
        
        - Cas 2 : On suppose que $sum_(i=1)^n lambda_i = 0$.

            Alors pour tout $i in [|1, n|]$, $lambda_i = 0$
            et $lambda_(n+1) = 1$.

            $ sum_(i=1)^(n+1) lambda_i a_i = a_(n+1) in I $
    $square$
]