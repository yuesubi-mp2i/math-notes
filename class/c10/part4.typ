#import "../../template/ytemplate.typ": *


#ypart[Approximation décimale]


#yprop(name: [Encadrement décimal d'une réel])[
    $forall x in RR, forall n in NN$,
    $ floor(10^n x)/10^n <= x < (floor(10^n x) + 1)/10^n $
]

#yproof[
    Soient $x in RR$ et $n in NN$.

    Par définition de la partie entière
    $ floor(10^n x) <= 10^n x < floor(10^n x) + 1 $
    et $10^n > 0$ donc
    $ floor(10^n x)/10^n <= x < (floor(10^n x) + 1)/10^n $
    $square$
]

#ydef(name: [Approximation décimale par défaut/exès d'un réel])[
    Soient $x in RR$ et $n in NN$.

    On dit que
    - $floor(10^n x)/10^n$ est
        l'*approximation décimale par défaut de $x$ de $x$ à $10^(-n)$ près*
    - $floor(10^n x)/10^n + 1/10^n$ est celle *par exès*.
]