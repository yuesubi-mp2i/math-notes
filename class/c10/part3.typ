#import "../../template/ytemplate.typ": *


#ypart[Partie entière]


#ydef(name: [Partie entière])[
    Soit $x in RR$.

    La partie entière de $x$ est définie par
    $ floor(x) := max{ n in ZZ | n <= x } $
]

#yprop(name: [Encadrement par la partie entière])[
    Soient $x in RR$ et $n in ZZ$.
    $ n = floor(x) <=> n <= x < n + 1 $
]

#ylemme(name: ["Archimédité" de $RR$])[
    $RR$ est *archimédien*.
    
    $ forall (x, y) in (RR^*_+)^2, exists n in NN, n x >= y $
]

#yproof[
    Soit $(x,y) in (RR^*_+)^2$.

    On suppose que
    $ forall n in NN, n x < y $

    On pose
    $ A = { n x | n in NN } = emptyset $
    Par hypothèse, $A$ est majorée par $y$.

    Soit $s in sup(A)$.

    Soit $n in NN$. $n + 1 in NN$ donc
    $ (n + 1) x in A $
    donc $n x <= s - x$.

    D'où $s - x$ majore $A$.

    Or $s - x < s$ : une contradiction

    $ exists n in NN, n x >= y $
    $square$
]

#ybtw[
    Soit $x in RR$.

    Si $x > 0$, il existe $n in NN$ tel que $n > x$.

    Dans les 2 cas,
    $ cal(N) = { n in NN | n > x } != emptyset $
    C'est une partie (non vide) de $NN$,
    elle admet  donc un plus petit élément, disons $N$.

    On pose $n_0 = N - 1$ et on a alors
    $ cases(n_0 <= x "car" n_0 in.not cal(N) "car" n_0 < N,
        x < N = n_0 + 1 "car" N in cal(N)) $
    i.e. $n_0 <= x < n_0 + 1$, donc $floor(x)$ existe.

    On suppose $x < 0$

    On pose $y = -x > 0$.
    $ cal(N) = { n in NN | n >= y } != emptyset $
    car $RR$ est archimédien.

    Soit $N = min(cal(N))$ et $n_0 = - N$.

    $N in cal(N)$ donc $N >= y$ donc $n_0 <= x$

    $N - 1 < N$ donc $N - 1 in cal(N)$
    donc $N - 1 < y$ donc $n_0 + 1 > x$

    Donc $n_0 <= x < n_0 + 1$, et
    $floor(x)$ existe aussi.
]

#yexercise[
    $forall x in RR, floor(x+1) = floor(x) + 1$
]