#import "../../template/ytemplate.typ": *


#ypart[Valeur absolue]


#ydef(name: [Valeur absolue])[
    Soit $x in RR$.
    
    La valeur absolue de $x$ notée $abs(x)$ est définie par
    $ abs(x) := max(x, -x) $
]

#yprop(name: [Valeur absolue écrite avec une disjoncation de cas])[
    $forall x in RR$,
    $ abs(x) = cases(x "si" x >= 0, -x "si" x <= 0) $
    $square$
]

#ydate(2023, 12, 2)

#yprop(name: [Valeur absolue du produit])[
    $ forall (x, y) in RR^2, abs(x y) = abs(x) abs(y) $
    $square$
]

#yprop(name: [Inégalité triangulaire (des réels)])[
    $ forall (x,y) in RR^2, abs(x+y) <= abs(x) + abs(y) $
]

#yproof[
    Soit $(x,y) in RR^2$.
    $ cases(x <= abs(x), y <= abs(y)) $
    donc $x+y <= abs(x) + abs(y)$
    $ cases(-x <= abs(x), -y <= abs(y)) $
    donc $-x -y <= abs(x) + abs(y)$

    donc
    $ abs(x) + abs(y) &>= max(x+y, -(x+y)) \
        &>= abs(x+y) $
    $square$
]

#yprop(name: [Majoration d'une valeur absolue])[
    $forall (x,y) in RR^2$,
    $ abs(x) <= y <=> cases(y >= x, y >= -x) $
    $square$
]

#yprop(name: [Encadrement de $x in RR$ quand on connais un majorant de $abs(x - a)$])[
    $forall (x,a) in RR^2, forall l in RR^+$,
    $ abs(x-a) <= l <=> a-l <= x <= a+l $
]

#yproof[
    Soient $(x,a) in RR^2$ et $l in RR^+$.

    $ abs(x-a) <= l
        &<=> cases(l >= x-a, l >= -(x-a)) \
        &<=> cases(x <= l + a, x >= -l + a) \
        &<=> a - l <= x <= a + l $
    $square$
]