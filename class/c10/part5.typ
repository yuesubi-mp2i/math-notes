#import "../../template/ytemplate.typ": *


#ypart[Densité]


#ydef(name: [Densité])[
    Soit $A in cal(P)(RR)$.

    On dit que $A$ est *dense* dans $RR$ si
    $ forall (x,y) in RR^2, x < y,
        yt exists a in A, x < a < y $
]

#ylemme(name: [Intersection des entiers et d'un intervalle de largeur suppérieur à 1])[
    Soit $(x, y) in RR^2$ avec $y - x > 1$.

    Alors $ZZ sect ]x, y[ != emptyset$
]

#yproof[
    On suppose $ZZ sect ]x, y[ = emptyset$.

    Soit $n = floor(x)$. $n <= x < n+1$

    $n + 1 > x$ donc $n+1 >= y$

    Donc
    $ cases(y <= n + 1, -x <= -n) $
    d'où $y - x <= 1$ : une contradiction.
    $square$
]

#ytheo(name: [Densité des rationnels dans les réels])[
    $QQ$ est dense dans $RR$.
]

#yproof[
    Soient $x$ et $y$ deux réels avec $x < y$.

    Alors $y - x > 0$.

    $RR$ est archimédien, on considère $n in NN^*$ tel que $n(y - x) > 1$,
    et donc $n y - n x > 1$.

    Donc $ZZ sect ]n x, n y[ != emptyset$ :
    soit $p in ZZ$ tel que
    $ n x < p < n y $
    
    Alors
    $ cases(x < p/n < y, p/n in QQ) $
    $square$
]

#ytheo(name: [Densité de $RR without QQ$ dans les réels])[
    $RR without QQ$ est dense dans $RR$.
]

#yproof[
    Soient $(x,y) in RR^2$ avec $x < y$.

    $QQ$ est dense dans $RR$, donc il existe un rationnel $r$ tel que
    $ x - sqrt(2) < r < y - sqrt(2) $

    D'où
    $ x < r + sqrt(2) < y $
    $r + sqrt(2) in RR without QQ$ car si $r + sqrt(2) in QQ$,
    alors $sqrt(2) = (r + sqrt(2)) - r in QQ$ : contradiction.
    $square$
]


=== Utilisation classique de la densité


#yprop(name: [Existance d'une suite qui tend vers un certain réel])[
    $ forall x in RR, exists (r_n) in QQ^NN, r_n -->_(n -> +oo) x $
]

#yproof[
    Soit $x in RR$.

    Pour tout $n in NN$, l'intervalle
    $ ]x - 1/(n+1), x + 1/(n+1)[ $
    rencontre $QQ$ : 

    soit $r_n in QQ sect ]x - 1/(n+1), x + 1/(n+1)[$.

    Par encadrement, $r_n -->_(n -> +oo) x$
    $square$
]

#yexercise[
    Trouver toutes les applications de $RR$ dans $RR^*_+$ telle que
    $ forall (x, y) in RR^2, f(x+y) = f(x) f(y) $

    Soit $f: RR --> RR^*_+$ une telle fonction
    $ 0 != f(0) &= f(0 + 0) \
        &= f(0)^2 $
    donc $f(0) = 1$.

    On pose $a = f(1) in RR^*_+$,
    $ forall n in NN^*, f(n) &= f(sum_(i=1)^n 1) \
        &= product_(i=1)^n f(1) \
        &= a^n $
    
    Soit $p in ZZ^-$. On pose $n = -p in NN$.
    $ f(p) f(n) = f(p + n) = f(0) = 1 $
    donc $f(p) = 1/f(n) = 1/a^n = a^(-n) = a^p$.

    Soit $r in QQ$ : $r = p/q$ avec $(p, q) in ZZ times NN^*$.
    $ f(q r) = f(p) = a^p $
    $ f(sum_(i=1)^q r) = product_(i=1)^q f(r) = f(r)^q $

    Donc $f(r) = (a^p)^(1/q) = a^(p/q) = a^r$

    Soit $x in RR$. Soit $(r_n) in QQ^NN$ telle que $r_n -->_(n -> +oo) x$.

    $f$ est continue donc $f(r_0) -->_(n -> +oo) f(x)$.

    $ f(x) = lim_(n -> +oo) a^(r_n) = a^x $

    _Synthèse_ :
    Soit $a in RR^*_+$ et $f: x |-> a^x$
    On prouve que $f$ est bien solution.
]