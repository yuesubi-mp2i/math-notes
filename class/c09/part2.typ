#import "../../template/ytemplate.typ": *


#ypart[Récurrences]


#yprop(name: [Principe de récurrence à partir d'un certain rang])[
    Soit $P$ un prédicat sur $NN$ et $N in NN$.
    On suppose
    $ cases(P(N) "vraie", forall n > N\, P(n) => P(n + 1)) $
    Alors
    $ forall n >= N, P(n) "est vraie" $
]

#yproof[
    On forme
    $ A = { k in NN | k >= N and P(k) "fausse"} $
    On suppose $A != emptyset$.

    $A$ admet un plus petit élément, disons $a$.
    $ cases(a >= N, P(a) "est fausse") $

    $P(N)$ est vraie donc $a > N$.

    On remarque que $a - 1 in NN$, et $a - 1 in A$
    donc $a - 1 < N$ ou $P(a - 1)$ est vraie.

    Or $a > N$ donc $a - 1 > N - 1$, donc $P(a - 1)$ est vraie,
    donc $P(a)$ est vraie une contradiction.

    Donc $A = emptyset$ donc pour tout $k >= N$, $P(k)$ est vraie.
    $square$
]

#yexample[
    On considère un circuit automobile, disposant de $n$ pompes à essance.
    La quantité totale de carburant correcond exactement à la quantité
    nécessaire pour parcourir une fois le circuit en entier.

    Le véhicule a un réservoir vide au départ.
    Montrer qu'il est possible de choisir la position de départ de la voiture
    pour effectuer le circuit.
]

#yexample[
    *Raisonnement faux*

    On pose, pour $n in NN$,
    $ P(n) : n^2 = n $

    - _Initialisation_ : $0^2 = 0$ donc $P(0)$ est vraie.

    - _Hérédité_ : Soit $n in NN$. On suppose $P(n)$ vraie.

        $n^2 = n$ donc $n^3 = n^2 = n$.

        Donc
        $ n^3 - 1 = n - 1 $
        Or,
        $ n^3 - 1 = (n - 1)(1 + n + n^2) $
        (*Vrai seulement si $n != 1$ !*)

        Donc $1 + n + n^2 = 1$

        donc $1 + 2n + n^2 = n + 1$

        i.e. $(n+1)^2 = n + 1$

        $P(n+1)$ est vraie.
]

#yexample[
    Montrer que
    $ forall n in NN^*, forall k in [|0, n|],
        yt n!/(k! (n-k)!) in NN $
    
    Pour $n in NN^*$,
    $ P(n) : forall k in [|0, n|], n!/(k! (n-k)!) in NN $

    - _Initialisation_ :
        D'une part,
        $ 1!/(0! (1 - 0)!) = 1 in NN $
        d'autre part,
        $ 1!/(1! (1 - 1)!) = 1 in NN $
    
    - _Hérédité_ : Soit $n in NN$. On suppose $P(n)$ vraie.

        Soit $k in [|0, n|]$. On suppose
        $cases(k > 0, k <= n)$.

        $ (n+1)!/(k! (n+1-k)!) &= binom(n + 1, k) \
            &= binom(n, k) + binom(n, k - 1) $

        $binom(n, k) in NN$ d'après $P(n)$ et
        $binom(n, k - 1) in NN$ d'après $P(n)$.

        et donc $binom(n + 1, k) in NN$.

        - Si $k = 0$,
            $ binom(n+1, k) = binom(n+1, 0) = 1 in NN $
        
        - Si $k = n + 1$,
            $ binom(n+1, k) = binom(n+1, n + 1) = 1 in NN $
    $square$
]

#ydate(2023, 11, 29)

#yexample[
    Soit $u$ la suite définite par
    $ cases(u_0 = 0,
        u_1 = 1,
        forall in NN\, u_(n+2) = u_(n+1) + u_n) $
    Montrer que, pour tout $n in NN$, $u_n in NN$.

    Pour $n in NN$, $P(n) : u_n in NN and u_(n+1) in NN$.
    
    - _Initialisation_ : $u_0 = 0 in NN$ et $u_1 = 1 in NN$

    - _Hérédité_ : Soit $n in NN$. On suppose $P(n)$ vraie.
        $ u_(n+1) = u_n + u_(n-1) $

        Or $u_n in NN$ et $u_(n+1) in NN$ par hypothèse de récurrence.
        Donc $u_(n+2) in NN$.
]

#yprop(name: [Récurrence double])[
    Soit $P$ un prédicat sur $NN$ et $N in NN$.
    On suppose
    $ cases(
        P(N) "vraie",
        P(N+1) "est vraie",
        forall n >= N\,
        yt (P(n) and P(n+1) => P(n+2))) $
    Alors
    $ forall n >= N, P(n) "est vraie" $
]

#yproof[
    Pour $n$ entier, on pose,
    $ Q(n) : P(n) and P(n+1) $

    - _Initialisation_ : $Q(N)$ est vraie car on a supposé $P(N)$ et $P(N+1)$.

    - _Hérédité_ : Soit $n >= N$. On suppose $Q(n)$ vraie.

        On sait que $P(n+1)$ est vraie.
        Comme $P(n)$ et $P(n+1)$ sont vraies, $P(n+2)$ aussi.
        Donc $Q(n+1)$ est vraie.
    
    Par récurrence, $Q(n)$ est vraie pour tout $n >= N$.
    En particulier, $P(n)$ est vraie pour tout $n >= N$.
    $square$
]

#yexample[
    On reprends l'exemple précédent.

    On pose $P(n) : u_n in NN$, pour $n in NN$.
    - $u_0 = 0 in NN$ donc $P(0)$ est vraie
    - $u_1 = 1 in NN$ donc $P(1)$ est vraie

    - Soit $n in NN$. On suppose $P(n)$ et $P(n+1)$ vraies.
        $ u_(n+2) = u_(n+1) + u_n $
        est la somme de 2 entiers, et donc $P(n+2)$ est vraie.
]

#yprop(name: [Récurrence d'ordre $p$])[
    Soit $p in NN^*$, $P$ un prédicat sur $NN$, $N in NN$.
    On suppose
    $ cases(
        forall k in [|0, p-1|]\, P(N + k) "vraie",
        forall n >= N\,
            yt (forall k in [|0, p-1|], P(n+k))
                ytt => P(n+p)
        ) $
    Alors
    $ forall n >= N, P(n) "est vraie" $
]

#yproof[
    On pose, pour $n >= N$,
    $ Q(n) : forall k in [|0, p-1|], P(n + k) $
    On prouve que $Q(n)$ est vraie pour tout $n >= N$ par récurrence.
    $square$
]

#yexample[
    Montrer que tout entier supérieur ou égal à 2
    peut s'écrire comme un produit de nombres premiers

    Pour $n >= 2$, on note
    $ P(n) : forall k in [|2, n|],
        yt k "produit de nombres premiers" $
    
    - _Initialisation_ : 2 est premier donc $P(2)$ est vraie.

    - _Hérédité_ : Soit $n >=2$. On suppose $P(n)$ vraie.

        Soit $k in [|2, n+1|]$.
        Si $k <= n$, alors on sait déjà que $k$ est produit de nombres premiers.
        
        - Si $n+1$ est premier, alors $P(n+1)$ est déjà vraie.

        - On suppose $n+1$ non premier.
            Soit $p in [|2, n|]$ un diviseur de $n+1$,
            et $q in [|2, n|]$ tel que $n+1 = p q$.

            $2 <= p <= n$ donc $p$ est un produit de nombres premiers.
            Idem pour $q$.

            Donc $n+1$ est un produit de facteurs premiers.
]

#yprop(name: [Récurrence forte])[
    Soit $P$ un prédicat sur $N$, $N in NN$.
    On suppose
    $ cases(
        P(N) "vraie",
        forall n >= N\,
            yt (forall k in [|N, n|]\, P(k))
                ytt => P(n+1)
    ) $
    Alors
    $ forall n >= N, P(n) "est vraie" $
]

#yproof[
    Pour $n >= N$, on pose,
    $ Q(n) : forall k in [|N, n|], P(k) "vraie" $
    On démontre $Q(n)$ vraie pour $n >= N$ par récurrence.
    $square$
]

#yexample[
    Même exemple avec la suite de Fibonnacci.

    Dans l'hérédité, on devra de toute façon faire la deuxième initialisation.
    Car on ne peut pas écrire $u_(n-1)$ pour tout $n in NN$.
]