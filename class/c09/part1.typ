#import "../../template/ytemplate.typ": *


#ypart[ Axiomes ]


Les axiomes de cette partie, ne sont pas exigibles aux concours.

#ydef(name: [Entiers naturels])[
    L'ensemble $NN$ est muni d'une relation d'ordre $<=$ vérifiant

    + $NN != emptyset$
    + $<=$ est total
    + Toute partie non vide de $NN$ a un plus petit élément
    + Toute partie non vide majorée admet un plus grand élément
    + $NN$ n'a pas de plus grand élément
]

#ydef(name: [Nombres $0$ et $1$])[
    - $0 = min(NN)$ (possible grâce aux axiomes 1 et 3)
    - $1 = min(NN without {0})$ (possible grâce aux axiomes 5 et 3)
]

#ynot(name: [Inférieur strictement])[
    On note $<$ la relation
    $ cases(x <= y,
        x != y) $
]

#ydef(name: [Successeur d'un entier])[
    Soit $n in NN$.

    Le *successeur* de $n$ est
    $ n^* = min{k in NN | k > n} $

    ($n^*$ existe d'après les axiomes 2, 3 et 5)
]

#yprop(name: [Entier entre un autre et son succésseur])[
    $ forall n in NN, exists.not k in NN, n < k < n^* $
]

#yproof[
    Soient $n in NN$ et $k in NN$ tel que $n < k < n^*$.

    $k > n$ donc $k >= n^*$ car $n^*$ est le plus petit entier
    strictement supérieur à $n$ : une contradiction.
    $square$
]

#ydef(name: [Prédecesseur d'un entier])[
    Soit $n in NN without {0}$.

    Le *prédecesseur* de $n$ est
    $ n_* = max{k in NN | k < n} $

    ($n_*$ existe d'après les axiomes 4 et 2)
]

#yprop(name: [Composition de prédécesseur, succésseur])[
    + $forall n in NN, (n^*)_* = n$
    + $forall n in NN without {0}, (n_*)^* = n$
]

#yproof[
    + Soit $n in NN$. On sait que
        $ (n^*)_* = max{ k in NN | k < n^* } $

        Or $n < n^*$ donc $(n^*)_* >= n$.

        Or $(n^*)_* < n^*$ :  $n <= (n^*)_* < n^*$.

    
    + Similaire à 1.
    $square$
]

#ydate(2023, 11, 28)

#ytheo(name: [Principe de récurrence])[
    Soit $P$ un prédicat définit sur $NN$ tel que
    $ cases(P(0) "est vraie",
        forall n in NN\, P(n) => P(n^*)) $
    Alors
    $ forall n in NN, P(n) "est vraie" $
]

#yproof[
    Soit $A = { n in NN | P(n) "est fausse" }$.

    On suppose $A != emptyset$.
    
    A étant une partie non vide de $NN$,
    elle admet un plus petit élément $N$.

    $P(0)$ est vraie, donc $0 in.not A$,
    donc $N != 0$.

    $N_* < N$ donc $N_* in.not A$.
    Donc $P(N_*)$ est vraie.

    Donc $P((N_*)^*)$ est vraie.
    Ainsi $P(N)$ est vraie.

    Or $N in A$, i.e. $P(N)$ est fausse.

    Cette contradiction prouve que $A = emptyset$,
    i.e. pour tout $n in N$, $P(N)$ est vraie.
    $square$
]

#ydef(name: [Addition d'entiers naturels])[
    On définit l'addition par récurrence sur $NN$.
    $ forall n in NN, cases(n + 0 = n,
        forall p in NN\, n + p^* = (n + p)^*) $
]

#yprop(name: [Écriture du succésseur comme une addition])[
    $ forall n in NN, n + 1 = n^* $
]

#yproof[
    Soit $n in NN$.

    $ n+1 &= n + 0^* \
        &= (n + 0)^* \
        &= n^* $
    $square$
]

#yprop(name: [Associativité de l'addition dans $NN$])[
    $ forall (a, b, c) in NN^3,
        yt a + (b + c) = (a + b) + c $
]

#yproof[
    On fixe $(a, b) in NN^2$. et on pose, pour $c in NN$,
    $ P(c) : a + (b + c) = (a + b) + c $

    - _Initialisation_ :
        D'une part
        $ a + (b + 0) = a + b $
        et d'autre part
        $ (a + b) + 0 = a + b $
    
    - _Hérédité_ :
        Soit $c in NN$. On suppose $P(c)$.

        $ a + (b + c*) &= a + (b + c)^* \
            &= (a + (b + c))^* \
            &= ((a + b) + c)^* \
            &= (a + b) + c^* $
    $square$
]

#yprop(name: [Commutativité de l'addition dans $NN$])[
    $ forall (a, b) in NN^2,
        yt a + b = b + a $
]

#yproof[
    On fixe $a in NN$, Pour $b in NN$, on pose
    $ P(b) : a + b = b + a $

    - _Initialisation_ :
        D'une part,
        $ a + 0 = a $

        D'autre part, montrons part récurrence que $0 + a = a$.
        - _Initialisation_ : 0 + 0 = 0
        - _Hérédité_ : Soit $a in NN$ tel que $0 + a = a$
            $ 0 + a^* = (0 + a)^* = a^* $
    
    - Montrons aussi que $P(1)$ par récurrence sur $a$.
        - _Initialisation_ :  $1 + 0 = 0 + 1 = 1$
        - _Hérédité_ : Soit $a in NN$, tel que $a + 1 = 1 + a$.
            $ 1 + a^* &= (1 + a)^* \
                &= (a + 1)^* \
                &= (a + 1) + 1 \
                &= a^* + 1 $
    
    - _Hérédité_ : soit $b in NN$. On suppose $P(b)$.
        $ a + b^* &= (a + b)^* \
            &= (b + a)^* \
            &= b + a^* \
            &= b + (a+1) \
            &= b + (1+a) \
            &= (b+1) + a \
            &= b^* + a $
    $square$
]

#ydef(name: [Multiplication d'entiers naturels])[
    On définit la multiplication sur $NN$ par récurrence : 
    $ forall n in NN,
        yt cases(n times 0 = 0,
            forall p in NN\, n times p^* = n times p + n) $
]

#yprop(name: [Propriétés de la multiplication dans $NN$])[
    La multiplication est associative commutative
    et $1$ est sont élément neutre.
    $square$
]

#ydef(name: [Soustraction d'entiers naturels])[
    Non donnée T-T.
]

#yprop(name: [Stabilité de $<$ par l'addition et la soustraction])[
    $ a < b =>
        yt cases(forall n in NN\, a + n < b + n,
            forall p <= min(a, b)\, a - p < b - p) $
    $square$
]

#yprop(name: [Existence de la division euclidienne])[
    Soit $(a, b) in NN times NN^*$.

    $ exists! (q, r) in NN^2, cases(a = b q + r, r < b) $
]

#yproof[
    On pose
    $ A = { k in NN | b k <= a } $
    $A != emptyset$ car $0 in A$.

    $A$ est majorée par $a$. $A$ admet un plus grand élément.

    On note $q = max(A)$.

    On pose $r = underbrace(a - b q, in NN)$

    Donc $(q, r) in NN^2$ et $a = b q + r$.
    $ q + 1 > q $
    donc $q+1 in.not A$,
    donc $b (q+1) > a$
    donc $b q + b > a$
    donc $b > a - b q = r$.

    - Soit $(q', r') in NN^2$ tel que
        $ cases(a = b q' + r', r' < b) $

        D'où, $b q + r = b q' + r'$, i.e. $b (a - q') = r' - r$.

        - Si $r <= r'$, avec $0 <=  r' -r < b$
            et donc $r' - r = 0$, i.e. $r = r'$.

        - Sinon $0 < r -r' < b$
    
        Contradiction !

        Donc $r = r'$, donc $b(q - q') = 0$, donc $q - q' = 0$
    
    $square$
]