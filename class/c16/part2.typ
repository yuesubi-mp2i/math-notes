#import "../../template/ytemplate.typ": *


#ypart[Propriétés globales des fonctions dérivables]


#let let_i() = {[Soit $I$ un intervalle ouvert non vide.]}

=== 1. Accroissements finis

#ytheo(name: [Théorème de Rolle])[
    Soit $f : [a,b] -> RR$ continue, dérivable sur
    $] a , b [$ telle que $f (a) = f (b)$.
    
    Alors il existe $c in ] a , b [$ tel que $f' (c) = 0$.
]


#yproof[
    Comme $f$ est continue sur le segment $[a, b]$,
    elle y admet un minimum $f(c)$ et un maximum $f(d)$ :
    $ forall x in [a, b], f(c) <= f(x) <= f(d) $

    - Si $f(c)=f(d)$, alors $f$ est constante sur $[a,b]$
        et donc de dérivée nulle.

    - On suppose $f(c) < f(d)$.
    
        Dans ce cas, $c in ]a, b[$ ou $d in ]a, b[$
        (sinon $f(c) = f(a) = f(d)$ car $f(a) = f(b)$).
        
        On en déduit que $c$ ou $d$ est un point critique.
]

#ybtw[
    Si un point mobile sur un axe est revenu à sa position de départ, sa
    vitesse s'est annulée au moins une fois. (Il faut s'arrêter pour faire
    demi-tour sur un axe).
]



#ytheo(name: [Théorème des accroissements finis])[
    Soit $f : [a,b] -> RR$ continue sur $[a , b]$, dérivable
    sur $] a , b [$.
    
    Il existe $c in ] a , b [$ tel que
    $ f (b) - f (a) = (b - a) f prime (c) $
]


#yproof[
    Soit $g : x |-> f(x) - tau (x-a)$ où $tau = (f(b)-f(a))/(b-a)$.
    
    La fonction $g$ est continue sur $[a, b]$ et dérivable sur $]a,b[$.
    
    De plus $g(b) = f(a) = g(a)$.
    
    D'après le théorème de Rolle, il existe $c in ]a, b[$ tel que $g'(c)=0$.

    Or, pour tout $x in ]a,b[$,
    $ g'(x) = f'(x) - tau $
    donc il existe $c in ]a, b[$ tel que $f'(c) = tau$.
]

#ybtw[
    Ce théorème signifie que si un point réalise un trajet à vitesse
    moyenne $v$, alors il existe un instant auquel la vitesse
    instantanée est $v$.
]

#ycor(name: [Monotonie d'une fonction avec la dérivée])[
    (Corollaire du théorème des accroissements finis)

    #let_i()

    + Si $f$ est dérivable sur $I$ de dérivée positive (resp. strictement positive) alors
        $f$ est croissante (resp. strictement croissante).

    + Si $f$ est dérivable sur $I$ de dérivée négative (resp. strictement négative) alors
        $f$ est décroissante (resp. strictement décroissante).

    + Si $f$ est dérivable sur $I$ de dérivée nulle, alors $f$ est
        constante.
]

#yproof[
    On suppose $f$ dérivable sur $I$.
    
    Soit $a<b$ deux éléments de $I$.
    
    Soit $c in ]a, b[$ tel que
    $ f(b) - f(a) = f'(c) (b-a) $
    ($c$ existe d'après le théorème des accroissements finis).
    
    Si $f'$ est positive (resp. strictement positive, négative,
    strictement négative, nulle), alors $f'(c) >= 0$

    (resp. $f'(c) >0$, $f'(c)<=0$, $f'(c) < 0$, $f'(c)=0$) et donc $f(b)>=f(a)$

    (resp. $f(b)> f(a)$, $f(b)<=f(a)$, $f(b)<f(a)$, $f(b)=f(a)$).
]



#yprop(name: [Inégalité des accroissements finis])[
    Soit $f : [a,b] -> RR$ continue sur $[a , b]$, dérivable sur
    $] a , b [$ tel que
    $ forall t in ] a , b [, m lt.eq f prime (t) lt.eq M $
    
    Alors pour
    $ m (b - a) lt.eq f (b) - f (a) lt.eq M (b - a) $
]


#ycor(name: [Conséquence d'une fonction dérivable et bornée])[
    (Corollaire de l'inégalité des accroissements finis)

    #let_i()

    Si $f$ est dérivable sur $I$ et pour tout $x in I$,
    $abs(f prime (x)) lt.eq k$, alors $f$ est $k$-lipschitzienne.
]


=== 2. Prolongement $C^1$

#ytheo(name: [Théorème de la limite de la dérivée])[
    #let_i()

    Si $f : I -> RR$ est continue, dérivable sur $I without { a }$ et
    $f prime$ admet une limite finie $ell$ en $a$, alors $f$ est dérivable
    en $a$, de dérivée $ell$.
]

#yproof[
    Soit $f$ continue sur l'intervalle $I$, dérivable sur $I without {a}$,
    telle que $f'$ a une limite $ell in RR$ en $a$.
    
    Soit $x in I without {a}$.
    
    D'après le théorème des accroissements finis,
    il existe $c_x in I$ tel que
    $ f(x)-f(a) = f'(c_x)(x-a) $
    et $c_x$ compris entre $a$ et $x$.
    
    On en déduit que $lim_(x -> a) c_x = a$ et donc
    $ lim_(x -> a) (f(x)-f(a))/(x-a) = lim_(x->a ) f'(c_x) = ell $
    
    Donc $f$ est dérivable en $a$.
]


#yprop(name: [Prolongement $C^k$])[
    #let_i()

    Si $f : I -> RR$ est de classe $C^k$ sur $I$, $C^(k + 1)$
    sur $I without { a }$ et $f^((k + 1))$ admet une limite finie en $a$,
    alors $f$ est de classe $C^(k + 1)$ sur $I$.
]

#yproof[
    Par récurrence sur $k$.
]


#yexample[
    Soit $f : RR -> RR$, $x arrow.r.bar e^(- 1 / x^2)$ si
    $x > 0$ et $0$ si $x lt.eq 0$. Montrer que $f$ est $C^oo$.
]



=== 3. Application aux fonctions réciproques

#ydef(name: [$C^k$-difféomorphisme])[
    Soit $f$ une application de classe $C^k$, $k gt.eq 1$.
    
    On dit que $f$ est un #emph[$C^k$-difféomorphisme] si 
    - $f$ est de classe $C^k$, 
    - bijective ,
    - $f^(- 1)$ est de classe $C^k$.
]

#ytheo(name: ["Caractérisation" d'un $C^k$-difféomorphisme pour une application $C^k$ bijective sur un intervalle])[
    #let_i()

    Une application $f : I arrow.r J$ bijective de classe $C^k$ est un
    $C^k$-difféomorphisme si et seulement si sa dérivée ne s'annule pas.
]