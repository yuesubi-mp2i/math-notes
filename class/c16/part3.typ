#import "../../template/ytemplate.typ": *


#ypart[Formules de Taylor]


#let let_i() = {[Soit $I$ un intervalle ouvert non vide.]}

#ytheo(name: [Formule de Taylor avec reste intégral])[
    #let_i()
    Soit $f$ une fonction de classe $C^(n+1)$ sur un intervalle $I$
    à valeurs réelles et $a in I$. 

    Pour tout $x in I$,
    $ f(x) =& sum_(k=0)^n (f^((k))(a))/(k!) (x-a)^k \
        &+ integral_a^x ((x-t)^n)/(n!) f^((n+1))(t) dif t $
]


#yproof[
    Par récurrence sur $n$. 

    - Soit $f$ une fonction de classe $C^1$ sur $I$ et $a in I$.
    
        D'après le théorème fondamental de l'analyse,
        $ sum_(k=0)^n (f^((k))(a))/(k!) (x-a)^k
                ytt + integral_a^x ((x-t)^n)/(n!) f'(t) dif t
            yt = f(a) + (f(x)-f(a))
            yt = f(x) $

    - Soit $n in NN$.
        
        On suppose la formule de Taylor avec reste intégral pour toute fonction de classe $C^(n+1)$ sur $I$.
        
        Soit $f$ une fonction de classe $C^(n+2)$ sur $I$.
        
        Puisque $f$ est aussi de classe $C^(n+1)$, on peut écrire pour tout $x in I$, 


        $ f(x) =& sum_(k=0)^n (f^((k))(a))/(k!) (x-a)^k \
            &+ integral_a^x ((x-t)^n)/(n!) f^((n+1))(t) dif t $

    Une intégration par parties donne pour tout $x in I$, 

    $ integral_a ^x ((x-t)^n)/(n!) f^((n+1))(t) dif t
        yt = [-((x-t)^(n+1))/((n+1)!) f^((n+1))(t)]_a^x
            ytt - integral_a^x [-((x-t)^(n+1))/((n+1)!)]
                yttt f^((n+2))(t) dif t
        yt = ((x-a)^(n+1))/((n+1)!)
            ytt + integral_a^x ((x-t)^(n+1))/((n+1)!) f^((n+2))(t) dif t $

    D'où le résultat. 
]

#ycor(name: [Inégalité de Taylor-Lagrange])[
    (Corollaire de la formule de Taylor avec reste intégral)

    #let_i()
    Soit $f$ une fonction de classe $C^(n+1)$ sur un intervalle $I$.
    
    On suppose que $f^((n+1))$ est bornée sur $I$.
    
    Soit $M in RR$ tel que pour tout $x in I$, $ |f^((n+1))(t)| <= M $
    
    Alors, pour tout $x in I$,
    $ abs( f(x) - sum_(k=0)^n ((x-a)^k)/(k!) f^((k))(a) )
        yt <= M abs((x-a)^(n+1))/((n+1)!) $
]


#yproof[On exploite le théorème de Taylor avec reste intégral et l'inégalité triangulaire :
    Pour tout $x in I$,
    $ lr(| f(x) - sum_(k=0)^n ((x-a)^k)/(k!) f^((k))(a) |)
        yt = lr(|integral_a^x ((x-t)^n)/(n!) f^((n+1))(t) dif t|)
        yt <= integral_(min(a,x))^(max(a,x)) lr(|((x-t)^n)/(n!)| )
            yttt |f^((n+1))(t)| dif t
        yt <= integral_(min(a,x))^(max(a,x)) M lr(|((x-t)^n)/(n!)| ) dif t
        yt <= M lr(|((x-a)^(k+1) )/((k+1)!)|). $
]

#yexample[
    + Pour tout $x in [0, pi/2]$, $x - x^3 / 2 <= sin(x) <= x - x^3/2 + x^5/120. $
    + $ forall x in RR, lim_(n -> +oo) sum_(k=0)^n (x^k)/(k!) = e^x. $
]


#ytheo(name: [Formule de Taylor-Young])[
    Soit $f$ une fonction définie sur un intervalle $I$ de classe $C^n$ en $a$. 
    $ f(x) =& sum_(k=0)^n (f^((k))(a)) /(k!) (x-a)^k \
        &+ limits(o_(x->a) ((x-a)^n)) $ 
]

#yproof[
    Par récurrence sur $n$.

    - Soit $f$ une fonction continue en $a$.
    
        Par définition, $f(x)-f(a) = o(1)$.

    - Soit $n$ un entier naturel.
    
        On suppose la formule de Taylor-Young vraie pour toutes les fonctions de classe $C^n$ en $a$.
        
        Soit $f$ une fonction de classe $C^(n+1)$ en $a$.
        
        Par définition, $f'$ est de classe $C^n$ en $a$ et donc 

        $ f'(x) =& sum_(k=0)^n ((x-a)^k)/(k!) f'^((k))(a) \
            &+ o_(x->a) ((x-a)^(n)) $

    En intégrant ce développement limité, on obtient 

    $ f(x) - f(a)
        yt = sum_(k=0)^n ((x-a)^(k+1))/((k+1)!) f^((k+1))(a)
        ytt + o_(x->a)((x-a)^(n+1)) $
]