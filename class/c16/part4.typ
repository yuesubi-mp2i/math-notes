#import "../../template/ytemplate.typ": *


#ypart[Extensions aux fonctions à valeurs dans $CC$]


#let let_i() = {[Soit $I$ un intervalle ouvert non vide.]}

#ydef(name: [Dérivabilité d'une fonction complexes, en un point, sur un intervalle, $k$-fois])[
    #let_i()

    + On dit que $f$ est #emph[dérivable en $a in I$] si
        $frac(f (x) - f (a), x - a)$ admet une limite en $a$. On la note alors
        $f prime (a)$ et on l'appelle #emph[dérivée] de $f$ en $a$.

    + On dit que $f$ est #emph[dérivable sur $I$] si $f$ est dérivable en
        chaque point de $I$.

    + De même que précédemment on définit les fonctions $k$ fois dérivables,
        de classe $C^k$ et de classe $C^oo$.
]

#yexample[
    $t arrow.r.bar e^(i t)$ est dérivable de dérivée
    $t arrow.r.bar i e^(i t)$. Elle est même $C^oo$.

]

#yprop(name: ["Caractérisation" de la dérivée d'une fonction complexes par les parties réelles et imaginaires])[
    $f$ est dérivable si et seulement si $"Re"(f)$ et $"Im"(f)$ le sont et on
    a alors
    $ f prime = "Re" (f) prime + i "Im"(f) prime $
]

#yproof[
    C'est juste la caractérisation de la limite par parties
    rélles et imaginaires.
]

#yprop(name: [Opérations élémentaires sur les dérivées de fonctions complexes])[
    Les opérations sur les fonctions dérivables réelles sont encore
    valables pour les fonctions à valeurs dans $CC$.
]


#ybtw[
    On ne conserve pas la notion d'extremum pour une fonction à valeurs
    complexes. On n'a donc plus d'équivalent du théorème de Rolle : par
    exemple $t arrow.r.bar e^(i t)$ prend la même valeur en $1$ et $2 pi$
    mais sa dérivée ne s'annule pas sur $[0 , 2 pi]$. On ne conserve donc
    pas le théorème des accroissements finis. Seule l'inégalité des
    accroissements finis reste.
]

#ytheo(name: [Inégalité des accroissements finis (pour les complexes)])[
    Soit $f : [a,b] -> CC$ de classe $C^1$ sur $] a , b [$
    telle que $M >= lr(|f prime (t)|)$ pour tout $t in \] a , b \[$.

    Alors $lr(|f (b) - f (a)|) lt.eq M (b - a)$.
]

#yproof[
    On ne peut plus utiliser le théorème des accroissements finis puisqu'il est faux en général pour les fonctions à valeurs complexes. 

    $ abs(f(b) - f(a))
        yt = abs(integral_a^b f'(t) dif t)
        yt <= integral_a^b abs(f'(t)) dif t
        yt <= integral_a^b M d t
        yt = M abs(b-a) $
]



#ycor(name: [Conséquence que la dérivée est nulle sur un intervalle (pour les complexes)])[
    (Corollaire de l'inégalité des accroissements finis (pour les complexes))

    #let_i()

    // Keske ce truc fait là ?! :
    // + Une fonction $f : I -> RR$ telle que $lr(|f prime|)$ est
    //     majorée par $M$ sur $I$ est $M$-lipschitzienne.

    Une fonction dont la dérivée est nulle sur un intervalle $I$ est
    constante.
]