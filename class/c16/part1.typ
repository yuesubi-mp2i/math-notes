#import "../../template/ytemplate.typ": *


#ypart[Dérivée en un point, fonction dérivée]


#let let_i() = {[Soit $I$ un intervalle ouvert non vide.]}


=== 1. Généralités

#ydef(name: [Dérivée en un point et sur un intervalle])[
    #let_i()
    Soit $f : I -> RR$ une application et $a in I$.

    On dit que $f$ est #emph[dérivable en $a$]
    si le taux d'acroissement $(f(x) - f(a))/(x - a)$ admet une
    limite finie $ell$ quand $x$ tend vers $a$.
    
    Cette limite est alors
    appelée #emph[dérivée en $a$];, on la note $f prime (a)$ (ou
    $D f (a)$, $frac(d f, d x) (a)$).


    On dit que $f$ est #emph[dérivable sur $I$] si $f$ est dérivable en
    chaque point de $I$. On note $f prime$ l'application qui à $a in I$
    fait correspondre la dérivée de $f$ en $a$.
]

#ydef(name: [Dérivée à droite (resp. à gauche)])[
    #let_i()
    Soit $f : I -> RR$ une application et $a in I$.

    Si $frac(f (x) - f (a), x - a)$ admet une limite à droite (resp. à
    gauche) de $a$, on dit que $f$ est #emph[dérivable à droite] (resp.
    #emph[à gauche];) en $a$ et on note $f prime_d (a)$ cette limite
    (resp. $f prime_g (a)$).
]

#ybtw[
    On peut aussi (en posant $x = a + h$) regarder la limite en $0$ de
    $frac(f (a + h) - f (a), h)$.
]

#yexample[
    + Pour $n in NN$, $x |-> x^n$ est dérivable en tout point
    $a in RR$, de dérivée $a arrow.r.bar n a^(n - 1)$.

    + La fonction $x |-> 1 / x$ est dérivable en $a eq.not 0$, de
    dérivée $a |-> - 1 / (a^2)$.

    + La fonction $sqrt(.)$ n'est pas dérivable en $0$.
]


#yprop(name: [Existance de la dérivée selon l'existance de celles à droite et à gauche])[
    #let_i()
    Soit $f : I -> RR$ une application et $a in I$.

    $f$ est dérivable en $a$ si et seulement si $f$ est dérivable à gauche
    et à droite en $a$ et $f prime_d (a) = f prime_g (a)$.
]

#yproof[
    Il s'agit d'un résultat classique sur les limites.
    $qed$
]

#yexample[
    La fonction $x arrow.r.bar lr(|x|)$ est continue sur $RR$ mais non
    dérivable en $0$.
]

#yprop(name: ["Caractérisation" de la dérivée par un DL])[
    #let_i()
    Soit $f : I -> RR$ et $a in I$.

    $f$ est dérivable en $a$ si et seulement si on peut écrire pour tout $x$
    au voisinage de $a$,
    $ f (x) =_a f (a) + ell (x - a) + o (x - a) $
    Dans ce cas, on a $ell = f prime (a)$.
]

#yproof[
    Voir le chapitre sur les développements limités.
    $qed$
]

#yprop(name: [Continuité d'une fonction dérivable])[
    Si $f$ est dérivable en $a$, alors $f$ est continue en $a$.
]

#yproof[
    Si $f$ est dérivable en $a$, alors $f$ admet au voisinage de $a$
    un développement limité d'ordre 1, et donc par troncature un développement
    limité d'ordre 0, et donc $f$ est continue en $a$.
    $qed$
]

#ytheo(name: [Particulatité d'un extremum d'une fonction dérivable])[
    #let_i()
    Soit $f : I -> RR$ une application et $a in I$.

    #ymega_emph[Si] $f$ admet un extremum local en $a$ et est dérivable en $a$, alors
    $f prime (a) = 0$.
]

#yproof[
    On suppose que $f$ est dérivable en $a$ et que $f(a)$ est par exemple
    un maximum local de $f$.
    
    Soit donc $V$ un voisinage de $a$ tel que
    pour tout $x in V$, $f(x) <= f(a)$.
    
    Ainsi, pour tout $x in V sect ]-oo, a[$,
    $ (f(x)-f(a))/(x-a) >= 0 $
    
    En passant à la limite (on rappelle que $f$ est dérivable en $a$), $f'(a) >= 0$.

    Pour tout $x in V sect ]a, +oo[$,
    $ (f(x)-f(a))/(x-a) <= 0 $
    et donc $f'(a) <= 0$.

    Il en suit que $f'(a)=0$.
    $qed$
]

#ybtw[
    La réciproque est FAUSSE (par exemple la dérivée de $x arrow.r.bar x^3$ s'annule en
    $0$ sans présenter d'extremum, même local).
    Ne pas oublier qu'on a supposé depuis le début que $I$ était ouvert, cet
    énoncé ne marche pas pour une borne de l'intervalle de définition (par
    exemple $x |-> x^2$ sur $[0 , 1]$).
]

#ydef(name: [Point critique et valeur critique])[
    #let_i()
    Soit $f : I ->RR$ dérivable en $a$ ($a in I$).

    On dit que $a$ est un #emph[point critique] de $f$ si $f prime (a) = 0$.

    Dans ce cas, on dit aussi que $f (a)$ est une #emph[valeur critique] de
    $f$.
]

#ybtw[
    Pour chercher les extrema d'une fonction dérivable, on détermine les
    points où sa dérivée s'annule. Cela nous donne les candidats aux points
    où les extrema sont atteints.
    En revanche il faut ensuite vérifier s'il s'agit d'un extremum
    (parce que la réciproque de la proposition précédente est fausse).
]


=== 2. Opérations sur les fonctions dérivables

#yprop(name: [Dérivée de la somme et du produit])[
    #let_i()
    Soient $f$ et $g$ deux applications de $I -> RR$ dérivables en
    $a$.
    
    Alors
    + $f + g$ est dérivable en $a$ de dérivée $f prime (a) + g prime (a)$.

    + $f g$ est dérivable en $a$ de dérivée
        $f prime (a) g (a) + f (a) g prime (a)$.
]

#yproof[
    + On sait que pour $x$ au voisinage de $a$, 
        $ f(x) + g(x)
            yt = f(a) + (x-a)f'(a)
                ytt + o(x-a)
                ytt + g(a) + (x-a) g'(a)
                ytt + o(x-a)
            yt = f(a) + g(a)
                ytt +(x-a) (f'(a) + g'(a))
                ytt + o(x-a) $
        donc $f+g$ est dérivable en $a$ et
        $ (f+g)'(a) = f'(a) + g'(a) $

    + Pour $x$ au voisinage de $a$,
        $ f(x) g(x)
            yt = (f(a) + (x-a)f'(a)
                yttt + o(x-a))
                ytt times (g(a) + (x-a)g'(a)
                yttt + o(x-a))
            yt = f(a)g(a)
                ytt + (x-a) (f'(a)g(a)
                yttt + f(a)g'(a))
                ytt + o(x-a) $
        donc $f g$ est dérivable en $a$ et
        $ (f g)'(a) = f'(a) g(a) + f(a) g'(a) $
    $qed$
]


#yprop(name: [Dérivée de la composée])[
    Soient $I$ et $J$ deux intervalles ouverts non vides.
    Soient $f : I -> RR$, $g : J -> RR$ avec
    $f (I) subset J$, $f$ dérivable en $a$, $g$ dérivable en
    $f (a)$.
    
    Alors $g compose f$ est dérivable en $a$ et
    $ (f compose g)'(a) = f prime (a) g prime (f (a)) $
]

#yproof[
    Pour $x$ au voisinage de $a$,
    $ f(x) = &f(a) + (x-a)f'(a) \
        & + o(x-a) $
    et pour $y$ au voisinage de $f(a)$,
    $ g(y) =& g(f(a)) \
        & + (y-f(a)) g'(f(a)) \
        &+ o(y-f(a)) $

    Comme $f$ est continue en $a$, $f(x) -> f(a)$ quand $x$ tend vers $a$,
    et donc au voisinage de $a$ :
    $ g(f(x))
        yt = g(f(a))
            ytt + (f(x)-f(a))g'(f(a))
            ytt + o(f(x)-f(a))
        yt = g(f(a))
            ytt + g'(f(a))(f'(a)(x-a)
            ytt + o(x-a))
            ytt + o(f'(a)(x-a) + o(x-a))
        yt = g(f(a))
            ytt + g'(f(a))f'(a) (x-a)
            ytt + o(x-a) $

    Donc $g compose f$ est dérivable en $a$ et
    $ (g compose f)'(a) = g'(f(a))f'(a) $
    $qed$
]



#yprop(name: [Dérivée du quotient])[
    Si $f$ et $g$ sont dérivables en $a$ et $g (a) eq.not 0$.
    
    Alors $f / g$ est dérivable en $a$, de dérivée
    $ (f/g)'(a) = frac(f prime (a) g (a) - f (a) g prime (a), g (a)^2) $
]

#yproof[
    $f/g$ est le produit de $f$ et de $1/g$, et $1/g$ est
    la composée de $g$ avec la fonction inverse.
    
    Il suffit donc de prouver que la fonction inverse est
    dérivable sur $RR^*$ de dérivée $x |-> -1/(x^2)$.
]

#ybtw[
    + Tous ces énoncés sont vrais avec des fonctions dérivables sur
        $I$ et non en un point.

    + On montre ainsi la dérivabilité de fonctions explicites avec un
        raisonnement du type "$f$ est dérivable comme
        composée/somme/produit/quotient de
        fonctions dérivables".
]


#yprop(name: [Dérivée de la réciproque])[
    Soient $I$ et $J$ deux intervalles et $f : I arrow.r J$ bijective monotone,
    dérivable en $a$.
    
    Alors $f^(- 1)$ est dérivable en $f (a)$ si et seulement
    si $f prime (a) eq.not 0$ et alors
    $ (f^(-1)) ' (f(a)) = frac(1, f prime (a)) $
]

#yproof[
    D'après le chapitre sur la continuité, on sait que $f$ et $f^(-1)$ sont continues.

    Donc 
    $ lim_(y -> f(a)) (f^(-1)(y) - f^(-1)(f(a)) ) / (y - f(a))
        yt = lim_(x -> a) (x - a) /(f(x) - f(a))
        yt = 1/(f'(a))  $
]




=== 3. Dérivées d'ordre supérieur

#ydef(name: [Dérivée $n$-ième])[
    Par convention, toute fonction est $0$ fois dérivable et
    $f^((0)) = f$.
    
    On définit ensuite par récurrence la notion de
    fonction $k$ fois dérivable sur $I$ et la dérivée $k$-ième
    $f^((k))$ d'une telle fonction : $f$ est $k + 1$ fois dérivable sur $I$ si
    $f$ est $k$ fois dérivable sur $I$ et $f^((k))$ est dérivable sur $I$.
    On note alors $f^((k + 1))$ la fonction $f^((k) prime)$.
    
    On note dans ce chapitre $D^k (I,RR)$ l'ensemble
    des fonctions $k$ fois dérivable.
]

#ydef(name: [Fonction dérivable indéfiniment])[
    On dit que $f$ est #emph[indéfiniment dérivable] si $f$ est $k$ fois
    dérivable pour tout $k in N$.
]

#ydef(name: [Fonction de classe $C^n$ et $C^oo$])[
    #let_i()

    + On dit que $f$ est de classe $C^k$ sur $I$ si $f$ est $k$ fois
        dérivable et sa fonction dérivée $f^((k))$ est continue sur $I$. On
        note $C^k (I,RR)$ l'ensemble des fonctions de classe $C^k$ sur
        $I$.

    + On dit que $f$ est de classe $C^oo$ sur $I$ si $f$ est de classe $C^k$
        pour tout $k in N$. On note $C^oo (I,RR)$ l'ensemble
        des fonctions de classe $C^oo$ sur $I$.
]



#yexample[
    + La fonction exponentielle est indéfiniment dérivable (et $C^oo$) et
        pour tout $k in NN$, $exp^((k)) = exp$.

    + La fonction $x arrow.r.bar x^n$ est indéfiniment dérivable (et $C^oo$)
        et pour tout $k in NN$, sa dérivée $k$-ième est
        $x arrow.r.bar n (n - 1) dots.h (n - k + 1) x^(n - k)$. En particulier
        si $n in NN$, elle est nulle pour $k > n$.

    + La fonction $cos$ et la fonction $sin$ sont indéfiniment dérivables (et
        $C^oo$), pour $k in NN$, $cos^((k)) (x) = cos (x + k pi / 2)$ et
        $sin^((k)) (x) = sin (x + k pi / 2)$.

    + La fonction $f: x arrow.r.bar x^2 cos (1/x) $, que l'on prolonge en 0 en posant $f(0)=0$
        est continue sur $RR$, dérivable en $0$ de dérivée nulle, dérivable sur $RR^*$
        de dérivée $x|-> 2 x cos (x^(- 1)) + sin (x^(- 1))$ mais n'est pas de classe $C^1$ en 0 
        (une fonction dérivable n'est donc pas nécessairement de classe $C^1$).

    + La fonction $x arrow.r.bar 1 / x$ est indéfiniment dérivable (et
        $C^oo$) sur $R_+^*$. Sa dérivée $k$-ième est
        $x arrow.r.bar frac((-1)^k k !, x^(k + 1))$.

    + Les fonctions $"ch"$ et $"sh"$ sont de classe 
        $C^oo$ sur $RR$ et $forall n in NN$, $"ch"^((n)) = "ch"$
        si $n$ est pair, $"sh"$ sinon, $"sh"^((n)) = "ch"$ si $n$ est
        impair, $"sh"$ sinon.
]

#yprop(name: [Relations d'inclusions entre les fonctions de classe $C^n$ et dérivables $n$-fois])[
    #let_i()

    Pour tout $k in NN$,
    $ D^k (I,RR) supset.neq C^k (I,RR) supset.neq D^(k+1) (I,RR) $
    et toutes ces inclusions sont strictes.

    Une fonction indéfiniment dérivable est de classe $C^oo$.
]

#yproof[
    Soit $f$ dérivable $k+1$ fois. Alors $f$ est dérivable $k$ fois et $f^((k))$ est dérivable, donc $f^((k))$ est continue, donc $f$ est de classe $C^k$.
    $qed$
]

#yprop(name: [Dérivée $n$-ième de la somme])[
    Soient $f$ et $g$ deux fonctions dérivables $k$ fois en $a$ (resp. de classe $C^k$.)
    Alors $f+g$ est dérivable $k$ fois (resp. de classe $C^k$) et
    $ (f+g)^((k))(a) = f^((k))(a) + g^((k))(a) $
]

#yproof[
    Par récurrence sur $k$.
    $qed$
]


#ytheo(name: [Théorème de Leibniz])[
    Si $f$ et $g$ sont $k$ fois dérivables en $a$ (resp. de classe $C^k$), alors $f g$ est $k$ fois
    dérivable en $a$ (resp. de classe $C^k$) et
    $ (f g)^((k))(a) = sum_(i = 0)^k binom(k, i) f^((i))(a) g^((k - i))(a) $
]

#yproof[
    Par récurrence (similaire à la preuve de la formule du binôme de Newton).
    $qed$
]


#yprop(name: [Dérivabilité $n$-ième (resp. classe $C^n$-ité) de la composée])[
    #let_i()

    Si $f : I -> RR$ est $k$ fois dérivable en $a$ (resp. de classe $C^k$)
    et $g : J -> RR$ est $k$ fois dérivable en $f(a)$ (resp. de classe $C^k$),
    alors $g compose f$ est $k$ fois dérivable en $a$ (resp. de classe $C^k$.)
]

#yproof[
    Par récurrence sur $k$.
    
    On sait que ce résultat est vrai pour $k=1$.

    Soit $k in NN^*$ tel que la proposition ci-dessus soit vraie.
    
    Soient $f$ dérivable $k+1$ fois en $a$ (resp. de classe $C^(k+1)$) et $g$ dérivable $k+1$ fois en $f(a)$ (resp. de classe $C^(k+1)$). 

    Donc $f'$ est dérivable $k$ fois en $a$ (resp. de classe $C^k$) et $g'$ est dérivable $k$ fois en $f(a)$ (resp. de classe $C^k$) et donc $(g' compose f) times f'$ est dérivable $k$ fois en $a$ (resp. de classe $C^k$) et donc $g compose f$ est dérivable $k+1$ fois en $a$ (resp. de classe $C^(k+1)$).
    $qed$
] 


#ycor(name: [Dérivabilité $n$-ième (resp. classe $C^n$-ité) du quotient])[
    (Corollaire de la dérivabilité $n$-ième (resp. classe $C^n$-ité) de la composée)

    Si $f$ et $g$ sont dérivables $k$ fois en $a$ (resp. de classe $C^k$), avec $g(a) != 0$,
    alors $f / g$ est dérivable $k$ fois en $a$ (resp. de classe $C^k$).
    $qed$
]