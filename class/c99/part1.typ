#import "../../template/ytemplate.typ": *


#ypart[Action de groupe]


#ydef(name: [Groupe symétrique])[
    Soit $E$ un ensemble.

    On appelle _groupe symétrique #footnote[définition de Wikipédia]
    de $E$_ l'ensemble des applications bijectives de $E$ sur $E$
    muni de la composition d'applications (la loi $compose$).
    
    On le note $S(E)$ ou parfois $frak(S)(E)$.
]

#ydef(name: [Action de groupe])[
    Soit $(G, dot)$ un groupe et $X$ un ensemble.

    On appelle _action du groupe $G$ sur $X$_
    un morphisme de groupes $ phi: G -> S(X) $ qui
    vérifie, pour tout $(g, h) in G^2$,
    $ phi(g dot h) = phi(g) underbrace(dot, compose) phi(h) $
]

#ynot(name: [Allègement des notation pour les action de groupe])[
    Soit $phi$ une action de $G$ sur $X$.

    Pour $x in X$, on écrit $g dot x$ à la place de
    $phi(g)(x)$ s'il n'y a pas d'ambiguité.

    Exemple : on a alors, pour $(g,h) in G^2$,
    $ forall x in X, (g dot h) dot x = g dot (h dot x) $
]

#yprop(name: [Propriété directes de la définition d'action de groupe])[
    Soit $phi$ une action de $G$ sur $X$.

    + Pour $(g,h) in G^2$,
        $ forall x in X, (g dot h) dot x = g dot (h dot x) $

    + Pour $x in X$, $1_G dot x = x$
]

#yproof[
    + C'est une réécriture de la propriété de la définition.

    + Soit $x in X$.
        $ e dot x &= phi(e)(x) \
            &=id_X (x) \
            &= x $
]

#yprop(name: [Action de groupe comme loi externe])[
    Soit $phi: G times X -> X$ telle que

    + Pour $(f, g) in G^2$ et $x in X$,
        $ phi(g dot h, x) = phi(g, phi(h, x)) $
    + $ forall x in X, phi(1_G, x) = x $

    On pose
    $ phi: &G -> S(x) \
        &g |-> (x |-> phi(g, x)) $
    $phi$ est une action de groupe.
]