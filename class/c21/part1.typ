#import "../../template/ytemplate.typ": *


#ypart[Espace probabilisé]


On va donner une définition précise de ce qu'est une probabilité. Pour
cela, on décide de représenter mathématiquement les événements par des
#emph[ensembles]. Dans ce contexte, le résultat d'une expérience
aléatoire est un élément d'un ensemble noté habituellement $Omega$ et
appelé #emph[univers]. Cette année, nous n'étudierons les probabilités
que sur des univers finis (donc pas de loi normale !). Les événements
sont alors des sous-ensembles de $Omega$.


#yexample[
On considère l'expérience aléatoire qui consiste à lancer deux fois de
suite un dé à 6 faces.

Les résultats de cette expérience sont des couples d'entiers compris
entre 1 et 6. Donc on pose
$Omega eq brace.l 1 comma dot.basic dot.basic dot.basic comma 6 brace.r^2$.

L'événement ''la somme des chiffres vaut 7'' correspond alors à
l'ensemble
$A eq brace.l lr((i comma j)) in Omega divides i plus j eq 7 brace.r$.

]
#ydef(name: [Probabilité et espace probabilisé fini])[
  #set enum(numbering: "(P1)")
Soit $Omega$ un ensemble fini.

On appelle #emph[probabilité] sur $Omega$
toute application $P$, définie sur $cal(P) lr((Omega))$, à valeurs dans
$lr([0 comma 1])$ et vérifiant les propriétés suivantes : 

+ $P lr((Omega)) eq 1$ 

+ Pour tout $A comma B in cal(P) lr((Omega))$ tel que $A sect B eq nothing$,
    $ P lr((A union B)) eq P lr((A)) plus P lr((B)) $

Dans ce cas, on dit que $lr((Omega comma P))$ est un #emph[espace
probabilisé].
]

#ydef(name: [Vocabulaire sur les événements])[
Soit $(Omega, P)$ un espace probabilisé fini.

- $Omega$ est appelé l'_univers_ des résultats possibles.

- Les éléments de $cal(P) lr((Omega))$ sont alors appelés
#emph[événements].

- Les événements de la forme $brace.l omega brace.r$
avec $omega in Omega$ sont dit #emph[élémentaires].

- Si $A$ et $B$ sont deux événements tels que $A sect B eq nothing$, alors
on dit que ces événements sont #emph[incompatibles].

- $Omega$ est l'événement _certain_ et $emptyset$ est l'événement _impossible_.
]

#yprop(name: [Équiprobabilité])[
Soit $Omega$ un ensemble fini et
$ P colon &cal(P)(Omega) &-> [0, 1] \
    &A &arrow.r.bar frac(|A|, abs(Omega)) $
appelée l'équiprobabilité.

Alors 
+ $P$ est une probabilité sur $Omega$.
+ $forall (omega, omega') in Omega^2, P({omega}) = P({omega'})$.
]

#yproof[ 
+ Montrons que $P$ est une probabilité.

  + Pour tout $A in cal(P)(Omega)$, $0 <= |A| <= |Omega|$ et donc $P(A) in [0,1]$.

  + $P(Omega) = abs(Omega)/(|Omega|) = 1$.

  + Soit $(A, B) in cal(P)(Omega)^2$ tel que $A sect B = nothing$.
  
    Alors $|A union B| = |A| + |B|$ et donc
    $ P(A union B) = P(A)+P(B) $

+ Pour tout $omega in Omega$, $P({omega}) = 1/(|Omega|)$.
]

#block[
On modélise souvent une expérience aléatoire avec une équiprobabilité
quand on a des raisons de penser que les événements élémentaires sont
aussi fréquents les uns que les autres. Dans les exercices, si l'énoncé
ne précise pas ce qu'il entend par ''au hasard'', on modélisera
l'expérience par l'équiprobabilité.

]
#yexample[
On reprend l'exemple précédent : on lance deux fois un dé que l'on
suppose parfaitement équilibré. On peut alors faire
l'#strong[#emph[hypothèse]] que tous les résultats sont équiprobables.
Dans ce cas, l'espace probabilisé correspondant à cette expérience
aléatoire est $lr((Omega comma P))$ avec
$Omega eq brace.l 1 comma dot.basic dot.basic dot.basic comma 6 brace.r^2$
et $P colon A in cal(P) lr((Omega)) arrow.r.bar frac(|A|, 36)$.

Ainsi, la probabilité (par rapport au modèle que l'on vient de définir)
que la somme des dés fasse 7, événement noté $A$, est $6 / 36 eq 1 / 6$
puisque
$A eq brace.l lr((1 comma 6)) comma lr((2 comma 5)) comma lr((3 comma 4)) comma lr((4 comma 3)) comma lr((5 comma 2)) comma lr((1 comma 6)) brace.r$.

]
#yprop(name: [Probabilité de $n$ événements incompatibles])[
Soit $(Omega, P)$ un espace probabilisé fini.

Soient $A_1 comma dots.h comma A_n$ des événements deux-à-deux
incompatibles.

Alors
$P lr((A_1 union dots.h union A_n)) eq sum_(i eq 1)^n P lr((A_i))$.

]

#yproof[Par récurrence sur le nombre d'événements.]

#ydef(name: [Système complet d'événements])[
Soient $A_1 comma dots.h comma A_n$ des événements.

On dit qu'ils
forment un #emph[système complet d'événements] si
$lr((A_1 comma dot.basic dot.basic dot.basic comma A_n))$ est une
partition de $Omega$.

]
#yprop(name: [Construction d'une probabilité avec les évènements élémentaires])[
Soit
$Omega eq brace.l omega_1 comma dot.basic dot.basic dot.basic comma omega_n brace.r$
un ensemble fini de cardinal $n$, et
$p_1 comma dot.basic dot.basic dot.basic comma p_n$ des réels
#emph[positifs] tels que $sum_(i eq 1)^n p_i eq 1$.

Pour tout
$A in cal(P) lr((Omega))$, on pose
$ P lr((A)) eq sum_(i | omega_i in A) p_i $

Alors $P$
est une probabilité sur $Omega$.

]

#yproof[
+ Soit $A in cal(P)(Omega)$.

    Alors $0 <= P(A) <= sum_(i=1)^n p_i = 1$.

+ $P(Omega) = sum_(i=1)^n p_i = 1$.

+ Soit $(A, B) in cal(P)(Omega)^2$ tel que $A sect B = nothing$.

    Alors ${i | omega_i in A union B}$ est la réunion disjointe de ${i | omega_i in A}$ et ${i | omega_i in B}$
    
    Donc $P(A union B) = P(A) + P(B)$.
]

#yprop(name: [Propriétés élémentaire sur les probabilités])[
Soit $lr((Omega comma P))$ un espace probabilisé.

Alors 
+ $P lr((nothing)) eq 0$ 

+ Pour $A in cal(P) lr((Omega))$,
    $ P lr((Omega backslash A)) eq 1 minus P lr((A)) $

+ Pour $A comma B in cal(P) lr((Omega))$
    $ &P lr((A union B)) \
        &eq P lr((A)) plus P lr((B)) minus P lr((A sect B)) $

]

#yproof[
+ $P(nothing) = P(nothing union nothing) = 2 P(nothing)$ donc $P(nothing)=0$.

+ Soit $A in cal(P)(Omega)$.

    Comme $A union (Omega without A) = Omega$ et comme cette réunion est disjointe,
    $ P(A)+P(Omega without A) = P(Omega)=1 $ et donc $P(Omega without A) = 1 - P(A)$.

+ Soit $(A, B) in cal(P)(Omega)^2$.

    On pose $C = A sect B$, $A' = A without C$ et $B'=B without C$.
    
    Les événements $A', B'$ et $C$ sont deux-à-deux incompatibles, et leur réunion est $A union B$, d'où
    $ P(A union B) = P(A') + P(B') + P(C) $
    
    Enfin, $A = A' union C$ donc $ P(A') = P(A) - P(C) $
    
    De même $P(B') = P(B) - P(C)$ et donc
    $ &P(A union B) \
        &= P(A) + P(B) - P(A sect B) $
]

#yprop(name: [Somme des probabilités d'un système complet])[
    Soit $(A_1, ..., A_n)$ une partition de $Omega$, et $P$
    une probabilité sur $Omega$.

    Alors $sum_(i = 1)^n P(A_i) = 1$. $qed$
]

#ycor(name: [Somme des probabilités des évènements élémentaires])[
    Soit $(Omega, P)$ un espace probabilisé fini.

    Alors $sum_(omega in Omega) P({omega}) = 1$. $qed$
]

#yprop(name: [Croissance d'une probabilité])[
    Soit $(Omega, P)$ un espace probabilisé fini.

    Alors $P$ est croissante.

    _I.e._, pour tout $(A, B) in P(Omega)^2$,
    $ A subset B => P(A) <= P(B) $
]

#yproof[
    Soit $(A, B) in P(Omega)^2$.
    
    On suppose $A subset B$.

    Alors
    $ cases(B = A union (B without A), A sect (B without A) = emptyset) $

    Donc
    $ P(B) &= P(A) + underbrace(P(B without A), >= 0) \
        &>= P(A) $
    $qed$
]