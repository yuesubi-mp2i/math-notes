// Espaces probabilisés finis
#import "../../template/ytemplate.typ": *

#ychap[Espaces probabilisés finis]

#include "part1.typ"
#include "part2.typ"
#include "part3.typ"
#include "part4.typ"
#include "part5.typ"