#import "../../template/ytemplate.typ": *


#ypart[Variables aléatoires]


Soit $lr((Omega comma P))$ un espace probabilisé fini.

#ydef(name: [Variable aléatoire])[
Soit $lr((Omega comma P))$ un espace probabilisé fini.

Une #emph[variable aléatoire] définie sur $Omega$ à valeurs dans un
ensemble $E$ est une application $X colon Omega arrow.r E$.
]

#ydef(name: [Vocabulaire pour les variables aléatoires])[
Soit $lr((Omega comma P))$ un espace probabilisé fini
et $X colon Omega arrow.r E$ une variable aléatoire.

- On dit que $X$ est #emph[réelle] si $E subset bb(R)$.

- On dit que $X$ est un _vecteur aléatoire_ si $X(Omega)$ est un sous ensemble d'un espace vectoriel.
]
#yprop(name: [Loi d'une variable aléatoire])[
Soit $lr((Omega comma P))$ un espace probabilisé fini
et $X colon Omega arrow.r E$ une variable aléatoire.

L'application
$ P_X colon & cal(P) lr((E)) & arrow.r & lr([0 comma 1])\
 & A & arrow.r.bar & P ( X in A) $
est une probabilité sur $E$.

On dit alors que $P_X$ est #emph[la loi de
$X$].

]


#yproof[
+ Pour tout $A in cal(P)(E)$, $X^(-1)(A) in cal(P)(Omega)$ et donc $P(X^(-1)(A)) in [0,1]$.

+ $P_X (E) = P(X^(-1)(E)) = P(Omega) = 1$.

+ Soient $A$ et $B$ deux parties disjointes de $E$.

    Alors $X^(-1)(A) sect X^(-1)(B) = nothing$ et donc
    $ P_X (A union B) yt = P(X^(-1)(A) union X^(-1)(B))
        yt = P(X^(-1)(A)) + P(X^(-1)(B))
        yt = P_X (A) + P_X (B). $
]



#ynot(name: [Notations pour les variables aléatoires])[
Soit $X colon Omega arrow.r E$ une variable aléatoire.

- Pour toute partie $A$ de $E$, l'événement $X^(minus 1) lr((A))$ est
  plutôt noté $lr((X in A))$.

- Pour tout $x in E$, l'événement $X^(minus 1) brace.l x brace.r$ est
  noté $lr((X eq x))$.

- Dans le cas où $E eq bb(R)$, on note
  - $lr((X lt.eq b)) eq X^(minus 1) lr((bracket.r minus oo comma b bracket.r))$,
  - $lr((X lt b)) eq X^(minus 1) lr((bracket.r minus oo comma b bracket.l))$,
  - $lr((X gt.eq a)) eq X^(minus 1) lr((bracket.l a comma plus oo bracket.l))$,
  - $lr((X gt a)) eq X^(minus 1) lr((bracket.r a comma plus oo bracket.l))$,
  - $lr((a lt.eq X lt.eq b)) eq X^(minus 1) lr((lr([a comma b])))$,
  - etc...

]
#ybtw[
Soit $X colon Omega arrow.r E$ une variable aléatoire. L'image de
$Omega$ est finie. Donc $P_X$ est entièrement déterminée par les nombres
$P lr((X eq x))$ pour $x in X lr((Omega))$.

]
#yexample[
On lance un dé équilibré deux fois de suite. On note alors $S$ la somme
des deux résultats obtenus. Déterminer la loi de $S$.

On modélise l'expérience aléatoire part l'espace probabilisé
$Omega eq brace.l 1 comma dot.basic dot.basic dot.basic comma 6 brace.r^2$
et $P$ l'équiprobabilité sur $Omega$. Dans ce cas,
$S colon lr((omega_1 comma omega_2)) arrow.r.bar omega_1 plus omega_2$.
L'image de $S$ est
$S lr((Omega)) eq brace.l 2 comma dot.basic dot.basic dot.basic comma 12 brace.r$.
La loi de $S$ est donnée dans le tableau ci-dessous.

#table(
  columns: 12,
  $x$,..range(2,13).map(str),
  $P(X=x)$, $1/36$,$2/36$,$3/36$,$4/36$,$5/36$,$6/36$,$5/36$,$4/36$,$3/36$,$2/36$,$1/36$
)


On peut "vérifier" nos résultats à l'aide d'une simulation informatique.
]

#ydef(name: [Fonction de répartition])[
Soit $lr((Omega comma P))$ un espace probabilisé fini
et $X colon Omega arrow.r RR$ une variable aléatoire réelle.

La #emph[fonction de répartition] d'une variable aléatoire réelle $X$
est la fonction
$ F_X colon &RR -> [0, 1] \ &x arrow.r P lr((X lt.eq x)) $
]
#yexample[
On reprend l'exemple de la somme des deux dés. Le graphe de la fonction
de répartition de $S$ est donné ci-dessous.

]
#align(center)[

]
#yprop(name: [Loi d'une variable avec la fonction de répartition])[
La loi d'une variable aléatoire réelle est entièrement déterminée par sa
fonction de répartition.
]

#ynot(name: [Composition avec une variable aléatoire])[
    Soit $(Omega, P)$ un espace probabilisé fini, $E$ et $F$
    deux ensembles, $X : Omega -> E$ une variable aléatoire et $f: E -> F$.

    On note $f(X)$ à la place de $f compose X$.
]