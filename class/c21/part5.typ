#import "../../template/ytemplate.typ": *


#ypart[Variance]


#ydef(name: [Variance et écart-type d'une variable aléatoire])[
Soit $X colon Omega arrow.r bb(R)$ une variable aléatoire.

La
#emph[variance de $X$] est
$ &V lr((X))\ &eq E lr((lr([X minus E lr((X))])^2)) \
    &= sum_(x in X(Omega)) (x - E(X))^2 P(X = x) \
    &>= 0 $

L'#emph[écart-type] de $X$ est $sigma_X eq sqrt(V lr((X)))$.

]
#ytheo(name: [Théorème de Koenig-Huygens])[
Soit $X$ une variable aléatoire réelle. On a
$ V lr((X)) eq E lr((X^2)) minus E lr((X))^2 dot.basic $

]

#yproof[
  $ V(X) &= E((X - E(X))^2)
    yt = E(X^2 - 2 X E(X) + E(X)^2)
    yt = E(X^2) -2 E(X) E(X)
        ytt + E(X)^2
    yt = E(X^2) - E(X)^2. $
]

#yprop(name: [Variance des lois usuelles])[
Soit $X$ une variable aléatoire réelle.

+ Si $X tilde.op cal(B) lr((p))$ alors $V lr((X)) eq p lr((1 minus p))$.

+ Si $X tilde.op cal(B) lr((n comma p))$ alors
  $ V lr((X)) eq n p lr((1 minus p)) $

]

#yproof[
+ Comme $X$ suit une loi de Bernoulli de paramètre $p$, $X^2$ aussi et donc $E(X^2)=p$, de sorte que
    $ V(X) = p-p^2 = p(1-p) $

+ On peut effectuer le calcul (formule de Koenig-Huygens et formule de transfert), ou utiliser un théorème sur la variance d'une somme de variables indépendantes (voir le prochain chapitre de probabilités).
]

#ybtw[
La variance n'est pas linéaire mais quadratique.

]
#yprop(name: [Variance de $a X + b$])[
Soit $X$ une variable aléatoire.
$ forall a comma b in bb(R) comma med V lr((a X plus b)) eq a^2 V lr((X)) dot.basic $

]

#yproof[Soit $(a, b) in RR^2$.

On pose $Y = a X + b$.

Ainsi $E(Y) = a E(X) + b$ et donc
$ V(Y) &= E((Y-E(Y))^2) \
    &= E(a^2 (X-E(X))^2) \
    &= a^2 V(X) $
]


#ycor(name: [Varible aléatoire centrée réduite])[
Soit $X$ une variable aléatoire réelle de variance non nulle.

Alors
$frac(X minus E lr((X)), sigma_X)$ est une variable aléatoire
d'espérance nulle et d'écart-type 1 : on dit que cette variable est
#emph[centrée réduite].

]
#ytheo(name: [Inégalité de Bienaymé-Tchebychev])[ Soit $X$ une variable aléatoire
réelle.

Pour $t gt 0$,
$ P lr((lr(|X minus E lr((X))|) gt.eq t)) lt.eq frac(V lr((X)), t^2) dot.basic $

]

#yproof[On exploite l'inégalité de Markov sur la variable $Y = |X - E(X)|$.]


#ybtw[ Avec les notations précédentes,
$ forall a gt 0 comma med P lr((lr(|X minus E lr((X))|) gt.eq a sigma_X)) lt.eq 1 / a^2 dot.basic $]


#yexample[On répète $n$ fois à l'identique une expérience de Bernoulli qui a une probabilité $p$ de réussir. Comment choisir $n$ pour être sûr à 95% que la fréquence de succès soit égale à $p$ à $10^(-2)$ près ?

Pour tout $i in bracket.l.double 1, n bracket.r.double$, on note $X_i$ la variable aléatoire qui prend la valeur 1 si la $i$-ème expérience est un succès, 0 sinon. On note aussi $S_n = sum_(i=1)^n X_i$ le nombre de succès et $Y_n = 1/n S_n$ la fréquence de succès. Le problème consiste donc à trouver $n$ pour que $P(|Y_n - p| < 10^(-2)) >= 95%$. 

D'après l'inégalité de Bienaymé-Tchebychev, $P(|Y_n - p| < 10^(-2)) >= 1- 10000 V(Y_n))$.

De plus, $S_n$ suit une loi binomiale de paramètres $n$ et $p$, donc $V(Y_n) = 1/(n^2) V(S_n) = (p (1-p))/n <= 1/(4 n)$ car la fonction quadratique $p|->p(1-p)$ atteint son maximum en $1/2$.

D'où 

$ P(|Y_n -p| < 10^(-2)) >= 0.95 &<== 1- 2500/n >= 95/100 <== n >= 2500 times 20 = 50 000. $

Il suffit donc de répéter l'expérience cinquante mille fois.

]
