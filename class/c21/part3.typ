#import "../../template/ytemplate.typ": *


#ypart[Lois usuelles]


=== 1. Loi uniforme
#block[
On lance un dé à 20 faces équilibré et on note $X$ le résultat. Quelle
est la loi de $X$ ?

]
#ydef(name: [Loi uniforme])[
Soit $(Omega, P)$ un espace probabilisé fini
et $X colon Omega arrow.r E$ une variable aléatoire.

On dit que
$X$ suit la loi #emph[uniforme] si $P_X$ est l'équiprobabilité sur
$X lr((Omega))$, c'est-à-dire
$ forall x in X lr((Omega)) comma P lr((X eq x)) eq 1 / abs(Omega) $

En notant
$X lr((Omega)) eq brace.l x_1 comma dot.basic dot.basic dot.basic comma x_N brace.r$,
on note cette situation
$X tilde.op cal(U) lr((x_1 comma dot.basic dot.basic dot.basic comma x_N))$.

]
=== 2. Loi de Bernoulli

#ydef(name: [Loi de Bernoulli])[
Soit $(Omega, P)$ un espace probabilisé fini
et $X$ une variable aléatoire sur $Omega$.

On dit que
$X$ suit une #emph[loi de Bernoulli de paramètre $p$] si
+ $X lr((Omega)) subset brace.l 0 comma 1 brace.r$
+ $P lr((X eq 1)) eq p$.

Dans ce cas, on note $X tilde.op cal(B) lr((p))$.

#strong[Situation classique] On considère une expérience aléatoire qui a
deux résultats possibles : succès ou échec. On note $p$ la probabilité
de succès et $X$ la variable aléatoire qui vaut 0 en cas d'échec et 1 en
cas de succès. Quelle est la loi de $X$ ?
]
=== 3. Loi binomiale
#ydef(name: [Loi binomiale])[
Soit $(Omega, P)$ un espace probabilisé fini
et $X$ une variable aléatoire sur $Omega$.

On dit que
$X$ suit la #emph[loi binomiale de paramètres $n in bb(N)$ et
$p in lr([0 comma 1])$] si

+ $X lr((omega)) subset [|0, n|]$

+ pour tout $k in X lr((Omega))$,
    $ P lr((X eq k)) eq binom(n, k) p^k lr((1 minus p))^(n minus k) $

On note alors $X tilde.op cal(B) lr((n comma p))$.

#strong[Situation classique] On considère une expérience aléatoire qui a
deux résultats possibles : succès ou échec. On note $p$ la probabilité
de succès. On répète $n$ fois cette expérience aléatoire à l'identique.
On note $X$ le nombre de succès obtenus. Quelle est la loi de $X$ ?

]
#align(center)[

]
=== 4. Loi hypergéométrique (Hors-programme)

#ydef(name: [Loi hypergéométrique])[
Soit $(Omega, P)$ un espace probabilisé fini
et $X$ une variable aléatoire sur $Omega$.

On dit que
$X$ suit la #emph[loi hypergéométrique de paramètres $N, n, k$] si

+ $X lr((Omega)) subset [|0, k|]$

+ pour $i in [|0, k|]$,
    $ P lr((X eq i)) &= abs(X^(-1) {i})/abs(Omega) \
        &= frac(binom(n, i) binom(N minus n, k minus i), binom(N, k)) $

On note alors $X tilde.op cal(H) lr((N comma n comma k))$.

#strong[Situation classique] On dispose d'un stock de $N$ pièces, dont
$n$ sont défectueuses. On prélève simultanément $k$ pièces et on note
$X$ le nombre de pièces défectueuses dans l'échantillon de $k$ pièces.
Quelle est la loi de $X$ ?

On pose $ Omega = lr({ A in cal(P)([|1, n|]) | abs(A) = k}) $
Alors $abs(Omega) = binom(N, k)$.
]
#align(center)[

]