#import "../../template/ytemplate.typ": *


#ypart[Espérance d'une variable aléatoire réelle]


#ydef(name: [Éspérence d'un variable aléatoire])[
Soit $X colon Omega arrow.r bb(R)$ une variable aléatoire.

#emph[L'espérance] de $X$ est le réel
$ E lr((X)) eq sum_(x in X lr((Omega))) x P lr((X eq x)). $

]
#yprop(name: [Espérance des lois usuelles])[
+ Si
  $X tilde.op cal(U) lr((x_1 comma dot.basic dot.basic dot.basic comma x_N))$
  alors $E lr((X))$ est la moyenne des réels
  $x_1 comma dot.basic dot.basic dot.basic comma x_N$.

+ Si $X tilde.op cal(B) lr((p))$, alors $E lr((X)) eq p$.

+ Si$X tilde.op cal(B) lr((n comma p))$ alors $E lr((X)) eq n p$.

]

#yproof[
+ $ E(X) &= sum_(i=1)^n P(X=x_i) x_i \
        &= 1/n sum_(i=1)^n x_i $

+ $ E(X) &= sum_(i=0)^1 i P(X=i) \
        &= p $

+ $ E(X) &= sum_(i=0)^n i P(X=i) \
    &= sum_(i=1)^n i binom(n, i) p^i q^(n-i) \
    &= n p sum_(i=1)^(n) binom(n-1, i-1) p^(i-1) q^(n-i) \
    &= n p (p+q)^(n-1) \
    &= n p $
]

#ylemme(name: [Expression alternative de l'espérance])[
Soit $X colon Omega arrow.r bb(R)$.

Alors
$display(E lr((X)) eq sum_(omega in Omega) X lr((omega)) P lr((brace.l omega brace.r)))$.

]

#yproof[
  $ sum_(omega in Omega) X(omega) P({omega})
    yt = sum_(x in X(Omega)) sum_(omega in Omega\ X(omega) = x) X({omega}) P({omega})
    yt = sum_(x in X(Omega)) x sum_(omega in Omega\ X(omega)=x) P({omega})
    yt = sum_(x in X(Omega)) x P(X=x)
    yt = E(X) $
]


#ytheo(name: [Linéarité de l'espérence])[
L'espérance est linéaire :
pour $X comma Y in bb(R)^Omega$ et $(lambda comma mu) in bb(R)^2$,
    $ E lr((lambda X plus mu Y)) eq lambda E lr((X)) plus mu E lr((Y)) dot.basic $

]

#yproof[Soient $X$ et $Y$ deux variables aléatoires réelles et $lambda, mu$ deux réels.
$ E(lambda X + mu Y)
    yt = sum_(omega in Omega) (lambda X(omega) + mu Y(omega)) P({omega})
    yt = lambda sum_(omega in Omega) X(omega) P({omega})
        ytt + mu sum_(omega in Omega) Y(omega)P({omega})
    yt = lambda E(X) + mu E(Y). $
]

#yexample[
Déterminer l'espérance d'une variable aléatoire suivant une loi
hypergéométrique.

]
#ytheo(name: [Formule de transfert])[
 Soit $X colon Omega arrow.r bb(R)$ une variable
aléatoire et $f colon bb(R) arrow.r bb(R)$.
$ E lr((f lr((X)))) eq sum_(x in X lr((Omega))) f lr((x)) P lr((X eq x)) dot.basic $

]

#yproof[
  $ E(f(X)) 
    yt = sum_(omega in Omega) f(X(omega)) P({omega})
    yt = sum_(x in X(Omega)) sum_(omega in Omega\ X(omega)=x) f(X(omega)) P({omega})
    yt = sum_(x in X(Omega)) f(x) sum_(omega in Omega\ X(omega)=x) P({omega})
    yt = sum_(x in X(Omega)) f(x) P(X=x). $
]


#ybtw[
Grâce à la formule de transfert il n'est pas nécessaire de déterminer la
loi de $f lr((X))$ pour calculer son espérance.

]
#ytheo(name: [Inégalité de Markov])[
Soit $X colon Omega arrow.r bb(R)^plus$ une variable aléatoire positive
ou nulle.
$ forall a gt 0 comma med P lr((X gt.eq a)) lt.eq frac(E lr((X)), a) dot.basic $

]

#yproof[
  Soit $a > 0$.
$ E(X)
    yt = sum_(x in X(Omega)) x P(X = x)
  yt = sum_(x in X(Omega)\ x < a) x P(X=x)
        ytt + sum_(x in X(Omega)\ x >= a) x P(X=x)
  yt >= sum_(x in X(Omega)\ x >= a) x P(X=x)
  yt >= a sum_(x in X(Omega)\ x >= a) P(X=x)
  yt >= a P(X=a). $

]