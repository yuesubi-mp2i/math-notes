#import "../../template/ytemplate.typ": *


#ypart[Produit scalaire]


#ydef(name: [Produit scalaire])[
    Soit $E$ un $RR$-ev.

    Un _produit scalaire_ sur $E$ est une application
    $ phi: E times E -> RR$
    telle que

    #set enum(numbering: "(PS1)")

    + pour tout $v in E$, l'application
        $ &E &-> RR \
            &u &|-> phi(u, v) $
        est linéaire. \
        (linéarité à gauche)
    
    + $forall (u,v) in E^2, phi(u,v) = phi(v,u)$ \
        (symétrie)
    
    + $forall u in E, phi(u,u) >= 0$ \
        ($phi$ est positive)        

    + $forall u in E, (phi(u,u) = 0 => u = 0_E)$ \
        ($phi$ est définie)

    En d'autre termes, un produit scalaire
    est une _forme bilinéaire symétrique définie positive_.
]

#ydef(name: [Espace préhilbertien])[
    Soit $phi$ un produit scalaire sur un $RR$-ev $E$.

    Alors on dit que $(E, phi)$ est un _espace préhilbertien_.
]

#ydef(name: [Espace euclidien])[
    Un _espace euclidien_ $(E, phi)$ est un
    _espace préhilbertien_ de dimension finie.
]

#yprop(name: [Produit scalaire canonique de $RR^n$])[
    L'application
    $ RR^n times RR^n &-> RR \
        ((x_1, ..., x_n), (y_1, ..., y_n)) &|-> sum_(i=1)^n x_i y_i $
    est un produit scalaire.

    C'est le produit scalaire _canonique_ sur $RR^n$.
]

#yproof[
    On vérifie les propriétés du produit scalaire.
]

#yprop(name: [Produit scalaire canonique des fonctions continues])[
    Soit $E = cal(C)^0 ([a,b], RR)$ avec $a<=b$.
    $ &E times E &-> RR \
        &(f,g) &-> integral_a^b f(t) g(t) dif t $
    est un produit scalaire.

    C'est le produit scalaire _canonique_ sur $E$.
]

#yproof[
    On vérifie les propriétés du produit scalaire.
]

#let scal(vec1,vec2) = {$lr(angle.l vec1 mid(|) vec2  angle.r)$}

#ynot(name: [Produit scalaire])[
    Souvent, un produit scalaire est noté

    $ scal(dot,dot) "ou" angle.l dot, dot angle.r $

]

#yprop(name: [Produit scalaire de deux sommes de vecteurs])[
    Soit $(E, scal(dot, dot))$ un espace préhilbertien

    Pour tout $(u, v, w, t) in E^4$,
    $ scal(u + v, w + t)
        =& scal(u,w) + scal(u,t) \
        &+ scal(v,w) + scal(v,t) $
]

#yproof[
    $ &scal(u+v, w+t) \
        &=^1 scal(u, w+t) + scal(v, w+t) \
        &=^2 scal(u, w) + scal(u, t) + scal(v, w) \
            &wide + scal(v, t) $
    + Linéarité à gauche.
    + Linéarité à droite (symétrie et linéarité à gauche).
]

#ydef(name: [Norme euclidienne])[
    Soit $(E, scal(dot, dot))$ un espace préhilbertien.
    On définit la _norme (euclidienne)_
    d'un vecteur $u in E$ par
    $ norm(u) = sqrt(scal(u, u)) in RR^+ $
]

#ytheo(name: [Inégalité de Cauchy-Schwarz])[
    Soit $(E, scal(dot, dot))$ un espace préhilbertien
    et $(u, v) in E^2$. Alors
    $ abs(scal(u,v)) <= norm(u) norm(v) $
    et
    $abs(scal(u,v)) = norm(u) norm(v)$
    ssi $u$ et $v$ sont colinéaires.
]

#yproof[
    - Cas 1: On suppose $v != 0$.

        On pose
        $ P: &RR -> RR \
            & x |-> norm(u+x v)^2 $
        
        Pour $x in RR$,
        $ P(x)
            &= scal(u + x v, u + x v) \
            &= norm(u)^2 + x scal(u, v) + x scal(v, u) \
                &wide + x^2 norm(v)^2 \
            &= norm(u)^2 + 2 x scal(u,v) + x^2 norm(v)^2 $
        Comme $v != 0$, $norm(v)^2 != 0$, donc $P$ est polynomiale
        de degré 2.

        Comme, pour tout $x in RR$,
        $ P(x) = norm(u + x v)^2 >= 0 $
        alors nécessairement, le discriminant $Delta$ de $P$
        est négatif ou nul.

        Or
        $ Delta = (2 scal(u,v))^2 - 4 norm(u)^2 norm(v)^2 $ 
        donc
        $ scal(u,v)^2 - norm(u)^2 norm(v)^2 <= 0 $ 
        et
        $ abs(scal(u,v)) <= norm(u) norm(v) $

        De plus,
        $ &abs(scal(u,v)) = norm(u) norm(v) \
            &<=> Delta = 0 \
            &<=> exists x in RR, P(x) = 0 \
            &<=> exists x in RR, u + x v = 0 \
            &<=> u "et" v "colinéaires" $
    
    - Cas 2 :
        $abs(scal(u,v)) = 0 = norm(u) norm(v)$
        et $u$ et $v$ sont bien colinéaires.
]

#yprop(name: [Propriétés élémentaires de la norme euclidienne])[
    Soit $(E, scal(dot,dot))$ un espace préhilbertien.

    + $forall x in E, (norm(x) = 0 => x = 0)$ \
        (séparation)

    + $forall x in E, forall lambda in RR^+, norm(lambda x) = lambda norm(x)$ \
        (homogénéité positive)
    
    + $forall (x,y) in E^2, norm(x+y) <= norm(x) + norm(y) $ \
        (inégalité triangulaire)
    
    N.B. ça sera les axiomes d'une norme l'année prochaine.
]

#yproof[
    + Soit $x in E$
        $ norm(x) = 0
            &=> norm(x)^2 = 0 \
            &=> scal(x,x) = 0 \
            &=> x = 0 $
    
    + Soit $x in E$, $lambda in RR$
        $ norm(lambda x)^2
            &= scal(lambda x, lambda x) \
            &= lambda scal(x, lambda x) \
            &= lambda^2 scal(x,x) \
            &= lambda^2 norm(x)^2 $
        D'où,
        $ norm(lambda x) = abs(lambda) norm(x)$.
    
    + Soit $(x,y) in E^2$.
        $ norm(x+y)^2
            &= scal(x+y, x+y) \
            &= norm(x) + 2 scal(x,y) + norm(y) $

        D'après l'inégalité de Cauchy-Schwartz
        $ abs(scal(x,y)) <= norm(x) norm(y) $

        et donc
        $ scal(x,y) <= norm(x) norm(y) $

        D'où,
        $ norm(x+y)^2
            &<= norm(x)^2 + 2 norm(x)norm(y) + norm(y)^2 \
            &<= (norm(x) + norm(y))^2 $
        et donc
        $ norm(x+y) <= norm(x) + norm(y) $
]

#yprop(name: [Condition d'égalité de l'inégalité triangulaire pour la norme euclidienne])[
    Soit $(E, scal(dot,dot))$ un espace préhilbertien, $(x,y) in E^2$.

    $ &norm(x+y) = norm(x) + norm(y) \
        &<=> exists lambda in RR^+, x = lambda y or y = lambda x $
]

#yproof[
    $ &norm(x+y) = norm(x) + norm(y) \
        &<=> norm(x+y)^2 = norm(x)^2 + norm(y)^2 + 2 norm(x) norm(y) \
        &<=> norm(x)^2 + norm(y)^2 + 2 scal(x,y)  \
            &wide = norm(x)^2 + norm(y)^2 + 2 norm(x) norm(y) \
        &<=> scal(x,y) = norm(x) norm(y) \
        &<=> cases(scal(x,y) >= 0,
                abs(scal(x,y)) = norm(x) norm(y)) \
        &<=> cases(scal(x,y) >= 0,
                x "et" y "sont colinéaires") \
        &<=> cases(scal(x,y) >= 0,
                exists lambda in RR\, x = lambda y or y = lambda x) \
        &<=> (exists lambda in RR, cases(x = lambda y, lambda norm(y)^2 >= 0)) \
            &wide or (exists lambda in RR, cases(y = lambda x, lambda norm(x)^2 >= 0)) \
        &<=> exists lambda in RR^+, x = lambda y or y = lambda x $
]

#yprop(name: [Formules de polarisation])[
    Soit $(E, scal(dot,dot))$ un espace préhilbertien.

    Pour tout $(x,y) in E^2$,
    $ scal(x,y)
        &= 1/2 (norm(x+y)^2 - norm(x)^2 - norm(y)^2) \
        &= 1/2 (norm(x)^2 + norm(y)^2 - norm(x+y)^2) \
        &= 1/4 (norm(x+y)^2 - norm(x - y)^2) $
]

#yproof[
    Soit $(x,y) in E^2$.
    $ &1/2 (norm(x+y)^2 - norm(x)^2 - norm(y)^2) \
        &= 1/2 (norm(x)^2 + norm(y)^2 + 2 scal(x,y) \
            &wide - norm(x)^2 - norm(y)^2) \
        &= scal(x,y) $
    
    $ &1/4 (norm(x+y)^2 - norm(x-y)^2) \
        &= 1/4 (norm(x)^2 + norm(y)^2 + 2 scal(x,y) - norm(x)^2 \
            &wide - norm(y)^2 + 2 scal(x,y)) \
        &= scal(x,y) $
]

#yprop(name: [Identité du parallélogramme])[
    $ 2 (norm(x)^2 + norm(y)^2) = norm(x+y)^2 + norm(x-y)^2$
]

#yproof[
    On fait le calcul.
]

#ydef(name: [Distance d'un espace préhilbertien])[
    Soit $(E, scal(dot,dot))$ un espace préhilbertien, $(x,y) in E^2$.

    La _distance_ de $x$ à $y$ est $norm(x-y)$.

    On la note $d(x,y)$.
]

#yprop(name: [Propriétés élémentaires de la distance d'un espace préhilbertien])[
    Soit $(E, scal(dot,dot))$ un espace préhilbertien.

    + $forall (x,y) in E^2, (d(x,y) = 0 => x = y)$
        (séparation)
    
    + $forall (x,y) in E^2, d(x,y) = d(y,x)$
        (symétrie)
    
    + $forall (x,y,z) in E^3$,
        $ wide d(x,z) <= d(x,y) + d(y,z) $
        (inégalité triangulaire)
    
    N.B. ce sont les axiomes d'une distance.
    Un ensemble muni d'une distance est appelé espace métrique.
]

#yprop(name: [Innégalité triangulaire inversée])[
    Soit $(E, scal(dot, dot))$ un espace préhilbertien.

    $ forall (x, y) in E^2, abs(x - y) >= abs(abs(x) - abs(y)) $
]

#yproof[
    Soit $(x, y) in E^2$.

    Avec l'inégalité triangulaire classique
    $ norm(x - y) + norm(y) >= norm(x - y + y) $
    donc
    $ norm(x - y) >= norm(x) - norm(y) $

    De façon symétrique,
    $ norm(y - x) >= norm(y) - norm(x) $

    Ainsi
    $ norm(x - y) >= abs(norm(x) - norm(y)) $
]