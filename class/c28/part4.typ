#import "../../template/ytemplate.typ": *


#ypart[Distance à un sous-ev]


#let scal(vec1,vec2) = {$lr(angle.l vec1 mid(|) vec2  angle.r)$}
#let letep() = {[Soit $(E, scal(dot,dot))$ un espace préhilbertien]}


#ydef(name: [Distance à un ensemble])[
    #letep(), $A in cal(P)(E)$ non vide et $x in E$.

    La _distance de $x$ à $A$_ est
    $ d(x,A) = inf_(u in A) norm(x-y) $
]

#ytheo(name: [Formule de la distance à un sous-ev])[
    #letep(), $F$ un sous-ev de dimension finie de $E$
    et $x in E$. Alors
    $ d(x, F) = norm(x - p_F (x)) $
    où $p_F$ est la projection orthogonale sur $F$.
]

#yproof[
    Soit $y in F$.
    $ x = underbrace(p_F (x), in F) + underbrace((x - p_F (x)), in F^perp) $
    $ &norm(x-y)^2 \
        &= norm(underbrace((x- p_F (x)), in F^perp) +
            underbrace((p_F (x) - y), in F)) $
    
    D'après le théorème de Pythagore
    $ norm(x-y)^2
        &= norm(x - p_F (x))^2 \
            &wide + norm(p_F (x) - y)^2 \
        &= norm(x - p_F (x))^2 $
    
    Donc $norm(x - p_F (x))$ \
    minore ${ norm(x-y) | y in F}$. \
    et $norm(x-p_F (x)) in {norm(x-y) | y in F}$.

    Donc $norm(x - p_F (x)) = min_(y in F) norm(x-y) $.
]