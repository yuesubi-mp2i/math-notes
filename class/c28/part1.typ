#import "../../template/ytemplate.typ": *


#ypart[Rappels et motivations]


=== Rappel

#let norm(vector) = {$abs(abs(vector))$}

$ arrow(u) dot arrow(v) in RR $
$ = norm(arrow(u)) norm(arrow(v)) cos(arrow(u), arrow(v)) $
$ = x x' + y y' $
où $(x, y)$ coordonnées de $arrow(u)$
$(x', y')$ coordonnées de $arrow(v)$
dans une base orthonormée.

$ norm(arrow(u))^2 = arrow(u) dot arrow(u) in RR^+ $
$ arrow(u) perp arrow(v) <=> arrow(u) dot arrow(u) = 0 $