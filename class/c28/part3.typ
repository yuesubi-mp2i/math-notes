#import "../../template/ytemplate.typ": *


#ypart[Orthogonalité]


#let scal(vec1,vec2) = {$lr(angle.l vec1 mid(|) vec2  angle.r)$}

#let letep() = {[Soit $(E, scal(dot,dot))$ un espace préhilbertien]}
#let letee() = {[Soit $(E, scal(dot,dot))$ un espace euclidien]}


#ydef(name: [Vecteurs orthogonaux])[
    #letep().

    Deux vecteurs $x$ et $y$ sont _orthogonaux_
    si $scal(x,y) = 0$.

    On note alors $x perp y$.
]

#ydef(name: [Famille orthogonale et orthonormale de vecteurs])[
    #letep()
    et $(u_i)_(i in I)$ une famille de vecteurs.

    On dit que cette famille est _orthogonale_ si
    $ forall i != j, u_i perp u_j $

    On dit que cette famille est _orthonormale_ (ou _orthonormée_)
    si
    $ cases(forall i != j\, u_i perp u_j,
        forall i\, norm(u_i) = 1) $
]

#yprop(name: [Libertée d'une famille orthogonale de vecteurs])[
    Toute famille orthogonale de vecteurs non nuls est libre.
]

#yproof[
    Soit $(u_i)_(i in I)$ une famille orthogonale
    telle que
    $ forall i in I, u_i != 0$.

    Soit $(lambda_i)_(i in I)$ une famille presque nulle de réels.

    On suppose que $sum_(i in I) lambda_i u_i = 0$.

    Soit $j in I$.

    D'une part,
    $ scal(u_j, sum_(i in I) lambda_i u_i) = scal(u_j, 0) = 0 $
    d'autre part
    $ scal(u_j, sum_(i in I) lambda_i u_i)
        &= sum_(i in I) lambda_i scal(u_j, u_i) \
        &= lambda_j underbrace(norm(u_j)^2, != 0) $
    donc
    $lambda_j = 0$.
]

#ytheo(name: [Théorème de Pythagore])[
    #letep()
    et $(u_i)_(1<=i<=n)$ une famille orthogonale finie.

    $ norm(sum_(i=1)^n u_i)^2 = sum_(i=1)^n norm(u_i)^2 $
]

#yproof[
    $ norm(sum_(i=1)^n u_i)^2 
        &= scal(sum_(i=1)^n u_i, sum_(i=1)^n u_i) \
        &= sum_(i=1)^n sum_(j=1)^n scal(u_i, u_j) \
        &= sum_(i=1)^n scal(u_i,u_i) \
        &= sum_(i=1)^n norm(u_i)^2 $
]

#ytheo(name: [Décomposition d'un vecteur dans une base orthogonale])[
    #letep()
    et $(e_i)_(i in I)$ une base orthonormée  de $E$ et $x in E$.
    $ x = sum_(i in I) scal(x, e_i) e_i $
]

#yproof[
    Soit $(x_i)_(i in I)$ une famille presque nulle
    de scalaires telle que
    $ x = sum_(i in I) x_i e_i $

    Soit $j in I$.
    $ scal(x, e_j)
        &= scal(sum_(i in I) x_i e_i, e_j) \
        &= sum_(i in I) x_i scal(e_i, e_j) \
        &= x_j $
]

#ycor(name: [Norme d'un vecteur dans une base orthonormée])[
    #letee(), $(e_1, ..., e_n)$ une base orthonormée
    de $E$ et $x in E$.
    $ norm(x)^2 = sum_(i=1)^n scal(x, e_i)^2 $
]

#yproof[
    On décompose $x$ dans la base.
    $ x = sum_(i=1)^n underbrace(scal(x, e_i) e_i, u_i) $
    $(u_1, ..., u_n)$ est orthogonale.
    
    D'après le théorème de Pythagore
    $ norm(x)^2
        &= sum_(i=1)^n norm(u_i)^2 \
        &= sum_(i=1)^n scal(x, e_i)^2 underbrace(norm(e_i)^2, =1) $
]

#yprop(name: [Produit scalaire dans une base orthonormée])[
    #letee(), $(e_1, ..., e_n)$ une base orthonormée de $E$
    et $(x,y) in E^2$.
    $ scal(x,y) &= sum_(i=1)^n scal(x, e_i) scal(y, e_i) \
        &= sum_(i=1)^n x_i y_i $
    où $(x_1, ..., x_n)$ sont les coordonnées de $x$
    dans la base $(e_1, ..., e_n)$ et $(y_1, ..., y_n)$ celles de $y$.
]

#yproof[
    $ scal(x,y)
        &= scal(sum_(i=1)^n x_i e_i, sum_(j=1)^n y_j e_j) \
        &= sum_(i=1)^n sum_(j=1)^n x_i y_i scal(e_i, e_j) \
        &= sum_(i=1)^n x_i y_i $
]

#ymetho(name: [Procédé d'orthonormalisation de Gram-Schmidt])[
    #letee() et $cal(B) = (e_1, ..., e_n)$ une base de $E$.

    On construit une base orthonormée
    $(u_1, ..., u_n)$ de $E$ récursivement.

    - $u_1 = e_1/norm(e_1)$

    - Soit $i < n$, avec $(u_1, ..., u_i)$ déjà construite.
        On pose
        $ v_(i+1) = e_(i+1) - sum_(k=1)^i scal(e_(i+1), u_k) u_k \
            u_(i+1) = v_(i+1)/norm(v_(i+1)) $
]

#yproof[
    Montrons par récurrence que pour tout
    $i in [|1,n|]$, la propriété
    $P(i)$ : "$(u_1, ..., u_i)$ est une base orthonormée de
    $"Vect"(e_1, ..., e_i)$" est vraie.

    _Initialisation_ :
    $u_1$ est colinéaire à $e_1$ et non nul
    donc $"Vect"(u_1) = "Vect"(e_1)$.

    De plus,
    $ norm(u_1) = norm(e_1/norm(e_1)) = 1/norm(e_1) norm(e_1) = 1$

    _Hérédité_ : 
    Soit $i < n$. On suppose $P(i)$ vraie.
    
    Si $v_(i+1) = 0$, alors
    $ e_(i+1) in &"Vect"(u_1, ..., u_i) \
        &= "Vect"(e_1, ..., e_i) $
    une contradiction.

    Donc $v_(i+1) != 0.$

    Pour $j in [|1, i|]$,
    $ &scal(u_(i+1), u_j) \
        &= 1/norm(v_(i+1)) scal(v_(i+1), u_j) \
        &= 1/norm(v_(i+1)) (scal(e_(i+1), u_j) \
            &wide - sum_(k=1)^i scal(e_(i+1), u_k) scal(u_k, u_j)) \
        &= 1/norm(v_(i+1)) (scal(e_(i+1), u_j) \
            &wide - scal(e_(i+1), u_j) underbrace(norm(u_j)^2, =1)) \
        &= 1 $
    
    $norm(u_(i+1)) = 1/norm(v_(i+1)) norm (v_(i+1)) = 1$

    (...)

    De plus, pour tout $j in [|1, i|]$
    $ u_j in "Vect"(e_1, ..., e_i, e_(i+1)) $
    donc
    $ v_(i+1) in &"Vect"(e_(i+1), u_1, ..., u_i) \
        &subset "Vect"(e_1, ..., e_i, e_(i+1)) $
    
    Donc
    $ "Vect"(u_1, ..., u_(i+1)) subset "Vect"(e_1, ..., e_(i+1)) $
    or $dim("Vect"(u_1, ..., u_(i+1))) = i+1 $
    car $(u_1, ..., u_(i+1))$ est libre.

    Donc
    $ &"Vect"(u_1, ..., u_(i+1)) \
        &= "Vect"(e_1, ..., e_(i+1)) $
]

#ymetho(name: [Procédé d'orthogonalisation de Gram-Schmidt])[
    #letee() et $cal(B) = (e_1, ..., e_n)$ une base de $E$.

    On construit une base $(u_1, ..., u_n)$ orthogonale
    de $E$ de la façon suivante :

    - $u_1 = e_1$

    - pour $i < n$, soit $(u_1, ..., u_i)$ déjà construits,
        formant une base orthogonale de $"Vect"(e_1, ..., e_n)$.

        On cherche $u_(i+1) in "Vect"(e_1, ..., e_(i+1))$ non nul
        orthogonal à tous les $u_j$, $j in [|1, i|]$.

        Soit $(lambda_1, ..., lambda_(i+1)) in RR^(i+1)$.
        $ u_(i+1) = sum_(k=1)^(i+1) lambda_k e_k $

        On résout le système linéaire
        $ forall j in [|1, i|], scal(u_(i+1), u_j) = 0 $
        C'est un système d'équations linéaire à $i+1$ inconnues :
        il a au moins une solution non nulle.
]

#ycor(name: [Base orthonormée d'un espace euclidien])[
    Tout espace euclidien a au moins une base orthonormée.
]

#yproof[
    C'est un corollaire de l'un des procédés de Gram-Schmidt.
]

#ydef(name: [Orthogonal d'un ensemble])[
    #letep(), et $A in cal(P)(E)$.
     
    L'_orthogonal_ de $A$ est
    $ A^perp = { u in E | forall a in A, u perp a } $
]

#yprop(name: [Structure d'un orthogonal])[
    #letep() et $A in cal(P)(E)$.

    Alors $A^perp$ est un sous-ev de $E$.
]

#yproof[
    $ A^perp = sect.big_(a in A) ker(u |-> scal(u, a)) $ 
]

#yprop(name: [Relation entre le sous-ev engendré et l'orhogonal de l'orthogonal, pour un ensemble fixé])[
    #letep().

    $ forall A in cal(P)(E), "Vect"(A) subset (A^perp)^perp $
]

#yproof[
    Soit $A in cal(P)(E)$.

    Soit $a in A$.
    $ a in (A^perp)^perp <=> forall u in A^perp, scal(u, a) = 0 $

    Or, pour $u in A^perp$, $u$ est orthogonal à tout vecteur de $A$, donc $u perp a$.

    Ainsi $A subset (A^perp)^perp$.

    Comme $(A^perp)^perp$ est un sous-ev de $E$
    $ "Vect"(A) subset (A^perp)^perp $
]

#yprop(name: [Intersection d'un ensemble et de son orthogonal])[
    #letep()
    et $F$ un sous-ev de $E$. Alors
    $ F sect F^perp = {0} $
]

#yproof[
    Soit $x in F sect F^perp$.

    $x in F^perp$ donc $forall u in F, scal(x,y) = 0$.

    En particulier, comme $x in F$,
    $ scal(x,x) = 0$.

    Donc $x = 0$.
]

#ytheo(name: [Supplémentaire orthogonal d'une sous-ev])[
    #letep() et $F$ un sous-ev #ymega_emph[de dimension finie]
    de $E$.

    Alors $F xor F^perp = E$.

    On dit alors que $F^perp$ est le _supplémentaire orthogonal_.
]

#yproof[
    Soit $(e_1, ..., e_p)$ une base orthogonormée de $F$.
    Soit $x in E$.

    On pose
    $ u = sum_(k=1)^p scal(x,e_k) e_k in F \
        v = x - u $
    
    Ainsi
    $ cases(x = u + v, u in F) $

    Montrons que $v in F^perp$.

    Soit $i in [|1, p|]$.
    $ scal(v, x_i)
        &= scal(x, e_i) - scal(u, e_i) \
        &= scal(x,e_i) - sum_(k=1)^p scal(x,e_k) scal(e_k, e_i) \
        &= scal(x, e_i) - scal(x,e_i) underbrace(norm(e_i)^2, =1) \
        &= 0 $
    
    Ainsi
    $ v in {e_1, ..., e_p}^perp
        &= "Vect"(e_1, ..., e_p)^perp \
        &= F^perp $
]

#ydef(name: [Projection orthogonale])[
    #letep() et $F$ un sous-ev de dimension finie de $E$.

    La _projection orthogonale_ sur $F$
    est la projection sur $F$ parallèlement à $F^perp$.
]

#yprop(name: [Inégalité de Bessel])[
    #letep() et  $p$ un projection orthogonale.
    $ forall x in E, norm(p(x)) <= norm(x) $
]

#yproof[
    Soit $x in E$.
    $ x = underbrace(p(x), in F) + underbrace((x - p(x)), in F^perp) $
    où $F = "Im"(p)$ et $F^perp = ker(p)$.

    $x - p(x) in F^perp$ donc $(x-p(x)) perp p(x)$.

    D'après le théorème de Pythagore
    $ norm(x)^2
        &= norm(p(x))^2 + underbrace(norm(x-p(x))^2, >= 0) \
        &>= norm(p(x))^2 $
]

#yprop(name: [Orthogonal de l'orhogonal d'un sous-ev])[
    #letep() et $F$ un sous-ev de dimension finie de $E$.

    Alors $(F^perp)^perp = F$.
]

#yproof[
    On sait que $F subset (F^perp)^perp$.

    Soit $x in (F^perp)^perp$.

    Soit $(u,v) in F times F^perp$ tel que $x = u+v$
    (possible car les sous-ev sont supplémentaires car $F$ est de dimension finie).

    $ underbrace(x, in (F^perp)^perp) perp underbrace(v, in F^perp) $
    donc $scal(x,v) = 0$ donc $scal(u,v) + norm(v)^2 = 0$.

    Or, $scal(u,v) = 0$ car $u in F$ et $v in F^perp$.

    Donc $norm(v)^2 = 0$ donc $v = 0$ donc $x = u in F$.
]

