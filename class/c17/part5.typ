#import "../../template/ytemplate.typ": *


#ypart[Applications linéaires]


#let let_ef_ev() = {[
    Soit $(E, +, dot)$ et $(F, +, dot)$ deux espaces vectoriels
    sur un même corps $KK$.
]}

=== 1. Définitions et premières propriétés

#ydef(name: [Application linéaire])[
    #let_ef_ev()

    Une application $f$ de $E$ dans $F$ est dite #emph[linéaire] si

    + pour tout $(u , v) in E^2$, $ f(u + v) = f (u) + f (v) $

    + pour tout $lambda in bb(K)$ et $u in E$,
        $ f (lambda u) = lambda f (u) $
]

#yexample[

+ L'application $ f : & E & arrow.r & F\
     & u & arrow.r.bar & 0_F $ est linéaire.

+ L'application $ f : & E & arrow.r & E\
     & u & arrow.r.bar & u $ est linéaire. 
+ La dérivation est une application linéaire de $C^1(I, CC)$ dans $C^0(I, CC)$.
]

#yprop(name: [Définition équivalente d'application linéaire])[
    #let_ef_ev()

    Soit $f : E arrow.r F$ une application.
    
    Alors $f$ est linéaire #strong[si et seulement si]
    $ forall (u, v) in E^2, forall (lambda , mu) in KK^2,
        yt f (lambda u + mu v) = lambda f (u) + mu f (v) $

]

#yproof[
    - On suppose dans un premier temps $f$ linéaire.
    
        Soit $(u,v) in E^2$ et $(lambda, mu) in KK^2$.
        
        Comme $f$ est linéaire,
        $ f(lambda u + mu v) &= f(lambda u) + f(mu v) \
            &= lambda f(u) + mu f(v) $

    - Réciproquement, on suppose que pour tout $(u,v) in E^2$
        et tout $(lambda, mu) in KK^2$,
        $ f(lambda u + mu v) = lambda f(u) + mu f(v) $
        
        Soit $(u,v) in E^2$ et $lambda in KK$. 

        $ f(u + v) &= f(1_KK u + 1_KK v) \
            &= 1_KK f(u) + 1_KK f(v) \
            &= f(u) + f(v) $
        $ f(lambda u) &= f(lambda u + 0_KK u) \
            &= lambda f(u) + 0_KK f(u) \
            &= lambda f(u) $
]



#yprop(name: [Image par une application linéaire du neutre, de l'opposé, et d'une combinaison linéaire])[
    #let_ef_ev()

    Soit $f : E arrow.r F$ une application linéaire. Alors :

    + $f (0_E) = 0_F$

    + $forall u in E$, $f (- u) = - f (u)$

    + $forall (lambda_i) in KK^[|1, n|], forall (u_i) in E^[|1, n|]$,
        $ f (sum_(i = 1)^n lambda_i u_i) = sum_(i = 1)^n lambda_i f (u_i) $

]

#yproof[
    Les deux premiers points sont une conséquence du fait que $f$
    est aussi un morphisme de groupes additifs. 
    Le troisième point se prouve par récurrence immédiate.
]

#ydef(name: [Application linéaire endomorphique, isomorphique et automorphique])[
    #let_ef_ev()
    Soit $f : E arrow.r F$ une application linéaire.
    
    On dit que $f$ est un
    + #emph[endomorphisme] si $F = E$ et ces deux espaces ont les mêmes lois ;

    + #emph[isomorphisme] si $f$ est bijective ;

    + #emph[automorphisme] si $f$ est à la fois un endomorphisme et un
        isomorphisme.
]


#yprop(name: [Image directe et réciproque d'un sous-ev par une application linéaire])[
    #let_ef_ev()

    Soit $f : E arrow.r F$ une application linéaire, $E prime$ un
    sous-espace vectoriel de $E$ et $F prime$ un sous-espace vectoriel de
    $F$.

    Alors $f (E prime)$ est un sous-espace vectoriel de $F$ et
    $f^(- 1) (F prime)$ est un sous-espace vectoriel de $E$.
]

#yproof[
    - Soit $(u, v) in f(E')^2$ et $(lambda, mu) in KK^2$.
        
        On écrit $u = f(x)$ et $v = f(y)$ avec $(x, y) in E'^2$.
        
        Ainsi $lambda u + mu v = f(lambda x + mu y) in f(E')$
        
        Comme $0_F = f(0_E)$, $0_F in f(E')$.
        
        Donc $f(E')$ est un sous-espace vectoriel de $F$.

    - Soit $(u, v) in f^(-1)(F')^2$ et $(lambda, mu) in KK^2$.

        Donc $f(u) in F'$ et $f(v) in F'$.
        
        Ainsi $ f(lambda u + mu v) = lambda f(u) + mu f(v) in F' $
        et donc $lambda u + mu v in f^(-1)(F')$.
        
        Comme $0_F = f(0_E)$, $0_E in f^(-1)(F')$.
        
        Donc $f^(-1)(F')$ est un sous-espace vectoriel de $E$.
]

#ynot(name: [Ensemble des application linéaire normales, endomorphiques, isomorphiques et automorphiques])[
    #let_ef_ev()
    + L'ensemble des applications linéaires de $E$ dans $F$ est noté
        $cal(L) (E , F)$.

    + L'ensemble des endomorphimes de $E$ est noté $cal(L) (E)$.

    + L'ensemble des isomorphismes de $E$ dans $F$ est noté
        $cal(G L) (E , F)$.

    + L'ensemble des automorphismes de $E$ est noté $cal(G L) (E)$.
]

#yprop(name: [Les applications sont un sous-ev])[
    #let_ef_ev()

    L'ensemble $cal(L) (E , F)$ est un sous-espace vectoriel de $F^E$.

    N.B. l'ensemble $cal(L) (E)$ est un espace vectoriel.
]


#yproof[
    L'application $x |-> 0_F$ est linéaire donc $cal(L)(E, F)$ est non vide.
    
    Soient $(f, g) in cal(L)(E, F)^2$ et $(lambda, mu) in KK^2$.
    
    Montrons que $lambda f + mu g$ est linéaire. 

    Soit $(x, y) in E^2$ et $(alpha, beta) in KK^2$.
    $ (lambda f + mu g)(alpha x + beta y)
        yt = lambda f(alpha x + beta y) + mu g(alpha x + beta y)
        yt = alpha lambda f(x) + beta lambda f(y)
            ytt + alpha mu g(x) + beta mu g(y)
        yt = alpha (lambda f + mu g)(x) 
            ytt + beta (lambda f + mu g)(y) $

     Ainsi $lambda f + mu g in cal(L)(E, F)$.
    ]


#yprop(name: [Particularité de la réciproque d'une application linéaire])[
    #let_ef_ev()

    Soit $f in cal(G L) (E , F)$.
    
    Alors l'application réciproque $f^(- 1)$
    appartient à $cal(G L) (F , E) .$
]

#yproof[
    Soit $(x, y) in F^2$ et $(lambda, mu) in KK^2$.
    
    On pose $u = f^(-1)(x)$ et $v = f^(-1)(y)$.
    
    On remarque que
    $ f(lambda u + mu v) &= lambda f(u) + mu f(v) \
        &= lambda x + mu y $
    
    Donc $lambda u + mu v = f^(-1)(lambda x + mu y)$.
    
    Ainsi $f^(-1)$ est linéaire.
]


#yprop(name: [Composées d'applications linéaires])[
    #let_ef_ev()

    + Soient $f$ est une application linéaire de $E$ dans $F$, $g$ une
        application linéaire de $F$ dans $G$.
        
        Alors l'application $g compose f$ est une application linéaire
        de $E$ dans $G$.

    + Soit $f$ est un endomorphisme de $E$.

        Alors, pour $k in bb(N)^*$, l'application
        $ f^k = underbrace(f compose f compose ... compose f, k "fois") $
        est un endomorphisme de $E$.
]

#yproof[
    + Soit $(x, y) in E^2$ et $(lambda, mu) in KK^2$.
        $ (g compose f) (lambda x + mu y)
            yt = g(f(lambda x + mu y))
            yt = g(lambda f(x) + mu f(y))
            yt = lambda g(f(x)) + mu g(f(y)) $
        Donc $g compose f$ est linéaire.

    + Par récurrence immédiate.
]

#yprop(name: [Groupe général linéaire])[
    Soit $E$ un $KK$-espace vectoriel.

    $(cal(G L) (E) , compose)$ est un groupe.
    On l'appelle #emph[le groupe général linéaire] de $E$.
]

=== 2. Noyau, image

#ydef(name: [Noyau d'une application linéaire])[
    #let_ef_ev()

    Soit $f in cal(L) (E , F)$.
    
    On appelle #emph[noyau] de $f$ et on note
    $"Ker" f$ l'ensemble $ "Ker" f := { u in E divides f (u) = 0_F } $
]

#ybtw[
    Le noyau de $f$ est donc l'ensemble des antécédents de $0_F$ par $f$.
]

#yprop(name: [Particularité du noyau d'une application linéaire])[
    #let_ef_ev()

    Soit $f in cal(L) (E , F)$.
    
    $"Ker" f$ est un sous-espace vectoriel de $E$.
]

#yproof[
    Le noyau de $f$ est $f^(-1)({0_F})$.

    Comme ${0_F}$ est un sous-espace vectoriel de $F$,
    le noyau de $f$ est un sous-espace vectoriel de $E$.
]


#yprop(name: [Caractérisation de l'injectivité d'une application linéaire par le noyau])[
    #let_ef_ev()

    Soit $f in cal(L) (E , F)$. 

    $f$ est injective si et seulement si $ "Ker" f = { 0_E } $
]

#yproof[
    Comme $f$ est linéaire, $f$ est aussi un morphisme de groupes
    et son noyau en tant qu'application linéaire
    et en tant que morphisme de groupes sont identiques.
]

#yprop(name: [Particularité de l'image d'une application linéaire])[
    #let_ef_ev()

    Soit $f in cal(L) (E , F)$. $"Im" (f)$ est un
    sous-espace vectoriel de $F$.
]

#yproof[
    L'image de $f$ est $f(E)$.
    
    Comme $E$ est un sous-espace vectoriel de $E$,
    $f(E)$ est un sous-espace vectoriel de $F$.
]

#yprop(name: [Caractérisation de la surjectivité d'une application linéaire par l'image])[
    #let_ef_ev()

    Soit $f in cal(L) (E , F)$.
    
    Alors, $f$ est surjective si et seulement si $"Im"(f) = F$.
]

#yproof[
    Par définition d'une application surjective.
]

#yprop(name: [Sous-ev engendré par l'image d'une famille génératrice par une application linéaire])[
    #let_ef_ev()

    Soit $f in cal(L) (E , F)$.
    Soit $(u_i)_(i in I)$ une famille génératrice de $E$.

    Alors $"Im"(f) = "Vect"(f (u_i) | i in I)$.
]

#yproof[
    Soit $y in "Im"(f)$.
    
    On peut écrire $y = f(x)$ avec $x in E$.
    
    Il existe une famille de scalaires $(lambda_i)$ presque nulle
    telle que $x = sum_(i in I) lambda_i u_i$.
    
    Comme $f$ est linéaire, $y = sum_(i in I) lambda_i f(u_i)$ donc $y in "Vect"(f(u_i) | i in I)$.
    
    De plus, pour tout $i in I$, $f(u_i) in "Im"(f)$.
    
    Comme $"Im"(f)$ est un sous-espace vectoriel de $F$,
    $"Vect"(f(u_i) | i in I) subset "Im"(f)$.
]

#yprop(name: [Sous-ev image d'une famille libre par une application linéaire])[
    #let_ef_ev()

    Soit $f in cal(L)(E, F)$ et $(u_i)_(i in I)$ une famille libre de $E$.
    
    Si $f$ est injective, alors $(f(u_i))_(i in I)$ est libre.
]

#yproof[
    On suppose $f$ injective.

    Soit $(lambda_i)_(i in I)$ une famille presque nulle de scalaires.
    $ sum_(i in I) lambda_i f(u_i) = 0_F
        yt <=> f(sum_(i in I) lambda_i u_i) = 0_E
        yt <=> sum_(i in I) lambda_i u_i in "Ker"(f)={0_E}
        yt <=> forall i in I, lambda_i = 0_KK $
]


=== 3. Projecteurs et symétrie
// Dans ce paragraphe, on suppose que $E = E_1 xor E_2$.

#let let_e_e1_e2() = {
    [Soit $E$ un espace vectoriel et $E_1, E_2$ tels que $E = E_1 xor E_2$.]
}

#ydef(name: [Projection d'un ev])[
    #let_e_e1_e2()

    On définit la #emph[projection] $p$ sur $E_1$ parallèlement à $E_2$ de
    la façon suivante.

    Soit $x in E$. Il existe un unique couple $(x_1 , x_2) in E_1 times E_2$
    tel que $x = x_1 + x_2$. On pose alors $p (x) = x_1$.
]

#yprop(name: [Propriétés élémentaires sur les projections d'ev])[
    #let_e_e1_e2()

    Soit $p$ la projection sur $E_1$ parallèlement à $E_2$.

    + $p$ est un endomorphisme de $E$.
    + $p_(|E_1) = "id"_(E_1)$ et $p_(|E_2) = 0$.
    + $p compose p = p$.
    + $"Ker" p = E_2$ et $"Im" p = E_1$.
]


#yproof[
    + Soit $(x, y) in E^2$ et $(lambda, mu) in KK^2$.
    
        On écrit $x=x_1 + x_2$ et $y = y_1 + y_2$ avec
        $cases((x_1, x_2) in E_1 times E_2, (y_1, y_2) in E_1 times E_2)$.
        
        Alors
        $ lambda x + mu y
            yt = (lambda x_1 + mu y_1) + (lambda x_2 + mu y_2) $
        
        Comme $cases(lambda x_1 + mu y_1 in E_1, lambda x_2 + mu y_2 in E_2)$,
        $ p(lambda x + mu y) &= lambda x_1 + mu y_1 \
            &= lambda p(x) + mu p(y) $

    + 
        - Soit $x in E_1$.
            Alors $x = x + 0_E$ avec $(x, 0_E) in E_1 times E_2$ donc $p(x)=x$.

        - Soit $x in E_2$.
            Alors $x = 0_E + x$ avec $(0_E, x) in E_1 times E_2$ donc $p(x)=0_E$.

    + Soit $x in E$.
        Alors $p(x) in E_1$ et donc $p(p(x))=p(x)$.

    + Soit $x in E$.
    
        On pose $x = x_1 + x_2$ avec $(x_1, x_2) in E_1 times E_2$.
        $ x in "Ker"(p) &<=> p(x) = 0_E \
            &<=> x_1 = 0_E \
            &<=> x in E_1 $

        Soit $y in E$.
        $ y in "Im"(p) &<=> exists x in E, y = p(x) \
            &=> y in E_1 $

        Réciproquement, si $y in E_1$, alors $y = p(y) in "Im"(p)$.
]

#ydef(name: [Projecteur])[
    On appelle #emph[projecteur] tout endomorphisme $f$
    vérifiant $f compose f = f$.
]

#yprop(name: [Relation entre projecteur et projection d'un ev])[
    Soit $f$ un projecteur d'un espace vectoriel $E$. $"Ker" f$ et $"Im" f$ sont
    supplémentaires et $f$ est la projection sur $"Im" f$ parallèlement à
    $"Ker" f$.
]

#yproof[
    - Soit $x in E$.

        On peut écrire $x = f(x) + (x-f(x))$ et $f(x) in "Im"(f)$.

        De plus, $f(x-f(x)) = f(x) -f(f(x))=f(x)-f(x)=0_E$ donc $x-f(x) in "Ker"(f)$. 

        Donc $E = "Im"(f) + "Ker"(f)$. 

    - Montrons que cette somme est directe.

        Soit $x in "Ker"(f) sect "Im"(f)$.
        
        Soit $u in E$ tel que $x = f(u)$ ($x$ est dans l'image de $f$).
        
        Comme $x $ est dans le noyau de $f$, $f(x)=0_E$ et donc $f(f(u))=0_E$.
        
        Comme $f$ est un projecteur, $f(f(u))=f(u)=x$ et donc $x = 0_E$.
        
        Ainsi l'intersection du noyau et de l'image est nulle,
        donc leur somme est directe.

    - Soit $p$ la projection sur $"Im"(f)$ parallèlement à $"Ker"(f)$
        ($p$ existe car ces deux sous-espaces sont supplémentaires).

        On a vu que la décomposition de tout vecteur $x$ de $E$
        comme somme d'un vecteur de l'image et d'un vecteur du noyau
        est $x = f(x) + (x-f(x))$ donc $p(x)=f(x)$.
]


#ydef(name: [Symétrie d'un ev])[
    #let_e_e1_e2()

    On définit la #emph[symétrie] $s$ par rapport à $E_1$ parallèlement à
    $E_2$ de la façon suivante.
    
    Soit $x in E$. Il existe un unique couple
    $(x_1 , x_2) in E_1 times E_2$ tel que
    $x = x_1 + x_2$.
    On pose alors $s (x) = x_1 - x_2$.
]

#yprop(name: [Propriétés élémentaires de la symétrie d'ev])[
    #let_e_e1_e2()

    Soit $s$ la symétrie par rapport à $E_1$ parallèlement à $E_2$.

    + $s$ est un endomorphisme de $E$.
    + $s_(|E_1) = "id"_(E_1)$ et $s_(|E_2) = - "id"_(E_2)$.
    + $s compose s = "id"_E$.
    + $cases("Ker"(s - "id"_E) = E_1, "Ker"(s + "id"_E) = E_2)$
]

#yproof[
    + Soit $p$ la projection sur $E_1$ parallèlement à $E_2$.
        
        On remarque que $s = 2 p - id_E$.
        
        Donc $s$ est un endomorphisme.

    + 
        - Soit $x in E_1$. Alors
            $ s(x) = 2 p(x) -x = 2 x -x = x $
        - Soit $x in E_2$.  Alors
            $ s(x) &= 2 p(x) -x \
                &= 0_E -x \
                &=-x $

    + $ s compose s &= (2 p - id_E) compose (2 p - id_E) \
        &= 4 p compose p - 4 p + id_E \
        &= id_E $
    + Soit $x in E$.
        $ x in "Ker"(s-id_E) <=> s(x)-x=0_E <=> 2 p(x) - 2 x = 0_E <=> p(x) = x <=> x in E_1 $
        et 
        $ x in "Ker"(s+id_E) <==> s(x)+x = 0_E <==> 2 p(x) = 0_E <==> x in E_2 . $
    ]


#ydef(name: [Involution])[
    On appelle #emph[involution] de $E$ toute application $f$
    vérifiant $f compose f = id_E$.
]


#yprop(name: [Particularité d'un endomorphime involutif])[
    Soit $f$ un endomorphisme, d'un espace vectoriel $E$, involutif.

    $"Ker"(f - "id"_E)$ et $"Ker"(f + "id"_E)$ sont supplémentaires
    et $f$ est la symétrie par rapport à $"Ker"(f - "id"_E)$
    parallèlement à $"Ker"(f + "id"_E)$.
]

#yproof[
    Soit $x in E$.
    
    On écrit $ x = 1/2 (f(x)-x) + 1/2 (f(x)+x) $
    
    On remarque que
    $ (f+id_E)(f(x)-x)
        yt = f(f(x)-x)+f(x)-x
        yt = 0_E $
    donc $1/2 (f(x)-x) in "Ker"(f+id_E)$.
    
    On prouve de même que $1/2 (f(x)+x) in "Ker"(f-id_E)$.
    
    Ainsi $E = "Ker"(f-id_E) + "Ker"(f+id_E)$.

    Soit $x in "Ker"(f+id_E) sect "Ker"(f-id_E)$.

    D'où $f(x)+x = f(x)-x=0_E$ et donc
    $ x &= 1/2 (f(x)+x) + 1/2(f(x)-x) \
        &= 0_E $
    
    Donc la somme des deux noyaux est directe.


    Soit $s$ la symétrie parallèlement à $"Ker"(f+id_E)$ par rapport à $"Ker"(f-id_E)$ ($s$ existe car ces deux sous-espaces sont supplémentaires).
    
    On a vu que la décomposition de tout vecteur $x$ de $E$ comme somme
    d'un vecteur de $"Ker(f-id_E)"$ et d'un vecteur de $"Ker"(f+id_E)$
    est $ x = 1/2 (f(x)+x) + 1/2 (f(x)-x) $ donc
    $ s(x) &= 1/2 (f(x)+x) - 1/2 (f(x)-x) \
        &= f(x) $
]



=== 4. Problème linéaire

#ydef(name: [Problème linéaire])[
    On appelle #emph[problème linéaire] une équation de la forme $f(x)=y$,
    d'inconnue $x in E$, où $f$ est une application linéaire d'un espace
    vectoriel $E$ dans un espace vectoriel $F$ et $y in F$.
]

#yprop(name: [Décomposition d'un problème linéaire avec le problème homogène associé])[
    Soit $f : E arrow.r F$ une application linéaire.
    
    Le problème linéaire homogène $f (x) = 0$ admet pour ensemble de
    solutions l'espace vectoriel $"Ker" f$.
    
    Si $x_0$ est une solution particulière du problème linéaire
    $f (x) = y$, alors les solutions du problème $f (x) = y$ sont les
    vecteurs de $E$ de la forme $x + x_0$ avec $x in "Ker" f$.
]

#yproof[Ce résultat a déjà été démontré dans le chapitre sur les groupes.]