#import "../../template/ytemplate.typ": *


#ypart[Somme de sous-espaces vectoriels]


#ydef(name: [Somme de deux ev])[
    Soient $E$ un espace vectoriel et $F$, $G$ deux sous-espaces vectoriels.

    On définit la #emph[somme] de $F$ et $G$ par
    $ F + G = { f + g divides f in F , med g in G } $
]


#yprop(name: [La somme de deux ev est un ev])[
    Soient $E$ un espace vectoriel et $F$, $G$ deux sous-espaces vectoriels.

    La somme $F + G$ est un sous-espace vectoriel de $E$.
]

#yproof[
    $0_E$ est un élément de $F$ et de $G$ donc $0_E + 0_E in F + G$, donc $0_E in F+G$.

    Soit $(x, y ) in (F+G)^2$ et $(lambda, mu) in KK^2$.
    
    On pose $x = u +v$ et $y = a+b$ avec $(u, a) in F^2$ et $(v, b) in G^2$.
    
    D'où  $lambda x + mu y = (lambda u + mu a) + (lambda v + mu b)$ avec $lambda u + mu a in F$ et $lambda v + mu b in G$, donc $lambda x + mu y in F+G$.
]

#yprop(name: [Sous-ev engendré de la réunion d'ev])[
    Avec les notations précédentes, $ F+G = "Vec"(F union G) $
]

#yproof[
    Par double inclusion.

    On sait que $F+G$ est un sous-espace vectoriel de $E$,
    et $F union G subset F+G$ donc $"Vect"(F union G) subset F+G$.
    
    De plus, tout élément de $F+G$ est de la forme $x +y$ avec $x in F$ et $y in G$
    et comme $F subset "Vect"(F union G)$ et de même pour $G$, $x +y in "Vect"(F union G)$.
]


#ynot(name: [Notation $Sigma$ sur une famille d'ev])[
    Soit $(F_i)_(i in I)$ une famille de sous-espaces vectoriels de $E$.
    
    On note 

    $sum_(i in I) F_i
        = {sum_(j in J) u_j
            | cases(J subset I\, J "fini", forall j in J\, u_J in F_j, ) }$
]


#yprop(name: [Sous-ev engendré de la réunion de toute une famille d'ev])[
    Avec les notations précédentes,
    $ sum_(i in I) F_i = "Vect"(union.big_(i in I) F_i) $
    c'est donc un sous-espace vectoriel de $E$.
]

#yproof[
    On note $F = "Vect"(union.big_(i in I) F_i)$ et $G = display(sum_(i in I) F_i)$.
    
    Par définition de $G$, toute combinaison linéaire finie de vecteurs de
    $union.big_(i in I) F_i$ se trouve dans $G$ donc $F subset G$.
    
    Réciproquement tout vecteur de $G$ est combinaison linéaire finie
    de vecteurs de $union.big_(i in I) F_i$ donc $G subset F$.
]


#ydef(name: [Somme directe d'ev])[
    Soient $F$ et $G$ deux sous-espaces vectoriels de $E$.
    
    On dit que la
    somme de $F$ et $G$ est #emph[directe] si tout élément de $F + G$ se
    décompose de façon #underline[unique] comme somme $f + g$ avec $f in F$
    et $g in G$.
    
    On note alors $F xor G$ à la place de $F + G$.
]

#yprop(name: [Caractérisation d'une somme directe d'ev par l'intersection])[
    Deux sous-espaces vectoriels $F$ et $G$ d'un espace vectoriel $E$ sont
    en somme directe si et seulement si $F sect G = { 0_E }$.
]


#yproof[
    - On suppose dans un premier temps que $F$ et $G$ sont en somme directe.
    
        Soit $x in F sect G$.
        
        Alors $0_E = x + (-x)$ avec $x in F$ et $-x in G$.
        
        Or $0_E = 0_E + 0_E$ et $(0_E, 0_E) in F times G$.
        
        Comme la décomposition en somme d'un vecteur de $F$ et d'un vecteur de $G$ est unique, nécessairement $x = 0_E$.

    - Réciproquement, on suppose que $F sect G = {0_E}$.

        Montrons que la somme de $F$ et $G$ est directe.
        
        Soit $x in F+G$. On suppose que $x = u + v = a + b$ avec $(u, v) in F times G$ et $(a, b) in F times G$. D'où $u-a = v-b$.
        
        Or, $u-a in F$ et $v-b in G$. Ces deux vecteurs étant égaux, ils sont dans $F sect G = {0_E}$, et donc $u=a$ et $v=b$ : la décomposition est unique.
]



#ydef(name: [Sous-ev suplémentaires])[
    Deux sous-espaces vectoriels $F$ et $G$ d'un espace vectoriel $E$ sont
    dits #emph[supplémentaires] si $E = F xor G$.
]

#ybtw[
En d'autres termes, deux sous-espaces vectoriels sont supplémentaires si
et seulement si tout vecteur de $E$ se décompose de feçon unique comme
somme d'un vecteur de $F$ et d'un vecteur de $G$.

]

#ydef(name: [Somme directe d'une famille d'ev])[
    Soit $(F_i)_(i in I)$ une famille de sous-espaces vectoriels de $E$.
    
    On dit que la somme $sum_(i in I) F_i$ est #emph[directe] si tout vecteur de cette somme s'écrit de manière unique comme somme finie d'éléments de $F_i$.
    
    Dans ce cas, on écrit $plus.circle.big_(i in I) F_i$ à la place de $sum_(i in I) F_i$.
]

#ybtw[Attention, dès qu'il y a plus de 3 sous-espaces vectoriels, vérifier que les intersections deux à deux sont réduites à $0_E$ ne suffit pas pour prouver que leur somme est directe.]

