#import "../../template/ytemplate.typ": *


#ypart[Espaces vectoriels]


Dans ce chapitre, $(KK, +, times)$ désignera un corps.

#ydef(name: [Espace vectoriel])[
    #set enum(numbering: "(EV1)")

    Soit $E$ un ensemble muni d'une loi de composition interne $+$, et d'une
    loi de composition externe $dot.op$ à opérateurs dans $KK$, #emph[i.e.] pour tout $lambda in KK$ et tout $x in E$, $lambda dot.op x in E$.

    On dit que $E$ est un #emph[$KK$-espace vectoriel] s'il vérifie les
    proriétés suivantes :

    + $(E , +)$ est un groupe abélien.

    + $forall (lambda, x) in KK times E, lambda dot x in E$

    + Pour tout $(lambda,mu)$ et tout $x in E$,
        $ lambda dot.op (mu dot.op x) = (lambda times mu) dot.op x $
        (pseudo-associativité)

    + Pour tout $x in E$,
        $ 1_KK dot.op x = x $
        (pseudo-neutre)

    + Pour tout $(lambda,mu) in KK^2$  et $x in E$,
        $ (lambda+mu) dot.op x = (lambda dot.op x) + (mu dot.op x) $
        (pseudo-distributivité à gauche)

    + Pour tout $lambda in KK$ et $x , y in E$, 
        $ lambda  dot.op (x+y) = lambda dot.op x+ lambda dot.op y $
        (pseudo-distributivité à droite)
]

#ydef(name: [Vecteur et scalaire])[
    Les éléments d'un $KK$-espace vectoriel $E$ sont appelés
    #emph[vecteurs] et ceux de $KK$ sont dits #emph[scalaires].
]


#yexample[
    + Notons $RR^n$ l'ensemble des $n$-uplets de réels. On le munit de
        l'addition + définie par la relation

        $ (x_1 , x_2 , dots.h , x_n) + (y_1 , y_2 , dots.h , y_n) = (x_1 + y_1 , dots.h , x_n + y_n)  $
        et de la multiplication scalaire définie par
        $ lambda dot.op (x_1,...,x_n) = (lambda x_1, ... ,lambda x_n). $
        Alors $(RR^n,+,dot.op)$ est un $RR$-espace vectoriel.

    + Soit $D$ un ensemble quelconque. On note $KK^D$ l'ensemble de
        toutes les applications $f$ de $D$ à valeurs dans $KK$. On le munit
        de l'addition + suivante
        $ f + g : x arrow.r.bar (f + g) (x) = f (x) + g (x) , $ et de la
        multiplication scalaire définie par
        $ lambda dot.op f : x |-> lambda f(x). $
        $(K^D, +, dot.op)$ est un $KK$-espace vectoriel.

    + En particulier, l'ensemble des suites $KK^NN$ est un $KK$-espace
        vectoriel.

    + L'ensemble des fonctions polynômiales à coefficients dans $KK$ muni des mêmes lois que l'espace précédent est
        un $KK$-espace vectoriel.

    + L'ensemble des matrices $cal(M)_(n,p) (KK)$ muni de l'addition des matrices et de la multiplication usuelle par un scalaire est un $KK$-espace vectoriel.
]


#yprop(name: [Éléments absorbants d'un espace vectoriel])[
    Soit $(E, +, dot.op)$ un $KK$-espace vectoriel. 
    + Pour tout $x in E$, $0_KK dot.op x = 0_E$.
    + Pour tout $lambda in KK$, $lambda dot.op 0_E = 0_E$.
]

#yproof[
    + Soit $x in E$. On pose $u = 0_KK dot.op x in E$.
        $ u = 0_KK dot.op x = (0_KK + 0_KK) dot.op x = u + u $ 
        donc $u = 0_E$ puisque $(E, +)$ est un groupe.

    + Soit $lambda in KK$. On pose $u = lambda dot.op 0_E$.
        $ u = lambda dot.op 0_E = lambda dot.op (0_E + 0_E) = u + u $
        donc $u = 0_E$.
    $qed$
]

#yprop(name: [Opposé du produit d'un scalaire et d'un vecteur])[
    Soit $(E, +, dot.op)$ un $KK$-espace vectoriel.
    
    Pour tout $lambda in KK$ et tout $u in E$,
    $ -(lambda dot.op u) = (-lambda) dot.op u $

    N.B. on a aussi $ -(lambda dot u) = lambda dot (-u)$
]

#yproof[
    Soit $(lambda, u) in KK times E$.
    
    Par définition, $-(lambda dot.op u)$ est le seul vecteur $v$ vérifiant $v + (lambda dot.op u) = 0_E$.

    Or,
    $ (-lambda dot.op u) + (lambda dot.op u)
        &= (-lambda + lambda) dot.op u \
        &= 0_KK dot.op u \
        &= 0_E $

    N.B.
    $ (-lambda dot u) = lambda dot (-u)
        yt <=> 0_E = lambda dot u + lambda dot (-u)
        yt <=> 0_E = lambda dot 0_E $
]

Dorénavant, on allège les notations en écrivant $lambda x$ au lieu de
$lambda dot.op x$ pour $lambda in KK$ et $x in E$.

#yprop(name: [Régle du produit nul et celles d'identifications pour les ev])[
    Soit $(E, +, dot.op)$ un $KK$-espace vectoriel.

    Soient $(u , v) in E^2$ et $(lambda, mu) in KK^2$.

    + $lambda u = 0_E <==> lambda = 0 or u = 0_E$.

    + $lambda u = lambda v <==> lambda = 0 or u = v$.

    + $lambda u = mu u <==> lambda = mu or u = 0_E$.
]


#yproof[
    + On suppose $lambda != 0_KK$.
  
        Comme $KK$ est un corps, $lambda$ a un inverse $lambda^(-1)$ pour $times$. 
        
        Donc 
        $ lambda u = 0_E
            yt => lambda^(-1) (lambda u) = lambda^(-1)  0_E
            yt => (lambda^(-1) times lambda)  u = 0_E
            yt => 1_KK  u = 0_E
            yt => u = 0_E $

    + $ lambda u = lambda v
        yt <==> lambda u - lambda v = 0_E
        yt <==> lambda (u - v) = 0_E
        yt <==> lambda = 0_KK or u-v = 0_E $

    + $ lambda u = mu u
        yt <==> lambda u + (-mu . u) = 0_E
        yt <==> (lambda - mu) u = 0_E
        yt <==> lambda = mu or u = 0_E. $
  ]



#ydef(name: [Combinaison linéaire d'une famille finie])[
    Soit $(E, +, dot.op)$ un $KK$-espace vectoriel.

    Un vecteur $x$ de $E$ est une #emph[combinaison linéaire] d'une famille
    finie $(u_i) in E^[|1,n|]$ de $n$ vecteurs s'il existe une famille de $n$
    scalaires $(lambda_i) in KK^[|1, n|]$ telle que
    $ x = sum_(i=1)^n lambda_i u_i $
]


#ydef(name: [Famille de scalaires presque nulle et combinaison linéaire])[
    + On dit qu'une famille $(lambda_i)_(i in I)$ de scalaires
        est #emph[presque nulle] si $ {i in I | lambda_i != 0_E} $ est fini. 

    + Soit $(E, +, dot.op)$ un $KK$-espace vectoriel, $(u_i)_(i in I)$
        une famille de vecteurs et $x in E$.
    
        On dit que $x$ est une _combinaison linéaire_ des $u_i$ ($i in I$) s'il existe une famille presque nulle de scalaires $(lambda_i)_(i in I)$ telle que 
        $ x = sum_(i in I) lambda_i u_i $

    N.B. On considère des familles presque nulles dans la définition précédente pour que les sommes soient finies.
]