#import "../../template/ytemplate.typ": *


#ypart[Familles génératrices, libres et bases d'un espace vectoriel]


=== 1. Familles génératrices

#ydef(name: [Famille génératrice])[
    Soit $(E, +,.)$ un $KK$-espace vectoriel et $(u_i)_(i in I)$ une famille de vecteurs de $E$.
    
    On dit que cette famille est #emph[génératrice] de $E$, ou qu'elle engendre $E$ si $E = "Vect"(u_i | i in I)$, en d'autres termes si tout vecteur de $E$ peut s'écrire comme combinaison linéaire finie des vecteurs $u_i$.
]

#ybtw[
    Avec les notations précédentes, $(u_i)_(i in I)$ engendre $E$ si et seulement si $E = sum_(i in I) "Vect"(u_i)$.
]


#yprop(name: [Transformations d'une famille génératrice qui laissent invariant l'ev engendré])[
    Soit $(u_i)_(i in I)$ une famille génératrice de $E$.
    
    Si :

    + on change l'ordre des vecteurs de cette famille 


    + ou l'on remplace l'un des vecteurs $u_i$ par la somme de $u_i$ et
        d'une combinaison linéaire finie des autres,

    + ou l'on remplace l'un des vecteurs $u_i$ par le produit de $u_i$ avec
        un scalaire non nul,

    + ou l'on enlève de $(u_i)_(i in I)$ un de ses vecteurs qui
        serait lui-même une combinaison linéaire des autres,

    alors chacune de ces nouvelles familles est encore une famille
    génératrice de $E$.
]

#yprop(name: [Particularité d'une famille qui continent une famille génératrice])[
    Toute famille qui contient une famille génératrice est elle-même une famille génératrice.
]

#yexample[
    Montrer que ${ (1 , 1) , (1 , - 1) , (2 , 3) }$ est une famille
    génératrice de $RR^2$.
]

#ybtw[
    L'intérêt de disposer d'une famille génératrice finie est qu'elle permet
    l'introduction de coordonnées. En effet, un vecteur $x$ de $E$ peut
    alors s'écrire sous la forme $x=sum_(i in I) lambda_i u_i$ ($(lambda_i)$ une famille presque nulle de scalaires), et
    on peut utiliser alors les nombres $lambda_i$ comme
    coordonnées.
    Le problème est qu'#emph[a priori], ces coordonnées ne sont peut-être pas uniques.
]

=== 2. Familles libres

#ydef(name: [Famille libre/liée])[
    - On dit qu'une famille $(u_i)_(i in I)$ est #emph[libre]
        (ou que ses vecteurs sont #emph[linéairement indépendants]) si pour
        toute famille presque nulle de scalaires $(lambda_i)_(i in I)$, 
        $ sum_(i in I) lambda_i u_i = 0_E => forall i in I, lambda_i = 0_KK. $

    - On dit qu'une famille est #emph[liée] (ou que ses vecteurs sont
        #emph[linéairement dépendants]) si elle n'est pas libre.
]

#ybtw[
    Soit $F$ un sous-espace vectoriel de $E$, et $(u_i)$ une famille de vecteurs de $F$. Comme $0_F = 0_E$, $(u_i)$ est libre dans $E$ si et seulement si elle est libre dans $F$.
]

#ybtw[Une famille libre ne comporte pas le vecteur nul. Donc si une famille
    comporte le vecteur nul, alors elle est automatiquement liée.

]

#yprop(name: [Transformations d'une famille qui la laisse libre])[
    Soit $(u_i)_(i in I)$ une famille libre de $E$.
    
    Si :

    + on change l'ordre des vecteurs de cette famille,


    + ou l'on remplace l'un des vecteurs $u_i$ par la somme de $u_i$ et
        d'une combinaison linéaire finie des autres,

    + ou l'on remplace l'un des vecteurs $u_i$ par le produit de $u_i$ avec
        un scalaire non nul,

    + ou l'on ajoute à $(u_i)_(i in I)$ un vecteur qui
        n'est pas combinaison linéaire des autres des $u_i$,

    alors chacune de ces nouvelles familles est encore une famille
    libre de $E$.
]

#yprop(name: [Particularité d'une sous-famille d'une famille libre])[
    Toute sous-famille d'une famille libre est libre.
]

== 3. Bases

#ydef(name: [Base d'un ev])[
    On dit qu'une famille $(u_i)_(i in I)$ est une #emph[base] de $E$ si elle
    est à la fois génératrice de $E$ et libre.
]

#ybtw[
    Avec les notations précédentes, $(u_i)$ est une base de $E$
    si et seulement si $E = plus.circle.big_(i in I) "Vect"(u_i)$.
]

#yexample[
    Dans $KK^n$, on considère pour tout $i$ le vecteur $e_i$ dont toutes
    les composantes sont nulles sauf la $i$-ème qui vaut 1. Alors la famille
    $(e_1 , dots.h , e_n)$ est une base de $KK^n$. On l'appelle #emph[base canonique]
    de $KK^n$.
]

#ytheo(name: [Particularité de la décomposition d'un vecteur dans une base d'un ev])[
    Soient $(u_i)_(i in I)$ une base de $E$.
    
    Tout vecteur de $E$ se
    décompose de façon #underline[unique] sous la forme $sum_(i in I) lambda_i u_i$ où $(lambda_i)_(i in I)$ est une famille presque nulle de scalaires.
]

#yexample[
    Trouver une base de $F={(x,y,z) in RR^3 |  x+y+z=0}$.
]

#ybtw[
    Un même espace vectoriel peut admettre des bases différentes. \
    #emph[Exemple] : si $E=RR^2$ alors $((1 , 0) , (- 1 , 1))$ et
    $((1 , 1) , (0 , 1))$ sont deux bases de $RR^2$.
]

#yprop(name: [Base d'une somme directe d'ev])[
    Soient $F$ et $G$ deux sous-espaces vectoriels de $E$ en somme directe,
    $(e_i)_(i in I)$ une base de $F$ et
    $(f_j)_(j in J)$ une base de $G$.
    
    Alors
    $(e_i) union (f_j)$ est une base de
    $F xor G$.
    
    On dit que cette base est #emph[adaptée] à la somme directe.
]