#import "../../template/ytemplate.typ": *


#ypart[Sous-espaces vectoriels]


#ydef(name: [Sous-espace vectoriel])[
    #set enum(numbering: "(SEV1)")
    Soient $E$ un espace vectoriel et $F$ un sous-ensemble de $E$.
    
    On dit que $F$ est un #emph[sous-espace vectoriel] si 
    + $F$ est non vide ;
    + $F$ est stable par combinaisons linéaires :
        $ forall x, y in F, forall lambda, mu in KK,
            yt lambda x + mu y in F $
]


#yprop(name: [Les sous-ev sont des ev])[
    Soit $F$ un sous-espace vectoriel de $(E, +, dot)$.
    
    $(F, +, dot)$ est lui-même un espace vectoriel.
]


#yproof[
    + Pour tout $(x, y) in F^2$, $x - y = 1_KK . x + (-1_KK). y in F$ donc $F$ est un sous-groupe de $(E, +)$, donc $(F, +)$ est un groupe abélien.
    + Pour tout $x in F$ et tout $lambda in KK$, $lambda x = lambda x + 0_KK . x in F$.
]

#ybtw[
Ce résultat est très pratique pour prouver qu'un ensemble est un espace
vectoriel.]


#yprop(name: [Définition équivalente d'un sous-ev (plus intuitive)])[
    Soit $(E, +, dot)$ un $KK$-espace vectoriel  et $F$ un sous-ensemble de $E$.
    
    Alors $F$ est un sous-espace vectoriel de $E$ si et seulement si
    - $0_E in F$,

    - $forall x , y in F$, $x + y in F$,

    - $forall lambda in KK, forall x in F$, $lambda x in F$.
]

#yproof[
    + On suppose que $F$ est un sous-espace vectoriel de $E$.
    
        Alors $(F, +, .)$ est un $KK$-espace vectoriel, donc $F$ satisfait aux 3 propriétés ci-dessus.

    + On suppose que $F$ vérifie ces 3 propriétiés.

        Alors $F$ est non vide, et stable par combinaison linéaire en combinant les deux dernières propriétés.
]


#yexample[
    L'ensemble des solutions d'un système linéaire homogène à $n$ variables
    est un sous-espace vectoriel de $KK^n$.
]

#ytheo(name: [Particularité de l'ensemble des combinaisons linéaires d'une suite de vecteur])[
    Soit $(u_i)_(i in I)$ une famille de vecteurs d'un espace vectoriel $(E, +, dot)$.

    L'ensemble de toutes les combinaisons linéaires de
    $(u_i)_(i in I)$ (sous-ev engendré) forme un sous-espace vectoriel $F$ de $E$.
]

#yproof[
    $F$ est non vide car il contient le vecteur nul (combinaison linéaire $sum_(i in I) 0_KK dot u_i$) et stable par combinaison linéaire, puisqu'une combinaison linéaire de deux combinaisons linéaires est encore une combinaison linéaire.
]


#ydef(name: [Sous-espace vectoriel engendré])[
    Soit $(u_i)_(i in I)$ une famille de vecteurs de $E$.
    
    L'ensemble des combinaisons linéaires des $u_i$ est appelé #emph[sous-espace vectoriel engendré par les $u_i$] et noté $"Vect"(u_i | i in I)$.
]


#ybtw[
    Toute intersection de deux sous-espaces vectoriels d'un espace vectoriel
    est un sous-espace vectoriel.

]


#yprop(name: [Particularité d'une intersection de sous-ev])[
    Une intersection quelconque de sous-espaces vectoriels est un sous-espaces vectoriel.
]


#yproof[
    Soit $(F_i)_(i in I)$ une famille de sous-espaces vectoriels et $F = sect.big_(i in I) F_i$.
    
    - Comme $0_E in F_i$ pour tout $i in I$, $0_E in F$.

    - Soit $(x, y) in F^2$ et $(lambda, mu) in KK^2$.
        
        Pour tout $i in I$, $(x, y) in F_i^2$ et comme $F_i$ est un sous-espace-vectoriel,
        $lambda x + mu y in F_i$.
        
        Donc $F$ est stable par combinaison linéaire.
]




#ybtw[L'union de deux sous-espaces vectoriels n'est en général pas un sev de
    $E$.
]

#yprop(name: [Plus petit ev contenant une famille de vecteur particuliaire])[
    Soit $(u_i)_(i in I)$ une famille de vecteurs d'un $KK$-espace vectoriel $E$.
    
    $"Vect"(u_i | i in I)$ est le plus petit sous-espace vectoriel de $E$ contenant les $u_i$.
]

#yproof[
    - On sait déjà que $"Vect"(u_i | i in I)$ est un sous-espace vectoriel de $E$ et qu'il contient les     vecteurs $u_i$ ($u_i = sum_(j in I) delta_(i, j) u_j$ où $delta_(i,j)$ est le symbole de Kronecker).

    - Soit $F$ un sous-espace vectoriel de $E$ contenant tous les $u_i$.
        
        Comme $F$ est stable par combinaison linéaire, $F$ contient toutes les combinaisons linéaires des $u_i$, donc $F supset "Vect"(u_i | i in I)$.
]

#ycor(name: [Intersection de tous les ev qui contient une famille de vecteurs particuliaire])[
    (Corollaire de $"Vect"(u_i | i in I)$ est le plus petit sous-ev de $E$ contenant les $u_i$.)
    
    Avec les notations précédentes, 
    $ "Vect"(u_i | i in I) = sect.big_(F " sous-ev de " E \ forall i in I, u_i in F) F. $
]


#yproof[
    $G = sect.big_(F " sous-ev de " E \ forall i in I, u_i in F) F$ est un sous-espace vectoriel de $E$ (en tant qu'intersection de sous-espaces-vectoriels) qui contient tous les $u_i$, donc contient $"Vect"(u_i | i in I)$.
    
    Or, $"Vect"(u_i | i in I) supset G$ puisque $G$ est inclus dans tout sous-espace vectoriel de $E$ contenant tous les $u_i$.
]


#ymetho(name: [Montrer qu'un ensemble est un ev])[
    Pour montrer qu'un ensemble $E$ est un espace
    vectoriel, on prouve l'un des points suivants :

    - $E$ est un sous-espace vectoriel d'un espace vectoriel connu

    - $E$ est engendré par une famille de vecteurs d'un espace vectoriel
        connu (voir chapitre suivant).

    - $E$ est l'intersection de deux sous-espaces vectoriels d'un espace
        vectoriel connu.

    - (en dernier recours !!) $E$ vérifie les propriétés de la définition
        d'un espace vectoriel.
]
