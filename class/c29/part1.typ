#import "../../template/ytemplate.typ": *


#ypart[Intégrale d'une fonction en escalier]


#let subdiv(a,b) = {$frak(S)_[#a,#b]$}


#ydef(name: [Subdivision d'un segment])[
    Un _subdivision_ du segment $[a,b]$
    est une suite finie $(x_0, ..., x_n)$
    de longeur quelconque telle que
    $ a = x_0 < x_1 < ... < x_n = b $
]

#ydef(name: [Fonction en escalier])[
    On dit que $f: [a,b] -> RR$ est
    _en escalier_ s'il existe une subdivision
    $(x_0, ..., x_n)$ de $[a,b]$ telle que
    pour tout $i in [|0, n-1|]$,
    $f_(| \]x_i, x_(i+1)\[)$ est constante.

    On dit alors que $(x_0, ..., x_n)$ est
    _adaptée_ à $f$.
]

#ynot(name: [Ensemble des subdivisions])[
    On note $subdiv(a,b)$ l'ensemble des
    subdivisions de $[a,b]$.

    C'est pas une notation officielle.
]

#ydef(name: [Subdivision plus fine])[
    Soit $sigma = (x_0, ..., x_n)$ et
    $tau = (y_0, ..., y_p)$
    deux subdivisions de $[a,b]$.

    On dit que $tau$ est _plus fine que_ $sigma$
    si
    $ forall i in [|0, n|], exists j in [|0, p|], x_i = y_i $
    _i.e_
    $ {x_0, ..., x_n} subset {y_0, ..., y_n} $

    On écrit alors $tau prec sigma$.
]

#yprop(name: [Particularité d'une subdivision plus fine qu'une subdivision adaptée à une fonction])[
    Soit $f: [a,b] -> RR$ une fonction
    en escalier, $sigma in subdiv(a,b)$ adaptée
    à $f$.

    Toute subdivision plus fine que $sigma$ est
    adaptée à $f$.
]

#yproof[
    Facile.
]

#yprop(name: [Subdivision plus fine que deux subdivisions])[
    Soit $sigma_1, sigma_2 in subdiv(a,b)$.

    $ exists sigma_3 in subdiv(a,b),
        cases(sigma_3 prec sigma_1,
            sigma_3 prec sigma_2) $
]

#yproof[
    Facile#super[TM].
]

#ydef(name: [Intégrale d'une fonction en escalier])[
    Soit $f :[a,b] -> RR$
    une fonction en escalier.

    Soit $sigma = (x_0, ..., x_n)$ une subdivision
    adaptée à $f$.

    On pose $m_i$ la valeur constante
    de $f$ sur $]x_i, x_(i+1)[$ pour $i in [|0,n-1|]$.

    L'_intégrale de $f$ sur $[a,b]$_ est
    $ integral_[a,b] f = sum_(i=0)^(n-1) m_i (x_(i+1) - x_i) $
]

#yprop(name: [Intégrale d'une fonction en escalier selon la subdivision utilisée])[
    L'intégrale d'une fonction en escalier $f$ sur $[a,b]$
    ne dépend pas de la subdivision choisie.
]

#yproof[
    Soit $sigma, tau in subdiv(a,b)$ deux subdivision adaptées à $f$.

    - Cas 1 :
        On suppose $tau prec sigma$.

        On pose $sigma = (x_0, ..., x_n)$ \
        et $tau = (y_0, ..., y_p)$.

        Soit $i in [|0, p-1|]$.

        Il existe $j_i in [|0, n-1|]$,
        $ x_(j_i) <= y_i < x_(j_i + 1) $

        Donc $]y_i, y_(i+1) subset ]x_(j_i), x_(j_i + 1)[$.

        Ainsi
        $ &sum_(i=0)^(p-1) m_(j_i) (y_(i+1) - y_i) \
            &= sum_(j=0)^(n-1) sum_(j | j_i = j) m_(j_i) (y_(i+1) - y_i) \
            &= sum_(j=0)^(n-1) m_j sum_(i | j_i = j) (y_(i+1) - y_i) \
            &= sum_(j=0)^(n-1) m_j sum_(i | x_j <= y_i < x_(j+1)) (y_(i+1) - y_i) \
            &= sum_(j=0)^(n-1) m_j (x_(j+1) - y_j) $
    
    - Cas 2 :
        On ne suppose plus $tau < sigma$.
        
        Soit $phi in subdiv(a,b)$ une subdivision plus fine que $tau$ et $sigma$.

        L'intégrale de $f$ calculée avec $sigma$ est
        égale à celle calculée avec $phi$ car $phi prec sigma$
        qui est égale à celle calculée avec $tau$ car $phi prec tau$.
]