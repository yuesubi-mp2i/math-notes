#import "../../template/ytemplate.typ": *


#ypart[Théorème fondamental de l'analyse]


#let subdiv(a,b) = {$frak(S)_[#a,#b]$}


#ytheo(name: [Théorème fondamental de l'analyse])[
    Soit $f : [a,b] -> RR$ *continue*.

    Alors $f$ est intégrable et
    $ phi: x |-> integral_[a,x] f $
    est une primitive de $f$ sur $[a,b]$.
]

#yproof[
    $f$ est continue sur $[a,b]$ donc
    bornée.

    Soit
    $ phi^+ : &[a,b] -> RR \
        &x |-> I_[a,x]^+ (f) $
    et
    $ phi^- : &[a,b] -> RR \
        &x |-> I_[a,x]^- (f) $
    
    _Montrons que $phi^+$ et $phi^-$ sont dérivables
    et que $(phi^+)' = (phi^-)' = f$._

    Soit $x in [a,b]$ et $h > 0$ tel que $x + h in [a,b]$.
    $ &(phi^+ (x+h) - phi^+ (x))/h \
        &= 1/h (I_[a,x+h]^+ (f) - I_[a,b]^+ (f)) \
        &= 1/h I_[x, x+h]^+ (f) $
    de même,
    $ (phi^- (x+h) - phi^- (x))/h 
        = 1/h I_[x, x+h]^- (f) $
    
    $f$ est continue sur $[x,x+h]$
    donc elle est bornée et atteint ses bornes
    donc il existe $c_h in [x,x+h]$ telle que
    $ f(c_h) = max_[x,x+h] (f) $

    Avec l'inégalité des accroissement finis (je crois),
    $ I_[x,x+h]^+ (f) <= f(c_h) (x + h - x) $
    donc
    $ (phi^+ (x+h) - phi^+ (x))/h <= f(c_h) $

    Soit $d_h in [x, x+h]$ tel que
    $ f(d_h) = min_[x,x+h] (f) $
    donc
    $ I_[x, x+h]^- (f) >= f(d_h) (x +h - x) $
    et donc
    $ (phi^- (x+h) - phi^- (x))/h  >= f(d_h) $

    D'où,
    $ f(d_h) &<= (phi^- (x+h) - phi^- (x))/h \
        &<= (phi^+ (x+h) - phi^+ (x))/h \
        &<= f(c_h) $
    
    Par encadrement,
    $ c_h -->_(h->0) x $

    Comme $f$ est continue en $x$, par caractérisation séquencielle,
    $ f(c_h) -->_(h->0) f(x) $

    De même, $d_h -->_(h->0) x$
    donc $f(c_h) -->_(h->0) f(x)$.

    Donc par encadrement,

    $ f(x) &= lim_(h->0^+) (phi^- (x+h) - phi^- (x))/h \
        &= lim_(h->0^+) (phi^+ (x+h) - phi^+ (x))/h $

    On a montré que $phi^+$ et $phi^-$ sont dérivables
    et $(phi^+)' = (phi^-)' = f$.

    Il existe $K in RR$ tel que
    $ forall x in [a,b], phi^+ (x) = phi^- (x) + K $

    En particulier
    $ underbrace(phi^+ (0), =0) = underbrace(phi^- (0), =0) + K $
    donc $K = 0$.

    Et donc pour $X = b$
    $ phi^+ (b) = phi^- (b) $

    Ainsi $g$ est intégrable sur $[a,b]$.
]

#ynot(name: [Intégrale avec $integral$ et $dif dot$])[
    Soit $f: [a,b] -> RR$ continue.

    On écrit $integral_a^b f(x) dif x$ à la place de
    $integral_[a,b] f$.

    On écrit aussi $integral_b^a f(x) dif x = - integral_[a,b] f$.
]

#ydef(name: [Fonction continue par morceaux])[
    Soit $f: [a,b] -> RR$.

    On dit que $f$ est _continue par morceaux_ sur $[a,b]$
    s'il existe $sigma = (x_0, ..., x_n) in subdiv(a,b)$
    telle que

    + pour tout $i in [|0, n-1|]$ $f$ est continue
        sur $]x_i, x_(i+1)[$
    
    + $forall [|1, n-1|]$,
        $ lim_(t->x_i \ >) f(t) in RR and lim_(t -> x_i \ <) f(t) in RR $
    
    + $lim_(t->a \ >) f(t) in RR and lim_(t -> b \ <) f(t) in RR$

    On dit que $sigma$ est _adaptée_ à $f$.
]

#yprop(name: [Intégrale d'une fonction continue par morceaux])[
    Soit $f: [a,b] -> RR$ une fonction continue par morceaux et
    $sigma = (x_0, ..., x_n)$ une subdivision adaptée.

    Alors $f$ est intégrable sur $[a,b]$ et
    $ integral_[a,b] f = sum_(i=0)^(n-1) integral_[x_i, x_(i+1)] f $
]

#yproof[
    On fixe $i in [|0, n-1|]$.

    On peut prolonger $f_(| \]x_i, x_(i+1) \[)$
    par continuité en $x_i$ et $x_(i+1)$.

    On obtient une fonction $f_i$ continue
    sur $[x_i, x_(i+1)]$, donc intégrable sur $[x_i, x_(i+1)]$.

    Comme $f_i$ est $f$ coïcident sur $]x_i, x_(i+1)[$,
    $ forall t in subdiv(x_i, x_(i+1)),
        cases(S_tau^- (f) = S_tau^- (f_i),
            S_tau^+ (f) = S_tau^+ (f_i)) $

    Donc $f$ est intégrable qur $[x_i, x_(i+1)]$.

    D'après la relation de Chasles, $f$ est intégrable
    sur $[a,b]$.
]

#yprop(name: [Condition suffisante avec une intégrale pour qu'une fonction soit nulle])[
    Soit $f: [a,b] -> RR^+$ #ymega_emph[continue].

    Si
    $ integral_a^b f(x) dif x = 0$
    alors
    $ forall x in [a,b], f(x) = 0 $
]