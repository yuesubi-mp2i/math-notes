#import "../../template/ytemplate.typ": *


#ypart[Sommes de Riemann]


#yprop(name: [Écriture d'une intégrale avec  méthode des rectangles à droite])[
    Soit $f: [a,b] -> RR$ continue par morceaux.

    $ &lim_(n->+oo) sum_(k=0)^n (b-a)/n f(a+ k(b-a)/n) \
        &= integral_a^b f(x) dif x $
    
    #figure(canvas(
        length: 2em,
        {
        riesketcher(
            start: 0,
            end: 2.2,
            plot-x-tick-step: 1,
            plot-y-tick-step: 1,
            x => -0.5 * calc.pow(x - 1, 2)*calc.sin(x+1)  +0.25
        )
    }),
        caption: [Typst c'est génial]
    )
    
    N.B. en exercice on peux l'utiliser pour transformer une
    somme en intégrale.
]

#yproof[
    Dans le cas où $f$ est continue sur $[a,b]$.

    On pose, pour tout $n in NN^*$
    et $k in [|0, n|]$,
    $ x_k = a + k (b-a)/n $
    alors $x_0 = a$ et $x_n = b$.

    Soit $n in NN^*$,
    $ &abs(sum_(k=0)^(n-1) (b-a)/n f(x_k) - integral_a^b f(x) dif x) \
        &<= abs(sum_(k=0)^(n-1) (b-a)/n f(x_k) \
            &wide - sum_(k=0)^(n-1) integral_(x_k)^(x_(k+1)) f(x) dif x) \
        &<= sum_(k=0)^(n-1) abs((b-a)/n f(x_k) 
            - integral_(x_k)^(x_(k+1)) f(x) dif x) \
        &<= sum_(k=0)^(n-1) abs(
            integral_(x_k)^(x_(k+1)) f(x_k) dif x 
            - integral_(x_k)^(x_(k+1)) f(x) dif x) \
        &<= sum_(k=0)^(n-1) integral_(x_k)^(x_(k+1))
            abs(f(x_k) - f(x) ) dif x $

    _La preuve n'est au programe que pour une
    fonction lipschitzienne._

    Soit $epsilon > 0$.

    Comme $f$ est uniformément continue sur $[a,b]$
    d'après le théorème de Heine,
    $ exists eta > 0, forall (x,y) in [a,b], \
        abs(x - y) <= eta => abs(f(x) - f(y)) <= epsilon $

    $(b-a)/n -->_(n->+oo) 0$
    il existe donc $N in NN^*$ tel que
    $ forall n in NN, (b - a)/n <= epsilon $

    Soit $n >= N$.
    $ &forall k in [|0, n-1|], forall x in [x_k, x_(k+1)], \
        &wide abs(x - x_k) <= (b-a)/n <= eta $
    et donc pour $k in [|0, n-1|]$,
    $ &integral_(x_k)^(x_(k+1)) abs(f(x) - f(x_k)) dif x \
        &<= integral_(x_k)^(x_(k+1)) epsilon dif x = epsilon (b-a)/n $
    
    Donc pour $n >= N$,
    $ &abs(sum_(k=0)^(n-1) (b-a)/n f(x_k) - integral_a^b f(x) dif x) \
        &<= sum_(k=0)^(n-1) epsilon (b-a)/n = epsilon (b - a) $
]

// dans le poly il y a l'autre construction