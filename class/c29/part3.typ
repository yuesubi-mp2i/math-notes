#import "../../template/ytemplate.typ": *


#ypart[Propriétés de l'intégrale]


#let subdiv(a,b) = {$frak(S)_[#a,#b]$}


#yprop(name: [Intégrale inférieure/supérieure d'une somme de fonctions])[
    Soit $f: [a,b] -> RR$ et $g: [a,b] -> RR$ deux fonctions bornées.
    $ I_[a,b]^- (f+g) >= I_[a,b]^- (f) + I_[a,b]^- (g) $
    $ I_[a,b]^+ (f+g) <= I_[a,b]^+ (f) + I_[a,b]^+ (g) $
]

#yproof[
    #let Im(f) = {$I_[a,b]^- (#f)$}
    #let Ip(f) = {$I_[a,b]^+ (#f)$}

    $ &Im(f+g) >= Im(f) + Im(g) \
        &<=> Im(f+g) - Im(g) >= Im(f) \
        &<=> forall sigma in subdiv(a,b), \
            &wide Im(f+g) - Im(g) >= S_sigma^- (f) \ 
        &<=> forall sigma in subdiv(a,b), \
            &wide Im(f+g) - S_sigma^- (f) >= Im(g) \ 
        &<=> forall (sigma, tau) in subdiv(a,b)^2, \
            &wide Im(f+g) - S_sigma^- (f) >= S_tau^- \ 
        &<=> forall (sigma, tau) in subdiv(a,b)^2, \
            &wide Im(f+g) >= S_sigma^- (f) + S_tau^- (f) $ 
    
    ---
        
    Soit $phi = (x_0, ..., x_n) in subdiv(a,b)$.

    Soit $i in [|0, n-1|]$.

    On pose $m_i = inf_(\]x_i, x_(i+1)\[) (f + g)$.

    $ S_phi^- (f+g) = sum_(i=0)^(n-1) (x_(i+1) - x_i) m_i $

    On fixe $i in [|0, n-1|]$.
    
    Pour tout $t in]x_i, x_(i+1)[$,
    $ &(f+g)(t) \
        &= f(t) + g(t) \
        &>= inf_(\]x_i, x_(i+1)\[) (f) + inf_(\]x_i, x_(i+1)\[) (g) $

    Donc $inf_(\]x_i, x_(i+1)\[) (f) + inf_(\]x_i, x_(i+1)\[) (g)$
    minore $ { (f+g)(t) | t in \]x_i, x_(i+1)\[}$.

    Donc 
    $ inf_(\]x_i, x_(i+1)\[) (f) + inf_(\]x_i, x_(i+1)\[) (g) <= m_i $

    D'où,
    $ S_phi^- (f) + S_phi^- (g) &<= S_phi^- (f+g) \
        &<= I^- (f+g) $
    
    ---
    
    Soit $(sigma, tau) in subdiv(a,b)^2$.

    Soit $phi in subdiv(a,b)$ une subdivision plus fine que $sigma$
    et $tau$.

    $ S_sigma^- (f) + S_tau^- (g) &<= S_phi^- (f) + S_phi^- (g) \
        &<= I^- (f+g) $
    
    ---

    On fait la même chose avec des les intégrales supérieures. $qed$
]

#ycor(name: [Intégrale d'une somme de fonctions])[
    Sit $f$ et $g$ (bornées) sont intégrables sur $[a,b]$
    alors $f+g$ aussi et
    $ integral_[a,b] (f+g) = integral_[a,b] (f) + integral_[a,b] (g) $
]

#yproof[
    $ &I_[a,b]^- (f+g) \
        &>= I_[a,b]^- (f) + I_[a,b]^- (g) \
        &wide =I_[a,b]^+ (f) + I_[a,b]^+ (g) \
        &>= I_[a,b]^+ (f+g) $

    Or,
    $I_[a,b]^- (f+g) <= I_[a,b]^+ (f+g) $.
]

#yprop(name: [Intégrale inférieure/supérieure multipliée par un scalaire])[
    Soit $f: [a,b] -> RR$ bornée, $lambda in RR$.
    $ I_[a,b]^- (lambda f) =
        cases(lambda I_[a,b]^- (f) &"si" lambda >= 0,
            lambda I_[a,b]^+ (f) &"si" lambda < 0) $
    $ I_[a,b]^+ (lambda f) =
        cases(lambda I_[a,b]^+ (f) &"si" lambda >= 0,
            lambda I_[a,b]^- (f) &"si" lambda < 0) $
]

#ycor(name: [Intégrale d'une fonction multipliée par un scalaire])[
    Si $f$ est intégrable, alors
    pour tout $lambda in RR$, $lambda f$ est intégrable et
    $ integral_[a,b] (lambda f) = lambda integral_[a,b] f $
]

#yprop(name: [Intégrale inférieure d'une fonction positive])[
    Si $f: [a,b] -> RR^+$ bornée.

    Alors $I_[a,b]^- (f) >= 0$.
]

#yproof[
    Soit $sigma = (x_0, ..., x_n) in subdiv(a,b)$.

    Soit $ i in [|0, n-1|]$.

    On pose $m_i = inf_(\]x_i, x_(i+1)\[) (f) $.

    Or
    $ forall t in ]x_i, x_(i+1)[, f(t) >= 0 $
    donc $m_i >= 0$.

    Donc
    $ S_sigma^+ (f) = sum_(i=0)^(n-1) (x_(i+1) -x_i) m_i >= 0 $
    et enfin
    $ I_[a,b]^- (f) >= S_sigma^- (f) >= 0 $
]

#ycor(name: [Intégrale d'une fonction positive])[
    Si $f$ est intégrable et positive, alors
    $ integral_[a,b] f >= 0 $
]

#yproof[
    Car $I_[a,b]^- (f) >= 0$.
]

#ycor(name: [Intégrales de fonctions comparées])[
    Soit $f$ et $g$ intégrables sur $[a,b]$ telles que
    $ forall t in [a,b], f(t) <= g(t) $
    Alors
    $ integral_[a,b] f <= integral_[a,b] g $
]

#yproof[
    $ integral_[a,b] g - integral_[a,b] f >= 0 $
]

#yprop(name: [Relation de Chasles pour les intégrales inférieures/supérieure])[
    Soit $f: [a,b] -> RR$ bornée
    et $c in [a,b]$. Alors
    $ I_[a,b]^- (f) = I_[a,c]^- (f) + I_[c,b]^- (f) $
    $ I_[a,b]^+ (f) = I_[a,c]^+ (f) + I_[c,b]^+ (f) $
]

#yproof[
    Flemme de copier la preuve, voici une contrepartie :
    #figure(canvas(
        length: 2em,
        {
        riesketcher(
            start: 0,
            end: 2.2,
            plot-x-tick-step: 1,
            plot-y-tick-step: 1,
            x => calc.pow(x - 1, 3) + 1
        )
        // riesketcher(
        //     start: 0,
        //     end: 2.2,
        //     plot-x-tick-step: 1,
        //     plot-y-tick-step: 1,
        //     x => calc.pow(x - 1, 3) + 1
        // )
    }),
        caption: [Contrepartie]
    )

]

#yprop(name: [Relation de Chasles pour les intégrales])[
    Soit $f: [a,b] --> RR$ bornée et $c in ]a,b[$.

    $f$ est intégrable sur $[a,b]$ ssi 
    $f$ est intégrable sur $[a,c]$ et $[c, b]$.

    Dans ce cas,
    $ integral_[a,b] f = integral_[a,c] f + integral_[c,b] f $
]

#yproof[
    On suppose $f$ intégrable sur $[a,b]$
    $ I_[a,c]^- (f) <= I_[a,c]^+ (f) $
    $ I_[c,b]^- (f) <= I_[c,b]^+ (f) $

    D'où, en sommant ces inégalités
    $ (*) I_[a,b]^+ (f) <= I_[a,b]^+ (f) $
    Comme $f$ est intégrable sur $[a,b]$,
    $(*)$ est une égalité.

    Donc
    $ I_[a,c]^- (f) = I_[a,c]^+ $
    $ I_[c,b]^- (f) = I_[c, b]^+ $
    et donc $f$ est intégrable sur $[a,c]$ et $[c, b]$.

    Réciproquement, on suppose $f$ intégrable sur $[a,c]$
    et $[c, b]$.
    $ I_[a,b]^-
        &= I_[a,c]^- (f) + I_[c,b]^- (f) \
        &= I_[a,c]^+ (f) + I_[c,b]^+ (f) \
        &= I_[a,b]^+ $
    donc $f$ est intégrable sur $[a,b]$.
]

#yprop(name: [Intégalité triangulaire pour les intégrales])[
    Soit $f$ intégrable sur $[a,b]$.

    $ abs(integral_[a,b] (f)) <= integral_[a,b] abs(f) $
]

#yproof[
    Voir poly.
]