#import "../../template/ytemplate.typ": *


#ypart[Sommes de Darboux]


#let subdiv(a,b) = {$frak(S)_[#a,#b]$}


#ydef(name: [Somme de Darboux supérieure/inférieure])[
    Soit $f: [a,b] -> RR$ une fonction bornée.

    Soit $sigma = (x_0, ..., x_n) in subdiv(a,b)$.

    On note, pour tout $i in [|0, n-1|]$,
    $ M_i = sup_(\]x_i, x_(i+1)\[) (f) $
    $ m_i = inf_(\]x_i, x_(i+1)\[) (f) $
    (Elles existent car les ensembles ${f(x) | x_i < x < x_(i+1)}$ sont non vides majorés.)

    La _somme de Darboux supérieure de $f$ pour $sigma$_
    est le nombre
    $ S_sigma^+ (f) = sum_(i=0)^(n-1) M_i (x_(i+1) - x_i) $
    et la _somme de Darboux inférieure de $f$ pour $sigma$ est_
    $ S_sigma^- (f) = sum_(i=0)^(n-1) m_i (x_(i+1) - x_i) $

    N.B. notations non officielles.
]

#yprop(name: [Somme de Darboux pour une subdivision plus fine qu'une autre])[
    Soit $f: [a,b] -> RR$ bornée et $(sigma, tau) in  subdiv(a,b)^2$
    telle que $tau prec sigma$.
    Alors
    $ cases(S_tau^+ (f) <= S_sigma^+ (f),
        S_tau^- (f) >= S_sigma^- (f)) $
]

#yproof[
    On note $sigma = (x_0, ..., x_n)$ \
    et $tau = (y_0, ..., y_n)$.

    Soit $ i in [|0, n-1|]$. Soit $j$ tel que $x_i = y_j$
    et $k$ tel que $x_(i+1) = y_(j+k)$.

    Soit $ell in [|0, k-1|]$.

    Comparer $sup_(\]y_(j+ell) , y_(j+ell+1)\[) (f)$ \
    et $sup_(\]x_(i+1) , x_(i+1)\[) (f) = M_i$.

    Pour $t in \]y_(j+ell) - y_(j+ell+1)\[$,
    $ f(t) <= M_i$
    car $M_i$ majore ${f(t) | x_i < t < x_(i+1)}$.

    Donc $M_i$ majore
    $ {f(t) | y_(j+ell) < t < y_(j+ell+1)} $
    et donc $M_i >= sup_(\]y_(j+ell) , y_(j+ell+1)\[) (f)$.

    D'où,
    $ &sum_(ell=0)^(k-1) (y_(j+ell+1) - y_(j+ell)) sup_(\]y_(j+ell) , y_(j+ell+1)\[) (f) \
        &<= sum_(ell=0)^(k-1) (y_(j+ell+1) - y_(j+ell)) M_i \
        &<= M_i (y_(j+k) - y_j) \
        &<= M_i (x_(i+1) - x_i) $
    
    donc
    $ &S_tau^+ (f) \
        &= sum_(i=0)^(n-1) sum_(ell=0)^(k-1) (y_(j+ell+1) - y_(j+ell)) \
            &wide times sup_(\]y_(j+ell) , y_(j+ell+1)\[) (f) \
        &<= sum_(i=0)^(n-1) M_i (x_(i+1) - x_i) \
        &= S_sigma^+ (f) $
]

#ydef(name: [Intégrale supérieure/inférieure d'une fonction])[
    Soit $f: [a,b] -> RR$ bornée

    L'_intégrale supérieure de $f$ sur $[a,b]$_ 
    est
    $ I_[a,b]^+ (f) =  inf_(sigma in subdiv(a,b)) (S_sigma^+ (f)) $
    et l'_intégrale inférieure de $f$ sur $[a,b]$_ est
    $ I_[a,b]^- (f) = sup_(sigma in subdiv(a,b)) (S_sigma^- (f)) $

    N.B. notation pas officielles.
]

#ylemme(name: [Relation entre les intégrales supérieure et inférieures])[
    Pour toute fonction $f : [a,b] -> RR$ bornée,
    $ I_[a,b]^- (f) <= I_[a,b]^+ (f) $
]

#yproof[
    Pour toute subdivision $sigma in subdiv(a,b)$,
    $ S_sigma^- (f) <= S_sigma^+ (f) $

    Soit $(sigma, tau) in subdiv(a,b)^2$
    et $phi in subdiv(a,b)$ telle que
    $ cases(phi prec sigma, phi prec tau) $

    Alors
    $ S_tau^- (f) <= S_phi^- (f) <= S_phi^+ <= S_sigma^+ (f) $

    Or,
    $ &I_[a,b]^- (f) <= I_[a,b]^+ (f) \
        &<=> forall sigma in subdiv(a,b), I_[a,b]^- (f) <= S_sigma^+ (f) \
        &<=> forall tau in subdiv(a,b), forall sigma in subdiv(a,b), \
            &wide S_tau^- (f) <= S_sigma^+ (f) $
    
    D'où,
    $ I_[a,b]^- (f) <= I_[a,b]^+ (f) $
]

#yprop(name: [Relation entre les intégrales supérieure, inférieure et l'intégrale d'une fonction en escalier])[
    Soit $f: [a,b] -> RR$ une fonction en escalier. Alors
    $ I_[a,b]^+ (f) =I_[a,b]^- (f) = integral_[a,b] f $
]

#yproof[
    Soit $sigma = (x_0, ..., x_n)$ une subdivision
    adaptée à $f$.
    $ integral_[a,b] = sum_(i=0)^(n-1) m_i (x_(i+1) - x_i) $
    où $m_i$ est la valeur de $f$ sur l'interval $]x_i, x_(i+1)[$.

    On remarque que
    $ integral_[a,b] f = S_sigma^+ (f) = S_sigma^- (f) $

    On en déduit que
    $ I_[a,b]^- (f) >= integral_[a,b] f >= I_[a,b]^+ (f) $
    car $I_[a,b]^+$ minore toutes les sommes de
    Darboux supérieures, $I_[a,b]^- (f)$ majore toutes les sommes de Darboux
    inférieures.

    Or on sait que,
    $ I_[a,b]^- (f) <= I_[a,b]^+ (f) $

    Ainsi,
    $ I_[a,b]^+ (f) =I_[a,b]^- (f) = integral_[a,b] f $
]

#ydef(name: [Fonction Riemann-intégrable, et intégrale de celle-ci])[
    Soit $f: [a,b] -> RR$ bornée.

    On dit que $f$ est _Riemann-intégrable_ si
    $I_[a,b]^- (f) = I_[a,b]^+ (f)$.

    Dans ce cas, l'_intégrale de $f$_ est
    $ I_[a,b]^- (f) = I_[a,b]^+ (f) $
    et elle est notée
    $ integral_[a,b] f $
]