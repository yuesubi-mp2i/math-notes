#import "../../template/ytemplate.typ": *


#ypart[Familles positives]


#let seq(u, i, I, E) = {$(#u _#i)_(#i in #I) in #E ^#I$}

#ydef(name: [Famille sommable positive])[
    Soit $seq(u, i, I, (RR^+))$.

    On dit que $(u_i)$ est _sommable_ si
    $ {sum_(i in J) u_i mid(|) cases(J subset I, J "fini") } $
    est majorée.
]

#ydef(name: [Somme d'une famille sommable positive])[
    Soit $seq(u, i, I, (RR^+))$ une famille.

    Si la famille est sommable, on définit la _somme_
    de la famille par
    $ sum_(i in I) u_i = sup_(J subset I \ J "fini") (sum_(i in J) u_i) $

    Si la famille n'est pas sommable, on convient que
    $ sum_(i in I) u_i = + oo $
]

#yprop(name: [Somme d'une famille finie positive])[
    Toute famille finie de réels positifs ou nuls est sommable.
]

#yprop(name: [Caractérisation d'une famille sommable positive par une série])[
    Soit $seq(u, n, NN, (RR^+))$.

    La famille $(u_n)$ est sommable ssi $sum u_n$ converge
    et dans ce cas,
    $ sum_(n in NN) u_n = sum_(n=0)^(+oo) u_n $
]

#yproof[
    On suppose que $sum u_n$ converge.

    Soit $J subset NN$ fini.
    On pose $N = max(J)$, et donc $J subset [|0, N|]$.
    $ sum_(j in J) u_j <= sum_(n=0)^N u_n <= sum_(n=0)^(+oo) u_n $

    Ainsi,
    $ { sum_(j in J) u_j mid(|) cases(J subset NN, J "fini") } $
    est majoré par $sum_(n=0)^(+oo) u_n$
    donc la famille est sommable et
    $ sum_(n in NN) u_n <= sum_(n=0)^(+oo) u_n $

    Soit $epsilon > 0$.
    Par définition d'un limite d'une suite croissante,
    il existe $N in NN$ tel que
    $ sum_(n=0)^(+oo) u_n - sum_(n=0)^N u_n <= epsilon $
    donc
    $ sum_(n=0)^(+oo) u_n - epsilon <= sum_(n=0)^N u_n $

    Donc $sum_(n=0)^(+oo) u_n - epsilon$ ne majore pas
    $ { sum_(j in J) u_j mid(|) cases(J subset NN, J "fini") } $

    D'où
    $ sum_(n=0)^(+oo) u_n = sum_(n in NN) u_n $

    ---

    On suppose désormais que $sum u_n$ diverge.

    Alors
    $ &forall M in RR, exists N in NN, forall n >= N, \
        &wide sum_(k=0)^n u_k > M $

    On suppose 
    $ { sum_(j in J) u_j mid(|) cases(J subset NN, J "fini") } $
    majorée par $ell in RR$.

    Or il existe $N in NN$ tel que
    $ forall n >= N, sum_(k=0)^n u_k > ell $

    Donc
    $ { sum_(j in J) u_j mid(|) cases(J subset NN, J "fini") } $
    ne majore pas $ell$ : ce qui est une contradiction.
]

#yprop(name: [Somme d'un sous-famille d'une famille positive])[
    Un sous-famille $(u_i)_(i in J)$ d'une famille sommable
    $(u_i)_(i in I)$ de réels positifs ou nuls est sommable et
    $ sum_(i in J) u_i <= sum_(i in I) u_i $
]

#yproof[
    Soit $K subset J$, $K$ fini.

    Comme $J subset I$, alors $K subset I$ et donc
    $ sum_(k in K) u_k <= sum_(i in I) u_i $

    Donc
    $ { sum_(k in K) u_k mid(|) cases(K subset J, K "fini")} $
    est majorée donc $(u_j)_(j in J)$ est sommable
    et
    $ sum_(j in J) u_j = sup_(K subset J \ K "fini") (sum_(k in K) u_k)
        <= sum_(i in I) u_i $
]

#yprop(name: [Somme d'une somme de familles positives])[
    Soit $(u_i)_(i in I)$ et $(v_i)_(i in I)$ deux
    familles de réels positifs ou nuls.

    Alors
    $ sum_(i in I) (u_i + v_i) = sum_(i in I) u_i + sum_(i in I) v_i $
    et
    $(u_i + v_i)_(i in I)$ est sommable ssi $(u_i)$ et $(v_i)$
    sont sommables.
]

#yproof[
    On suppose $(u_i)_(i in I)$ et $(v_i)_(i in I)$
    sommables.

    Soit $J subset I$ fini
    $ sum_(j in J) (u_j + v_j) &= sum_(j in J) u_j + sum_(j in J) v_j \
        &<= sum_(i in I) u_i + sum_(i in I) v_i $
    
    Ainsi $(u_i + v_i)$ est sommable et
    $ sum_(i in I) (u_i + v_i) <= sum_(i in I) u_i + sum_(i in I) v_i $

    Soit $epsilon > 0$. Il existe $J subset I$ fini tel que
    $ sum_(i in I) u_i - epsilon/2 < sum_(j in J) u_j $
    et il existe $K subset I$ fini tel que
    $ sum_(i in I) v_i - epsilon/2 < sum_(k in K) v_k $

    On pose $L = J union K$. Alors $J subset I$ et il est fini.
    $ &sum_(ell in L) (u_ell + v_ell) \
        &= sum_(ell in L) u_ell + sum_(ell in L) v_ell \
        &>= sum_(j in J) u_j + sum_(k in K) v_k \
        &> sum_(i in I) u_i - epsilon/2 + sum_(i in I) v_i - epsilon/2 \
        &> (sum_(i in I) u_i + sum_(i in I) v_i) - epsilon $
    
    Donc
    $ sum_(i in I) (u_i + v_i) = sum_(i in I) u_i + sum_(i in I) v_i $

    On suppose désormais que
    $(u_i + v_i)_(i in I)$ est sommable.

    Soit $J subset I$, $J$ fini
    $ sum_(j in J) u_j <= sum_(j in J) (u_j + v_j)
        <= sum_(i in I) (u_i + v_i) $
    
    Donc $(u_i)_(i in I)$ est sommable. De même pour la
    famille $(v_i)_(i in I)$.
]

#yprop(name: [Somme de famille positives comparées])[
    Soit $(u_i)_(i in I)$ et $(v_i)_(i in I)$ deux
    familles de réels positifs ou nuls telles que
    $ forall i in I, 0 <= u_i <= v_i $

    Si $(v_i)_(i in I)$ est sommable, alors
    $(u_i)_(i in I)$ est sommable. De plus,
    $ sum_(i in I) u_i <= sum_(i in I) v_i $
]

#yproof[
    On suppose $(v_i)_(i in I)$ sommable.

    Soit $J subset I$ fini, alors
    $ sum_(j in J) u_j <= sum_(j in J) v_j <= sum_(i in I) v_i $

    Donc $(u_i)_(i in I)$ est sommable et
    $ sum_(i in I) u_i <= sum_(i in I) v_i $
]

#yprop(name: [Somme d'une produit d'une famille positive par un scalaire])[
    Soit $seq(u, i, I, (RR^+))$  et $lambda > 0$.

    La famille $(u_i)_(i in I)$ est sommable
    ssi $(lambda u_i)_(i in I)$ est sommable.

    De plus,
    $ sum_(i in I) lambda u_i = lambda sum_(i in I) u_i $
]

#yproof[
    On suppose $(u_i)_(i in I)$ sommable.

    Soit $J subset I$ avec $J$ fini.
    $ sum_(j in J) lambda u_j &= lambda sum_(j in J) u_j \
        &<= lambda sum_(i in I) u_i $
    
    Donc  $(lambda u_i)_(i in I)$ est sommable et
    $ sum_(i in I) lambda u_i <= lambda sum_(i in I) u_i $

    On suppose désormais que $(lambda u_i)_(i in I)$ sommable.

    D'après le raisonnement précédent, comme $1/lambda > 0$,
    $ (u_i)_(i in I) = (1/lambda lambda u_i)_(i in I)$ est
    sommable
    et
    $ sum_(i in I) 1/lambda (lambda u_i) <= 1/lambda sum_(i in I) lambda u_i $
    et donc
    $ lambda sum_(i in I) u_i <= sum_(i in I) lambda u_i $
]

#yprop(name: [Somme d'une permutation d'une famille positive])[
    Soit $seq(u, i, I, (RR^+))$ et
    $sigma: I -> I$ une bijection.

    La famille $(u_i)_(i in I)$ sommable ssi
    $(u_sigma(i))_(i in I)$ sommable.

    De plus,
    $ sum_(i in I) u_sigma(i) = sum_(i in I) u_i $
]

#yproof[
    $ &{ sum_(j in J) u_sigma(j) mid(|) cases(J subset I, J "fini")} \
        &= { sum_(i in J) u_i mid(|) cases(J subset I, J "fini")} $

    Par le théorème de changement d'indices sur les sommes finies.
]

#ytheo(name: [Théorème de sommation par paquets (positif)])[
    Sot $(u_i)_(i in I)$ une famille de réels positifs.

    Soit $(I_k)_(k in K)$ une partition de $I$ :
    $ I = union.big.dot_(k in K) I_k $

    Alors la famille $(u_i)_(i in I)$ est sommable
    ssi,
    $ cases(forall k in K\, (u_i)_(i in I_k) "est sommable",
        (sum_(i in I_k) u_i)_(k in K) "est sommable") $
    
    De plus,
    $ sum_(i in I) u_i = sum_(k in K) sum_(i in I_k) u_i $
]

#yproof[
    _La preuve n'est pas au programme._

    "$<==$" On suppose que
    $ cases(forall k in K\, (u_i)_(i in I_k) "est sommable",
        (sum_(i in I_k) u_i)_(k in K) "est sommable") $
    
    Soit $J subset I$, $J$ fini.

    On pose, pour $k in K$, $J_k = J sect I_k$.

    Pour $k in K$, $J_k$ est fini et $J= union.big.dot_(k in K) J_k$.
    $ sum_(j in J) u_j = sum_(k in K) sum_(j in J_k) u_j
        <= sum_(k in K) sum_(i in I_k) u_i $
    
    Donc $(u_i)_(i in I)$ est sommable et
    $ sum_(i in I) u_i <= sum_(k in K) sum_(i in I_k) u_i $

    "$==>$" On suppose $(u_i)_(i in I)$ sommable.

    Soit $k in K$.

    $(u_i)_(i in I_k)$ est un sous-famille de $(u_i)_(i in I)$,
    donc elle est sommable.

    Soit $epsilon > 0$.

    Il existe $(epsilon_k)_(k in K)$ sommable positive
    telle que
    $ sum_(k in K) epsilon_k = epsilon $
    et, pour $k in K$, $epsilon_k > 0$.

    Il existe $J_k subset I_k$, $J_k$ fini telle que
    $ sum_(i in I_k) u_i - epsilon_k < sum_(i in J_k) u_i $

    Soit $L subset K$, $L$ fini
    $ sum_(ell in L) (sum_(i in I_k) u_i)
        &<= sum_(ell in L) (sum_(i in J_ell) u_i + epsilon_k) \
        &<= sum_(ell in L) sum_(i in J_ell) u_i + sum_(ell in L) epsilon_ell \
        &<= sum_(i in I) u_i + epsilon $
    
    Donc $(sum_(i in I_k) u_i)_(k in K)$ est sommable
    et
    $ sum_(k in K) (sum_(i in I_k) u_i) <= sum_(i in I) u_i + epsilon $

    En passant à la limite quand $epsilon --> 0^+$,
    on obtient
    $ sum_(k in K) sum_(i in I_k) u_i <= sum_(i in I) u_i $
]

#ytheo(name: [Théorème de Fubini (positif)])[
    Soit $(u_(i,j))_((i,j) in K) in (RR^+)^K$ où $K = I times J$.

    La famille $(u_(i,j))$ est sommable ssi
    $ cases(forall j in J\, (u_(i,j))_((i,j) in L_j) "sommable",
        (sum_(j in J) sum_(i in J_j) u_(i,j)) "sommable") $
    
    D'où,
    $ sum_(i in I) sum_(j in J) u_(i,j)
        = sum_(j in J) sum_(i in I) u_(i,j) $
    (énoncé pas clair)
]

#yproof[
    $ K &= union.big.dot_(i in I) {(i,j) mid(|) cases(j in J, (i,j) in K)} \
        &= union.big.dot_(i in I) K_i $

    $(u_(i,j))$ est sommable ssi
    $ cases(forall i in I\, (u_(i,j))_(j in K_i) "sommable",
        (sum_(i in I) sum_(j in K_i) u_(i,j)) "sommable") $
    
    $ sum_((i,j) in K) = sum_(i in I) sum_(j in K) u_(i,j) $

    On a aussi
    $ K &= union.big.dot_(j in J) { (i,j) mid(|) cases(i in I, (i,j) in K)} \
        &= union.big.dot_(j in J) L_j $
    
    $(u_(i,j))$ sommable ssi
    $ cases(forall j in J\, (u_(i,j))_((i,j) in L_j) "sommable",
        (sum_(j in J) sum_(i in J_j) u_(i,j)) "sommable") $
    
    De plus,
    $ sum_((i,j) in K) u_(i,j) = sum_(j in J) sum_(i in L_j) u_(i,j) $

    D'où,
    $ sum_(i in I) sum_(j in K_i) u_(i,j)
        = sum_(j in J) sum_(i in L_j) u_(i,j) $
]