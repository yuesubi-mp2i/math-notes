#import "../../template/ytemplate.typ": *


#ypart[Produit de Cauchy]


#ydef(name: [Produit de Cauchy])[
    Soient $(u_n) in CC^NN$ et $(v_n) in CC^NN$.

    Le _produit de Cauchy_ des séries
    $sum u_n$ et $sum v_n$ est la série $sum w_n$
    où
    $ forall n in NN, w_n = sum_(k=0)^n u_k v_(n-k) $
]

#ytheo(name: [Produit de Cauchy comme le produit de séries])[
    Si $sum u_n$ et $sum v_n$ convergent absolument,
    alors leur produit de Cauchy $sum w_n$ aussi et
    $ sum_(n=0)^(+oo) w_n = (sum_(n=0)^(+oo) u_n) (sum_(n=0)^(+oo) v_n) $
]

#yproof[
    On pose $a_(k,n) = u_k v_(n-k)$ pour $n in NN$
    et $k in [|0, n|]$.

    Soit $T = {(k,n) in NN^2 | k <= n}$.

    #align(center)[
        ```text
        n
        ^
        + + + +
        + + +
        + +
        +-------> k
        ```
    ]

    Soit $U$ une partie finie non vide de $T$.

    Soit $N = max{n in NN | exists k, (k,n) in U }$

    $ &sum_((k,n) in U) abs(a_(k,n)) \
        &<= sum_(n=0)^N sum_(k=0)^n abs(a_(k,n)) \
        &wide = sum_(n=0)^N sum_(k=0)^n abs(u_k) abs(v_k) \
        &wide = sum_(k=0)^N sum_(n=k)^N abs(u_k) abs(v_(n-k)) \
        &wide = sum_(k=0)^N (abs(u_k) sum_(ell=0)^(N-k) abs(v_ell)) $
    
    Or, pour $k in [|0, N|]$,
    $ sum_(ell=0)^(N-k) abs(v_ell) <= sum_(ell=0)^(+oo) abs(v_k) $

    D'où
    $ &sum_((k,n) in U) abs(a_(k,n)) \
        &<= sum_(k=0)^N abs(u_k) sum_(ell=0)^(+oo) abs(v_ell) \
        &<= (sum_(k=0)^(+oo) abs(u_k)) (sum_(ell=0)^(+oo) abs(v_ell)) $
    
    Comme $(sum_(k=0)^(+oo) abs(u_k)) (sum_(ell=0)^(+oo) abs(v_ell))$
    ne dépend pas de $U$, la famille $(a_(k,n))_((k,n) in T)$ est
    sommable.

    D'après le théorème de Fubini,
    $ sum_((k,n) in T) a_(k,n)
        &= sum_(k in NN) sum_(n >= k) u_k v_(n-k) \
        &= sum_(k=0)^(+oo) u_k sum_(n = k)^(+oo) v_(n-k) \
        &= sum_(k=0)^(+oo) u_k sum_(ell=0)^(+oo) v_ell  $
    (avec $ell = n-k$)
    et aussi
    $ sum_((k,n) in T) a_(k, n)
        &= sum_(n in NN) sum_(k=0)^n u_k v_(n-k) \
        &= sum_(n=0)^(+oo) w_n $
]