#import "../../template/ytemplate.typ": *


#ypart[Familles sommables de réels]


#let seq(u, i, I, E) = {$(#u _#i)_(#i in #I) in #E ^#I$}

#ydef(name: [Famille sommable])[
    Soit $seq(u,i,I,RR)$.

    On dit que $(u_i)_(i in I)$ est _sommable_
    si $(abs(u_i))_(i in I)$ est sommable.

    Dans ce cas, on définit $sum_(i in I) u_i$ par
    $ sum_(i in I) u_i = sum_(i in I) u_i^+ - sum_(i in I) u_i^- $
    où, pour $i in I$,
    $ cases(u_i^+ = max(u_i, 0),
        u_i^- = max(-u_i, 0)) $
    
    N.B. cette définition est compatible avec la définition pour les
    familles positives.
]

#ynot(name: [Ensemble des familles sommables])[
    On note $ell^1 (I)$ l'ensemble des familles
    sommables indexées par $I$.
]

#ybtw[
    On suppose $(u_i)_(i in I)$ sommable
    $ forall i in I,  abs(u_i) = u_i^+ + u_i^- >= u_i^+ $
    donc
    $ sum_(i in I) u_i^+ < +oo$.

    De même $sum_(i in I) u_i^- < +oo$.

    Donc $sum_(i in I) u_i in RR$.
]

#yprop(name: [_Sommabilité_ d'une famille finie])[
    Toute famille finie de réels est sommable.
]

#yprop(name: [Caractérisation d'une famille sommable par une série])[
    Soit $(u_n) in RR^NN$.

    La famille $(u_n)$ est sommable ssi
    $sum (u_n)$ converge absolument.

    Dans ce cas,
    $ sum_(n in NN) u_n = sum_(n=0)^(+oo) u_n $
]

#yprop(name: [Combinaison linéaire de famille sommables])[
    Soit $(u_i)_(i in I)$ et $(v_i)_(i in I)$ deux
    familles réelles sommable, $(lambda, mu) in RR^2$.

    Alors $(lambda u_i + mu v_i)_(i in I) in ell^1 (I)$
    et
    $ sum_(i in I) (lambda u_i + mu v_i)
        = lambda sum_(i in I) u_i + mu sum_(i in I) v_i $
]

#yproof[
    Pour $i in I$,
    $ abs(lambda u_i + mu v_i) <=
        abs(lambda) abs(u_i) + abs(mu) abs(v_i) $
    
    $(abs(u_i))$ et $(abs(v_i))$ sont sommables positives,
    $abs(lambda) >= 0$ et $abs(mu) >= 0$.

    Donc $(abs(lambda) abs(u_i) + abs(mu) + abs(v_i))_(i in I)$
    est sommable \
    donc $(abs(lambda u_i + mu v_i))_(i in I)$ est sommable \
    donc $(lambda u_i + mu v_i)_(i in I)$ est sommable.

    $ &sum_(i in I) (lambda u_i + mu v_i) \
        &= sum_(i in I) (lambda u_i + mu v_i)^+ \
            &wide - sum_(i in I) (lambda u_i + mu v_i)^- $

    - #underline[Lemme] :
        Soit $(u_i) in RR^I$ sommable.

        Alors
        $ &forall epsilon > 0, exists J_epsilon subset I, J_epsilon "fini",
            forall J in cal(P)(I), \
            &cases(J "fini", J_epsilon subset J) =>
                abs(sum_(i in I) u_i - sum_(i in J) u_i) <= epsilon $
        
        De plus, si $S in RR$ vérifie
        $ &forall epsilon > 0, exists J_epsilon subset I, J_epsilon "fini",
            forall J in cal(P)(I), \
            &cases(J "fini", J_epsilon subset J) =>
                abs(S - sum_(i in J) u_i) <= epsilon $
        alors $S = sum_(i in I) u_i$.
    
    - #underline[preuve] :
        Soit $epsilon > 0$.
        
        Il existe $J_epsilon^+ subset I$, fini tel que 
        $ sum_(i in I) u_i^+ - epsilon/2
            < sum_(i in J_epsilon^+) u_i^+
            <= sum_(i in I) u_i^+ $
        
        Il existe $J_epsilon^- subset I$, fini tel que 
        $ sum_(i in I) u_i^- - epsilon/2
            < sum_(i in J_epsilon^-) u_i^-
            <= sum_(i in I) u_i^- $
        
        On pose $J_epsilon = J_epsilon^+ union J_epsilon^-$ :
        c'est une partie finie de $I$.

        Soit $J$ une partie finie de $I$
        telle que $J_epsilon subset J$.

        $ &abs(sum_(i in I) u_i - sum_(i in J) u_i) \
            &= abs(sum_(i in I) u_i^+ sum_(i in I) u_i^- \
                &wide - sum_(i in J) u_i^+ + sum_(i in J) u_i^-) \
            &= abs((sum_(i in I) u_i^+ - sum_(i in J) u_i^+) \
                &wide - (sum_(i in I) u_i^- - sum_(i in J) u_i^-)) \
            &<= abs(sum_(i in I) u_i^+ - sum_(i in J) u_i^+) \
                &wide + abs(sum_(i in I) u_i^- - sum_(i in J) u_i^-) $
        
        Comme $J_epsilon^+ subset J$ et commme
        $ forall i in I, u_i^+ >= 0 $

        $ sum_(i in I) u_i^+ - epsilon/2
            &<= sum_(i in J_epsilon^+) u_i^+ \
            &<= sum_(i in J) u_i^+ \
            &<= sum_(i in I) u_i^+ $
        donc
        $ abs(sum_(i in I) u_i^+ - sum_(i in J) u_i^+) <= epsilon/2 $

        De même, $J_epsilon^- subset J$,
        $ abs(sum_(i in I) u_i^- - sum_(i in J) u_i^-) <= epsilon/2 $

        On a bien
        $ abs(sum_(i in I) u_i - sum_(i in J) u_i) <= epsilon $

        ---

        Soit $S in RR$ comme dans l'énoncé.

        Soit $epsilon > 0$.
        Il existe $J_epsilon subset I$ et $K_epsilon subset I$
        finis tel que , pout toutes parties finies $J$ et $K$
        de $I$,

        $ J_epsilon subset J
                => abs(sum_(i in I) u_i - sum_(i in J) u_i) <= epsilon $
        $ K_epsilon subset K => abs(S - sum_(i in K) u_i) <= epsilon $

        Soit $L = J_epsilon union K_epsilon$.
        $ &abs(sum_(i in I) u_i - S) \
            &= abs(sum_(i in I) u_i - sum_(i in L) u_i + sum_(i in L) u_i - S) \
            &<= abs(sum_(i in I) u_i - sum_(i in L) u_i)
                + abs(S - sum_(i in L) u_i) \
            &<= epsilon + epsilon = 2 epsilon $
        
        On en déduit que
        $ abs(sum_(i in I) u_i - S) = 0 $
        _i.e._ $S = sum_(i in I) u_i$. $qed$

    On pose $S = lambda sum_(i in I) u_i + mu sum_(i in I) v_i$.

    Soit $epsilon > 0$.

    Soit $J$ une partie finie de $I$
    $ &abs(S - (sum_(i in J) (lambda u_i + mu v_i))) \
        &= abs(lambda (sum_(i in I) u_i - sum_(i in J) u_i) \
            &wide + mu (sum_(i in I) v_i - sum_(i in J) v_i)) \
        &<= abs(lambda) abs(sum_(i in I) u_i - sum_(i in J) u_i) \
            &wide + abs(mu) abs(sum_(i in I) v_i - sum_(i in J) v_i) $
    
    Soit $J_epsilon$ fini tel que, pour toute partie
    fine $J$ telle que $J_epsilon subset J$,
    $ abs(sum_(i in I) u_i - sum_(i in J) u_i) <= epsilon $
    et soit $K_epsilon$ finie telle que, pour toute
    partie finie J telle que $K_epsilon subset J$,
    $ abs(sum_(i in I) v_i - sum_(i in J) v_i) <= epsilon $

    On pose $L_epsilon = J_epsilon union K_epsilon$.

    Ainsi, pour toute partie finie $J$ telle
    que $L_epsilon subset J$,
    $ &abs(S - sum_(i in J) (lambda u_i + mu v_i)) \
        &<= abs(lambda) abs(sum_(i in I) u_i - sum_(i in J) u_i) \
            &wide + abs(mu) abs(sum_(i in I) v_i - sum_(i in J) v_i) \
        &= abs(lambda) epsilon + abs(mu) epsilon $
    
    D'après le lemme
    $ S = sum_(i in I) (lambda u_i + mu v_i) $ $qed$

    Soit $L = J_epsilon union K_epsilon$.

    $ &abs(sum_(i in I) u_i - S) \
        &= abs(sum_(i in I) u_i - sum_(i in L) u_i + sum_(i in L) u_i - S) \
        &<= abs(sum_(i in I) u_i - sum_(i in L) u_i) + abs(S- sum_(i in L) u_i) \
        &<= epsilon + epsilon = 2 epsilon $

    On en déduit que
    $ abs(sum_(i in I) u_i - S) = 0 $
    _i.e._ $S = sum_(i in I) u_i$.

    #underline[Remarque] :
    #emph[
        On peut démontrer la linéarité de façon plus
        simple dans le cas où $I$ est dénombrable infini.
    ]

    Soit $phi : NN -> I$ une bijection.

    On pose
    $ forall n in NN, a_n u_phi(n) $

    Pour tout $n in NN$,
    $ sum_(n=0)^N a_n^+ &= sum_(i in phi([|0, N|])) u_i^+ \
        &<= sum_(i in I) u_i^+ $
    
    Donc la série $sum_n a_n^+$ converge.

    De même, $sum_n a_n^-$ converge.

    $ forall n in NN, a_n = a_n^+ + a_n^- $

    Par linéarité (pour les séries), $sum a_n$ converge.

    $ sum_(n=0)^(+oo) &= sum_(n=0)^(+oo) a_n^+ + sum_(n=0)^(+oo) a_n^- \
        &= sum_(n in NN) a_n^+ + sum_(n in NN) a_n^- \
        &= sum_(i in I) u_i^+ - sum_(i in I) u_i^+ \
        &= sum_(i in I) u_i $
    
    #emph[
        Il n'y a pas de différencee entre calculer
        $sum_(i in I) u_i$ et calculer la somme de
        la série $sum a_n$ dans le cas où cette
        série converge absolument.

        La linéarité au niveau des séries donne
        la linéarité au niveau des familles
        sommables.
    ]
]

#yprop(name: [Somme d'une permutation d'une famille])[
    Soit $seq(u, i, I, RR)$ une famille sommable
    et $sigma: I -> I$ une bijection.
    $ sum_(i in I) u_i = sum_(i in I) u_sigma(i) $
]

#yproof[
    $ sum_(i in I) u_sigma(i)
        &= sum_(i in I) u_sigma(i)^+ - sum_(i in I) sum_sigma(i)^- \
        &= sum_(i in I) u_i^+ - sum_(i in I) u_i^- \
        &= sum_(i in I) u_i $
]

#ytheo(name: [Théorème de sommation par paquets])[
    Soit $seq(u,i,I, RR)$ et $I = union.big.dot_(k in K) I_k$.

    $ &(u_i)_(i in I) in ell^1 (I) \
        &<=> cases(forall k in K, (u_i)_(i in I_k) in ell^1 (I_k),
            (sum_(i in I_k) u_i)_(k in K) in ell^1 (K)) $
    
    Dans ce cas,
    $ sum_(i in I) u_i = sum_(k in K) sum_(i in I_k) u_i $
]

#yproof[
    Hors-programme.
]

#ytheo(name: [Théorème de Fubini])[
    _Brouillon_

    Soit $(u_(i,j)) in RR^K$ où $K subset I times J$.

    $ K = union.big.dot_(i in I) underbrace({ (i, j) | (i,j) in K}, J_i) $

    $ &(u_(i,j)) in ell^1 (K) \
        &<=> cases(forall i in I\, (u_(i,j))_(j in J) in ell^1 (J_i),
            (sum_(j in J_i) u_(i,j))_i in ell^1 (I)) $
    et dans ce cas
    $ sum_((i,j) in K) u_(i,j) = sum_(i in I) sum_(j in J_i) u_(i,j) $

    $ K = union.dot.big_(j in J) underbrace({(i,j) | (i,j) in K}, I_j) $

    $ &(u_(i,j)) in ell^1 (K) \
        &<=> cases(forall j in J\, (u_(i,j))_(i in I_j) in ell^1 (I_j),
            (sum_()) in ell^1 (J)) $
    
    Dans ce cas,
    $ sum_((i, j) in K) u_(i,j) = sum_(j in J) sum_(i in I_j) u_(i,j) $
]