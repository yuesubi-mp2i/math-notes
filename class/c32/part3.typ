#import "../../template/ytemplate.typ": *


#ypart[Familles sommables de nombre complexes]


#let seq(u, i, I, E) = {$(#u _#i)_(#i in #I) in #E ^#I$}

#ydef(name: [Famille complexe sommable])[
    Soit $seq(u,k,K,CC)$.

    On dit que $(u_k)_(k in K)$ est _sommable_
    si $(abs(u_k))_(k in K)$ est sommable.
]

#ydef(name: [Somme d'une famille complexe sommable])[
    Soit $seq(u,k,K,CC)$ une famille sommable.

    Alors on définit la _somme_ d'une famille complexe par
    $ sum_(k in K) u_k = sum_(k in K) "Re"(k) + i sum_(k in K) "Im"(k) $

    N.B. La notation est cohérente car 
    $ forall k in K, abs("Re"(k)) <= abs(u_k) $
    donc $(abs("Re"(u_k)))_(k in K)$ est sommable.

    De même $(abs("Im"(u_k)))_(k in K)$ est sommable.

    Donc $sum_(k in K) u_k$ est bien définie.
]

#yprop(name: [Linéarité de la somme de familles complexes])[
    Soit $(u_i)_(i in I)$ et $(v_i)_(i in I)$ deux
    familles complexes sommable, $(lambda, mu) in CC^2$.

    Alors la famille $(lambda u_i + mu v_i)_(i in I)$ est sommable et
    $ sum_(i in I) (lambda u_i + mu v_i)
        = lambda sum_(i in I) u_i + mu sum_(i in I) v_i $
]

#yproof[
    En exercice.
]

#yprop(name: ["Sommabilité" d'une famille complexe à partir des parties réelles et imaginaires])[
    Soit $seq(u, k, K, CC)$ une famille.

    Si les familles $("Re"(u_k))_k$
    et $("Im"(u_k))_k$ sont sommables
    alors $(u_k)$.
]

#yproof[
    Par linéarité.
]

#yprop(name: [Somme d'une permutation d'une famille complexe])[
    Soit $(u_i)_(i in I) in ell_CC^1 (I)$ sommable
    et $sigma: I -> I$ une bijection.

    Alors $(u_(sigma(i)))_(i in I)$ est sommable et
    $ sum_(i in I) u_sigma(i) = sum_(i in I) u_i $
]

#ytheo(name: [Théorème de sommation par paquets (complexe)])[
    Même énoncé que pour les familles de réels.
]

#ytheo(name: [Théorème de Fubini (complexe)])[
    Même énéoncé que dans le cas réel.
]