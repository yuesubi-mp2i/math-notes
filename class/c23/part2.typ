#import "../../template/ytemplate.typ": *


#ypart[Propriétés algébriques de $cal(M)_(n,p) (KK)$]



#yprop(name: [Structures des matrices])[
$lr((cal(M)_(n comma p) lr((bb(K))) comma plus comma dot))$ est un
$bb(K)$-espace vectoriel isomorphe à $cal(L) lr((bb(K)^p comma bb(K)^n))$.

]

#yproof[
    On pose
    $ phi: &cal(L)(KK^p, KK^n) &-> cal(M)_(n,p)(KK) \
        & f &|-> "Mat"_(cal(B), cal(C)) (f) $
    $phi$ est bijective.

    Pour l'addition, on prouve
    - l'associativité,
    - la commutativité,
    - élément neutre
        $ 0_(n,p) = mat(0, dots.h, 0; dots.v, space, dots.v; 0, dots.h, 0) $
    - et existance des opposés.

    La _base canonique_ de $cal(M)_(n,p) (KK)$ est $(E_(i,j))_(1<=i<=n \ 1<=j<=p)$
    où
    $ E_(i, j) = (delta_(k,i) delta_(l, j))_(1<=k<=n \ 1<=l<=p) $
    Les matrices $E_(i,j)$ sont dites _élémentaires_.

    Une matrice $A in cal(M)_(n,p) (KK)$ peut s'écrire
    $ A = (a_(i,j)) = sum_(i,j) a_(i,j) E_(i,j) $
]


#ydef(name: [Matrice élémentaire])[
Pour tout
$lr((i comma j)) in bracket.l.double 1 comma n bracket.r.double times bracket.l.double 1 comma p bracket.r.double$,
la #emph[matrice élémentaire $E_(i comma j)$] est la matrice dont tous
les coefficients sont nuls sauf le coefficient sur la $i$-ème ligne et
$j$-ème colonne qui vaut 1.

    $ E_(i, j) &= (delta_(k,i) delta_(l, j))_(1<=k<=n \ 1<=l<=p) $
]


#yprop(name: [Écriture d'une matrice comme une somme de matrices élémentaires])[
Soit $A eq lr((a_(i comma j))) in cal(M)_(n comma p) lr((bb(K)))$.

Alors
$A eq sum_(i comma j) a_(i comma j) E_(i comma j)$.

]

#yprop(name: [Associativité de la multiplication matricielle])[
    Pour toutes trois matrices $A in cal(M)_(n, p) (KK)$, $B in cal(M)_(p, q) (KK)$
    et $C in cal(M)_(q, r) (KK)$,
    $ A (B C) = (A B) C $
]

#yproof[
    Soient $f: KK^p -> KK^n$, $g: KK^q -> KK^p$ et $h: KK^r -> KK^q$
    les applications linéaires canoniquement associées à respectivement 
    $A$, $B$ et $C$.

    $ A (B C) &= "Mat"(f) ("Mat"(g) "Mat"(h)) \
        &= "Mat"(f) "Mat"(g compose h) \
        &= "Mat"(f compose g compose h) \
        &= "Mat"(f compose g) "Mat"(h) \
        &= ("Mat"(f) "Mat"(g)) "Mat"(h) \
        &= (A B) C $
    $qed$
]


#ydef(name: [Matrice identité])[
    Pour $n in NN^*$, on note
    $ I_n = mat(1, space, (0); space, dots.down, space; (0), space, 1)
        in cal(M)_n (KK) $
    la matrice identité.
]

#yprop(name: [Matrice de la fonction identité])[
    Pour tout $KK$-ev $E$ de dimension $n$ et pour
    toute base $cal(B)$ de $E$,
    $ "Mat"_(cal(B), cal(B)) (id_E) = I_n $
    $qed$
]

#ycor(name: [Neutre de la multiplication matricielle])[
    Pour tout $A in cal(M)_(n, p) (KK)$,
    $ A I_p = A = I_n A $
]

#yproof[
    Soit $A in cal(M)_(n,p) (KK)$.

    On considère $f: KK^p -> KK^n$ l'application linéaire
    canoniquement associéee à $A$, $cal(B)$ et $cal(C)$
    les bases canoniques de $KK^p$ et $KK^n$.

    $ A I_p &= "Mat"_(cal(B), cal(C)) (f) "Mat"_(cal(B), cal(B)) (id_(KK^p)) \
        &= "Mat"_(cal(B), cal(C)) (f compose id_(KK^p)) \
        &= "Mat"_(cal(B), cal(C)) (f) \
        &= A$
    
    De même $I_p A = A$.
]


#ycor(name: [Caractérisation d'un isomorphisme par sa matrice])[
Soit $f colon E arrow.r F$ une application linéaire, $cal(B)$ une base
de $E$, $cal(C)$ une base de $F$, et
$A eq "Mat"_(cal(B) comma cal(C)) lr((f))$.

Alors $f$ est un
isomorphisme si et seulement si $A$ est inversible
(ssi il existe $B in cal(M)_n (KK)$ tel que $A B = B A = I_n$).

]

#yproof[ Si $f$ est un isomorphisme, nécessairement $dim(E) = dim(F)$.

On suppose que $dim(E)=dim(F)$ et on pose $n$ cette dimension commune.

$ f "est un isomorphisme" 
    yt <=> exists g in cal(L)(F, E), cases(g compose f = id_E, f compose g = id_F)
  yt <=> exists g in cal(L)(F, E),
    ytt cases("Mat"_(cal(C), cal(B)) (g) "Mat"_(cal(B), cal(C)) (f) = I_n,  "Mat"_(cal(B), cal(C)) (f) "Mat"_(cal(C), cal(B)) (g) = I_n)
  yt <=> exists B in cal(M)_(n) (KK),
    ytt A B = I_n = B A
  yt <=> A "est inversible". $

]

#yprop(name: [Caractérisation de l'inversibilité d'une matrice par le système linéaire associé])[
    Soit $A = (a_(i,j)) in cal(M)_n (KK$$)$.

    $A$ est inversible ssi le système linéaire homogène de matrice $A$
    est de Cramer (_i.e._ de rang $n$).

    Dans ce cas, $A^(-1)$ s'obtient en trouvant les solutions
    du système linéaire associé à $A$ avec un second membre quelconque.
]

#yproof[
    Soit $(H)$ le système linéaire
    $ cases(a_(1,1) x_1 &+ ... &+ a_(1, n) x_n &= 0,
        dots.v& & & dots.v,
        a_(n,1) x_1 &+ ... &+ a_(n, n) x_n &= 0) $

    "$==>$" Si $A$ est inversible
    $ (H) &<=> A X = 0 \
        &<=> A^(-1) A X = A^(-1) vec(0, dots.v, 0) \
        &<=>  X = vec(0, dots.v, 0) $
    Donc $(H)$ est un système de Cramer.

    "$<==$" On suppose que $(H)$ est un système de Cramer.

    Donc l'unique solution de $(H)$ est $vec(0, dots.v, 0)$.

    Soit $f in cal(L)(KK^n, KK^n)$ canoniquement associée à $A$.

    Soit $(x_1, ..., x_n) in KK^n$ et
    $cal(B)$ la base canonique de $KK^n$.

    $ &(x_1, ..., x_n) in ker(f) \
        &<=> f(x_1, ..., x_n) = 0 \
        &<=> A vec(x_1, dots.v, x_n)
            = mat(0, ..., 0; dots.v, , dots.v; 0, ..., 0) \
        &<=>^1 vec(x_1, dots.v, x_n) = vec(0, dots.v, 0) $
    
    $space^1$ car le système associé est de Cramer et que
    $vec(0, dots.v, 0)$ est la solution.
    
    Donc $f$ est injective, donc bijective, donc $A$ est inversible.

    Montrons désormais que l'on peut obtenir l'inverse :
    on suppose $A$ inversible.

    Soit $(b_1, ..., b_n) in KK^n$ et
    $ (S) cases(a_(1,1) x_1 &+ ... &+ a_(1, n) x_n &= b_1,
        dots.v& & & dots.v,
        a_(n,1) x_1 &+ ... &+ a_(n, n) x_n &= b_n) $
    
    On pose $B = vec(b_1, dots.v, b_n)$.

    $ (S) &<=> A X = B \
        &<=> A^(-1) A X = A^(-1) B \
        &<=> X = A^(-1) B \
        &<=> cases(x_1 &= sum_(i=1)^n ? b_i, dots.v&, x_n &= sum ? b_n) $
    $qed$
]

#ynot(name: [Ensemble des matrices inversibles])[
    On note $"GL"_n (KK)$ l'ensemble des matrices inversibles d'ordre $n$.
]

#yprop(name: [Condition nécessaire et suffisante pour avoir un inverse matriciel à gauche et à droite])[
    Soit $A in cal(M)_n (KK)$.
    $ &A in "GL"_n (KK) \
        &<=> exists B in cal(M)_n (KK), A B = I_n \
        &<=> exists C in cal(M)_n (KK), C A = I_n $
    Et dans ce cas $B = C = A^(-1)$.
]

#yproof[
    Soit $f in cal(L) (KK^n)$ canoniquement associé à $A$,
    $g in cal(L) (KK^n)$ associé à $B$.

    $ &A B = I_n \
        &=> f compose g = id_(KK^n) \
        &=> f compose g "surjective" \
        &=> f "surjective " \
        &=> f "bijective" \
        &=> A in "GL"_n (KK) $
    
    Or $A B = I_n$ donc $A^(-1) (A B) = A^(-1) I_n$
    donc $B = A^(-1)$.
    $qed$
]

#ydef(name: [Opération élémentaire sur les colonnes d'une matrice])[
On appelle #emph[opération élémentaire sur les colonnes] d'une matrice $M$
l'une des opérations suivantes :

- intervertir les colonnes $i$ et $j$, notée $C_i arrow.l.r C_j$.

- ajouter à la colonne $i$, $lambda$ fois la colonne $j$, notée
  $C_i arrow.l C_i plus lambda C_j$.

- multiplier la colonne $i$ par $lambda eq.not 0$, notée
  $C_i arrow.l lambda C_i$.

]

#ydef(name: [Opération élémentaire sur les lignes d'une matrice])[
On appelle #emph[opération élémentaire sur les lignes] d'une matrice $M$
l'une des opérations suivantes :

- intervertir les lignes $i$ et $j$, notée $L_i arrow.l.r L_j$.

- ajouter à la ligne $i$, $lambda$ fois la ligne $j$, notée
  $L_i arrow.l L_i plus lambda L_j$.

- multiplier la ligne $i$ par $lambda eq.not 0$, notée
  $L_i arrow.l lambda L_i$.

]


#yprop(name: [Résultat d'une opération élémentaire sur les colonnes d'une matrice])[
    
Soit $A in cal(M)_(n,p) (KK)$ et $A'$ la matrice obtenue en effectuant une opération élémentaire sur les colonnes de la matrice $A$.

On note $C$ la matrice obtenue en effectuant la même opération sur les colonnes de la matrice $I_p$.

Alors $A' = A C$.]

#yproof[Soit $cal(B)=(e_1,...,e_p)$ la base canonique de $KK^p$, $f, f', gamma$ les applications linéaires canoniquement associées à $A, A'$ et $C$.

Opérer sur les colonnes de $A$ revient à agir sur les vecteurs de la base $cal(B)$, et donc à lui appliquer $gamma$, ainsi $f' = f compose gamma$.]

#ybtw[
    Soit $A in cal(M)_(n, p) (KK)$.
    
    En appliquant l'algorithme du pivot sur les colonnes de la matrice $A$, on produit une matrice $A'$ échelonnée par colonnes, #emph[i.e.] une matrice dont le nombre de zéros précédent la première valeur non nulle d'une colonne augmente strictement colonne par colonne jusqu'à ce qu'il ne reste éventuellement que des zéros.
    
    Comme opérer sur les colonnes d'une matrice revient à la multiplier à droite, on obtient la factorisation $A' = A C_1 ... C_q$ où $C_j$ est la matrice obtenue en effectuant la $j$-ème opération de l'algorithme du pivot sur les colonnes de la matrice identité $I_p$.
    
    Ces matrices étant inversibles, on a aussi $A = A' C_q^(-1)... C_1^(-1)$.

Si $A$ est carrée et $A'=I_p$, alors on obtient $A^(-1) = C_1 ... C_q$, c'est-à-dire la matrice obtenue en effectuant les mêmes opérations que l'algorithme du pivot, mais appliquées cette fois à la matrice $I_p$.
]

#yprop(name: [Résultat d'une opération élémentaire sur les lignes d'une matrice])[
    Soit $A in cal(M)_(n,p) (KK)$ et $A'$ la matrice obtenue en effectuant une opération élémentaire sur les lignes de la matrice $A$.
    
    On note $L$ la matrice obtenue en effectuant la même opération sur les lignes de la matrice $I_n$.
    
    Alors $A' = L A$.

    Mémotechnique : $L$ comme _#strong[l]igne_ et _#strong[l]eft_.
]

#yproof[Il faut écrire les calculs, ou utiliser les propriétés de la transposition.]

#ybtw[De même que dans la , on peut utiliser l'algorithme du pivot sur les lignes de la matrice pour déterminer son inverse (si la matrice est inversible).]

// ymetho(name: [Methodes de calcul])[
//     $ &mat(a_1, space, z_1;
//         dots.v, , dots.v;
//         a_n, space, z_n)
// 
//         mat(space, alpha, space;
//             space, dots.v, space;
//             space, omega, space) \
//         
//         &= alpha vec(a_1, dots.v, a_n) + ... + omega vec(z_1, dots.v, z_n) $
// ]