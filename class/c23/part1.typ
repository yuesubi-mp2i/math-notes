#import "../../template/ytemplate.typ": *


#ypart[Matrices d'un vecteur]


Dans ce chapitre, tous les espaces vectoriels considérés sont de
dimension finie.

#ydef(name: [Matrice])[
Une #emph[matrice] à $n$ lignes et $p$ colonnes est un tableau
  rectangulaire de nombres, appelés #emph[coefficients] de la matrice,
  de $n$ lignes et $p$ colonnes (de sorte qu'il y a $n p$ coefficients).
]

#yexample[$mat(1,2,1;0,1,1)$ est une matrice à 2 lignes et 3 colonnes.]

#ynot(name: [Ensemble des matrices])[
    On note $cal(M)_(n, p)(KK)$ l'ensemble des matrices à $n$ lignes et $p$ colonnes à coefficients dans $KK$.

    On note $cal(M)_n (KK)$ l'ensemble des matrices carrées d'_ordre_ $n$
    ($n$ lignes et $n$ colonnes)
    à coefficients dans $KK$.
]

#ydef(name: [Vocabulaire sur les matrices])[
    + Une _matrice colonne_ est une matrice avec une seule colonne.

    + Une _matrice ligne_ est une matrice avec une seule ligne.

    + Une _matrice carrée_ est une matrice qui a autant de lignes
        que de colonnes.

        La _diagonale_ d'une matrice carrée est l'ensemble des
        coefficient ligne $i$ et colonne $i$, pour tout $i$.
    
    + Une _matrice triangulaire_ supérieure (resp. inférieure)
        est une matrice carrée dont tous les coefficients sous
        (resp. au-dessus de) la diagonale sont nuls.
    
    + Une _matrice diagonale_ est une matrice à la fois triangulaire
        supérieure et triangulaire inférieure.
]

#ydef(name: [Matrice d'un vecteur dans une base])[
Soit $E$ un espace vectoriel de dimension finie $n$,
$cal(B) eq lr((e_1 comma dot.basic dot.basic dot.basic comma e_n))$ une
base de $E$, $x$ un vecteur de $E$.

On note
$lr((x_1 comma dot.basic dot.basic dot.basic comma x_n))$ les
coordonnées de $x$ dans la base $cal(B)$, donc
$x eq sum_(i eq 1)^n x_i e_i$.

La #emph[matrice de $x$ dans la base
$cal(B)$] est la matrice colonne
$ "Mat"_(cal(B)) (x) = vec(x_1, x_2, dots.v, x_n) in M_(n,1) (KK) $

]

#ydef(name: [Matrice d'une application relativement à deux bases])[
Soit $f colon E arrow.r F$ une application linéaire,
$cal(B) eq lr((e_1 comma dot.basic dot.basic dot.basic comma e_p))$ une
base de
$E$,$cal(C) eq lr((u_1 comma dot.basic dot.basic dot.basic comma u_n))$
une base de $F$. On pose
$ forall j in [| 1 comma p|] comma med f lr((e_j)) eq sum_(i eq 1)^n a_(i comma j) u_i dot.basic $
La matrice $lr((a_(i comma j)))$ est appelée #emph[matrice de f dans les
bases $cal(B)$ et $cal(C)$] et notée
$"Mat"_(cal(B) comma cal(C)) lr((f))$.

N.B. On rappelle que $f$ est entièrement déterminée par les vecteurs
    $f(e_1), ..., f(e_p)$.
]

#ydef(name: [Addition et multiplication par un scalaire de matrices])[
On définit sur $cal(M)_(n comma p) lr((bb(K)))$ l'addition en posant
$ lr((a_(i comma j))) plus lr((b_(i comma j))) eq lr((a_(i comma j) plus b_(i comma j))) $
et la multiplication par un scalaire
$ lambda lr((a_(i comma j))) eq lr((lambda a_(i comma j))) $

]

// insert
#yprop(name: [Matrice d'une combinaison linéaire d'applications])[
Soient $f$ et $g$ deux applications linéaires de $E$ dans $F$, $cal(B)$
une base de $E$ et $cal(C)$ une base de $F$, $lambda$ et $mu$ deux
scalaires.

Alors
$ &"Mat"_(cal(B) comma cal(C)) lr((lambda f plus mu g)) \
    &eq lambda "Mat"_(cal(B) comma cal(C)) lr((f)) plus mu "Mat"_(cal(B) comma cal(C)) lr((g)) dot.basic $

]


#ydef(name: [Multiplication matricielle])[
Soient $A = (a_(n,p)) in cal(M)_(n comma p) lr((bb(K)))$ et
$B = (b_(p, q)) in cal(M)_(p comma q) lr((bb(K)))$,
on définit le produit de $A$ et
$B$ par la matrice $C = (c_(n,q)) in cal(M)_(n comma q) lr((bb(K)))$ dont le
coefficient de la ligne $i$ et de la colonne $j$ est
$ c_(i comma j) eq sum_(k eq 1)^p a_(i comma k) b_(k comma j) $
]


#yprop(name: [Matrice relativement à deux bases d'une composée d'applications linéaires])[
Soient $f colon E arrow.r F$ et $g colon F arrow.r G$ deux applications
linéaires, $cal(B) comma cal(C) comma cal(D)$ des bases respectives de
$E comma F$ et $G$.

$ &"Mat"_(cal(B) comma cal(D)) lr((g compose f)) \
    &eq "Mat"_(cal(C) comma cal(D)) lr((g)) "Mat"_(cal(B) comma cal(C)) lr((f)) dot.basic $
]

#yproof[Il suffit de vérifier que les matrices de part et d'autre du symbole $=$ ont les mêmes colonnes.

On note $(e_1,...,e_p)$ les vecteurs de la base $cal(B)$. Soit $j in bracket.l.double 1, p bracket.r.double$.

La $j$-ème colonne de $"Mat"_(cal(B), cal(D)) (g compose f)$ est la matrice colonne des coordonnées dans la base $cal(D)$ de $g(f(e_j))$.

Cette matrice est le produit de $"Mat"_(cal(C), cal(D)) (g)$ par la $j$-ème colonne de $"Mat"_(cal(B), cal(C)) (f)$.

]



#yprop(name: [Représentation de l'évaluation d'une application linéaire par une multiplication matricielle])[
Soit $f in cal(L)(E, F)$, $cal(B)$ une base de $E$, $cal(C)$ une base de $F$.

Alors pour tout $x in E$,
$ &"Mat"_(cal(C)) (f(x)) \
    &= "Mat"_(cal(B), cal(C)) (f) times "Mat"_(cal(B)) (x) $
]

#yproof[
On note
$ cases((e_1, ..., e_p) = cal(B), (f_1, ..., f_n) = cal(C)) $
    
Soit $x in E$, on note
$ X = "Mat"_(cal(B)) (x) = vec(x_1,dots.v,x_p) $
et
$ Y = "Mat"_(cal(C)) (f(x)) = vec(y_1,dots.v, y_n) $

Comme $A = (a_(i, j)) = "Mat"_(cal(B), cal(C)) (f)$, pour tout $j in bracket.l.double 1, p bracket.r.double$,
$ f(e_j) = sum_(i = 1)^n a_(i, j) f_i $

Par linéarité de $f$, 
$ f(x) &= f(sum_(j=1)^p x_j e_j) \
    &= sum_(j=1)^p x_j sum_(i=1)^n a_(i, j) f_i \
    &= sum_(i=1)^n (sum_(j=1)^p a_(i,j) x_j) f_i, $

et donc pour tout $i in bracket.l.double 1, n bracket.r.double$,
$ y_i = sum_(j=1)^p a_(i,j) f_i $
formule qui correspond au produit matriciel de $A$ par la matrice colonne $X$.
]

#yprop(name: [Condition suffisante avec un produit matriciel pour qu'une application soit linéaire])[

Soit $f$ une application d'un espace vectoriel $E$ dans un espace vectoriel $F$, $cal(B)$ une base de $E$ et $cal(C)$ une base de $F$.

On suppose qu'il existe une matrice $A in cal(M)_(n, p)(KK)$ telle que 
pour tout $x in E$,
$ "Mat"_(cal(C)) (f(x)) = A times "Mat"_(cal(B)) (x) $

Alors $f$ est linéaire.


]

#yproof[Soit $(x, y) in E^2$ et $(lambda, mu) in KK^2$. 

$ &"Mat"_(cal(C)) (f(lambda x + mu y)) \
    &= A "Mat"_(cal(B)) (lambda x + mu y)\
  &= A (lambda "Mat"_(cal(B)) (x) + mu "Mat"_(cal(C)) (y) )\
  &= lambda A "Mat"_(cal(B)) (x) + mu A "Mat"_(cal(C)) (y)\
  &= lambda "Mat"_(cal(C)) (f(x)) + mu "Mat"_(cal(C)) (f(y)) \
  &= "Mat"_(cal(C)) (lambda f(x) + mu f(y)) $

et donc $f(lambda x + mu y) = lambda f(x) + mu f(y)$.
]

#yprop(name: [Application linéaire canoniquement associée])[
Soit $A in cal(M)_(n comma p) lr((bb(K)))$.

Il
existe une unique application linéaire $f colon bb(K)^p arrow.r bb(K)^n$
telle que $"Mat"_(cal(B) comma cal(C)) lr((f)) eq A$, où $cal(B)$ et
$cal(C)$ désignent les bases canoniques respectives de $bb(K)^p$ et
$bb(K)^n$.

On dit que $f$ est #emph[l'application linéaire canoniquement associée à
$A$.]

]

#yproof[On sait qu'une application linéaire est entièrement déterminée par l'image d'une base. Or les coefficients de la matrice $A$ imposent l'image de la base canonique.]