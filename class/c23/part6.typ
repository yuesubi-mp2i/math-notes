#import "../../template/ytemplate.typ": *


#ypart[Transposition]


#ydef(name: [Transposée d'une matrice])[
Soit $A eq lr((a_(i comma j)))_(1 lt.eq i lt.eq n\
1 lt.eq j lt.eq p) in M_(n comma p) lr((bb(K)))$.

La #emph[transposée]
de $A$ est la matrice
$ A^T eq lr((a_(j comma i)))_(1 <= j <= p\ 1 <= i <= n) in M_(p comma n) lr((bb(K))) $

N.B. L'autre notation de la transposée : $ space^t A$ n'est pas au programme.
]

#yprop(name: [Linéarité de la transposition])[
La transposition est linéaire. ($A |-> A^T$)
]

#yproof[
    Soient $((a_(i,j)), (b_(i,j))) in cal(M)_(n,p) (KK)^2$
    et $(lambda, mu) in KK^2$.
    $ &(lambda (a_(i,j)) + mu (b_(i, j)))^T \
        &= (lambda a_(i,j) + mu b_(i, j))^T \
        &= (lambda a_(j,i) + mu b_(j, i)) \
        &= lambda (a_(j,i)) + mu (b_(j, i)) \
        &= lambda (a_(i,j))^T + mu (b_(i, j))^T $
]


#yprop(name: [Transposée du produit de matrices])[
Soient $A in M_(n comma p) lr((bb(K)))$ et $B in M_(p comma q) lr((A))$.

On a $(A B)^T eq B^T A^T$.

]

#yproof[Direct, par le calcul.]

#yprop(name: [Rang de la transposée])[
    Pour toute matrice $A in cal(M)_(n, p) (KK)$,
    $ "rg" A = "rg" A^T $
]

#yproof[
  On note $r$ le rang de $A$.
  
  On sait qu'il existe deux matrices inversibles $P$ et $Q$ telles que
  $ A = P J_r Q $
  
  On en déduit que
  $ A^T = Q^T J_r P^T $
  et donc $A^T$ est de rang $r$.
  ]


#yprop(name: [Trace de la transposée])[
    Pour toute matrice $A in cal(M)_n (KK)$,
    $ tr A = tr A^T $
    $qed$
]

#ydef(name: [Matrice symétrique / antisymétrique])[
Soit $A in M_n lr((bb(K)))$. On dit que

+ $A$ est #emph[symétrique] si $A eq A^T$ ;

+ $A$ est #emph[antisymétrique] si $A eq minus A^T$.

]

#yprop(name: [Somme de l'ev des matrices symétries et des matrices antisymétriques])[
    On note $S_n (KK)$ l'ensemble des matrices symétriques
    de $cal(M)_n (KK)$, et $A_n (KK)$ l'ensemble des matrices
    antisymétriques de $cal(M)_n (KK)$.

    Alors $S_n (KK)$ et $A_n (KK)$ sont des sous-ev de $cal(M)_n (KK)$ et
    $ cal(M)_n (KK) = S_n (KK) xor A_n (KK) $
]

#yproof[
    On vérifie que ce sont bien des sous-ev.

    La transposition est une symétrie par rapport à $S_n (KK)$
    parallèlement à $A_n (KK)$. $qed$
]