#import "../../template/ytemplate.typ": *


#ypart[Oublis]



#yprop(name: [Matrice de l'identité relativement à deux bases])[
    Soient $cal(B)$ et $cal(C)$ deux bases d'un ev $E$.
$ P_(cal(B) arrow.r cal(C)) eq "Mat"_(cal(C) comma cal(B)) lr(("id"_E)) $

]
#yprop(name: [Inverse d'une matrice de passage])[
    Soient $cal(B)$ et $cal(C)$ deux bases d'un ev $E$.
$ P_(cal(B) arrow.r cal(C))^(minus 1) eq P_(cal(C) arrow.r cal(B)) $

]

#yproof[
  $ P_(cal(B)-> cal(C))^(-1) &= "Mat"_(cal(C), cal(B)) (id_E)^(-1) \
    &= "Mat"_(cal(B), cal(C))(id_E^(-1)) \
    &= "Mat"_(cal(B), cal(C))(id_E) \
    &= P_(cal(C) -> cal(B))$.
]


#ynot(name: [Matrice diagonale])[
On note
$"diag"lr((lambda_1 comma dot.basic dot.basic dot.basic comma lambda_n))$
la matrice diagonale dont les coefficients diagonaux sont
$lambda_1 comma dot.basic dot.basic dot.basic comma lambda_n$.
$ "diag"lr((lambda_1 comma dot.basic dot.basic dot.basic comma lambda_n)) eq mat(delim: "(", lambda_1, 0, dots.h, 0; 0, lambda_2, , 0; dots.v, , dots.down, dots.v; 0, 0, dots.h, lambda_n) dot.basic $

]
#yprop(name: [Puissance d'une matrice diagonale])[
Pour tout $k in bb(N)^ast.basic$,
$ "diag"lr((lambda_1 comma dot.basic dot.basic dot.basic comma lambda_n))^k eq "diag"lr((lambda_1^k comma dot.basic dot.basic dot.basic comma lambda_n^k)) $

]

#yproof[Par récurrence sur $k$.]

#yexample[
Soit
$A eq 1 / 2 mat(delim: "(", 1, 1, minus 1; minus 1, 3, 1; minus 2, 2, 2)$.

Soit $cal(B) eq lr((e_1 comma e_2 comma e_3))$ la base canonique de
$bb(R)^3$ et $f$ l'endomorphisme canoniquement associé à $A$ :
$"Mat"_(cal(B)) lr((f)) eq A$.

On cherche une base $cal(C)$ de $bb(R)^3$ telle que
$"Mat"_(cal(C)) lr((f))$ soit diagonale.

+ Analyse. Soit $cal(C) eq lr((u_1 comma u_2 comma u_3))$ une base de
  $bb(R)^3$ et $a comma b comma c$ tels que
  $"Mat"_(cal(C)) lr((f)) eq mat(delim: "(", a, 0, 0; 0, b, 0; 0, 0, c)$.

  On a alors $u_1 eq.not 0$ et $f lr((u_1)) eq a u_1$, $u_2 eq.not 0$ et
  $f lr((u_2)) eq b u_2$, $u_3 eq.not 0$ et $f lr((u_3)) eq c u_3$. On
  cherche donc à résoudre des équations de la forme
  $f lr((u)) eq lambda u$ avec $u eq.not 0$.

  On pose $u eq lr((x comma y comma z))$. Ainsi
  $ f lr((u)) & eq lambda u arrow.l.r.double 1 / 2 mat(delim: "(", 1, 1, minus 1; minus 1, 3, 1; minus 2, 2, 2) vec(x, y, z) eq vec(lambda x, lambda y, lambda z) arrow.l.r.double 
  cases(x plus y minus z eq 2 lambda x,
         minus x plus 3 y plus z eq 2 lambda y,
         minus x plus y plus z eq lambda z)\
   & arrow.l.r.double cases(lr((1 minus 2 lambda)) x plus y minus z eq 0,
                      minus x plus lr((3 minus 2 lambda)) y plus z eq 0,
                      x minus y minus lr((1 minus lambda)) z eq 0) \
   & arrow.l.r.double cases(2 lr((1 minus lambda)) y plus lr((minus 1 plus lr((1 minus lambda)) lr((1 minus 2 lambda)))) z eq 0,
  2 lr((1 minus lambda)) y plus lambda z eq 0,
  x minus y plus lr((lambda minus 1)) z eq 0)\
   & arrow.l.r.double cases(2 lambda lr((2 minus lambda)) z eq 0,
                            2 lr((1 minus lambda)) y plus lambda z eq 0,
                            x minus y plus lr((lambda minus 1)) z eq 0). $ 

  On voit que ce système a
  des solutions non nulles si et seulement si $1 minus lambda eq 0$ ou
  $lambda lr((2 minus lambda)) eq 0$, si et seulement si
  $lambda in brace.l 0 comma 1 comma 2 brace.r$.

  Avec $lambda eq 0$, on trouve $y eq 0$ et $x eq z$. Donc en posant
  $u_1 eq lr((1 comma 0 comma 1))$, on aura
  $f lr((u_1)) eq 0 dot.basic u_1$.

  Avec $lambda eq 1$, on trouve $z eq 0$ et $x eq y$. Donc en posant
  $u_2 eq lr((1 comma 1 comma 0))$, on aura
  $f lr((u_2)) eq 1 dot.basic u_2$.

  Avec $lambda eq 2$, on trouve $x eq 0$ et $z eq y$. Donc en posant
  $u_3 eq lr((0 comma 1 comma 1))$, on aura
  $f lr((u_3)) eq 2 dot.basic u_3$.

+ Synthèse. On pose $u_1 eq lr((1 comma 0 comma 1))$,
  $u_2 eq lr((1 comma 1 comma 0))$, $u_3 eq lr((0 comma 1 comma 1))$.
  Montrons que $cal(C) eq lr((u_1 comma u_2 comma u_3))$ est une base de
  $bb(R)^3$. Pour cela, on va montrer que la matrice de passage $P$ de
  $cal(B)$ à $cal(C)$ est inversible, et déterminer son inverse (on en
  aura besoin pour la suite).

  On a $P eq mat(delim: "(", 1, 1, 0; 0, 1, 1; 1, 0, 1)$.

  Soient $X eq vec(x, y, z)$ et $Y eq vec(a, b, c)$.

  $ P X eq Y arrow.l.r.double cases(x plus y eq a,
                                    y plus z eq b,
                                    x plus z eq c) 
             arrow.l.r.double cases(x eq 1 / 2 lr((a minus b plus c)),
                                    y eq 1 / 2 lr((a plus b minus c)),
                                    z eq 1 / 2 lr((minus a plus b plus c))). $

  Donc $P$ est inversible et
  $P^(minus 1) eq 1 / 2 mat(delim: "(", 1, minus 1, 1; 1, 1, minus 1; minus 1, 1, 1)$.
  Donc $cal(C)$ est bien une base de $bb(R)^3$.

D'après la formule de changement de base,
$A eq "Mat"_(cal(B)) lr((f)) eq P "Mat"_(cal(C)) lr((f)) P^(minus 1) eq P D P^(minus 1)$
avec $D eq mat(delim: "(", 0, 0, 0; 0, 1, 0; 0, 0, 2) dot.basic$

On en déduit que pour tout $k$,
$A^k eq lr((P D P^(minus 1)))^k eq lr((P D P^(minus 1))) lr((P D P^(minus 1))) dot.basic dot.basic dot.basic lr((P D P^(minus 1))) eq P D^k P^(minus 1)$,
donc
$ A^k eq 1 / 2 mat(delim: "(", 1, 1, 0; 0, 1, 1; 1, 0, 1) mat(delim: "(", 0, 0, 0; 0, 1, 0; 0, 0, 2^k) mat(delim: "(", 1, minus 1, 1; 1, 1, minus 1; minus 1, 1, 1) eq 1 / 2 mat(delim: "(", 1, 1, minus 1; 1 minus 2^k, 1 plus 2^k, 2^k minus 1; minus 2^k, 2^k, 2^k) dot.basic $

]

#yprop(name: [Inverse de la transposée])[
Soit $A in G L_n lr((bb(K)))$.

Alors $A^T in G L_n lr((bb(K)))$ et $ (A^(-1))^T = (A^T)^(-1) $

]

#yproof[
  $ A A^(-1) = I_n $
  donc
  $ (A^(-1))^T A^T &= (A A^(-1))^T \
    &= I_n^T \
    &= I_n $
  donc $A^T$ est inversible d'inverse $(A^(-1))^T$.]

#ymetho(name: [Diagonaliser une matrice (méthode Balan)])[
  On veut diagonaliser une matrice $A in cal(M)_n (KK)$.

  On considère $cal(B)$ la base canonique de $KK^n$ et $f$ l'application
  canoniquement associée à $A$.
  On a alors $"Mat"_cal(B) (f) = A$.

  #smallcaps(underline[Analyse]) : 
  Soit un base $cal(C)$ telle que
  $ "Mat"_cal(C) (f) =
    mat(lambda_1, space, (0);
      space, dots.down, space;
      (0), space, lambda_n) $
  
  Chaque vecteur $u$ de la base $cal(C)$ est non nul, et vérifie
  $exists lambda in KK, f(u) = lambda u$.

  Soit $u = (x_1, ..., x_n)$ un vecteur et $lambda in KK$.

  On a alors le système $f(u ) = lambda u $.

  On essaye de réstreindre le plus possible le domaine des valeurs de $lambda$
  telles que $u$ et non nul.

  Pour chaqu'une des valeurs $lambda$ trouvées,
  on choisi un $u$ qui convient, on a alors une base $cal(C)$.

  #smallcaps(underline[Synthèse]) :
  On écrit la matrice de passage $P_(cal(B) -> cal(C))$
  puis on calcule sont inverse.

  On vérifie qu'on a bien
  $ P_(cal(B) -> cal(C)) "Mat"_cal(C) (f) P_(cal(C) -> cal(B)) = A $
]