#import "../../template/ytemplate.typ": *


#ypart[Matrice d'une famille de vecteurs]


#ydef(name: [Matrice d'une famille de vecteurs])[
Soit $E$ un $KK$-ev de dimension $n$.
Soit $lr((u_1 comma dot.basic dot.basic dot.basic comma u_k))$ une
famille de vecteurs de $E$, et $cal(B)$ une base de $E$.

La
#emph[matrice] de cette famille dans la base $cal(B)$ est la matrice
dont les colonnes sont $"Mat"_(cal(B)) lr((u_j))$ pour tout
$j in bracket.l.double 1 comma k bracket.r.double$.

]

#ydef(name: [Rang d'un matrice])[
Soit $A in M_(n comma p) lr((bb(K)))$.

On définit le #emph[rang de $A$]
comme le nombre maximal de colonnes linéairement indépendantes de $A$.

On note ce nombre $"rg"lr((A))$.

]

#yprop(name: [Caractérisation du rang d'un famille par la matrice de cette famille ])[
Soit $lr((u_1 comma dot.basic dot.basic dot.basic comma u_k))$ une
famille de vecteurs de $E$, et $cal(B)$ une base de $E$. Soit $A$ la
matrice de cette famille dans la base $cal(B)$.

Alors
$ "rg"lr((A)) eq "rg"lr((u_1 comma dot.basic dot.basic dot.basic comma u_k)) $

]

#yprop(name: [Caractérisation du rang d'une application par la matrice de celle-ci])[
Soit $f colon E arrow.r F$ une application linéaire, $cal(B)$ une base
de $E$, $cal(C)$ une base de $F$.

Alors
$"rg"lr((f)) eq "rg"lr(("Mat"_(cal(B) comma cal(C)) lr((f))))$.

]

#yproof[
    On note $cal(B) = (e_1, ..., e_p)$.
    $ "Mat"(f) = "Mat"(f(e_1), ..., f(e_p)) $

    $ "rg"(f) &= dim im f \
        &= dim "Vect"(f(e_1), ..., f(e_p)) \
        &= "rg"(f(e_1), ..., f(e_p)) \
        &= "rg" "Mat"(f(e_1), ..., f(e_p)) \
        &= "rg" "Mat"(f) $
]

#ycor(name: [Caractérisation de l'inversibilité par le rang])[
(Corollaire de la caractérisation du rang d'une application par la matrice de celle-ci)

Soit $A in cal(M)_n (KK)$.

La matrice $A$ est inversible si et seulement si elle est de rang $n$.]

#yproof[Soit $f$ l'endomorphisme canoniquement associé à la matrice $A$. $A$ est inversible si et seulement si $f$ est bijective, si et seulement si $f$ est surjective, si et seulement si $f$ est de rang $n$, si et seulement si $A$ est de rang $n$.]


#ycor(name: [Caractérisation d'une base par sa matrice])[
Soit $cal(F)$ une
famille de vecteurs d'un espace vectoriel $E$ telle que
$abs(cal(F)) = dim E$, et $cal(B)$ une base de $E$. 

Alors $cal(F)$ est une base de $E$ ssi la matrice de $cal(F)$ dans
la base $cal(B)$ est inversible.
]

#yproof[La famille $cal(F)$ comportant $n$ vecteurs, c'est une base de $E$ si et seulement si elle est libre, si et seulement si elle est de rang $n$, si et seulement si sa matrice est de rang $n$, si et seulement si elle est inversible.]

#yprop(name: [Effet des opération élémentaires sur le rang d'une matrice])[
Soit $A in cal(M)_(n comma p) lr((bb(K)))$ et $A prime$ la matrice obtenue à
partir de $A$ en effectuant une opération élémentaire sur les lignes ou
les colonnes de $A$.

Alors $"rg"lr((A)) eq "rg"lr((A prime))$.

]

#yproof[
  + On suppose avoir effectué une opération élémentaire sur les lignes de la matrice $A$ pour obtenir $A'$.

    Il existe donc une matrice inversible $R$ telle que $A' = R A$.
    
    Soit $f$ l'endomorphisme de $KK^n$ canoniquement associé à la matrice $R$, et $(u_1,...,u_p)$ une famille de vecteurs de $KK^n$ telle que la matrice de cette famille dans la base canonique de $KK^n$ soit $A$.
    
    Ainsi, $A'$ est la matrice de la famille $(f(u_1),..., f(u_p))$.

    On note $r$ le rang de la matrice $A$, qui est aussi le rang de $(u_1,...,u_p)$.
    
    Comme $f$ est un isomorphisme,
    $ &dim(f("Vect"(u_1,...,u_p))) \
        &= dim("Vect"(u_1,...,u_p)) \
        &= r $
    
    Or,
    $ &f("Vect"(u_1,...,u_p)) \
        &= "Vect"(f(u_1),...,f(u_p)) $
    et donc la dimension de cet espace est le rang de $(f(u_1),...,f(u_p))$, c'est-à-dire le rang de $A'$.
    
    Donc $A'$ est de rang $r$.

  + On suppose à présent que $A'$ s'obtient à partir d'une opération sur les colonnes de la matrice $A$.
    
    Soit $C$ une matrice inversible telle que $A' = A C$, et $f$ l'automorphisme de $KK^p$ canoniquement associé.
  
    On note aussi $g$ l'application linéaire canoniquement associée à $A$.
    
    Soit enfin $(e_1,...,e_p)$ la base canonique de $KK^p$.

    Le rang de $A'$ est le rang de $g compose f$, donc le rang de la famille de vecteurs
    $ (g(f(e_1)),...,g(f(e_p))) $

    Comme $f$ est un isomorphisme, $(f(e_1),...,f(e_p))$ est une base de $KK^p$ et donc 
    $ &"rg"(g(f(e_1)),..., g(f(e_p))) \
        &= dim("Im"(g)) \
        &="rg"(g) \
        &="rg"(A) $

  ]

#ytheo(name: [Transformation d'une matrice pour avoir un bloc identité])[
Soit $A in cal(M)_(n comma p) lr((bb(K)))$ de
rang $r$.

Alors il existe
$P in G L_n lr((bb(K))) comma Q in G L_p lr((bb(K)))$ telles que $A eq P J_r Q $
où
$ J_r eq mat(delim: "(", I_r, 0_(r comma p minus r); 0_(n minus r comma r), 0_(n minus r comma p minus r)). $

]

#yproof[En appliquant l'algorithme du pivot sur les colonnes de la matrice $A$, on obtient une matrice échelonnée par colonnes $A'$, et donc $A' = A C$ avec $C$ inversible.

En appliquant à présent l'algorithme du pivot sur les lignes de la matrice $A'$, on obtient $J_r$ (puisque le rang n'est pas modifié par opération élémentaire), et donc $J_r = R A'$ avec $R$ inversible.

Finalement $A = R^(-1) J_r C^(-1)$.]

#yprop(name: [Stabilité du rang ligne par les opérations élémentaire])[
Le rang-ligne est invariant par opérations linéaires élémentaires sur les lignes
et les colonnes. $qed$
]

#yprop(name: [Lien entre rang-colonne et rang-ligne])[
    Le rang-colonne et le rang-ligne de $J_r$ valent tous deux $r$.

    Le rang-ligne est égal au rang-colonne quelle que soit la matrice.
    $qed$
]

#yprop(name: [Lien entre le rang d'une matrice et celui d'un système linéaire])[
    Le rang d'un système linéaire est égal au rang de sa matrice. $qed$
]