#import "../../template/ytemplate.typ": *


#ypart[Matrices par blocs]



#yprop(name: [Trace d'un projecteur])[
Soit $p$ un projecteur. Alors $"tr"(p) eq "rg"(p)$.

]

#yproof[
    Soit $(u_1,...,u_r)$ une base de $"Im"(p)$ et $(u_(r+1),...,u_n)$
    une base de $"Ker"(p)$.
    
    Comme $p$ est un projecteur, $(u_1,...,u_n)$ est une base de $E$
    et la matrice de $p$ dans cette base est une diagonale de 1
    sur les $r$ premières colonnes, et une diagonale de 0 sur les colonnes suivantes
    (la matrice $J_r$). 
    
    Ainsi $tr(p)=r="rg"(p)$.
]

#ydef(name: [Matrice par blocs])[
    On appelle _matrice par blocs_ une matrice construite à partir
    d'autres matrices $A_(i, j)$ suivant le schéma ci-dessous
    $ mat(A_(1, 1), A_(1, 2), dots.h, A_(1, p);
        A_(2, 1), A_(2, 2), dots.h, A_(2, p);
        dots.v, dots.v, dots.down, dots.v;
        A_(n, 1), A_(n, 2), dots.h, A_(n, p)) $
    // les blocs diagonaux $A_(i , i)$ étant des matrices carrées.
]

#yprop(name: [Condition suffisante pour que la matrice d'une application soit diagonale par blocs])[
    Soit $f in cal(L)(E)$, $E = xor.big_(i=1)^p F_i$ tel que
    $ forall i in [|1, p|], f(F_i) subset F_i $
    ($F_i$ est stable par $f$)

    Soit $cal(B)_i$ une base de $F_i$ pour $i in [|1, p|]$
    et $cal(B) = cal(B)_1 union ... union cal(B)_p$.

    Alors,
    $ "Mat"_cal(B) (f) =
        mat(*, (0), ..., (0);
            (0), *, , (0);
            dots.v, , dots.down, dots.v;
            (0), (0), dots.h, *) $
    est _diagonale par blocs_.
]


#yprop(name: [Condition suffisante pour que la matrice d'une application soit triangulaire par blocs])[
    Soit $f in cal(L)(E)$ et
    $ F_1 subset F_2 subset ... subset F_p = E $
    une suite croissante de sous-ev de $E$ stable par $f$ :
    $ forall i in [|1, p|], f(F_i) subset F_i $

    - Soit $cal(B)_1$ une base de $F_1$, que l'on complète en une base
    - $cal(B)_2$ de $F_2$, que l'on complète en une base
    - ...
    - $cal(B)_p$ de $F_p = E$.

    Alors,
    $ "Mat"_cal(B) (f) =
        mat(*, *, ..., *;
            (0), *, , *;
            dots.v, , dots.down, dots.v;
            (0), (0), dots.h, *) $
    est _triangulaire par blocs_.
]

#yprop(name: [Multiplication de matrices par blocs])[
Soient
$ A eq mat(delim: "(", A_(1 comma 1), A_(1 comma 2), dots.h, A_(1 comma p); A_(2 comma 1), A_(2 comma 2), dots.h, A_(2 comma p); dots.v, dots.v, dots.down, dots.v; A_(n comma 1), A_(n comma 2), ..., A_(n comma p)) $
$ B eq mat(delim: "(", B_(1 comma 1), B_(1 comma 2), dots.h, B_(1 comma q); B_(2 comma 1), B_(2 comma 2), dots.h, B_(2 comma q); dots.v, dots.v, dots.down, dots.v; B_(p comma 1), B_(p comma 2), ..., B_(p comma q)) $
deux matrices par blocs.

Si pour tout $i comma j comma k$, le nombre de
colonnes du bloc $A_(i comma j)$ est égal au nombre de lignes du bloc
$B_(j comma k)$, alors le produit $A B$ peut être calculé par blocs : en
posant $C eq A B$, on a
$ C eq mat(delim: "(", C_(1 comma 1), C_(1 comma 2), dots.h, C_(1 comma q); C_(2 comma 1), C_(2 comma 2), dots.h, C_(2 comma q); dots.v, dots.v, dots.down, dots.v; C_(n comma 1), C_(n comma 2), ..., C_(n comma q)) $
avec pour tout $i comma j$,
$ C_(i comma k) eq sum_j A_(i comma k) B_(k comma j) $

// exemple avec une matrice 2x2
]

#yprop(name: [Transposée d'une matrice par blocs])[
    Pour $A, B, C, D$ quatre matrices,
    $ mat(A, B; C, D)^T = mat(A^T, C^T; B^T, D^T) $
    $qed$
]