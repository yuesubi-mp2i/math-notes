#import "../../template/ytemplate.typ": *


#ypart[Trace]


#ydef(name: [Trace d'une matrice])[
Soit $A in cal(M)_n lr((bb(K)))$.

La #emph[trace] de $A$ est la somme des
coefficients diagonaux de $A$. On la note $"tr"(A)$.
]

#yprop(name: [Linéarité de la trace])[
La trace est linéaire.
]

#yproof[
    Par le calcul.
    $ &tr(alpha A + beta B) \
        &= sum_(i=1)^n (alpha a_(i,i) + beta b_(i,i)) \
        &= alpha sum_(i=1)^n a_(i,i) + beta sum_(i=1)^n b_(i,i) \
        &= alpha tr A + beta tr B $
]

#yprop(name: [Trace de la multiplication d'un coté et de l'autre])[
    Soit $(A, B) in cal(M)_n (KK)^2$. Alors
    $ tr A B = tr B A $
]

#yproof[On note $(a_(i,j))$ les coefficients de la matrice $A$, et $(b_(i,j))$ ceux de la matrice $B$.

Les coefficients diagonaux de la matrice $A B$ sont
$ sum_(i=1)^n a_(i,j) b_(j, i) $ pour $j in bracket.l.double 1,n bracket.r.double$
et ceux de $B A$ sont $ sum_(i=1)^n b_(i,j) a_(j, i) $ d'où 

$ "tr"(A B) &= sum_(i=1)^n a_(i,j) b_(j, i) \
    &= sum_(j=1)^n b_(i,j) a_(j,i) \
    &= "tr"(B A) $

]


#ycor(name: [Lien entre les matrices semblables et leurs traces])[
    (Corollaire de la trace de la multiplication d'un coté et de l'autre)

Soient $A$ et $B$ deux matrices semblables.

Alors
$"tr"(A) eq "tr"(B)$.

]

#yproof[Soit $P$ une matrice inversible telle que $B = P A P^(-1)$.

Alors
$ "tr"(B) &="tr"((P A) P^(-1)) \
    &= "tr"(P^(-1)(P A)) \
    &= "tr"(A) $.  ]

#ycor(name: [Trace de la matrice d'une application dans des bases différentes])[
    Soit $f in cal(L)(E)$, $cal(B)$ et $cal(C)$ deux bases de $E$.
    $ tr "Mat"_cal(C) (f) = tr "Mat"_cal(B) (f) $
]

#yproof[
    D'après la formule de changement de bases, $"Mat"_cal(B) (f)$ et
    $"Mat"_cal(C) (f)$ sont semblables. $qed$
]


#ydef(name: [Trace d'une application linéaire])[
Soit $f in L lr((E))$ un endomorphisme.

On appelle #emph[trace de $f$] la trace de la
matrice de $f$ relativement à n'importe quelle base de $E$. On la note
$"tr" lr((f))$.

]

#yprop(name: [Rang de deux matrices semblables])[
    Le rang est un invariant de similitude.
]