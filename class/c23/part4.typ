#import "../../template/ytemplate.typ": *


#ypart[Formules de changement de base]

#yprop(name: [Formule de changement de base pour un vecteur])[
    Soit $E$ un $KK$-ev de dimension $n$, $x in E$,
    $cal(B)$ et $cal(C)$ deux
    bases de $E$.

    $ "Mat"_cal(C) (x) = P_(cal(C)->cal(B)) "Mat"_cal(B) (x) $
]

#yproof[
    $ &P_(cal(C)->cal(B)) "Mat"_cal(B) (x) \
        &= "Mat"_(cal(B), cal(C)) (id) "Mat"_cal(B) (x) \
        &= "Mat"_cal(C) (id_E (x)) \
        &= "Mat"_cal(C) (x) $
    $qed$
]

#yprop(name: [Formule de changement de bases d'une application])[
Soit $f$ une application linéaire de $E$ dans $F$, $cal(B)_1$ et
$cal(B)_2$ deux bases de $E$, $cal(C)_1$ et $cal(C)_2$ deux bases de
$F$.

$ &"Mat"_(cal(B)_2 comma cal(C)_2) lr((f)) \
    &= P_(cal(C)_2 -> cal(C)_1) "Mat"_(cal(B)_1 comma cal(C)_1) lr((f))
        P_(cal(B)_1 -> cal(B)_2) dot.basic $

]


#yproof[

$ &P_(cal(C)_2 -> cal(C)_1) "Mat"_(cal(B_1), cal(C_1)) (f) P_(cal(B)_1 -> cal(B)_2) \
    &= "Mat"_(cal(C_1), cal(C_2)) (id_F) "Mat"_(cal(B_1), cal(C_1)) (f) \
    &wide times "Mat"_(cal(B_2), cal(B_1)) (id_E) \
  &= "Mat"_(cal(B_1), cal(C_2)) (id_F compose f) "Mat"_(cal(B_2), cal(B_1)) (id_E) \
  &= "Mat"_(cal(B_2), cal(C_2)) (id_F compose f compose id_E) \
  &= "Mat"_(cal(B_2), cal(C_2)) (f). $
]

#ydef(name: [Matrice de passage d'une base à une autre])[
Soient
$cal(B)$ et
$cal(C)$ deux
bases de $E$.

La #emph[matrice de passage de la base $cal(B)$ à la base
$cal(C)$] est la matrice de la famille $cal(C)$ dans la base $cal(B)$.
On la note $P_(cal(B) arrow.r cal(C))$.
]

#yprop(name: [Formule de changement de base d'un endomorphisme])[
    Soit $f in cal(L)(E)$, $cal(B)$ et $cal(C)$ deux bases d'un espace vectoriel $E$. Alors
    $ "Mat"_cal(C) (f) = P_(cal(C) -> cal(B)) "Mat"_cal(B) (f) P_(cal(B) -> cal(C)) $
    $qed$
]

#ydef(name: [Matrices équivalentes])[
    On dit que deux matrices $A$ et $B$ de $cal(M)_(n, p) (KK)$ sont #emph[équivalentes] s'il existe deux matrices $N in G L_n (KK)$ et $P in G L_p (KK)$ telles que $B = N A P$.

N.B. Deux matrices sont équivalentes si et seulement si elles représentent la même application linéaire dans des paires de bases différentes.]

#ydef(name: [Matrices semblables])[
    Soient $A$ et $B$ deux matrices carrées de $cal(M)_n (KK)$.
    
    On dit qu'elles sont _semblables_ s'il existe $P in G L_n (KK)$ telle que
    $B = P A P^(-1)$.

    N.B. Donc $A$ et $B$ sont aussi équivalentes.
]