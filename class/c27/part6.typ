#import "../../template/ytemplate.typ": *


#ypart[Convergence absolue]


#ytheo(name: [Convergence avec la convergence absolue])[
    Soit $(u_n) in CC^NN$.

    Si $sum abs(u_n)$ converge alors $sum u_n$ converge.

    N.B. #ymega_emph[La réciproque est #smallcaps[fausse] !]
]

#yproof[
    - Cas 1 : $forall n in NN, u_n in RR$

        On pose, pour $n in NN$,
        $ u_n^+ = cases(u_n &"si" u_n >= 0,
            0 &"sinon") $
        $ u_n^- = cases(0 &"si" u_n >= 0,
            -u_n &"sinon") $
        
        Pour $n in NN$,
        $ &u_n^+ in RR^+ wide &(1) \
            &u_n^- in RR^+ wide &(2) \
            &u_n^+ - u_n^- = u_n wide &(3) \
            &u_n^+ + u_n^- = abs(u_n) wide &(4) $
        
        On suppose que $sum abs(u_n)$ converge.

        D'après (4) :
        $ forall n in NN, u_n^+ <= abs(u_n) $

        Par comparaison de séries à terme général positif
        (voir (1)), $sum u_n^+$ converge.

        De même,
        $ forall n, 0 <= u_n^- <= abs(u_n) $
        (d'après (2) et (4))
        et donc $sum u_n^-$ converge.

        D'après (3), par linéarité $sum u_n$ converge.

    - Cas 2 : On revient au cas général.

        Pour $n in NN$,
        $ 0 <= abs("Re"(u_n)) <= abs(u_n) $
        $ 0 <= abs("Im"(u_n)) <= abs(u_n) $

        On suppose que $sum abs(u_n)$ converge.

        Par comparaison de séries à terme général positif,
        $ sum abs("Re"(u_n))$ et $sum abs("Im"(u_n))$
        convergent.

        D'après le cas 1, $sum "Re"(u_n)$ et $sum "Im"(u_n)$ convergent.

        Or pour tout $n in NN$,
        $ u_n = "Re"(u_n) + i "Im"(u_n) $

        Par linéarité, $sum u_n$ converge.
]

#ydef(name: [Convergence absolue])[
    Soit $(u_n) in CC^NN$.

    On dit que $sum u_n$ _converge absolument_
    si $sum abs(u_n)$ converge.
]

#ydef(name: [Semi-convergence])[
    Soit $(u_n) in CC^NN$.

    On dit que $sum u_n$ est _semi-convergente_
    si $sum u_n$ converge et $sum abs(u_n)$ diverge.
]

#yprop(name: [Inégalité triangulaire sur les séries])[
    Soit $(u_n) in CC^NN$.

    Si $sum u_n$ converge absolument, alors
    $ abs(sum_(n=0)^(+oo) u_n) <= sum_(n=0)^(+oo) abs(u_n) $
]

#yproof[
    Soit $N in NN$.
    $ abs(sum_(k=0)^N u_n) < sum_(n=0)^N abs(u_n) $
    d'après l'inégalité triangulaire classique.

    On fait tendre $N$ vers $+oo$,
    $ abs(sum_(n=0)^(+oo) u_n) <= sum_(n=0)^(+oo) abs(u_n) $
]