#import "../../template/ytemplate.typ": *


#ypart[Définition]


#ydef(name: [Vocabulaire des séries])[
    Soit $u in CC^NN$.

    - Étudier la _série_ $sum u_n$
        c'est étudier la suite des sommes partielles
        $ (sum_(k=0)^n u_k)_(n in NN)$.

    - On dit qu'une suite _converge_ si la suite
        $(sum_(k=0)^n u_k)$ converge.

    - Si la série ne converge pas, on dit qu'elle _diverge_.

    - _Étudier la nature_ d'une série, c'est préciser
        si elle converge ou non.
]

#ydef(name: [Somme d'une série])[
    Soit $(u_n) in CC^NN$.

    Si la série $sum u_n$ converge,
    sa limite est appelée la _somme_ de la série et elle est notée
    $ sum_(k=0)^(+oo) u_k = lim_(n->+oo) sum_(k=0)^n u_k $

]

#ydef(name: [Restes partiels d'une série])[
    Soit $(u_n) in CC^NN$.
    
    Si la série $sum u_n$ converge, on définit
    les _restes partiels_ de la série par
    $ sum_(k=n+1)^(+oo) u_k = sum_(k=0)^(+oo) u_k - sum_(k=0)^n u_k $
]

#yprop(name: [Convergence d'une série avec la limite du terme général])[
    Soit $(u_n) in CC^NN$.

    + Si $u_n arrow.not_(+oo) 0$ alors $sum u_n$ diverge.

    + Si $sum u_n$ converge, alors $u_n -->_(+oo) 0$

    #ymega_emph[Attention : La réciproque est #smallcaps[fausse] !]
    La série harmonique est un contre exemple.
]

#yproof[
    1 est la contraposée de 2.

    On suppose que $sum u_n$ converge.

    On pose $S = lim_(n->+oo) S_n$ où
    $S_n = sum_(k=0)^n u_k$.

    Pour tout $n in NN^*$,
    $ u_n &= S_n - S_(n-1) \
    &-->_(n->+oo) S- S = 0 $
]

#yprop(name: [Convergence et limite de la série géométrique])[
    Soit $q in CC$.

    La série géométrique $sum q^n$ converge
    ssi $abs(q) < 1$.
    Dans ce cas $ sum_(k=0)^(+oo) q^k = 1/(1 - q) $
]

#yproof[
    Si $abs(q) >= 1$ alors
    $ forall n in NN^*, abs(q)^n >= 1 $
    donc $q^n arrow.not_(n->+oo) 0$.

    Si $abs(q) < 1$, alors

    Pour tout $n in NN^*$,
    $ sum_(k=0)^n q^k &= (1 - q^(n+1))/(1 - q) \
        &-->_(n->+oo) 1/(1 - q) $
]