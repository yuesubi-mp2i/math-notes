#import "../../template/ytemplate.typ": *


#ypart[Comparaison avec une intégrale]


#ytheo(name: [Convergence de la série de Riemann])[
    La _série de Riemann_ $sum 1/n^alpha$ ($alpha in RR$)
    converge ssi $alpha > 1$.
]

#yproof[
    On a déjà vu que $sum 1/n^alpha$ diverge
    grossièrement (_i.e._ que le terme général diverge)
    si $alpha <= 0$.
    
    On suppose $alpha > 0$.

    Soit
    $ f: &RR^*_+ &-> RR^+ \
        &x &|-> 1/x^alpha = x^(-alpha) $

    $ forall k>=1,
        integral_k^(k+1) (dif x)/x^alpha <= 
        integral_k^(k+1) (dif x)/k^alpha = 1/k^alpha $
    car $f$ est décroissante.

    D'où, pour $n in NN^*$,
    $ sum_(k=1)^n 1/k^alpha >=
        sum_(k=1)^n integral_k^(k+1) (dif x)/x^alpha
            = integral_1^(n+1) (dif x)/x^alpha $

    De même, pour $k >= 2$,
    $ 1/k^alpha <= integral_(k-1)^k (dif x)/x^alpha $
    et donc
    $ sum_(k=2)^n 1/k^alpha <= integral_1^n (dif x)/x^alpha $

    - On suppose $alpha in ]0,1[$. Soit $n in NN^*$.

        $ &integral_n^(n+1) (dif x)/x^alpha \
            &= [ (x^(-alpha + 1))/(-alpha + 1)]_(x=1)^(n+1) \
            &= 1/(1 + alpha) ((n+1)^(1 -alpha) - 1) \
            &-->_(n->+oo) +oo $
        
        Donc
        $ lim_(n->+oo) sum_(k=0)^n 1/k^alpha = +oo $
    
        La série $sum 1/n^alpha$ diverge.
    
    - On suppose $alpha > 1$. Soit $n in NN^*$.

        $ sum_(k=2)^n 1/k^alpha
            yt <= integral_1^n (dif x)/x^alpha
                ytt = 1/(1 - alpha) (n^(1 - alpha) - 1)
                ytt = (1 - n^(1 - alpha))/(alpha - 1) $
        
        La suite croissante $(sum_(k=2)^n 1/k^alpha)_n$ est majorée
        (par $1/(alpha - 1)$) donc elle converge.
    
    - On suppose $alpha = 1$. Soit $n in NN^*$.
        $ integral_1^(n+1) (dif x)/x = ln(n+1) -->_(+oo) +oo $

        La série $sum 1/n$ diverge.
]

#ydef(name: [Fonction zéta de Riemann])[
    Pour $alpha > 1$, on note $zeta(alpha)$ le nombre
    $ zeta(alpha) = sum_(n=1)^(+oo) 1/n^alpha $
    $zeta$ est appelée _fonction zéta de Riemann_.
]

#yprop(name: [Valeurs en 2, 3 et 4 de la fonction zéta de Riemann])[
    - $zeta(2) = pi^2/6$
    - $zeta(4) = pi^4/90$
    N.B. On ne sait même pas si $zeta(3)$ est rationnel ou irrationnel.
]

#yprop(name: [Convergence d'une série à terme général positif à l'aide d'une intégrale])[
    Soit $f: [a, +oo[ -> RR^+$ décroissante et continue.

    Alors $sum f(n)$ converge ssi $(integral_a^n f(t) dif t)_n$ converge.
]

#yproof[
    Soit $k in NN$ avec $k >= alpha + 1$.

    $f$ est continue sur $[k -1, k]$.

    Alors
    $ integral_(k-1)^k f(t) dif t
        >= integral_(k-1)^k f(k) dif t
        >= f(k) $
    car $forall t <= k, f(t) >= f(k)$.


    De même, $f$ est continue sur $[k, k+1]$.
    $ integral_k^(k+1) f(t) dif t
        <= integral_k^(k+1) f(k) dif t
        <= f(k) $
    
    D'où pour tout $n > floor(a) + 1$
    $ integral_(floor(a)+2)^(n+1) f(t) dif t 
        &<= sum_(k=floor(a)+2)^n f(k) \
        &<= integral_(floor(a)+1)^n f(t) dif t $

    - On suppose que $(integral_a^n f(t) dif t)_n$ converge vers $I in RR$.

        Pour tout $n > floor(a) + 1$,
        $ sum_(k= floor(a) + 1)^n f(k) <=
            integral_(floor(a)+1)^n f(t) dif t $

        D'où, pour tout $n > floor(a) + 1$,
        $ &sum_(k = floor(a) +1)^n f(k) \
            &<= integral_(floor(a)+1)^a f(t) dif t
                + integral_a^n f(t) dif t \
            &<= C + I $
        où $C = integral_(floor(a)+1)^a f(t) dif t$ est une constante.

        La suite $(sum_(k=floor(a) + 2)^n f(k))_n$ est croissante
        et majorée, donc elle converge,
        donc $sum f(n)$ converge.
    
    - On suppose que $(integral_a^n f(t) dif t)_n$ diverge.

        Comme cette suite est croissante,
        $ lim_(n->+oo) integral_0^n f(t) dif t = +oo $

        Pour tout $n > floor(a) + 1)$,
        $ &sum_(k = floor(a) + 2)^n f(k) \
            &>= integral_(floor(a) + 2)^a f(t) dif t
                +integral_a^(n+1) f(t) dif t \
            &-->_(n->+oo) +oo $
        
        Donc $sum f(n)$ diverge.

    N.B. Cette preuve est à connaître  : on peut en avoir 
    vesion pour trouver un équivalent des sommes partielles
    en cas de divergence, ou pour trouver un équivalent
    des restes partiels en cas de convergence.
]