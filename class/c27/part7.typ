#import "../../template/ytemplate.typ": *


#ypart[Séries alternées]


#ydef(name: [Série alternée])[
    Soit $(u_n) in RR^NN$.

    Une série $sum u_n$ est dite _alternée_
    s'il existe $(v_n) in RR^NN$ telle que
    - $forall n in NN$, $u_n = (-1)^n v_n$
    - $(v_n)$ ne change pas de signes.
]

#ytheo(name: [Théorème des séries alternées])[
    Soit $sum u_n$ une série alternée.

    Si $(abs(u_n))_(n in NN)$ est décroissante à partir
    d'un certain rang et de limite nulle,
    alors $sum u_n$ converge.    

    Pour tout $n in NN$,
    $sum_(k=n+1)^(+oo) u_k $
    est du même signe que $u_(n+1)$ et
    $ abs(sum_(k=n+1)^(+oo) u_k) <= abs(u_(n+1)) $

    N.B. on pourra entre "critère des séries alternées"
    ou a Nantes "critère spécial des séries alternée".
]

#yproof[
    On suppose sans perte de généralité que,
    $ forall n in NN, u_n = (-1)^n underbrace(abs(u_n), v_n) $

    On suppose $(v_n)$ décroissante de limite nulle.

    On pose
    $ forall n in NN, S_n = sum_(k=0)^n u_k $

    Soit $n in NN$.
    $ S_(2(n+1)) - S_(2n)
        &= S_(2n + 2) - S_(2n) \
        &= sum_(k=0)^(2n + 2) u_k - sum_(k=0)^(2n) u_k \
        &= u_(2n + 1) - + u_(2n + 2) \
        &= - v_(2n+1) + v_(2n+2) \
        &<= 0 $
    
    Ainsi $S_(2n)$ est décroissante.

    Pour $n in NN$,
    $ S_(2(n+1) + 1)
        &= S_(2n + 3) - S_(2n + 1) \
        &= u_(2n+2) + u_(2n+3) \
        &= v_(2n+2) - v_(2n + 3) \
        &>= 0 $
    
    Donc $(S_(2n + 1))$ est croissante.

    Pour $n in NN$,
    $ abs(S_(2n+1) - S_(2n)) &= abs(u_(2n+1)) \
        &= v_(2n + 1) \
        &-->_(+oo) 0 $
    
    Ainsi $(S_(2n))$ et $(S_(2n+1))$
    sont adjacentes, donc elles convergent
    et ont la même limite, donc $(S_n)$ converge,
    _i.e._ $sum u_n$ converge.

    De plus, notons $S = lim_(+oo) S_n$.
    $ forall n, S_(2n+1) <= S <= S_(2n) $
    et donc, en notant 
    $ R_n = sum_(k=0)^(+oo) u_k = S - S_n $

    Or $R_(2n) = underbrace(u_(2n+1), <=0) + u_(2n + 2) + ...$
    donc $R_(2n)$ est du même signe que sont premier terme.

    De même pour $R_(2n+1)$.

    De plus,
    $ R_(2n+1) - u_(2n + 2) &= u_(2n + 3) + ... \
        &= R_(2n + 2) <= 0 $
    donc $abs(R_(2n+2)) <= abs(u_(2n+2))$.

    $ abs(R_(2n)) - abs(u_(2n + 1)) &= -R_(2n) + u_(2n + 1) \
        &= -R_(2n + 1) <= 0 $
    donc $abs(R_(2n)) <= abs(u_(2n+1))$.

    On a démontré en plus que
]

#yprop(name: [Convergence de la série harmonique alternée])[
    La série $sum (-1)^n/n$ est semi-convergente.
]

#yproof[
    C'est une série alternée et
    $ forall n in NN, abs((-1)^n/n) = 1/n -->_(+oo) 0 $
    $(1/n)$ est décroissante.
]