#import "../../template/ytemplate.typ": *


#ypart[Dévellopement décimal d'un réel]


#yprop(name: [Convergence d'un développement décimal])[
    Pour toute suite $(d_i) in [|0, 9|]^NN^*$,
    la série $sum d_i/10^i$ converge.

    N.B. hors-programme.
]

#yproof[
    Pour $i in NN^*$,
    $ cases(d_i/10^i >= 0,
        d_i/10^i <= 9 (1/10)^i) $
    
    Donc $sum (1/10)^i$ converge car $abs(1/10) < 1$.

    Par linéarité, $sum 9 (1/10)^i$ converge.

    Par comparaison de séries à terme général positif,
    la série $sum d_i/10^i$ converge.
]

#ydef(name: [Dévellopement décimal d'un réel])[
    Soit $x in RR$, $d_0 in ZZ$ et $(d_i)_(i>=1) in [|0, 9|]^NN^*$.

    On dit que $(d_0, d_1, ...)$ est un
    _développement décimal_ de $x$ si
    $ x = d_0 + sum_(i=1)^(+oo) d_i/10^i $

    N.B. hors-programme.
]

#ytheo(name: [Éxistance et nombre de développements décimal d'un réel])[
    Soit $x in RR$.

    Alors $x$ a au moins un développement
    décimal.

    - Si $x$ n'est pas décimal,
        (_i.e_ pas de la forme $k/10^n$, $k in ZZ$, $n in NN$)
        alors ce développement est unique.

    - Si $x$ est décimal, alors $x$ a exactement $2$
        développement décimaux l'un n'a que des $0$
        à partir d'un certain rang,
        l'autre que des 9 à partir d'un certain rang.

    N.B. hors-programme.
]

#yproof[
    Dans le cas où $x in ]0, 1[$.

    #smallcaps[Existance] : On pose pour tout $n in NN^*$,
    $ d_n = floor(10^n x) - 10 floor(10^(n-1) x) $

    Soit $n >= 1$.
    Alors $d_n$ est une différence d'entiers donc $d_n in ZZ$.

    De plus on encadre les parties entières
    $ 10^n x - 1 < floor(10^n x) <= 10^n x $
    $ -10^n x - 10 &> -10 floor(10^(n-1) x) \
            &>= -10^n x $
    
    D'où
    $ &10^n x -1 - 10^n x \
        &< d_n \
        &< 10^n x - 10^n x + 10 $
    et donc
    $d_n in [|0, 9|]$.

    On sait que la série $sum_(n>=1) (d_n)/10^n)$ converge
    $ &sum_(n=1)^(+oo) (d_n)/10^n \
        &= sum_(n=1)^(+oo) (floor(10^n x) - 10 floor(10^(n-1) x))/10^n \
        &= sum_(n=1)^(+oo) (floor(10^n x)/10^n - floor(10^(n-1) x)/10^(n-1)) \
        &= lim_(n->+oo) floor(10^n x)/10^n - floor(x)/1 \
        &= x - 0 \
        &= x $

    #smallcaps[Unicité ?] Soit $(a_n) in [|0,9|]^NN^*$ tel que
    $ sum_(n=1)^(+oo) a_n/10^n = sum_(n=1)^n d_n/10^n $

    Soit $N = min{k in NN^* | a_k != d_k}$. Alors
    $ cases(forall k < N\, a_k = d_k,
        a_N != d_N) $
    
    D'où,
    $ &a_N/10^N + sum_(n=N+1)^(+oo) a_n/10^n \
        &= d_N/10^N + sum_(n=N+1)^(+oo) d_n/10^n $
    et donc
    $ a_N - d_N
        &= sum_(n=N+1) (d_n - a_n)/10^(n-N) \
        &=sum_(k=1) (d_(k+N) - a_(k+N))/10^k $
    
    Or
    $ -1 = sum_(k=1)^(+oo) (-9)/10^k 
        &<= sum_(k=1)^(+oo) (d_(k+N) - a_(k+N))/10^k \
        &<= sum_(k=1)^(+oo) 9/10^k = 1 $
    
    Donc $a_N - d_N in { -1, 1 }$.

    - Cas 1 : $a_N - d_N = 1$

        Nécessairement,
        $ forall k in NN^*, d_(k+N) - a_(k+N) = 9 $
        avec
        $ forall k in NN^*, cases(d_(k+N) = 9, a_(k+N) = 0) $

    - Cas 2 : $a_N - d_N = -1$
        $ forall k in NN^*, cases(d_(k+N) = 9, a_(k+N) = 0) $
    
    Dans les 2 cas, l'un des développements n'a que
    des 0 à partir d'un certain rang et alors $x$
    est décimal, et on est forcément dan le cas 2
    $ x &= 0. d_1 ... d_(N-1) d_N underline(0) ... \
        &= 0. d_1 ... d_(N-1) (d_N -1) underline(9) ... $
    
    On vérifie sans difficulté que
    $ 0. d_1 ... d_(N-1) (d_N -1) underline(9) ... $
    égale $x$.
]