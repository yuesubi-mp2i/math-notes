#import "../../template/ytemplate.typ": *


#ypart[Linéarité]


#yprop(name: [Convergence d'une combinaison linéaire de séries])[
    Soit $sum u_n$ et $sum v_n$ deux séries complexes et $lambda, mu in CC$.

    Si $sum u_n$ et $sum v_n$ convergent,
    alors $sum (lambda u_n + mu v_n)$ converge aussi et
    $ &sum_(n=0)^(+oo) (lambda u_n + mu v_n) \
        &= lambda sum_(n=0)^(+oo) u_n + mu sum_(n=0)^(+oo) v_n $
]

#yproof[
    Pour tout $n in NN$,
    $ &sum_(k=0)^n (lambda u_k + mu v_k) \
        &= lambda sum_(k=0)^n u_k + mu sum_(k=0)^n v_k \
        &-->_(n->+oo) lambda sum_(k=0)^(+oo) u_k + mu sum_(k=0)^(+oo) v_k \ $
]

#yprop(name: [Convergence du produit par un scalaire d'une série])[
    Soit $sum u_n$ une série complexe.

    Pout tout $lambda in CC^*$ non nul,
    $sum u_n$ converge ssi $sum lambda u_n$ converge.
]

#yprop(name: [Condition de divergence d'une somme de séries])[
    Soit $sum u_n$ et $sum v_n$ deux séries complexes.
    
    Si $sum u_n$ converge et $sum v_n$ diverge,
    alors $sum (u_n + v_n)$ diverge.
]

#yproof[
    On suppose que $sum u_n$ converge et $sum v_n$ diverge.

    Si $sum (u_n + v_n)$ converge,
    alors $sum v_n = sum (u_n + v_n) - sum u_n$ converge :
    ce qui est une contradiction.
]

#yprop(name: [Caractérisation de la convergence d'une suite avec la série téléscopique])[
    Soit $u$ une suite complexe.

    $(u_n)$ converge ssi $sum (u_(n+1) - u_n)$ converge.
]

#yproof[
    Pour tout $n in NN$,
    $ sum_(k=0)^n (u_(k+1) - u_k) = u_(n+1) - u_0 $

    - Si $u_n -->_(+oo) ell$ alors
        $sum_k (u_(k+1) - u_k)$ converge
        et $ sum_(n=0)^(+oo) (u_(n+1) - u_n) = ell - u_0 $

    - On suppose que la série converge,
        alors, pout tout $n >= 1$,
        $ u_n &= sum_(k=0)^(n-1) (u_(k+1) - u_k) + u_0 \
            &--> sum_(k=0)^(+oo) (u_(k+1) -u_k) + u_0 $
]

#ytheo(name: [Formule de Stirling])[
    $ n! tilde_(n->+oo) sqrt(2 pi n) (n/e)^n $
]

#yproof[
    On pose pour $n in NN^*$,
    $ u_n = ln(n!) = sum_(k=1)^n ln k $

    L'idée et d'écrire $u_n$ sous la forme
    $ u_n =_(+oo) d_n + o(1) $
    avec $d_n$ "simple", de sorte que
    $ n! = e^(d_n + o(1)) = e^(d_n) e^o(1) tilde e^(d_n) $

    #smallcaps[Étape 1]

    Pour tout $k >= 2$
    $ integral_(k-1)^k ln(x) dif x <= ln k <= integral_k^(k+1) ln x dif x $
    d'où, pour $n >= 2$,
    $ integral_1^n ln(x) dif x <= u_n <= integral_2^(n+1) ln x dif x $

    On calcule,
    $ integral_1^n ln(x) dif x
        &= [x ln x - x]_1^n \
        &= n ln n - x - 1 \
        &tilde n ln n  $
    et
    $ &integral_2^(n+1) ln(x) dif x \
        &= [x ln x - x]_2^(n+1) \
        &= (n+1) ln (n+1) - (x+1) \
            &wide - 2 ln 2 + 2 \
        &tilde (n + 1) ln(n +1) \
        &tilde n ln n $
    car
    $ ln(n+1) &= ln (n (1 + 1/n)) \
        &= ln n + ln(1 + 1/n) \
        &tilde ln n $
    
    D'où, $u_n tilde n ln n$ donc
    $ u_n = n ln n + o(n ln n) $

    #smallcaps[Étape 2]

    On pose pour $n >= 2$,
    $ v_n = u_n - n ln n $

    Pour tout $n >= 2$,
    $ &v_(n+1) - v_n \
        &= (u_(n+1) - u_n) - (n+1) ln(n+1) \
            &wide + n ln n \
        &= ln(n+1) - (n+1) ln (n+1) \
            &wide + n ln n \
        &= -n ln((n+1)/n) \
        &= -n ln(1 + 1/n) \
        &tilde -1 $
    on peut conjecturer
    #footnote[On verra en MPI pourquoi on a le droit ici] <conj>
    que
    $ sum_(k=1)^n (v_(k+1) - v_k) tilde -n $.

    #smallcaps[Étape 3]

    On pose, pour $n>=2$,
    $ w_n = v_n + n $

    Pour $n >=2$,
    $ &w_(n+1) - w_n \
        &= (v_(n+1) - v_n) + 1 \
        &= -n ln(1+1/n) + 1 \
        &= -n(1/n - 1/(2n^2) + o(1/n^2) ) \
        &= 1/(2n) + o(1/(2 n)) \
        &tilde 1/(2 n) $
    
    Conjecture
    #footnote[On verra aussi en MPI pourquoi on a le droit ici] <conj>

    (...)

    #smallcaps[Étape 4]

    On pose, pour $n >= 2$,
    $ &t_n \
        &= w_n - 1/2 ln n \
        &= w_(n+1) - w_n - 1/2 ln(n+1) + 1/2 ln n \
        &= n ln(1+ 1/n) + 1 - 1/2 ln(1+ 1/n) \
        &= "DLs à l'ordre 3 et 2" \
        &= (...) $
    
    (...)

    $sum -1/(12 n^2)$ converge ($2>1$)

    donc $sum (t_(n+1) - t_n)$ converge

    donc $(t_n)$ converge.

    Soit $C = lim_(n->+oo) t_n$.

    Alors $t_n = C + o(1)$ et donc
    $ u_n = n ln n - n + 1/2 ln n + C + o(1) $

    D'où,
    $ n! &tilde e^(n ln n - n + 1/2 ln n + C) \
        &tilde n^n e^(-n) sqrt(n) L \
        &tilde L sqrt(n) (n/e)^n $
    où $L = e^C$.

    D'après la formule de Wallis,
    $ I_(2n) tilde sqrt(pi/(4n)) $
    où
    $ I_(2 n) = integral_0^(pi/2) (sin t)^(2n) dif t
        = pi/2 ((2n)!)/(2^n n!)^2 $

    (...)

    Donc $L = sqrt(2pi)$.
]