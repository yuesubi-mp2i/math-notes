#import "../../template/ytemplate.typ": *


#ypart[Séries à terme général prositif]


#yprop(name: [Monotonie d'une série à terme général positif])[
    Soit $(u_n) in (RR^+)^NN$.

    Alors $(sum_(k=0)^n u_k)$ est croissante.
]

#yproof[
    Pour tout $n in NN$,
    $ sum_(k=0)^(n+1) u_k - sum_(k=0)^n u_k = u_(n+1) >= 0 $
]

#yprop(name: [Convergence de suites comparées à terme général positif])[
    Soient $(u,v) in ((RR^+)^NN)^2$ deux suites
    *positives* telles que
    $ forall n in NN, 0 <= u_n <= v_n $

    + Si $sum v_n$ converge alors $sum u_n$ aussi.

    + Si $sum u_n$ diverge alors $sum v_n$ aussi.

    Attention : La réciproque est fausse !
]

#yproof[
    Contre-exemple de la réciproque :
    #pad(left: 1em)[
    Pour tout $n$ assez grand,
    $ 0 <= e^(-n) <= 1/n $ car $n e^(-n) -->_(+oo) 0$.

    La série $sum e^(-n)$ converge car $abs(e^(-1)) < 1$
    mais la série harmonique diverge.
    ]

    2 est la contraposée de 1.

    On suppose que $sum v_n$ converge.
    Donc pour tout $n in NN$,
    $ sum_(k=0)^n v_k <= sum_(k=0)^(+oo) v_k = S $

    Par hypothèse $forall k, u_k <= v_k$.

    Donc pour tout $n in NN$,
    $ sum_(k=0)^n u_k <= sum_(k=0)^n v_k <= S $

    Donc $(sum_(k=0)^n u_k)$ est majorée par $S$.

    Comme cette suite est croissante, elle converge
    d'après le théorème de la limite monotone.
]

#yprop(name: [Convergence de suites dominées à terme général positif])[
    Soit $(u,v) in ((RR^+)^NN)^2$ telles que $u = Omicron(v)$.

    + Si $sum v_n$ converge, alors $sum u_n$ converge.

    + Si $sum u_n$ diverge, alors $sum v_n$ diverge.
    $qed$
]

#ycor(name: [Convergence de suites négligeables à terme général positif])[
    Soit $(u,v) in ((RR^+)^NN)^2$ telles que $u = omicron(v)$.

    + Si $sum v_n$ converge, alors $sum u_n$ converge.

    + Si $sum u_n$ diverge, alors $sum v_n$ diverge.
]

#yproof[
    Corollaire du même résultat avec des $Omicron$.
    $ u = o(v) => u = Omicron(v) $
]

#ytheo(name: [Convergence de suites équivalentes à terme général positif])[
    Soit $(u,v) in ((RR^+)^NN)^2$ telles que
    $u tilde_(n->+oo) v$.

    Alors la série $sum u_n$ converge
    ssi la série $sum v_n$ converge.
]

#yproof[
    $forall epsilon > 0, exists N in NN, forall n >= N$,
    $ abs(u_n - v_n) <= epsilon abs(v_n) $

    Soit $N in NN$ tel que
    $ forall n >= NN, -1/2 v_n <= u_n - v_n <= 1/2 v_n $
    et donc
    $ forall n >= NN, 1/2 v_n <= u_n <= 3/2 v_n $

    Donc $u = Omicron(v)$ et $v = Omicron(u)$.
]