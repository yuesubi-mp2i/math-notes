// Systèmes linéaires
#import "../../template/ytemplate.typ": *

#ychap[Systèmes linéaires]

#include "part1.typ"
#include "part2.typ"
#include "part3.typ"
#include "part4.typ"