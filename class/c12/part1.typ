#import "../../template/ytemplate.typ": *

Dans ce chapitre, pour les paragraphes sont décalés de 1.

#ypart[Exemples]


#yexample[
    + $ (S) : cases(x + y + z = 1, y - z = 0) $
        C'est un système de 2 équations à 3 inconnues.

        Soit $(x, y, z) in RR^3$.
        $ cases(x + y + z = 1, y - z = 0) $
        $ <=> cases(x + 2 y = 1, y = z) $
        $ <=> cases(x = 1 - 2y, y = z) $

        L'ensemble des solutions de $(S)$ est
        $ { (1 - 2y, y, y) | y in RR } $
        
        Bonus : pour $y in RR$,
        $ (1-2y, y, y)
            yt = (1,0,0) + (-2y, y, y)
            yt = underbrace((1,0,0), "solution \n particulière")
                + underbrace(y (-2, 1, 1),
                    "solution du système \n homogène associé") $
    
    + $ cases(x + y + z = 1,  
        x + y - z = 0,
        x + 2 y = 2) $

        Soit $(x,y,z) in RR^3$.

        $ cases(x + y + z = 1,  
            x + y - z = 0,
            underline(x) + 2 y = 2) $
        $ <=>_(L_1 <- L_1 - L_3 \ L_2 <- L_2 - L_3)
            cases(-y + underline(z) = -1,  
                -y - z = -2,
                underline(x) + 2 y = 2) $
        $ <=>_(L_2 <- (-1)/2 (L_2 - L_1))
            cases(-y + underline(z) = -1,
                underline(y) = 3/2,
                underline(x) + 2 y = 2) $
        $ <=>_(L_1 <- L_1 - L_2 \ L_3 <- L_3 - 2 L_2)
            cases(z = 1/2,
                y = 3/2,
                x = -1) $
    
    + $ cases(x + underline(y) + z = 1,
            x + y - z = 0,
            2x + 2y = 1) $
        $ <=>_(L_2 <- (-1)/2 (L_2 - L_1) \ L_3 <- L_3 - 2 L_1)
            cases(x + underline(y) + z = 1,
                underline(z) = 1/2,
                -2 z = -1) $
        $ <=>_(L_1 <- (L_1 - L_2) \ L_3 <- L_3 + 2 L_2)
            cases(x + underline(y) = 1/2,
                underline(z) = 1/2,
                0 = 0) $
        $ <=> cases(x + underline(y) = 1/2,
                underline(z) = 1/2) $
        $ <=> cases(y = 1/2 - x,
                z = 1/2) $

        L'ensemble des solutions est
        $ { (x, 1/2 - x, 1/2) | x in RR } $
    
    + $ cases(underline(x) + y + z = 1,
            x + y - z = 0,
            x + 2y + 3z = 3,
            3x + y + z = 2) $
        $ <=>_(L_2 <- L_2 - L_1 \
                L_3 <- L_3 - L_1 \
                L_4 <- L_4 - L_3)
            cases(underline(x) + y + z = 1,
                -2z = -1,
                underline(y) + 2z = 2,
                -2y -2z = -1) $
        $ <=>_(L_1 <- L_1 - L_3 \
                L_4 <- (L_4 - 2 L_3) 1/2)
            cases(underline(x) - z = -1,
                -2z = -1,
                underline(y) + 2z = 2,
                underline(z) = 3/2) $
        $ <=>_(L_1 <- L_1 + L_4 \
                L_2 <- L_2 + 2 L_4 \
                L_3 <- L_3 - 2 L_4)
            cases(underline(x) = 1/2,
                underbrace(0 = 2, "équation d'\nincompatibilité"),
                underline(y) = -1,
                underline(z) = 3/2) $
        
        Il n'y a pas de solution.
]

#yexample[
    On ne peut pas faire :
    $ cases(x + y = 1,
        x - y = 0) $
    $ cancel(<=>_(L_1 <- L_1 - L_2 \ L_2 <- L_2 - L_1))
        cases(2y = 1
            -2y = - 1) $
    $ <=> y = 1/2 $

    Mais on peut faire (pas la technique du pivot) :
    $ cases(x + y = 1,
        x - y = 0) $
    $ <=>_(L_1 <- 1/2 (L_1 + L_2) \ L_2 <- 1/2 (L_1 - L_2))
        cases(x = 1/2,
            y = 1/2) $
]