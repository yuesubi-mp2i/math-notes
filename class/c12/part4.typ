#import "../../template/ytemplate.typ": *


#ypart[Méthode du pivot de Gauss]


#ymetho(name: [Méthode du pivot de Gauss])[
    Soit $(S)$ un système linéaire :
    $ forall i in [|1, n|], sum_(j=1)^p a_(i,j) x_j = b_i $
    avec $n in NN$.

    On choisi $j in [|1, p|]$ et $i in [|1, n|]$ tel que
    $a_(i,j) != 0$.

    On utilise alors $L_i$ pour éliminer $x_j$ des autres lignes:
    $ forall k in [|1, n|], L_k <- L_k - a_(k,j)/a_(i,j) L_i $

    On itère ce procédé en choisissant
    $i' != i$ et $j' != j$ tel que $a_(i',j') != 0$
    et ainsi de suite jusqu'à ce qu'il n'y ait plus de choix
    possible.

    À la fin, on obtient un système de la forme,
    quitte à échanger des lignes,
    $ cases(&x_j_1 + sum_(j in J) &a'_(1,j) x_j = &b'_1,
        &x_j_2 + sum_(j in J) &a'_(2,j) x_j = &b'_2,
        &dots.v &dots.v &dots.v,
        &x_j_r + sum_(j in J) &a'_(r,j) x_j = &b'_r,
        & & 0 = &b'_(r+1),
        & & dots.v &dots.v,
        & & 0 = &b'_n) $
    où $J = [|1, p|] without {j_1, ... j_r}$.

    Si
    $ forall k >= r + 1, b'_k != 0 $
    alors le système est *imcompatible* : il n'a pas de solutions.

    Sinon, le système a au moins une solution : il suffit
    de choisir une valeur pour chaque $x_j, j in J$, on
    en déduit $x_j_1, ... x_j_r$.
    Si $r = p$, alors il y a une unique solution.

    Les inconnues $x_j_1, ..., x_j_r$ sont dites
    *inconnues principales*, les $x_j$ avec $j in J$
    sont appelées *paramètres* ou *inconnues secondaires*.

    L'entier $r$ (le nombre d'inconnues principales) ne
    dépend pas des choix des pivots : on l'appelle le *rang*
    du système.

    On démontrera ce résultat plus tard (dans le chapitre
    sur les sous-espaces affines).
    $qed$
]

#yexample[
    Soit $P$ le plan (dans $RR^3$) passant par
    $A(1, 0, 0)$ et dirigé par les vecteurs
    $arrow(u)(0, 1, 1)$ et $arrow(v)(-1, 0, 1)$.
    Trouver une équation de $P$.
    Soit $M(x,y,z) in RR^3$.

    $ M in P
        yt <=> exists (alpha, beta) in RR^2,
            ytt arrow(A M) = alpha arrow(u) + beta arrow(v)
        yt <=> exists (alpha, beta) in RR^2,
            ytt (S) : cases(x - 1 = - beta,
                y = alpha,
                z = alpha + beta) $
    
    $(S)$ est un système linéaire à 3 équations, 2 inconnues
    $alpha$ et $beta$, et 3 paramètres $x, y, z$.

    On cherche les paramètres $x,y,z$ pour que $(S)$ ait au
    moins une solution.
    $ M in P $
    $ <=> exists (alpha, beta) in RR^2
        yt cases(x - 1 = - beta,
            y = underline(alpha),
            z = alpha + beta) $
    $ <=>_(L_3 <- L_3 - L_2) exists (alpha, beta) in RR^2
        yt cases(x - 1 = - beta,
            y = underline(alpha),
            z - y = underline(beta)) $
    $ <=>_(L_1 <- L_1 + L_3) exists (alpha, beta) in RR^2
        yt cases(0 = x - 1 + z - y,
            y = underline(alpha),
            z - y = underline(beta)) $
    $ <=> x - y + z = 1 $
]