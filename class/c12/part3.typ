#import "../../template/ytemplate.typ": *


#ypart[Opérations élémentaires]


#ydef(name: [Opération élémentaire sur un système linéaire])[
    Une *opération élémentaire* sur les lignes
    d'un système linéaire est l'une des opérations
    suivantes :
    - échanger 2 lignes : $(L_i <-> L_j)$
    - multiplier une ligne par $lambda in KK$, avec
        $lambda != 0$ : $(L_i <- lambda L_i)$
    - ajouter à une ligne $(L_i)$ $lambda$ fois la
        ligne $(L_j)$ ($i != j$) : $(L_i <- L_i + lambda L_j)$
]

#yprop(name: [Équivalence des systèmes linéaire par une opération élémentaire])[
    Soit $(S)$ un système linéaire, $(S')$ le système
    obtenu en appliquant aux lignes de $(S)$ une opération
    opération élémentaire.

    Alors $(S)$ et $(S')$ ont les mêmes solutions.
]

#yproof[
    C'est évident si l'opération est un échange de lignes.

    On note $(S)$ le système
    $ cases(a_(1,1) &x_1 + ... + a_(1,p) &x_p = &b_1,
        &dots.v &dots.v &dots.v,
        a_(n,1) &x_1 + ... + a_(n,p) &x_p = &b_n) $
    et $(S')$ le système
    $ cases(forall k != i\, sum_(j=1)^p a_(k,j) x_j = b_k,
        lambda sum_(j=1)^p a_(i,j) x_j = lambda b_i) $
    
    Soit $(x_1, ..., x_p) in KK^p$.
    $ (x_1, ..., x_p) "solution de" (S)
        yt => forall k, sum_(j=1)^p a_(k,j) x_j = b_k
        yt => cases(forall k != i\, sum_(j=1)^p a_(k,j) x_j = b_k,
            lambda sum_(j=1)^p a_(i,j) x_j = lambda b_i)
        yt => (x_1, ... x_p) "solution de" (S') $

    $ (x_1, ..., x_p) "solution de" (S')
        yt => cases(forall k != i\, sum_(j=1)^p a_(k,j) x_j = b_k,
            lambda sum_(j=1)^p a_(i,j) x_j = lambda b_i)
        yt => cases(forall k != i\, sum_(j=1)^p a_(k,j) x_j = b_k,
            1/cancel(lambda) cancel(lambda) sum_(j=1)^p a_(i,j) x_j
                = 1/cancel(lambda) cancel(lambda) b_i)
        yt => forall k, sum_(j=1)^p a_(k,j) x_j = b_k
        yt => (x_1, ... x_p) "solution de" (S) $
    
    On pose $(S')$ le système
    $ cases(forall k != i\, sum_(l=1)^p a_(k,l) x_l = b_k,
        sum_(l=1)^p (a_(i,l) + lambda a_(j, l)) x_l = b_i + lambda b_j) $
    
    Soit $(x_1, ..., x_p) in KK^p$.

    On suppose que $(x_1, ..., x_p)$ est une solution de
    $(S)$. On sait déjà,
    $ forall k != i, sum_(l=1)^p a_(k,l) x_l = b_k $

    D'où,
    $ sum_(l=1)^p (a_(i,l) + lambda a_(j,l)) x_l
        yt = sum_(l=1)^p a_(i,l) x_l + lambda sum_(l=1)^p a_(j,l) x_l
        yt = b_i + lambda b_j $
    
    Donc $(x_1, ..., x_p)$ est une solution de $(S')$

    On passe de $(S')$ à $(S)$ avec l'opération
    $L_i <- L_i - lambda L_j $ (car la ligne $j$ de $(S')$
    est la ligne $j$ de $(S)$, car $i != j$).
    $qed$
]