#import "../../template/ytemplate.typ": *


#ydate(2023, 12, 20)

#ypart[Définitions]


On note $KK$ l'ensemble $RR$ ou $CC$.

#ydef(name: [Système linéaire])[
    Un *système linéaire* de $n$ équations à $p$ inconnues
    $x_1, ... x_p$ est la donnée de $n$ équations de la
    forme
    $ sum_(i=1)^p a_i x_i = b $
    où $(a_1, ..., a_p, b) in KK^(p + 1)$.
]

#ydef(name: [Système homogène associé])[
    Soit $(S)$ le système linéaire
    $ cases(a_(1,1) &x_1 + ... + a_(1,p) &x_p = &b_1,
        a_(2,1) &x_1 + ... + a_(2,p) &x_p = &b_2,
        &dots.v &dots.v &dots.v,
        a_(n,1) &x_1 + ... + a_(n,p) &x_p = &b_n) $

    On dit que $(S)$ est *homogène* si
    $ forall i in [|1, n|], b_i = 0 $

    Le *système homogène associé* à $(S)$ est
    $ (H) : cases(a_(1,1) &x_1 + ... + a_(1,p) &x_p = &0,
        a_(2,1) &x_1 + ... + a_(2,p) &x_p = &0,
        &dots.v &dots.v &dots.v,
        a_(n,1) &x_1 + ... + a_(n,p) &x_p = &0) $
]

#ybtw[
    Un système linéaire peut se représenter matriciellement
    sous la forme
    $ A X = B $

    où
    $A = mat(a_(1,1), ..., a_(1,p);
        dots.v, ,dots.v;
        a_(n,1), ..., a_(n,p))$,
    $X = vec(x_1, dots.v, x_p)$
    et $B = vec(b_1, dots.v, b_n)$.
]

#ydef(name: [Résoudre un système linéaire])[
    *Résoudre* un système linéaire d'inconnues
    $x_1, ..., x_p$, c'est trouver toutes les
    valeurs que peut prendre $(x_1, ..., x_p)$
    respectant les équations.
]