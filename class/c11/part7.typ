#import "../../template/ytemplate.typ": *


#ypart[Caractérisation séquentielle]


#ytheo(name: [Caractérisation séquencielle de la densité])[
    Soit $A in cal(P)(RR) without { emptyset }$.

    $ A "dense dans" RR <=>
        yt forall x in RR, exists (a_n) in A^NN, a_n -->_oo x $
]

#yproof[
    "$==>$" On suppose que $A$ est dense.

    Soit $x in RR$. Pour $n in NN$,
    $ ]x - 1/(n+1), x + 1/(n+1)[ sect A != emptyset $

    Soit $a_n$ dans cet ensemble. Pour tout $n in NN$,
    $ x - 1/(n + 1) < a_n < x + 1/(n + 1) $

    De plus,
    $ cases(x - 1/(n+1) -->_oo x, x + 1/(n+1) -->_oo x) $

    D'après le théorème d'existence d'une limite par
    encadrement, $a_n -->_oo x$.

    "$<==$" On suppose que
    $ forall x in RR, exists (a_n) in A^NN, a_n -->_oo x $

    Soit $(a,b) in RR^2$ avec $a < b$.

    On pose $x = (a + b)/2 in RR$.

    Soit $(a_n) in A^NN$ telle que $a_n -->_oo x$.

    On pose $epsilon = (b - a)/2 in RR$.

    Soit $n_0 in NN$ tel que
    $ forall n >= n_0, abs(a_n - x) <= epsilon $

    Donc
    $ x - epsilon <= a_(n_0) <= x + epsilon $

    Or
    $ x + epsilon &= (a + b)/2 + (b - a)/3 \
        &= (a + 5b)/6 \
        &< (b + 5b)/6 = b $
    et
    $ x - epsilon &= (a + b)/2 + (b - a)/3 \
        &= (5a + b)/6 \
        &> (5a + a)/6 = a $
    
    Donc
    $ cases(a_(n_0) in ]a\, b\[, a_(n_0) in A) $
    $A$ est dense dans $RR$.
    $square$
]

#yprop(name: [Caractérisation séquentielle de la borne supérieure])[
    Soit $A$ une partie non vide et majorée de $RR$, et $s in RR$.
    $ s = sup(A) <=>
        yt cases(forall a in A\, a <= s,
            exists (a_n) in A^NN\, s = lim_oo a_n) $
]

#yproof[
    "$==>$" On suppose $s = sup(A)$.

    On sait déjà que $s$ majore $A$.

    Soit $n in NN$. $s - 1/(n+1) < s$ donc $s - 1/(n+1)$
    ne majore pas $A$.

    Soit $a_n in A$ tel que $a_n > s - 1/(n+1)$.

    On vient de créer une suite $(a_n) in A^NN$ telle que
    $ forall n in NN, s - 1/(n+1) < a_n <= s $

    D'après le théorème d'existance d'une limite par
    encadrement, $a_n -->_oo s$.

    "$<==$" On suppose que $s$ est un majorant de $A$
    et qu'il existe $(a_n) in A^NN$ de limite $s$.

    Soit $epsilon > 0$. Soit $N in NN$ tel que
    $ forall n >= N, abs(a_n - s) <= epsilon $
    En particulier
    $ forall n >= N, a_n >= s - epsilon $

    Si $forall n >= N, a_n = s - epsilon$
    alors $a_n -->_oo s - epsilon != s$.

    Donc
    $ exists n >= N, a_n > s - epsilon $
    et donc $s - epsilon$ ne majore pas $A$.

    Donc $s = sup(A)$.
    $qed$
]

#ybtw[
    Il existe un résultat similaire pour la borne inférieure.
]