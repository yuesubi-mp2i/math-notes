#import "../../template/ytemplate.typ": *


#ypart[Suites extaites]


#ydef(name: [Suite extraite])[
    Soit $u$ une suite complexe.

    Un *sous-suite* de $u$ ou une *suite extraite* de $u$
    est une suite de la forme $(u_phi(n))$ où $phi: NN --> NN$
    est strictement croissante.

    On dit que $phi$ est une extractrice.

    // dessin
]

#ybtw[
    Une sous suite de $u$ peut aussi
    s'écrite sous la forme $(u_(n_k))_k$ où $(n_k)$
    est strictement croissante.
]

#ylemme(name: [Minoration d'une fonction de $NN$ dans $NN$ strictement croissante])[
    Soit $phi: NN --> NN$ strictement croissante.

    Alors $forall n in NN, phi(n) >= n$.
]

#yproof[
    Par récurrence sur $n$.

    - $phi(0) in NN$ donc $phi(0) >= 0$.

    - Soit $n in NN$. On suppose $phi(n) >= n$.
        $ phi(n+1) > phi(n) >= n $

        Donc $phi(n+1)$ est un entier strictement
        supérieur à $n$ donc $phi(n+1) >= n + 1$.
    $square$
]

#yprop(name: [Limite d'une suite extraite d'une suite convergente])[
    Soit $u$ une suite de limite complexe $l$ et
    $phi: NN --> NN$ strictement croissante.

    Alors $u_phi(n) -->_oo l$.
]

#yproof[
    Soit $epsilon > 0$.

    Soit $n_0 in NN$ tel que
    $ forall n >= n_0, abs(u_n - l) <= epsilon $

    Soit $n >= n_0$.
    Alors $phi(n) >= n >= n_0$.

    Comme $phi(n) >= n_0$,
    $ abs(u_phi(n) - l) <= epsilon $

    Donc $u_phi(n) -->_oo l$.
    $square$
]

#yprop(name: [Limite d'une suite extraite d'une suite qui tend vers un infini])[
    Soient $u$ une suite réelle, et
    $phi: NN --> NN$ strictement croissante.

    - Si $u_n -->_oo +oo$, alors $u_phi(n) -->_oo +oo$

    - Si $u_n -->_oo -oo$, alors $u_phi(n) -->_oo -oo$
]

#yproof[
    Soit $M in RR$.

    On considère $n_0 in NN$ tel que
    $ forall n >= n_0, u_n >= M $

    Soit $n >= n_0$. Alors $phi(n) >= n >= n_0$
    et donc $u_phi(n) >= M$.

    On a bien $u_phi(n) -->_oo +oo$.
    $square$
]

#yexample[
    Soit $q in CC without {1}$ tel que $abs(q) = 1$.

    Montrer que $(q^n)$ n'a pas de limite.

    On suppose que $(q^n)$ converge ver un complexe $l$.

    $q^(n+1)$ est une sous-suite de $(q^n)$ donc
    $q^(n+1) -->_oo l$.

    Or, pour tout $n in NN$,
    $ q^(n+1) = q q^n -->_oo q l $

    Par unicité de la limite, $q l = l$ donc
    $l = 0$ ou $q = 1$.

    De plus,
    $ abs(l) = lim_oo abs(q^n) = lim_oo abs(q)^n = 1 $
    donc $l != 0$.
]

#yprop(name: [Limite d'une suite dont les suites extraites paire et impaire convergent])[
    Soit $u in CC^NN$.

    On suppose que $(u_(2 n))$ et $(u_(2n + 1))$
    convergent vers la même limite $l in CC$.

    Alors $u_n -->_oo l$.
]

#yproof[
    Soit $epsilon > 0$.

    On cherche $n_0$ et $n_1$ deux entiers tels que.
    $ cases(forall n >= n_0\, abs(u_(2n) - l) <= epsilon,
        forall n >= n_1\, abs(u_(2n + 1) - l) <= epsilon) $
    
    On pose $u_2 = 2 max(n_0, n_1) + 1 in NN$.

    Soit $n >= n_2$.

    - Si $n$ est pair, on écrit $n = 2k$ avec $k in NN$.
        $ n >= n_2 > 2 n_0 $
        donc $k >= n_0$

        donc $abs(u_n - l) = abs(u_(2k) - l) <= epsilon$.
    
    - Si $n$ est impair, on écrit $n = 2k + 1$ avec $k in NN$.
        $ n >= n_2 >= 2 n_1 + 1 $
        donc $k >= n_1$

        donc $abs(u_n - l) = abs(u_(2k + 1) - l) <= epsilon$.

    Donc $u_n -->_oo l$.
    $square$
]

#yprop(name: [Limite d'une suite dont les suites extraites paire et impaire ont une limite réelle])[
    Soit $u in RR^NN$.

    On suppose que $(u_(2n))$ et $(u_(2n + 1))$ ont
    la même limite $l in overline(RR)$.

    Alors $u_n -->_oo l$.
    $square$
]

#ytheo(name: [Bolzano-Weierstrass])[
    De toute suite bornée, on peut extraire une
    sous suite convergente.
]

#ydate(2023, 12, 14)

#yproof[
    *Première preuve* : par dichotomie pour les suites réelles.

    Soit $u$ une suite réelle bornée : soit $(a_0, b_0) in RR^2$ tel que
    $ forall n in NN, a_0 <= u_n <= b_0 $

    On pose $c_0 = (a_0 + b_0)/2$ et
    $ cases(A_0 = { n in NN | a_0 <= u_n <= c_0 },
        B_0 = { n in NN | c_0 <= u_n <= b_0 }) $
    $A_0 union B_0 = NN$ donc $A_0$ et $B_0$
    ne sont pas finis simultanément.

    - Si $A_0$ est infinie, on pose
        $ cases(a_1 = a_0, b_1 = c_0, c_1 = 1/2 (a_1 + b_1)) $
        $ A_1 = { n in NN | a_1 <= u_n <= c_1 } $
        $ B_1 = { n in NN | c_1 <= u_n <= b_1 } $
    
    - Si $A_0$ est finie, on pose (jsp pourquoi $A_0 = B_0$)
        $ cases(a_1 = c_0, b_1 = b_0, c_1 = 1/2 (a_1 + b_1)) $
        $ A_1 = { n in NN | a_1 <= u_n <= c_1 } $
        $ B_1 = { n in NN | c_1 <= u_n <= b_1 } $
    
    On itère cette construction pour construire une
    suite $(A_n)_(n in NN)$ de parties infinies de $NN$
    décroissante
    $ forall n in NN, A_n supset A_(n+1) $

    Pour tout $n in NN$, on choisit un élément 
    strictement plus grand que $phi(n - 1)$ de $A_n$
    (possible car $A_n != emptyset$) que l'on note
    $phi(n)$.

    Donc, pour tout $n in NN$,
    $ a_(n+1) <= u_phi(n) <= b_(n+1) $

    Or $(a_n)$ et $(b_n)$ sont adjacentes ($(a_n)$ est
    croissante car $A_(n+1) subset A_n$, de même $(b_n)$
    est décroissante, et $(b_n - a_n)$ est une suite géométrique
    de raison $1/2$)

    Donc $(u_phi(n))$ converge.
    $square$

    *Deuxième preuve* : "Vue sur la mer"

    // dessin avec des points et leurs ligne qui dessendent
    // c'est des imeubles, et on prends seulement ceux qui
    // ont vue sur la mer, qui est à l'infini

    On pose
    $ A = { n in NN | forall p > n, u_n > u_p } $

    - Cas 1 : On suppose $A$ infini.

        On numérote les éléments de $A$ dont l'ordre est croissant.
        $ A = { phi(0), phi(1), phi(2), ... } $
        avec $phi$ strictement croissante.

        Soit $n in NN$. $phi(n) in A$ donc
        $ forall p > phi(n), u_phi(n) > u_p $
        $phi(n+1) > phi(n)$ donc
        $ u_phi(n) > u_phi(n+1) $

        La suite $(u_phi(n))$ est décroissante.

        On sait que $u$ est bornée donc minorée.
        Donc $(u_phi(n))$ est minorée.

        D'après le théorème de la limite monotone,
        $(u_phi(n))$ converge.

    - Cas 2 : On supppose $A$ finie.
        $A$ possède un plus grand élément $n_0$. Donc
        $ forall n > n_0, n in.not A $
        i.e.
        $ forall n > n_0, exists p > n, u_n <= u_p $

        On pose $phi(0) = n_0 + 1 > phi(0)$.
        $ {p in NN | p > phi(0) and u_phi(0) <= u_p }
            yt != emptyset $
        
        On note $phi(1)$ le plus petit élément de cette partie :
        $ cases(phi(1) > phi(0), u_phi(0) <= u_phi(1)) $

        On itère ce procédé : soit $n in NN$, on suppose
        avoir construit
        $ phi(0) < phi(1) < ... < phi(n) $

        $phi(n) in.not A$ donc 
        $ {p in NN | p > phi(n) and u_phi(n) <= u_p }
            yt != emptyset $

        On pose $phi(n+1)$ son plus petit élément,
        et on a alors
        $ cases(phi(n+1) > phi(n), u_phi(n) <= u_phi(n+1)) $

        On construit ainsi une sous-suite croissante de $u$,
        majorée (car $u$ est bornée) donc cette sous-suite
        convergente.
    $square$

    *Troisième preuve* : pour les suites complexes

    On suppose $u$ bornée, on considère $M in RR$ tel que
    $ forall n in NN, abs(u_n) <= M $

    Donc
    $ forall n in NN, abs("Re"(u_n)) <= abs(u_n) <= M $
    i.e.
    $ forall n in NN, -M <= "Re"(u_n) <= M $

    De même,
    $ forall n in NN, -M <= "Im"(u_n) <= M $
    $("Re"(u_n))$ est une suite réelle bornée.

    Soit $phi: NN --> NN$ strictement croissante
    telle que $("Re"(u_phi(n)))$ converge.

    $("Im"(u_phi(n)))$ est bornée et réelle.

    Soit $psi: NN --> NN$ strictement croissante telle que
    $"Im"(u_(phi compose psi(n)))$ converge.

    $("Re"(u_(phi compose psi(n))))$ est une sous-suite de
    $("Re"(u_phi(n)))$ donc elle converge.

    Donc $(u_(phi compose psi(n)))$ converge $phi compose psi$
    étant strictement croissante, $(u_(phi compose psi(n)))$
    est une sous-suite de $(u_n)$.
    $square$
]

#yexercise[
    Soit $(D_n)$ une suite de disques (dans le plan complexe)
    telle que
    $ forall n in NN, D_(n+1) subset D_n $

    //#cetz.canvas({
    //    import cetz: *
    //
    //    cetz.circle((0, 0))
    //})

    Montrer que $sect.big_(n in NN) D_n != emptyset$

    On note, pour $n in NN$, $z_n$ le centre de $D_n$.
    $ forall n in NN, z_n in D_n subset D_0 $

    Donc $(z_n)$ est bornée.

    Soit $phi: NN --> NN$ strictement croissante, telle que
    $(z_phi(n))$ converge.

    On note $l = lim_oo z_phi(n)$.

    Soit $n in NN$.
    $ forall k >= n, z_k in D_k subset D_n $
    donc
    $ forall k >= n, z_phi(k) in D_n $
    (care $phi(k) >= k$).

    On note $r_n$ le rayon de $D_n$.
    $ forall k >= n, abs(z_phi(k) - z_n) <= r_n $

    On fait tendre $k$ vers $+oo$ pour obtenir
    $ l - z_n <= r_n $
    i.e. $l in D_n$.

    Donc $l in sect.big_(n in NN) D_n$.
]