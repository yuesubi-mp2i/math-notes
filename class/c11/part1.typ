#import "../../template/ytemplate.typ": *

(Dans ce chapitre, les numérots de paragraphes sont tous décalés de 1)

#ypart[Modes de définition]


#ydef(name: [Suite d'éléments])[
    Soit $E$ un ensemble.

    Une *suite d'éléments* de $E$ est une
    foncion de $NN$ dans $E$.
]

#ynot(name: [Suite, élément d'une suite, suite définie à partir d'un certain rang])[
    Soit $u in E^NN$, avec $E$ un ensemble.

    Au lieu d'écrire $u(n)$, pour $n in NN$,
    on écrit $u_n$.

    On écrit aussi $(u_n)_(n in NN)$ à la place de $u$,
    ou plus simplement, $(u_n)$.

    Si une suite $u$ n'est définie que sur
    $NN sect [N, +oo[$, on peut écrire $(u_n)_(n >= N)$.
]

#yblock(kind: [Hypothèse])[
    Dans ce chapitre, toutes les suites sont définies
    sur $NN sect [N, +oo[$, avec $N in NN$.
]

#ybtw[
    On peut définir une suite $u$

    - en donnant une formule qui calcule $u_n$
        en fonction de $n$ (*définition explicite*)

        e.g. $forall n in NN^*, u_n = (-1)^n/n$
    
    - en donnant une relation entre
        $u_(n+p+1)$ et $u_n, u_(n+1), ..., u_(n+p)$
        (*définition récursive*)

        e.g.
        $ cases(forall n in NN\, u_(n+3) = u_n + u_(n+2),
            u_0 = 0, u_1 = 1, u_2 = 1) $
    
    - en donnant des propriétés satisfaires par
        $u_n$ et seulement par $u_n$
        
        e.g.
        pout tout $n in NN$, $u_n$ est la seule solution réelle
        de $x^5 + n x - 1 = 0$.
]

#ybtw[
    Une *suite numérique* est une suite à valeurs dans
    un ensemble $E$ (inclus dans $CC$).
]