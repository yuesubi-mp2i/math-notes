#import "../../template/ytemplate.typ": *


#ypart[Limite et inégalité]


Dans ce paragraphe, toutes les suites considérées
sont réelles.

#yprop(name: [Croissances comparées])[
    Soient $u$ et $v$ deux suites.

    Soit $N in NN$. On suppose
    $ forall n >= N, u_n <= v_n $

    + Si $u_n -->_(n->+oo) +oo$ alors $v_n -->_(+oo) +oo$
    + Si $v_n -->_(n->+oo) -oo$ alors $u_n -->_(n->+oo) -oo$
]

#yproof[
    + On suppose que $u_n --> +oo$.

        Soit $M in RR$. Soit $n_0 in NN$ tel que
        $ forall n >= n_0, u_n >= M $

        Soit $n_1 = max(n_0, N)$.

        Alors
        $ forall n >= n_1, v_n >= u_n >= M $

    $square$
]

#ytheo(name: [Théorème d'existance d'un limite par encadrement (dit des gendarmes)])[
    Soient $u$, $v$ et $w$ 3 suites et $N in NN$.

    On suppose que
    $ forall n >= N, u_n <= v_n <= w_n $
    et $u$ et $w$ convergent vers la même limite $l$.

    Alors $v$ a un limite et celle-ci vaut $l$.
]

#yproof[
    Soit $epsilon > 0$.

    Soit $n_0 in NN$ tel que
    $ forall n >= n_0, abs(u_n - l) <= epsilon $
    et $n_1 in NN$ tel que
    $ forall n >= n_1, abs(w_n - l) <= epsilon $

    On pose $n_2 = max(n_0, n_1)$.

    Alors, pour tout $n > n_2$,
    $ l - epsilon <= u_n <= v_n <= w_n <= l + epsilon $
    donc
    $ forall n > n_2, abs(v_n - l) <= epsilon $
    $square$
]

#yprop(name: [Limite réelle de deux suites dont on connais la position relative])[
    Soient $u$ et $v$ deux suites de limites
    $l_1 in RR$ et $l_2 in RR$. Soit $n in NN$.

    On suppose
    $ forall n >= N, u_n <= v_n $

    Alors
    $ l_1 <= l_2 $
]

#yproof[
    On suppose $l_2 < l_1$.

    On pose $epsilon = (l_1 - l_2)/3 > 0$.

    Soit $n_0 in NN$ tel que
    $ forall n >= n_0, abs(u_n - l_1) <= epsilon $
    et $n_1 in NN$ tel que
    $ forall n >= n_1, abs(v_n - l_2) <= epsilon $

    On pose $n = max(N, n_0, n_1)$.

    $ (2 l_1 + l_2)/3 = l_1 - epsilon
        yt <= u_n
        yt <= v_n
        yt <= l_2 + epsilon = (2l_2 + l_1)/3 $

    donc $2 l_1 + l_2 <= 2 l_2 + l_1$
    donc $l_1 <= l_2$ : une contradiction.
    $square$
]

#ytheo(name: [Limite monotone, pour les suites croissantes])[
    Soit $u$ une suite croissante.

    + Si $u$ est majorée, alors $u$ converge vers
        $sup_(n in NN) (u_n)$
    + Si $u$ n'est pas majorée, alors $u -->_(n->+oo) +oo$

    + Dans tous les cas, $u$ a une limite dans $R union {+oo}$
]

#yproof[
    + On suppose $u$ majorée.

        ${u_n | n in NN}$ est une partie non vide majorée de $RR$.

        Soit $l = sup{u_n | n in NN} = sup_(n in NN) (u_n)$.

        Soit $epsilon > 0$.

        $l$ est le plus petit majorant de ${u_n | n in NN}$
        $ forall n in NN, u_n <= l <= l + epsilon $

        $l - epsilon < l$ donc $l - epsilon$ ne majore pas ${ u_n | n in NN }$.

        Soit $N in NN$ tel que $u_N > l - epsilon$.

        $u$ est croissante donc
        $ forall n >= N, u_n >= u_N > l - epsilon$

        Donc
        $ forall n > N, l - epsilon <= u_n <= l + epsilon $
        i.e.
        $ forall n >= N, abs(u_n - l) <= epsilon $
    
    + Soit $M in RR$.

        $M$ ne majore pas ${u_n | n in NN}$ (puisque cet
        ensemble n'a pas de majorant)

        Soit $N in NN$ tel que $u_n > M$.

        $u$ est croissante donc
        $ forall n >= N, u_n >= u_N > M $

        Donc $u_n -->_(n->+oo) +oo$.
    
    $square$
]

#ytheo(name: [Limite monotone, pour les suites décroissantes])[
    Soit $u$ une suite décroissante.

    + Si $u$ est minorée, alors $u$ converge vers
        $inf_(n in NN) (u_n)$
    + Si $u$ n'est pas minorée, alors $u -->_(n->+oo) -oo$

    + Dans tous les cas, $u$ a une limite dans $R union {-oo}$
]

#yproof[
    ...
]

#ydate(2023, 12, 14)

#ydef(name: [Suites adjacentes])[
    Soient $u$ et $v$ deux suites réelles.

    On dit que $u$ et $v$ sont *adjacentes* si
    - l'une des deux suites est croissante à partir d'un certain rang
    - l'autre est décroissante à partir d'un certain rang
    - $lim_(n->+oo) (u_n - v_n) = 0$
]

#yprop(name: [Position relative de deux suites adjacentes])[
    Soient $u$ et $v$ deux suites adjacentes
    avec $u$ croissante et $v$ décroissante à
    partir d'un rang $N$.

    Alors
    $ forall n >= N, u_n <= v_n $
]

#yproof[
    $(u_n)_(n >= N)$ est croissante et $(-v_n)_(n>=N)$ aussi.

    Donc $(u_n - v_n)_(n >= N)$ est croissante.

    Cette suite a pour limite $0$.
    D'après le théorème de la limite monotone.
    $ 0 = sup_(n >= N) (u_n - v_n) $
    et donc
    $ forall n >= N, u_n - v_n <= 0 $
    $square$
]

#yprop(name: [Limites de deux suites adjacentes])[
    Deux suites adjacentes *convergent* vers la même limite.
    // dessin
]

#yproof[
    Soit $N in NN$.
    Soit $(u_n)_(n>=N)$ une suite croissante,
    $(v_n)_(n>=N)$ une suite décroissante.

    On suppose $lim_(+oo) (u_n - v_n) = 0$.

    On remarque que
    $ forall n >= N, u_n <= v_n <= v_N $
    Donc $(u_n)_(n>=N)$ est croissante et
    majorée $v_N$ donc elle converge.

    De même
    $ forall n >= N, v_n >= u_n >= u_N $
    D'où $(v_n)_(n >= N)$ est décroissante
    et minorée par $u_N$ donc elle converge.

    Enfin
    $ 0 = lim_(+oo) (u_n - v_n) = lim_(+oo) u_n - lim_(+oo) v_n $
    et donc $lim_(+oo) u_n = lim_(+oo) v_n$.
    $square$
]

#yprop(name: [Limite des l'approximations décimales])[
    Soit $x in RR$. On pose, pour $n in NN$,
    $ cases(u_n = floor(10^n x)/10^n,
        v_n = u_n + 1/10^n) $

    Alors $u_n -->_(+oo) x$ et $v_n -->_(+oo) x$.
]

#yproof[
    Soit $n in NN$.
    $ 10 floor(10^n x) <= 10 times 10^n x = 10^(n+1) x $
    donc
    $ 10 floor(10^n x) <= floor(10^(n+1) x) $
    d'où
    $ (10 floor(10^n x))/10^(n+1) <= floor(10^(n+1) x)/10^(n+1) $
    et donc $u_n <= u_(n+1)$.

    Donc $u$ est croissante.

    Soit $n in NN$.

    On sait que
    $ floor(10^(n+1) x) <= 10^(n+1) x < floor(10^(n+1)) + 1 $
    $ 10 floor(10^n x) <= 10^(n+1) x < 10 floor(10^n) + 10 $

    donc
    $ 10 floor(10^n x) + 10 >= floor(10^(n+1) x) + 1 $
    et donc
    $ u_n + 1/10^n >= u_(n+1) + 1/10^(n+1) $
    i.e. $v_n >= v_(n+1)$.

    $ lim_(+oo) (v_n - u_n) = lim_(+oo) 1/10^n = 0 $

    Donc $u$ et $v$ sont adjacentes : elles convergent
    vers une même limite $l in RR$.

    Or on a vu que
    $ forall n in NN, u_n <= x < v_n $

    En passant à la limite
    $ l <= x <= l $
    Donc $l = x$.
    $square$
]

#yexercise[
    Soit $u$ la suite définie par
    $ forall n in NN^*, u_n sum_(k=1)^n 1/k - ln(n) $

    Montrer que $u$ converge.

    *Astuce* : on pose
    $ forall n in NN^*, v_n = sum_(k+1)^n 1/k - ln(n+1) $
    et on montre que $u$ et $v$ sont adjacentes.
]