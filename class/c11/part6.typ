#import "../../template/ytemplate.typ": *


#ypart[Suites récurrentes]


$KK$ désigne $RR$ ou $CC$, des éléments de cet ensemble
sont appelés scalaires.

#yprop(name: [Expression explicite d'une suite arithmético-géométrique])[
    Soit $(a,b) in KK^2$ et $(u_n)$ une suite vérifiant
    $ forall n in NN, u_(n+1) = a u_n + b $

    Alors, si $a != 1$,
    $ forall n in NN, u_n = v_n + lambda $
    où $(v_n)$ est une suite géométrique de raison $a$,
    et $lambda$ est la solution de $x = a x + b$.
]

#yproof[
    Comme $a != 1$, l'équation
    $ x = a x + b $
    a une unique solution
    $ lambda = (-b)/(a - 1) $

    On pose
    $ forall n in NN, v_n = u_n - lambda $

    Soit $n in NN$.
    $ v_(n+1) - a v_n
        &= u_(n+1) - lambda - a (u_n - lambda) \
        &= u_(n+1) - a u_n + a lambda - lambda \
        &= b - b = 0 $
    donc $(v_n)$ est une suite géométrique de raison $a$.
    $square$
]

#yprop(name: [Expression explicite d'une suite récurente d'ordre deux])[
    Soit $(a,b) in CC^2$ et $u$ une suite vérifiant
    $ forall n in NN, u_(n+2) = a u_(n+1) + b u_n $

    Soit $C$ l'équation caractéristique associée :
    $ z^2 = a z + b $
    d'inconnue $z in CC$.

    + Si $C$ a deux racines simples $r_1 != r_2$,
        alors
        $ exists (A, B) in CC^2, forall n in NN,
            yt u_n = A r_1^n + B r_2^n $

    + Si $C$ a une racine double $r$,
        $ exists (A, B) in CC^2, forall n in NN,
            yt u_n = (A + B n) r^n $
]

#yproof[
    - Cas 1 :

        Analyse :
        On suppose que $A$ et $B$ existent.
        Soit $(A, B) in CC^2$ tel que
        $ forall n in NN, u_n = A r_1^n + B r_2^n $

        En particulier
        $ cases(A + B = u_0, A r_1 + B r_2 = u_1) $
        $ <=> cases(B = u_0 - A, A r_1 + (u_0 - A) r_2 = u_1) $
        $ <=> cases(B = u_0 - A, A (r_1 - r_2) = u_1 - u_0 r_2) $
        $ <=> cases(B = u_0 - A, A = (u_1 - u_0 r_2)/(r_1 - r_2)) $

        Synthèse :
        On pose $A = (u_1 - u_0 r_2)/(r_1 - r_2)$ et $B = u_0 - A$.

        Pour $n in NN$, $P(n)$: "$u_n = A r_1^n + B r_2^n$".

        $P(0)$ et $P(1)$ sont vraies par définition de $A$ et $B$.
        
        Soit $n in NN$. On suppose $P(n)$ et $P(n+1)$ vraies.
        $ u_(n+2)
            yt = a u_(n+1) + b u_n
            yt = a (A r_1^(n+1) + B r_2^(n+1))
                ytt + b (A r_1^n + B r_2^n)
            yt = A r_1^n underbrace((a r_1 + b), r_1^2)
                ytt + B r_2^n underbrace((a r_2 + b), r_2^2)
            yt = A r_1^(n+2) + B r_2^(n+2) $

        Donc $P(n + 2)$ est vraie.
    
    Cas 2 : en exercice

    $square$
]

#yprop(name: [Expression explicite d'une suite réelle récurente d'ordre deux])[
    Soit $(a,b) in RR^2$, $(u_n)$ une suite rélle vérifiant
    $ forall n in NN, u_(n+2) = a u_(n + 1) + b u_n $

    Soit $C$ l'équation carctéristique
    $ z^2 = a z + b $

    + Si $C$ a 2 racines simples réelles $r_1 != r_2$ alors
        $ exists (A, B) in RR^2, forall n in NN,
            yt u_n = A r_1^n + B r_2^n $
    
    + Si $C$ a une racine double $r in RR$ alors
        $ exists (A,B) in RR^2, forall n in NN,
            yt u_n = (A + B n) r^n $
    
    + Si $C$ a 2 racines non réelles conjugées
        $r e^(plus.minus i theta)$ avec $r in RR^*_+$
        et $theta in RR$, alors
        $ exists (A, B) in RR^2, forall n in NN,
            yt u_n = ytt r^n (A cos(n theta)
                ytt + B sin(n theta))
            yt u_n = r^n lambda cos(n theta + phi) $
    $square$
]

#yexample[
    Soit $u in RR^NN$ vérifiant
    $ forall n in NN, u_(n+1) = 1/2 (u_n + alpha/u_n) $
    où $alpha in RR^*_+$.

    Vérification de si la suite est bien définie :

    On suppose $u_0 != 0$.

    Soit $f: x |-> 1/2 (x + alpha/x)$.
    $f$ est dérivalbe sur $RR^*_+$.
    $ forall x != 0, f'(x) &= 1/2 (1 - alpha/x^2)\
        &= (x^2 - alpha)/(2 x^2) $

    La dérivée est positive sur $]-oo, -sqrt(alpha)]$
    et sur $[-sqrt(alpha), +oo[$, négative sur
    $[-sqrt(alpha), sqrt(alpha)] without {0}$.

    On en déduit les variations de $f$.
    $f$ est minorée par $sqrt(alpha)$, donc $f != 0$.

    Soit $n in NN$. On suppose $u_n != 0$.
    D'après le tableau de variations,
    $ u_(n+1) = f(u_n) != 0 $

    Par récurence, $(u_n)$ est bien définie.

    #underline([_*Si*_]) $(u_n)$ converge vers $l$.
    *Alors* $f(l)= l$

    Pour $x != 0$,
    $ f(x) - x &= 1/2 (x + alpha/x) - x \
        &= -1/2 x + alpha/(2 x) \
        &= (alpha - x^2)/(2 x) $
    
    $f$ est positive sur $RR without [-sqrt(alpha), sqrt(alpha)]$,
    négative sur $]-sqrt(alpha), sqrt(alpha)[$.

    On suppose $u_0 > 0$.
    - $u_1 = f(u_0) >= sqrt(alpha)$ d'après le tableau de variations
    - Soit $n in NN^*$. On suppose $u_n >= sqrt(alpha)$.
        Alors $u_(n+1) = f(u_n) >= sqrt(alpha)$ d'après
        le tableau de variations.
    - Par récurrence,
        $ forall n >= 1, u_n >= sqrt(alpha) $
    
    Soit $n >= 1$, $u_n >= sqrt(alpha)$
    donc $f(u_n) - u_n = 0$
    donc $u_(n+1) <= u_n$.

    La suite $(u_n)_(n >= 1)$ est décroissante et
    minorée par $sqrt(alpha)$, donc elle converge.
    On note $l = lim_oo u_n$.
    $ forall n in NN, u_(n+1) = f(u_n) $
    $(u_(n+1))$ est une sous-suite de $(u_n)$ donc
    $u_(n+1) -->_oo l$.

    $f$ est continue donc $f(u_n) -->_oo f(l)$.

    Par unicité de la limite $f(l) = l$.
    Donc $l in {sqrt(alpha), -sqrt(alpha)}$.

    Or
    $ forall n in NN^*, u_n >= sqrt(alpha) $
    En passant à la limite,
    $ l >= sqrt(alpha) $

    Donc $u_n -->_oo sqrt(alpha)$ si $u_0 > 0$.
    On procède de même pour $u_0 < 0$.
]

#ybtw[
    La méthode de l'exemple précédent, est un cas
    particulier de la méthode de Newton.
    // dessin
]

#yexample(name: [Suite logistique])[
    Soit $u_0 in ]0, 1[$.
    $ forall n in NN, u_(n+1) = lambda u_n (1 - u_n) $
    où $lambda in [0, 4]$.

    Dans le cas ou $lambda = 1$ :

    Un seul point fixe.

    #align(center)[#cetz.canvas(length: 7em, {
        import cetz: *

        plot.plot(
            x-tick-step: 0.5,
            y-tick-step: 0.25,
            axis-style: "school-book",
            {
                plot.add(domain: (0, 1), x => x * (1 - x))
                plot.add(domain: (0, 0.3), x => x)
            }
        )
    })]

    // dans le cas ou lambda = 2
    // parabole de 0 à 1, tangente à la courbe en haut de la parabole

    Dans le cas ou $lambda = 2$ :

    Il y a 2 points fixes, 0 est répulsif et 1/2 attire.

    #align(center)[#cetz.canvas(length: 7em, {
        import cetz: *

        plot.plot(
            x-tick-step: 0.5,
            y-tick-step: 0.5,
            axis-style: "school-book",
            {
                plot.add(domain: (0, 1), x => 2 * x * (1 - x))
                plot.add(domain: (0, 0.6), x => x)
            }
        )
    })]

    Dans le cas ou $lambda = 3$ :

    La suite oscille autour d'un point fixe.

    #align(center)[#cetz.canvas(length: 7em, {
        import cetz: *

        plot.plot(
            x-tick-step: 0.5,
            y-tick-step: 0.5,
            axis-style: "school-book",
            {
                plot.add(domain: (0, 1), x => 3 * x * (1 - x))
                plot.add(domain: (0, 0.9), x => x)
            }
        )
    })]

    Dans le cas ou $lambda = 4$ :

    La situation est chaotique, i.e.
    ${u_n | n in NN}$ est dense dans $[0, 1]$.

    La suite dépend fortement de $u_0$.

    #align(center)[#cetz.canvas(length: 7em, {
        import cetz: *

        plot.plot(
            x-tick-step: 0.5,
            y-tick-step: 1.0,
            axis-style: "school-book",
            {
                plot.add(domain: (0, 1), x => 4 * x * (1 - x))
                plot.add(domain: (0, 1.2), x => x)
            }
        )
    })]
]

#yprop(name: [Inégalité des accroissements finis])[
    Soit $f$ continue sur $[a, b]$ et
    dérivable sur $]a, b[$ et à valeurs réelles.

    On suppose $f'$ bornée.
    Alors
    $ cases(
        M = sup_(a < x < b) (abs(f'(x))),
        abs(f(a) - f(b)) <= M abs(a - b)) $
    $square$
]

#ytheo(name: [Point fixe])[
    Soit $f$ continue et $u$ vérifiant,
    $ forall n in NN, u_(n+1) = f(u_n) $

    #underline[_*Si*_] $(u_n)$ converge vers $l$,
    alors $f(l)= l$.
    $square$
]