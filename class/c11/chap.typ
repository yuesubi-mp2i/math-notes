// Suites numériques
#import "../../template/ytemplate.typ": *

#ydate(2023, 12, 12)

#ychap[Suites numériques]

#include "part1.typ"
#include "part2.typ"
#include "part3.typ"
#include "part4.typ"
#include "part5.typ"
#include "part6.typ"
#include "part7.typ"
#include "part8.typ"