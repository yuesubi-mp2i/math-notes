#import "../../template/ytemplate.typ": *


#ypart[Comparaisons de suite]


#ydef(name: [Suite dominée avec les grands $O$])[
    Soient $u$ et $v$ deux suites complexes.

    On dit que $v$ *domine* $u$ et on note
    $u = O(v)$ ($u_n = O(v_n)$) si
    $ exists M in RR, exists N in NN, forall n >= N,
        yt |u_n| <= M abs(v_n) $
]

#ydef(name: [Suite négligable avec les petits $o$])[
    Soient $u$ et $v$ deux suites complexes.

    On dit que $u$ est *négligable devant $v$*,
    et on note $u = o(v)$ (ou $u_n = o(v_n)$) si
    $ forall epsilon > 0, exists N in NN, forall n >= N,
        yt abs(u_n) <= epsilon abs(v_n) $
]

#yexample[
    $(u_n) in (RR_+)^NN$, $u_n = O(n)$ et $n = O(u_n)$.

    $ exists (M_1, M_2) in RR^2, exists N in NN, forall n >= N,
        yt M_2 n <= u_n <= M_1 n $
]

#yprop(name: [Propriétés des relations de domination et de négligabilité])[
    La relation de domination est réflexive et transitive.

    La relation de négligabilité est transitive.
    $square$
]

#ydef(name: [Équivalent d'une suites])[
    On dit que $u$ et $v$ sont *équivalentes* si
    $ forall epsilon > 0, exists N in NN, forall n >= N,
        yt abs(u_n - v_n) <= epsilon abs(v_n) $
    On note $u tilde v$.
]

#ydate(2023, 12, 19)

#ylemme(name: [Écriture d'un équivalent avec une suite qui tend vers 1])[
    $ u_n tilde v_n <=>
        yt exists (alpha_n),
            cases(exists N in NN \, forall n > N\,
                yt u_n = alpha_n v_n,
                alpha_n -->_oo 1) $
]

#yproof[
    "$==>$" On suppose $u_n tilde v_n$.

    Pour $n in NN$, on pose
    $ alpha_n = cases(u_n/v_n "si" v_n != 0, 1 "si" v_n = 0) $

    Soit $N in NN$ tel que
    $ forall n >= N, abs(u_n - v_n) <= abs(v_n) $

    Soit $n >= N$.

    Si $v_n != 0$, $alpha_n v_n = u_n/v_n v_n = u_n$.

    Si $v_n = 0$, $abs(u_n - v_n) = 0$, et donc
    $u_n = v_n = alpha_n v_n$

    Soit $epsilon > 0$.

    Soit $n in NN$.

    Si $v_n != 0$, alors
    $ abs(alpha_n - 1) = abs(u_n/v_n - 1) = abs(u_n - v_n)/abs(v_n) $

    Soit $n_0 in NN$ tel que
    $ forall n >= n_0, abs(u_n - v_n) <= epsilon abs(v_n) $

    Soit $n >= n_0$.

    Si $v_n != 0$, alors
    $abs(alpha_n - 1) <= epsilon abs(v_n)/abs(v_n) = epsilon$

    Si $v_n = 0$, alors $abs(alpha_n - 1) = 0 <= epsilon$

    Donc $alpha_n -->_oo 1$

    "$<==$" Soit $(alpha_n) in CC^NN$ et $N in NN$ tels que
    $ cases(forall n >= N\, u_n = alpha v_n,
        alpha_n -->_oo 1) $
    
    Soit $epsilon > 0$. Soit $n_0 in NN$ tel que
    $ forall n >= n_0, abs(alpha_n - 1) <= epsilon $

    Soit $n >= max(n_0, N)$.

    $ abs(u_n - v_n) &= abs(alpha_n v_n - v_n) \
        &= abs(alpha_n - 1) abs(v_n) \
        &<= epsilon abs(v_n) $
    
    Donc $u_n tilde v_n$.
    $square$
]

#yprop(name: [Les équivalents de suites forment une relation d'équivalence])[
    $tilde$ est une relation d'équivalence.
]

#yproof[
    *Réflexivité* :
    Soit $u$ une suite
    $ forall epsilon > 0, forall n in NN,
        yt abs(u_n - u_n) = 0 <= epsilon abs(u_n) $
    Donc $u tilde u$.

    *Symétrie* :
    Soit $(alpha_n)$ et $N in NN$ tels que
    $ cases(forall n >= N\, u_n = alpha_n v_n,
        alpha_n -->_oo 1 ) $
    
    Soit $n_0 in NN$ tel que
    $ forall n >= n_0, abs(alpha_n - 1) <= 1/2 $

    D'où, pour $n >= n_0$,
    $ abs(alpha_n)
        &= abs(alpha_n - 1 + 1) \
        &>= abs(abs(alpha_n - 1) - 1) \
        &>= 1 - abs(alpha_n - 1) >= 1/2 $
    et donc $alpha_n != 0$.

    D'où,
    $ forall n >= max(n_0, NN), v_n = 1/alpha_n u_n $

    De plus, $1/alpha_n -->_oo 1/1 = 1$.

    Donc $v_n tilde u_n$.

    *Transitivité* : Soient $u$, $v$ et $w$ trois suites telles que
    $ cases(u_n tilde v_n, v_n tilde w_n) $

    Soient $(alpha_n)$ et $(beta_n)$ deux suite et
    $(N, N') in NN^2$ tel que
    $ cases(forall n >= N\, v_n = alpha_n u_n and alpha_n -->_oo 1,
        forall n >= N'\, w_n = beta_n u_n and beta_n -->_oo 1) $
    
    On pose $N'' max(N, N')$.
    $ forall n >= N'', w_n = beta_n v_n = (beta_n alpha_n) u_n $
    et $beta_n alpha_n -->_oo 1 times 1 = 1$.

    Donc $w_n tilde u_n$.
    $qed$
]

#yprop(name: [Écriture d'un petit $o$ avec une suite qui tend vers 0])[
    Soient $u$ et $v$ deux suites.
    $ u_n = o(v_n) <=>
        yt exists (epsilon_n), exists N in NN,
        ytt cases(forall n >= N\, u_n = epsilon v_n,
            epsilon_n -->_oo 0) $
]

#yproof[
    "$==>$" On suppose que $u_n = o(v_n)$.

    On pose pour $n in NN$,
    $ epsilon_n = cases(u_n/v_n "si" v_n != 0, 0 "si" v_n = 0) $

    Soit $N in NN$ tel que $forall n >= N, abs(u_n) <= abs(v_n) $.

    Soit $n >= N$,
    - Si $v_n != 0$, alors $epsilon_n v_n = u_n/v_n v_n = u_n$
    - Si $v_n = 0$, alors $abs(u_n) <= 0$ et donc $u_n = 0 = epsilon_n v_n$
    
    Soit $epsilon > 0$. Soit $n_0 in NN$ tel que
    $ forall n >= n_0, abs(u_n) <= epsilon abs(v_n) $

    Soit $n >= max(n_0, N)$
    - Si $v_n != 0$, alors
        $abs(epsilon_n) = abs(u_n)/abs(v_n) <= epsilon abs(v_n)/abs(u_n) = epsilon$
    - Si $v_n = 0$, $abs(epsilon_n) = 0 <= epsilon$

    Donc $epsilon_n -->_oo 0$.

    "$<==$" Soient $(epsilon_n)$ une suite qui tend vers 0
    et $N in NN$ tel que
    $ forall n >= N, epsilon_n v_n = u_n $

    Soit $epsilon > 0$. Soit $n_0 in NN$ tel que
    $ forall n >= n_0, abs(epsilon_n) <= epsilon$

    Soit $n >= max(n_0, N)$.
    $ abs(u_n) = abs(epsilon_n v_n) = abs(epsilon_n) abs(v_n) <= epsilon abs(v_n) $

    Donc $u_n = o(v_n)$.
]

#yprop(name: [Écriture d'un $O$ avec une suite bornée])[
    Soient $u$ et $v$ deux suites.
    $ u_n = O(v_n) <=>
        yt exists (gamma_n), exists N in NN,
        ytt cases(forall n >= N\, u_n = gamma_n v_n,
            (gamma_n) "bornée") $
]

#yproof[
    "$==>$" On supppose que $u_n = O(v_n)$.

    On pose, pour $n in NN$,
    $ gamma_n = cases(u_n/v_n &"si" v_n != 0,
        0 &"si" v_n = 0) $
    
    Soit $M in RR^+$ et $N in NN$ tel que
    $ forall n >= N, abs(u_n) <= M abs(v_n) $

    Soit $n >= N$.

    Si $v_n != 0$, alors $gamma_n v_n = u_n/v_n v_n = u_n$

    Si $v_n = 0$, alors $abs(u_n) <= 0$ et donc
    $ u_n = 0 = gamma_n v_n $

    Soit $n >= N$.

    Si $v_n != 0$, $abs(gamma_n) <= M abs(v_n)/abs(v_n) = M$

    Si $v_n = 0$, $abs(gamma_n) = 0 <= M$

    Donc $(gamma_n)$ est bornée.

    "$<==$" Soit $(gamma_n)$ bornée et $N in NN$ tels que
    $ forall n >= N, u_n = gamma_n v_n $

    Soit $M in RR$ tel que
    $ forall n in NN, abs(gamma_n) <= M $

    Soit $n >= NN$.
    $ abs(u_n) = abs(gamma_n) abs(v_n) <= M abs(v_n) $

    Donc $u = O(v)$.
    $qed$
]