#import "../../template/ytemplate.typ": *


#ypart[Suites complexes]


#ydef(name: [Partie réelle/imaginaire d'une suite])[
    Soit $u$ une suite complexe.

    La *partie réelle* de $u$ est la suite $("Re"(u_n))_n$
    et sa partie *imaginaire* est la suite $("Im"_(u_n))_n$.
]

#yprop(name: [Décomposition de la limite d'une suite complexe])[
    Soient $u$ une suite complexe et $l in CC$.
    $ u_n -->_oo l
        <=> cases(
            "Re"(u_n) -->_oo "Re"(l),
            "Im"(u_n) -->_oo "Im"(l)) $
]

#yproof[
    "$<==$" On suppose $"Re"(u_n) --> "Re"(l)$ et $"Im"(u_n) --> "Im"(l)$.

    Pour tout $n in NN$,
    $ u_n &= "Re"(u_n) + i "Im"(u_n) \
        &-->_oo "Re"(l) + i "Im"(l) = l $
    
    "$==>$" Soit $epsilon > 0$. Soit $n_0 in NN$ tel que
    $ forall n >= n_0, abs(u_n - l) <= epsilon $

    Pour tout $n >= n_0$,
    $ abs("Re"(u_n) - "Re"(l)) &= abs("Re"(u_n - l)) \
        &<= abs(u_n - l) \
        &<= epsilon $
    
    donc $"Re"(u_n) -->_oo "Re"(l)$.

    De même pour $"Im"(u_n)$.
    $square$
]

#yprop(name: [Limite du module d'une suite convergente])[
    Soit $u$ une suite complexe de limite $l in CC$.

    Alors $abs(u_n) -->_oo abs(l)$.
]

#yproof[
    Pour tout $n in NN$,
    $ 0 <= abs(abs(u_n) - abs(l)) <= abs(u_n - l) $

    D'après le théorème d'*existance* d'une limite
    par encadrement,
    $ abs(abs(u_n) - abs(l)) -->_oo 0 $
    et donc $abs(u) -->_oo abs(l)$.
    $square$
]

#yprop(name: [Convergence d'une suite géométrique])[
    // Corrolaire de la proposition précédente
    Soit $q in CC$.

    $ (q^n)_(n in NN^*) "converge"
        yt <=> abs(q) < 1 or q = 1$
]

#yproof[
    - Si $q = 1$, alors $forall n, q^n = 1 -->_oo 1$

    - On suppose $abs(q) > 1$
        $ forall n in NN, abs(q^n) = abs(q)^n -->_oo +oo $
        donc $(q_n)$ n'a pas de limite complexe.

    - On suppose $abs(q) < 1$
        $ forall n in NN, abs(q^n) = abs(q)^n -->_oo 0 $
        et donc $q^n -->_oo 0$.

    - On suppose $abs(q) = 1$ et $q != 1$.

        Montrer que $(q^n)$ n'a pas de limite en utilisant
        les techniques du paragraphe suivant.

    $square$
]