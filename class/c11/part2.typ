#import "../../template/ytemplate.typ": *


#ypart[Limite d'une suite]


#ydef(name: [Limite d'une suite])[
    Soit $u$ une suite définie à partir d'un
    certain rang $N$ et $l in CC$.

    On dit que $l$ est une *limite* de $u$ si
    $ forall epsilon > 0, exists n_0 in NN, forall n >= n_0,
        yt abs(u_n - l) < epsilon $
    
    // #cetz.canvas(length: 3em, {
    //     import cetz.draw: *
    // })
]

#ydef(name: [Limite en l'infini d'une suite réelle])[
    Soit $u$ une suite réelle.

    - On dit que $+oo$ est une *limite* de $u$ si
        $ forall M in RR, exists n_0 in NN, forall n >= n_0,
            yt u_n >= M $

    - On dit que $-oo$ est une *limite* de $u$ si
        $ forall m in RR, exists n_0 in NN, forall n >= n_0,
            yt u_n <= m $
]

#yprop(name: [Unicité de la limite d'une suite])[
    Soit $u$ une suite complexe.
    $l_1$ et $l_2$ deux limites de $u$.
    Alors $l_1 = l_2$.
]

#yproof[
    On suppose $l_1 != l_2$.
    On pose $epsilon = abs(l_1 - l_2)/42$.

    Soit $n_0 in NN$ tel que
    $ forall n >= n_0, abs(u_n - l_1) <= epsilon $
    Soit $n_1 in NN$ tel que
    $ forall n >= n_1, abs(u_n - l_2) <= epsilon $

    Soit $n = max(n_0, n_1)$.

    $ 42 epsilon &= abs(l_1 - l_2) \
        &= abs(l_1 - u_n + u_n - l_2) \
        &<= abs(l_1 - u_n) + abs(u_n - l_2) \
        &<= 2 epsilon $
    
    Donc $epsilon < 0$ : une contradition.
    $square$
]

#yprop(name: [Unicité de la limite d'une suite réelle])[
    Soit $u$ une suite réelle, $(l_1, l_2) in overline(RR)^2$.
    deux limites de $u$. Alors $l_1 = l_2$.
]

#yproof[
    Si $(l_1, l_2) in RR^2$ on utilise les mêmes arguments
    que pour une suite complexe.

    - Cas 1 : On suppose $l_1 in RR$ et $l_2 = +oo$.

        On pose $epsilon = 1 > 0$ et $M = l_1 + 2$.
        $ forall n > n_0, abs(u_n - l_1) <= epsilon $
        et donc
        $ forall n > n_0, l_1 - 1 <= u_n <= l_1 + 1 $

        Soit $n_1 in NN$ tel que
        $ forall n >= n_1, u_n >= M = l_1 + 2 $

        On pose $n = max(n_0, n_1)$.

        On a donc $u_n >= l_1 + 2 > l_1 + 1 >= u_n$,
        i.e. $u_n > u_n$ : une contradiction.
    
    - Cas 2 : Pour $l_1 in RR$ et $l_2 = -oo$,
        similaire au cas 1
    
    - Cas 3 : On suppose $l_1 = -oo$ et $l_2 = +oo$.

        Soit $(n_1, n_2) in NN^2$ tels que
        $ cases(forall n > n_1\, u_n < -1,
                forall n > n_2\, u_n > 1) $

        On pose $n = max(n_1, n_2)$.
        $ u_n < -1 < 1 < u_n $
        une contradiction !

    $square$
]

#ydef(name: [Convergence et divergence d'une suite])[
    On dit qu'une suite *converge* s'il existe $l in CC$
    tel que $l$ soit une limite de $u$.

    On note alors $u_n -->_(n->+oo) l$ ou $l = lim_(n->+oo) u_n$.

    Si $u$ ne converge pas, on dit qu'elle *diverge*,
    c'est le cas si $u$ n'a pas de limite ou un limite infinie.
]

#ydate(2023, 12, 13)

#ylemme(name: [Borner une suite qui converge])[
    Soit $u$ convergente. Alors $u$ est bornée :
    $ exists M in RR, forall n in NN, abs(u_n) <= M $
]

#yproof[
    On note $l = lim_(n->+oo) u_n$.

    Soit $n_0 in NN$ tel que
    $ forall n >= n_0, abs(u_n - l) <= 1 $

    D'où, pour tout $n > n_0$,
    $ abs(u_n) &= abs((u_n - l) + l) \
        &<= abs(u_n - l) + abs(l) \
        &<= 1 + abs(l) $
    
    ${ abs(u_n) | n <= n_0 }$ est fini, donc
    il a un plus grand élément $mu$.

    On pose $M = max(mu, 1 + abs(l))$.

    Pour tout $n in NN$,
    $ abs(u_n) &<= cases(1 + abs(l) "si" n >= n_0, mu "sinon") \
            &<= M $
    $square$
]

#yprop(name: [Limite de la somme de deux suites])[
    Soient $u$ et $v$ deux suites complexes,
    de limites $l_1 in CC$ et $l_2 in CC$.

    $u + v$ converge vers $l_1 + l_2$
]

#yproof[
    Soit $epsilon > 0$.

    $forall n, abs((u_n + v_n) - (l_1 + l_2))
        yt = abs((u_n - l_1) + (v_n - l_2))
        yt <= abs(u_n - l_1) + abs(v_n - l_2) $
    
    Or $u_n -->_(n->+oo) l_1$ et $v_n -->_(n->+oo) l_2$.

    On considère $n_0 in NN$ tel que
    $ forall n >= n_0, abs(u_n - l_1) <= epsilon/2 $
    et $n_1 in NN$ tel que
    $ forall n >= n_1, abs(v_n - l_2) <= epsilon/2 $

    On pose $n_2 = max(n_0, n_1)$.
    $ forall n > n_2, abs((u_n + v_n) - (l_1 + l_2))
        yt <= epsilon/2 + epsilon/2 = epsilon $
    $square$
]

#yprop(name: [Limite du produit de deux suites])[
    Soient $u$ et $v$ deux suites complexes,
    de limites $l_1 in CC$ et $l_2 in CC$.

    $u v$ converge vers $l_1 l_2$
]

#yproof[
    Soit $epsilon > 0$.

    Pour tout $n$,
    $ abs(u_n v_n - l_1 l_2)
        yt = abs((u_n - l_1 + l_1) v_n - l_1 l_2)
        yt = abs((u_n - l_1) v_n + l_1 (v_n - l_2))
        yt <= abs(u_n - l_1) abs(v_n) + abs(l_1) abs(v_n - l_2) $
    
    $(v_n)$ converge donc est bornée.

    Soit $M in RR$ tel que
    $ forall n in NN, abs(v_n) <= M $

    - Cas 1 : On suppose $M != 0$ et $abs(l_1) != 0$.

        On considère $(n_0, n_1) in NN^2$ tel que
        $ cases(forall n >= n_0\, abs(u_n - l_1) <= epsilon/(2M) \
            forall n >= n_1\, abs(u_n - l_2) <= epsilon/(2 abs(l_1))) $

        On pose $n_2 = max(n_0, n_1)$.

        Pour tout $n >= n_2$,
        $ abs(u_n v_n - l_1 l_2)
            yt <= epsilon/(2 M) M + abs(l_1) epsilon/(2 abs(l_1))
            yt <= epsilon $
    
    - Cas 2 : On suppose $M = 0$.

        Alors $forall n in NN, v_n = 0$
        et donc $l_2 = 0$ et 
        $ forall n, u_n v_n = u_n 0 = 0 -->_(n->+oo) l_1 l_2 $

    - Cas 3 : On suppose $M != 0$ et $abs(l_1) = 0$.

        Soit $n_0 in NN$ tel que
        $ forall n >= n_0, abs(u_n - l_1) <= epsilon/M $

        Alors, pour tout $n$,
        $ abs(u_n v_n - l_1 l_2)
            yt <= abs(u_n l_1) abs(v_n)
            yt <= epsilon/M M = epsilon $

    $square$
]

#yprop(name: [Limite du quotient de deux suites])[
    Soient $u$ et $v$ deux suites complexes,
    de limites $l_1 in CC$ et $l_2 in CC$.

    Si $l_2 != 0$, alors $u/v$ converge vers $l_1/l_2$
]

#yproof[
    On suppose $l_2 != 0$. Montrons que
    $ 1/v_n -->_(n->+oo) 1/l_2 $

    Soit $epsilon > 0$.
    $ forall n, abs(1/v_n - 1/l_2) = abs((l_2 - v_n)/(l_2 v_n)) $
    (grace à l'inégalité triangulaire inverse)

    Soit $n_0 in NN$ tel que
    $ forall n >= n_0, abs(v_n - l_2) <= abs(l_2)/2 $

    D'où, pour tout $n >= n_0$,
    $ abs(v_n) &= abs((v_n - l_2) + l_2) \
        &>= abs(abs(v_n - l_2) - abs(l_2)) \
        &>= abs(l_2) - abs(v_n - l_2) \
        &>= abs(l_2) - abs(l_2)/2 = 1/2 abs(l_2) $
    
    D'où, pour tout $n > n_0$,
    $ abs(1/v_n - 1/l_2)
        yt <= abs(u_n - l_2)/(abs(l_2)/l_2)
        yt = 2/abs(l_2) abs(v_n - l_2) $

    Soit $n_1 in NN$ tel que
    $ forall n >= n_1, abs(v_n - l_2) <= epsilon/2 abs(l_2) $

    On pose $n_2 = max(n_0, n_1)$.

    Pour tout $n > n_2$,
    $ abs(1/v_n - 1/l_2)
        &<= 2/abs(l_2) abs(v_n - l_2) \
        &<= 2/abs(l_2) epsilon/2 abs(l_2) \
        &<= epsilon $
    $square$
]

#yprop(name: [Limite de la somme de deux suites, dont au moins une tend vers un infini])[
    Soient $u$ et $v$ deux suites réelles de limites
    $(l_1, l_2) in overline(RR)^2$.

    + Si $l_1 = +oo$ et $l_2 in RR$, alors $u_n + v_n --> +oo$
    + Si $l_1 = -oo$ et $l_2 in RR$, alors $u_n + v_n --> -oo$
    + Si $l_1 = l_2 = +oo$, alors $u_n + v_n --> +oo$
    + Si $l_1 = l_2 = -oo$, alors $u_n + v_n --> -oo$
]

#yproof[
    + On suppose $l_1 = +oo$ et $l_2 in RR$.

        Soit $M in RR$.

        Soit $n_0 in NN$ tel que
        $ forall n >= n_0, abs(v_n - l_2) <= 1 $

        D'où,
        $ forall n >= n_0, v_n >= l_2 - 1 $

        Soit $n_1 in NN$ tel que
        $ forall n >= n_1, u_n >= M - (l_2 - 1) $

        On pose $n_2 = max(n_0, n_1)$.

        Pour tout $n >= n_2$,
        $ u_n + v_n &>= M - (l_2 - 1) + l_2 - 1 \
            &= M $
    
    De même pour 2, 3 et 4.
    $square$
]

#yprop(name: [Limite du produit de deux suites, dont au moins une tend vers un infini])[
    Soient $u$ et $v$ deux suites réelles de limites $l_1$, $l_2$.

    + Si $l_1 = +oo$ et $l_2 in RR^*_+ union {+oo}$,
        alors $u_n v_n --> +oo$

    + Si $l_1 = +oo$ et $l_2 in RR^*_- union {-oo}$,
        alors $u_n v_n --> -oo$
]

#yproof[
    Dans le cas où $l_1 = +oo$ et $l_2 in RR^*_+$.

    Soit $M in RR^*_+$.

    Soit $n_0 in NN$ tel que
    $ forall n >= n_0, abs(v_n - l_2) <= l_2/2 $

    D'où,
    $ forall n >= n_0, v_n > l_2 - l_2/2 = l_2/2 > 0 $

    Soit $n_1 in NN$ tel que
    $ forall n >= n_1, u_n >= (2 M)/l_2 > 0 $

    On pose $n_2 = max(n_0, n_1)$.

    D'où,
    $ forall n > n_2, u_n v_n >= (2 M)/l_2 l_2/2 = M $
    $square$
]

#yprop(name: [Limite de l'inverse d'une suite qui tend vers l'infini ou zéro])[
    Soit $u$ une suite réelle, de limite $l$,
    et telle que
    $ exists N in NN, forall n >= N, u_n != 0 $

    - Si $l = plus.minus oo$, alors $1/u_n -->_(n->+oo) 0$

    - Si $l = 0$ et, pour tout $n > 0$, $u_n > 0$,
        alors $1/u_n -->_(n->+oo) +oo$

    - Si $l = 0$ et, pour tout $n > 0$, $u_n < 0$,
        alors $1/u_n -->_(n->+oo) -oo$
]

#yproof[
    Dans le cas où $l = 0$ et $u_n > 0$ pour $n >= N$.

    Soit $M in RR^*_+$. Soit $n_0 in NN$ tel que
    $ forall n >= n_0, abs(u_n - 0) <= 1/M $

    On pose $n_1 = max(n_0, N)$.
    $ forall n >= n_1, 0 < u_n <= 1/M $

    D'où,
    $ forall n >= n_1, 1/u_n >= M $
    $square$
]