#import "../../template/ytemplate.typ": *


#ypart[Groupes]


#ydef(name: [Groupe])[
    #set enum(numbering: "(G1)")
    Soit $G$ un ensemble muni d'une loi de composition notée $#emoji.star$.
    
    On dit que $(G , #emoji.star)$ est un #emph[groupe] si
    + $#emoji.star$ est associative;
    + $#emoji.star$ possède un élément neutre $e$ dans $G$;
    + tout élément de $G$ possède un symétrique dans $G$;
    + $#emoji.star$ est interne à $G$.
]

#ydef(name: [Groupe abélien])[
    Soit $(G , #emoji.star)$ un groupe.
    
    On dit que $(G , #emoji.star)$ est un groupe
    #emph[commutatif] ou que c'est un groupe #emph[abélien] si $#emoji.star$ est
    commutative.
]


#ynot(name: [Habitudes de notations pour les groupes simples et abéliens])[
    En général, les groupes sont notés #emph[multiplicativement];: on note
    $x y$ à la place de $x #emoji.star y$, le neutre est noté $1_G$, le symétrique de
    $x$ est appelé #emph[inverse] et noté $x^(- 1)$.

    En revanche, si $G$ est abélien, on utilise plutôt une notation
    #emph[additive] : la loi est notée $+$, le neutre $0_G$, le symétrique
    de $x$ est appelé #emph[opposé] de $x$ et noté $- x$.
]


#yexample[
    + $(RR, +)$ est un groupe abélien.
    + $(CC^*, times)$ est un groupe abélien.
    + Soit $E$ un ensemble. $(E^E, compose)$ n'est pas un groupe en général.
    + Soit $E$ un ensemble. $(cal(P)(E), sect)$ n'est pas un groupe en général.
    + L'ensemble des rotations qui conservent un carré est un groupe pour la composition.
]

#ydef(name: [Monoïde])[
    #set enum(numbering: "(M1)")
    Soit $M$ un ensemble muni d'une loi de composition $#emoji.star$.

    On dit que $(M, #emoji.star)$ est un monoïde si
    + $#emoji.star$ est associative;
    + $#emoji.star$ a un élément neutre $e in M$;
    + $#emoji.star$ est interne à $M$.
]

#ynot(name: [Ensemble des éléments symétrisables])[
    Soit $E$ un ensemble muni de la loi $#emoji.star$.

    On note $E^times$ l'ensemble des éléments symétrisables de $E$.
    $ forall x in E^times, x^(-1) in E $
]

#yprop(name: [Symétrique du résultat d'une opération sur deux éléments d'un groupe])[
    Soit $(G, #emoji.star)$ un groupe. 
    $ forall (x, y) in G^2, (x #emoji.star y)^(-1) = y^(-1) #emoji.star x^(-1). $
]


#yproof[
    Soit $(x, y) in G^2. $
    $ (x #emoji.star y) #emoji.star (y^(-1) #emoji.star x^(-1)) = x #emoji.star x^(-1) =e. $
    De même pour le calcul de
    $(y^(-1) #emoji.star x^(-1)) #emoji.star (x #emoji.star y)$.
]

#yprop(name: [Produit carthésien de deux groupes])[
    Soient $(G, #emoji.star)$ et $(H, #sym.suit.heart)$ deux groupes (resp. groupes commutatifs).
    
    Alors $(G times H, #sym.suit.spade)$ est un groupe (resp. groupe commutatif) où $#sym.suit.spade$ est définie par 
  $ (x, y) #sym.suit.spade (a, b) = (x #emoji.star a, y #sym.suit.heart b). $
]

#yproof[
    + Soient $(x, y), (a, b), (alpha, beta) in G times H$.
        $ (x, y) #sym.suit.spade ((a, b) #sym.suit.spade (alpha, beta))
            yt = (x, y) #sym.suit.spade (a #emoji.star alpha, b #sym.suit.heart beta)
            yt = (x #emoji.star (a #emoji.star alpha),  y #sym.suit.heart (b #sym.suit.heart beta) )
            yt = ((x #emoji.star a) #emoji.star alpha, (y #sym.suit.heart b) #sym.suit.heart beta)
            yt = (x #emoji.star a, y #sym.suit.heart b) #sym.suit.spade (alpha, beta)
            yt = ((x, y) #sym.suit.spade (a, b)) #sym.suit.spade (alpha, beta) $

    + Si $#emoji.star$ et $#sym.suit.heart$ sont commutatives, alors pour tout $(x, y), (a, b) in G times H$,
        $ (x, y) #sym.suit.spade (a, b)
            yt = (x #emoji.star a, y #sym.suit.heart b)
            yt = (a #emoji.star x, b #sym.suit.heart y)
            yt = (a, b) #sym.suit.spade (x, y), $
        #emph[i.e.] $#sym.suit.spade$ est commutative.

    + On note $e$ le neutre de $#emoji.star$ et $epsilon$ le neutre de $#sym.suit.heart$.
  
        D'où $(e, epsilon) in G times H$ et pour tout $(x, y) in G times H$,
        $ (x, y) #sym.suit.spade (e, epsilon)
            yt = (x #emoji.star e, y #sym.suit.heart epsilon) = (x, y)
            yt = (e #emoji.star x, epsilon #sym.suit.heart y). $
        Donc $(e, epsilon)$ est l'élément neutre de $#sym.suit.spade$.

    + Soit $(x, y) in G times H$. Alors 
        $ (x, y) #sym.suit.spade (x^(-1), y^(-1))
            yt = (x #emoji.star x^(-1), y #sym.suit.heart y^(-1))
            yt = (e, epsilon) $
        et 
        $ (x^(-1), y^(-1)) #sym.suit.spade (x, y)
            yt = (x^(-1) #emoji.star x, y^(-1) #sym.suit.heart y) = (e, epsilon). $
]


#ydef(name: [Sous-groupe])[
    #set enum(numbering: "(SG 1)")
    Soit $(G , #emoji.star)$ un groupe d'élément neutre $e$ et $H subset G$. On dit
    que $H$ est un #emph[sous-groupe] de $G$ si

    +  $e in H$; 

    +  $forall x , y in H$, $x #emoji.star y in H$; 

    +  $forall x in H$, $x^(- 1) in H$. 
]

#yexample[
    + $QQ$ est un sous-groupe de $(RR, +)$.
    + $NN$ n'est pas un sous-groupe de $(RR, +)$.
    + $ZZ^*$ n'est pas un sous-groupe de $(CC^*, times)$.
]

#yprop(name: [Un sous-groupe est un groupe])[
    Soit $H$ un sous-groupe de $(G , #emoji.star)$.

    Alors $(H , #emoji.star)$ est un groupe.
]

#yproof[
    + $#emoji.star$ est bien une loi interne de $H$ d'après (SG 2).
    + $#emoji.star$ est associative sur $G$, donc sur $H$ aussi.
    + $e in H$ d'après (SG 1) donc $H$ possède un élément neutre pour $#emoji.star$. 
    + Soit $x in H$. D'après (SG 3), $x^(-1) in H$.
    Donc $(H, #emoji.star)$ est un groupe.
]

#yprop(name: [Condition nécessaire et suffisante pour être un sous-groupe])[
    Soit $(G , #emoji.star)$ un groupe et $H subset G$. Alors $H$ est un sous-groupe
    de $(G , #emoji.star)$ si et seulement si $H$ est non vide et
    $ forall x , y in H , med x #emoji.star y^(- 1) in H . $
]

#yproof[
    + On suppose que $H$ est un sous-groupe de $G$.
        - D'après (SG 1), $e in H$ donc $H != emptyset$.

        - Soit $(x, y) in H^2$.
            
            D'après (SG 3), $y^(-1) in H$ et d'après (SG 2), $x #emoji.star y^(-1) in H$.

    + On suppose à présent que $H$ est non vide et $x #emoji.star y^(-1) in H$ pour tout $(x, y) in H^2$.
        - Comme $H$ est non vide, on considère $x$ un élément de $H$.
        
            Donc $(x, x) in H^2$ et par suite $x #emoji.star x^(-1) in H$.
            
            On en déduit que $e in H$.

        - Soit $x in H$.
        
            Comme $(e, x) in H^2$, $e #emoji.star x^(-1) in H$ et donc $x^(-1) in H$.

        - Soit $(x, y) in H^2$.
        
            D'après le calcul précédent, $y^(-1) in H$.
            
            Donc $(x, y^(-1)) in H^2$ et par suite $x #emoji.star (y^(-1))^(-1) in H$,
            #emph("i.e") $x #emoji.star y in H$.

    Donc $H$ est un sous-groupe de $(G, #emoji.star)$.
]


#ydef(name: [Homomorphisme / morphisme de groupes])[
    Soient $(G , #emoji.star)$ et $(H , #sym.suit.heart)$ deux groupes, et $f : G arrow.r H$
    une application.
    
    On dit que $f$ est un #emph[homomorphisme (ou plus simplement un morphisme) de groupes] si
    $ forall x , y in G , med f (x #emoji.star y) = f (x) #sym.suit.heart f (y) $
]

#yexample[
    + La fonction exponentielle est un morphisme de groupes de $(RR, +)$ dans $(RR_*^+, times)$.
    + Les fonctions de la forme $x |-> a x$ avec $a in RR$ sont des morphismes
        de groupes de $(RR, +)$ dans $(RR, +)$.
]


#yprop(name: [Image par un morphisme de groupes d'un élément neutre, et d'un inverse])[
    Soient $(G , #emoji.star)$ et $(H , #sym.suit.heart)$ deux groupes et $f : G arrow.r H$ un
    morphisme de groupes.
    
    On note $e$ le neutre de $G$ et $epsilon$ celui de $H$.
    Alors:

    + $f (e) = epsilon$;

    + $forall x in G , med f (x^(-1)) = f (x)^(-1)$.
]


#yproof[
    + On remarque que
        $ f(e) = f(e #emoji.star e) = f(e) #sym.suit.heart f(e) $
        Comme $f(e) in H$ et $(H, #sym.suit.heart)$ est un groupe, on obtient
        $ epsilon &= f(e) #sym.suit.heart f(e)^(-1) \
            &= (f(e) #sym.suit.heart f(e)) #sym.suit.heart f(e)^(-1) \
            &= f(e) #sym.suit.heart (f(e) #sym.suit.heart f(e)^(-1)) \
            &= f(e) $

    + Soit $x in H$.

        On remarque que
        $ epsilon &= f(e) \
            &= f(x #emoji.star x^(-1)) \
            &= f(x) #sym.suit.heart f(x^(-1)) $
        et donc $f(x)^(-1) = f(x^(-1))$.
]

#ybtw[
    Dans la proposition ci-dessus, $f (x)^(-1)$ est le symétrique de $f (x)$
    pour la loi $#sym.suit.heart$, alors que $x^(-1)$ est le symétrique de $x$ pour la
    loi $#emoji.star$.
]



#yprop(name: [Composition de deux morphismes de groupes])[
    La composée de deux morphismes de groupes est encore un morphisme de
    groupes.
]

#yproof[
    Soient $f : (G, #emoji.star) -> (H, #sym.suit.heart)$ et $g : (H, #sym.suit.heart) -> (K, #sym.suit.club)$ deux morphismes de groupes.
    
    On pose $h = g compose f$.
    
    Soit $(x, y) in G^2$.
    $ h(x #emoji.star y) &= g(f(x #emoji.star y)) \
        &= g(f(x) #sym.suit.heart f(y)) \
        &= g(f(x)) #sym.suit.club g(f(y)) \
        &= h(x) #sym.suit.club h(y). $
    Donc $h$ est bien un morphisme de groupes.
]



#ydef(name: [Endomorphisme, isomorphisme, automorphisme])[
    Soit $f : (G, #emoji.star) -> (H, #sym.suit.heart)$ un morphisme de groupes.
    
    On dit que $f$ est un
    - #emph[endomorphisme] de groupes si $H = G$ et $#emoji.star = #sym.suit.heart$;
    - #emph[isomorphisme] de groupes si $f$ est bijective;
    - #emph[automorphisme] de groupes si $f$ est à la fois un endomorphisme
        et un isomorphisme.
]

#yexample[
    + L'exponentielle est un isomorphisme de groupes de $(RR, +)$ dans $(RR_*^+, times)$.
    + Soit $(G, #emoji.star)$ un groupe. $id_G$ est un automorphisme de $G$.
]

#ybtw[
    Si deux groupes sont isomorphes (#emph[i.e.] il existe un isomorphisme de groupes de l'un dans l'autre), alors ces deux groupes ont exactement les mêmes propriétés algébriques.
]

#ynot(name: [Ensemble des automorphismes])[
    Soit $G$ un groupe.

    On note $"Aut"(G)$ l'ensemble des automorphismes de $G$.
]

#yprop(name: [Particularité de l'ensemble des automorphismes muni de la composition])[
    $("Aut"(G), compose)$ est un groupe.
]

#yproof[
    + $compose$ est associative.

    + $id_G$ est le neutre de $compose$ et est bien un automorphisme de groupes.

    + Soit $f$ et $g$ deux automorphismes de groupes.
        
        On sait que $f compose g$ est un morphisme de groupes et $f compose g$ est bijective, donc $f compose g$ est un automorphisme de groupes.

    + Soit $f$ un automorphisme de groupe.
        
        Comme $f$ est bijective, $f$ admet une réciproque, qui est bien son symétrique pour $compose$.
        
        
        $f^(-1)$ est bijective.
        
        Montrons que $f^(-1)$ est un morphisme de groupes. Soit $(x, y ) in G^2$.
        
        On note $a = f^(-1)(x)$ et $b = f^(-1)(y)$.
        
        Comme $f$ est un morphisme de groupes,
        $ f(a #emoji.star b) = f(a) #emoji.star f(b) = x #emoji.star y $ donc $a #emoji.star b = f^(-1)(x #emoji. y)$.
        
        Ainsi, $f^(-1) in "Aut"(G)$.
]


#yprop(name: [Particularité de l'image directe / réciproque de sous groupes par un morphisme de groupes])[
    Soit $f : (G, #emoji.star) -> (H, #sym.suit.heart)$ un morphisme de groupes, $G prime$ un sous-groupe
    de $G$ et $H prime$ un sous-groupe de $H$. Alors

    + $f (G prime)$ est un sous-groupe de $H$;

    + $f^(- 1) (H prime)$ est un sous-groupe de $G$.
]

#yproof[
    On note $e$ l'élément neutre de $#emoji.star$ et $epsilon$ celui de $#sym.suit.heart$.

    + 
        - $e in G'$ donc $f(e) in f(G')$ donc $epsilon in f(G')$.

        - Soit $(x, y) in f(G')^2$.

            Soient $(a, b) in G'^2$ tels que
            $ cases(x = f(a), y = f(b)) $
            
            Alors $x #sym.suit.heart y^(-1) = f(a #emoji.star b^(-1))$.
            
            Comme $a #emoji.star b^(-1) in G'$,
            $ x #sym.suit.heart y^(-1) in f(G') $

        Donc $f(G')$ est un sous-groupe de $(H, #sym.suit.heart)$.

    + 
        - $epsilon in H'$ et $f(e)=epsilon$ donc $e in f^(-1)(H')$.

        - Soit $(x, y) in f^(-1)(H')$.

            On pose $a = f(x)$ et $b = f(y)$.
            
            On sait que $(a, b) in H'^2$, donc $a #sym.suit.heart b'^(-1) in H'$.
            
            Or, $a #sym.suit.heart b^(-1) = f(x #emoji.star y^(-1))$.
            
            Donc $x #emoji.star y^(-1) in f^(-1)(H')$.

        Ainsi $f^(-1)(H')$ est un sous-groupe de $(G, #emoji.star)$.
]


#ydef(name: [Noyau d'un morphisme de groupes])[
    Soit $f : G arrow.r H$ un morphisme de groupes. On note $epsilon$
    l'élément neutre de $H$.
    
    Le #emph[noyau] de $f$ est
    $ ker (f) := { x in G divides f (x) = epsilon }. $ 
]

#yprop(name: [Le noyau est un sous-groupe])[
    Soit $f: G -> H$ un morphisme de groupes.
    
    $ker(f)$ est un sous-groupe de $G$.
]

#yproof[
    ${epsilon}$ est un sous-groupe de $H$ donc $ker(f)$ est un sous-groupe de $G$.
]

#ytheo(name: [Caractérisation d'une injection par son noyau])[
    Soit $f : (G, #emoji.star) -> (H, #sym.suit.heart)$ un morphisme de groupes. On note $e$ l’élément
    neutre de $G$.
    
    Alors $f$ est injective si et seulement si
    $ker (f) = { e }$.
]


#yproof[
    - On suppose $f$ injective.
        
        Soit $x in ker(f)$.
        
        Alors $f(x) = epsilon = f(e)$.
        
        Comme $f$ est injective, $x = e$.
        
        Donc $ker(f) subset {e}$.
        
        Or, $f(e)=epsilon$ donc ${e} subset ker(f)$.
        
        D'où $ker(f) = {e}$.

    - On suppose à présent $ker(f) = {e}$. Soit $(x, y) in G^2$.  
        $ f(x) = f(y)
            yt => f(x) #sym.suit.heart f(y)^(-1) = epsilon
            yt => f(x #emoji.star y^(-1)) = epsilon
            yt => x #emoji.star y^(-1) in ker(f)
            yt => x #emoji.star y^(-1) = e
            yt => x = y $

    Donc $f$ est injective.
]


#yprop(name: [Ensemble des solutions d'une équation à partir du noyau])[
    Soit $f : (G, #emoji.star) -> (H, #sym.suit.heart)$ un morphisme de groupes,
    $y in "Im"(y)$ et $(cal(E))$ l'équation $f(x) = y$ d'inconnue $x in G$.
    
    On note $x_0$ une solution particulière de $(cal(E))$.
    
    Alors l'ensemble des solutions de $(cal(E))$ est
    $ {h #emoji.star x_0 | h in ker(f)} $
]

#yproof[
    Soit $x in G$.
    $ f(x) = y
        yt <=> f(x) = f(x_0)
        yt <=> f(x) #sym.suit.heart f(x_0)^(-1) = epsilon
        yt <=> x #emoji.star x_0^(-1) in ker(f)
        yt <=> exists h in ker(f), x #emoji.star x_0^(-1) = h
        yt <=> exists h in ker(f), x = h #emoji.star x_0. $
]

#ybtw[
    Avec les notations de la proposition précédente, 
    - si $y in.not "Im"(f)$, alors l'équation n'a pas de solution,
    - sinon, il y a autant de solutions que d'éléments dans le noyau de $f$.
]