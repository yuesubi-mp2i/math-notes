// Structures algébriques
#import "../../template/ytemplate.typ": *

#ychap[Structures algébriques]

L'intégralité de ce chapitre est issu du polycopié de M.Balan

#include "part1.typ"
#include "part2.typ"
#include "part3.typ"