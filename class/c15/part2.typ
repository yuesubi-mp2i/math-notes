#import "../../template/ytemplate.typ": *


#ypart[Anneaux]


#ydef(name: [Anneau])[
    #set enum(numbering: it => "(A"+str(it)+")" )
    Soit $A$ un ensemble muni de deux lois de composition
    notées $+$ et $#emoji.star$.
    
    On dit que $(A , + , #emoji.star)$ est un #emph[anneau] si

    + $(A , +)$ est un groupe commutatif.

    + La loi $#emoji.star$ vérifie les propriétés suivantes :
        - $#emoji.star$ est associative;
        - $#emoji.star$ a un élément neutre $1_A$ dans $A$.

    + Pour tout $(a , b , c) in A^3 $,
        $ cases(a #emoji.star (b + c) = (a #emoji.star b) + (a #emoji.star c),
            (a + b) #emoji.star c = (a #emoji.star c) + (b #emoji.star c)) $
    
    + $*$ est interne à $A$.
]

#ydef(name: [Anneau commutatif])[
    Soit $(A , + , #emoji.star)$ un anneau.

    On dit que $A$ est _commutatif_ si $#emoji.star$ est commutative.
]

#ynot(name: [Habitudes de notations pour les anneaux])[
    Soit $(A , + , #emoji.star)$ un anneau. On utilise une notation additive pour
    $+$, donc le neutre de $+$ est noté $0_A$ et le symétrique pour $+$ d'un
    élément $x$ est noté $- x$.
]

#yexample[
    + $(RR, +, times)$ est un anneau commutatif.
    + $(ZZ, +, times)$ est un anneau commutatif.
    + $(M_n (CC), +, times)$ est un anneau non commutatif en général.
]


#yprop(name: [Élément absorbant d'un anneau])[
    Soit $(A , + , #emoji.star)$ un anneau.
    
    Alors $0_A$ est #emph[absorbant] pour $#emoji.star$ :
    $ forall x in A , med x #emoji.star 0_A = 0_A #emoji.star x = 0_A . $
]

#yproof[
    Soit $x in A$. 
    $ x #emoji.star 0_A
        &= x #emoji.star (0_A + 0_A) \
        &= (x #emoji.star 0_A) + (x #emoji.star 0_A) $
    et donc, en ajoutant $-(x #emoji.star 0_A)$ des deux côtés de l'égalité,
    on obtient $0_A = x #emoji.star 0_A$.
    
    De même pour $0_A #emoji.star x$.
]


#ydef(name: [Diviseur de zéro])[
    Soit $(A , + , #emoji.star)$ un anneau et $x in A$.
    
    On dit que $x$ est un #emph[diviseur de zéro] s'il
    existe $y in A \\ { 0_A }$ tel que
    $ x #emoji.star y = 0_A = y #emoji.star x $
]

#yprop(name: [Inverse d'un diviseur de zéro])[
    Un diviseur de zéro ne peut pas être inversible.
]

#yproof[
    Soit $x$ un diviseur de zéro.
    
    Soit $y in A without {0_A}$ tel que $x #emoji.star y = 0_A$.
    
    On suppose $x$ inversible.
    
    Alors
    $ 0_A &= x^(-1) #emoji.star 0_A \
        &= x^(-1) #emoji.star (x #emoji.star y) \
        &= 1_A #emoji.star y \
        &= y $
    une contradiction.
]


#ydef(name: [Anneau intègre])[
    Un anneau est dit #emph[intègre] si

    + $0_A eq.not 1_A$;

    + Pour tout $(x, y) in A^2$,
        $ x #emoji.star y = 0_A ==> x = 0_A or y = 0_A $
]

#yexample[
    + $(ZZ, + , times)$ est un anneau intègre.
    + $(M_n (CC), + , times)$ n'est pas intègre en général.
]


#yprop(name: [Binôme de Newton pour un anneau])[
    Soient $(A, +, #emoji.star)$ un anneau,
    et $(a, b) in A^2$ tel que $a #emoji.star b = b #emoji.star a$.
    
    Alors pour tout $n in NN$,
    $ (a + b)^n = sum_(k = 0)^n binom(n, k) a^k #emoji.star b^(n-k) $
]

#yproof[
    Identique à celle donnée dans $CC$.

    #block[#underline[Remarques :]]
    - Dans la formule ci-dessus, $x^n$ désigne $underbrace(x #emoji.star ... #emoji.star x, n "fois")$ si $n in NN^*$ et $1_A$ sinon, même dans le cas où $x = 0_A$.
    - De même, $k x$ est égal à $underbrace(x + ... + x, k "fois")$ si $k in NN^*$, et $0_A$ sinon. 
    - Les parenthèses sont inutiles car $k (x #emoji.star y) = (k x) #emoji.star y = x #emoji.star (k y)$ pour tout $(x, y) in A^2$ et $k in NN$.
    - Cette formule est fausse en général si $a$ et $b$ ne commutent pas.
]

#yprop(name: [Factorisation de $a^n - b^n$ dans un anneau])[
    Soit $(A, +, #emoji.star)$ un anneau, et $(a, b) in A^2$ tel que $a #emoji.star b = b #emoji.star a$.
    
    Alors pour tout $n in NN$, 
    $ a^n - b^n = (a-b) #emoji.star sum_(k = 0)^(n-1) a^k b^(n-1-k) $
]

#yproof[
    Identique à celle donnée dans $CC$.
]

#ydef(name: [Sous-anneau])[
    #set enum(numbering: it => "(SA" + str(it) + ")")
    Soit $(A , + , #emoji.star)$ un anneau et $B subset A$.

    On dit que $B$ est un #emph[sous-anneau de $A$] si
    + $B$ est un sous-groupe de $(A , +)$;
    + $1_A in B$;
    + $B$ est stable par $#emoji.star$.

    (N.B. stable veut dire que pour tout $x, y in B$, $x #emoji.star y in B$)
]

#yexample[$ZZ$ est un sous-anneau de $(RR, + , times)$.]

#yprop(name: [Un sous-anneau est un anneau])[
    Soit $(A , + , #emoji.star)$ un anneau et $B$ un sous-anneau de $A$.
    
    Alors $(B , + , #emoji.star)$ est un anneau.
]

#yproof[
    + Par définition, $B$ est un sous-groupe de $(A, +)$ donc $(B, +)$ est un groupe.
    + D'après (SA3), $#emoji.star$ est une loi interne de $B$.
    + Le neutre de $#emoji.star$ est un élément de $B$ d'après (SA2).
    + $#emoji.star$ est distributive sur + .
    Donc $(B, +, #emoji.star)$ est un anneau.
]



#ydef(name: [Homomorphisme / morphisme d'anneaux])[
    Soient $(A , + , #emoji.star)$ et $(B, square.small, hexa)$ deux anneaux, et
    $f : A arrow.r B$ une application.
    
    On dit que $f$ est un
    _homomorphisme (ou morphisme) d'anneaux_ si

    + $forall x , y in A , med f (x + y) = f (x) square.small f (y)$;
    + $forall x , y in A , med f (x #emoji.star y) = f (x) hexa f (y)$;
    + $f (1_A) = 1_B$.
]

#yprop(name: [Particularité de l'image directe/réciproque d'un sous-anneau par un morphisme])[
    L'image directe ou réciproque d'un sous-anneau par un morphisme
    d'anneaux est un sous-anneau.
]

#yproof[
    Soit $f: A -> B$ un morphisme d'anneaux, $A'$ un sous-anneau de $A$ et $B'$ un sous-anneau de $B$.
    + Comme $f$ est un morphisme de groupes pour l'addition, on sait déjà que $f(A')$ est un sous-groupe de $(B, +)$ et que $f^(-1)(B')$ est un sous-groupe de $(A, +)$.

    + Comme $f(1_A) = 1_B$, et comme $1_A in A'$, $1_B in f(A')$, et comme $1_B in B'$, $1_A in f^(-1)(B')$.

    + Soit $(x, y) in f(A')^2$.
    
        Soit $(a, b) in A'^2$ tels que $f(a)=x$ et $f(b)=y$.
        
        Alors $x #emoji.star y = f(a #emoji.star b) in f(A')$ car $a #emoji.star b in A'$.

    + Soit $(x, y) in f^(-1)(B')$.
    
        On pose $a = f(x)$ et $b=f(y)$.
        
        Alors $(a, b) in B'^2$ et donc $a #emoji.star b in B'$.
        
        Or $a #emoji.star b = f(x #emoji.star y)$.
        
        Donc $x #emoji.star y in f^(-1)(B')$.
]

#ydef(name: [Noyau d'un morphisme d'anneaux])[
    Soit $f : A -> B$ un morphisme d'anneaux.
    
    Le #emph[noyau] de $f$ est
    $ ker (f) = { x in A | f (x) = 0_B } $
]


#ybtw[
    Le noyau d'un morphisme d'anneaux n'est pas un sous-anneau en général.
]

#ydef(name: [Idéal])[
    #set enum(numbering: it => "(I"+str(it)+")" )

    Soit $f: A -> B$ un morphisme d'anneaux.

    $ker(f)$ est un *idéal* si
    + $(ker(f), +)$ est un groupe;
    + pour tout $a in A$, $y in ker(f)$,
        $ cases(a #emoji.star y in ker(f),
            y #emoji.star a in ker(f)) $
]