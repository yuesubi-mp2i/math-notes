#import "../../template/ytemplate.typ": *


#ypart[Corps]


#ydef(name: [Corps])[
    #set enum(numbering: it => "(K"+str(it)+")" )
    Soit $bb(K)$ un ensemble muni de deux lois de composition.
    
    On dit que $(bb(K) , + , #emoji.star)$ est un #emph[corps] si
    + $(bb(K), +, #emoji.star)$ est un anneau commutatif;
    + $0_(bb(K)) eq.not 1_(bb(K))$;
    + tout élément non nul de $bb(K)$ possède un inverse dans $bb(K)$.
]

#yprop(name: [Groupes abéliens d'un corps])[
    Soit $(bb(K) , + , #emoji.star)$ un corps.
    
    Alors $(bb(K) , +)$ et
    $(bb(K) \\ { 0_A } , #emoji.star)$ sont des groupes abéliens.
]

#ybtw[
On peut trouver dans certains ouvrages une définition différente de la
notion de corps: ils peuvent ne pas être commutatifs.

]

#yexample[
  + $(CC, +, times)$ est un corps.
  + $(ZZ, +, times)$ n'est pas un corps.
  ]


#ydef(name: [Sous-corps])[
    #set enum(numbering: it => "(SK"+str(it)+")" )
    Soit $(bb(K) , + , #emoji.star)$ un corps et $bb(L) subset bb(K)$.
    
    On dit que
    $bb(L)$ est un #emph[sous-corps] de $bb(K)$ si
    + $L != {0_KK}$ ;
    + $forall x , y in bb(L) , med x - y in bb(L)$;
    + $forall x , y in bb(L) \\ { 0_(bb(K)) } , med x #emoji.star y^(- 1) in bb(L)$.
]

#yexample[$RR$ est un sous-corps de $(CC, +, times)$.]


#yprop(name: [Les sous-corps sont de corps])[
    Soit $bb(L)$ un sous-corps de $(bb(K) , + , #emoji.star)$. Alors
    $(bb(L) , + , #emoji.star)$ est un corps.
]


#yproof[
    + On sait que $LL$ est un sous-groupe de $(KK, +)$, donc $(LL, +)$ est un groupe commutatif.

    + Soit $x in LL without {0_KK}$ (c'est possible car $LL != {0_KK}$).
        
        Comme $(x, x) in (LL without {0_KK})^2$,
        $ x #emoji.star x^(-1) in LL $
        et donc $1_KK in LL$. 

    + Soit $x in LL without {0_KK}$.
    
        Comme $(1_KK, x) in (LL without {0_KK})^2$,
        $ 1_KK without x^(-1) in LL $
        et donc $x^(-1) in LL$.
    + Soit $(x, y) in LL$.
        
        Si $x$ ou $y$ est nul, alors $x #emoji.star y = 0_KK in KK$.
        
        Sinon, $(x, y^(-1)) in (LL without {0_KK})^2$
        et donc $x #emoji.star y in LL$.
    
    Par suite $(LL, +, #emoji.star)$ est un corps.
    $qed$
]


#ydef(name: [Homomorphisme/morphisme de corps])[
    Soient $(bb(K) , + , #emoji.star)$ et $(bb(L) , + , #emoji.star)$ deux corps, et
    $F : bb(K) arrow.r bb(L)$ une application.
    
    On dit que $f$ est un
    #emph[homomorphisme (ou morphisme) de corps] si
    + $forall x , y in bb(K) , med f (x + y) = f (x) + f (y)$;
    + $forall x , y in bb(K) , med f (x #emoji.star y) = f (x) #emoji.star f (y)$.
]

#ybtw[
Un morphisme de corps est aussi un morphisme d'anneaux.

]

#yexample[La conjugaison est un morphisme de corps de $CC$ dans lui-même.]


#ytheo(name: [Injectivité d'un morphisme de corps])[
    Un morphisme de corps est toujours injectif.
]


#yproof[
    Soit $f : KK -> LL$ un morphisme de corps. 
    
    Soit $x in ker(f)$.

    Si $x != 0_KK$, alors $x$ est inversible, et donc $f(x)$ aussi ;
    or $f(x)=0_KK$ n'est pas inversible.

    Donc $x=0_KK$.
    
    Ainsi $ker(f)={0_KK}$ et donc $f$ est injective.
    $qed$
]