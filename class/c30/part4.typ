#import "../../template/ytemplate.typ": *


#ypart[Formules de Laplace]


#ydef(name: [Mineur])[
    Soit $A = (a_(i,j))_(1<=i,j<= n) in cal(M)_n (KK)$.

    On appelle _mineur_ d'indices $i,j$ de $A$ le
    déterminant obtenu en supprimant la ligne $i$ et
    la colonne $j$ de la matrice $A$.
]

#yprop(name: [Formules de Laplace])[
    Soit $A = (a_(i,j))_(1<=i,j<= n) in cal(M)_n (KK)$.

    Pour $(i,j) in [|1,n|]^2$, on note $m_(i,j)$ le
    mineur d'indices $i,j$ de $A$.

    Pour tout $j in [|1,n|]$,
    $ det(A) = sum_(i=1)^n (-1)^(i+j) a_(i,j) m_(i,j) wide (1) $
    et pour tout $i in [|1,n|]$,
    $ det(A) = sum_(j=1)^n (-1)^(i+j) a_(i,j) m_(i,j) wide (2) $

    Dans le cas (1), on dit qu'on a développé selon la $j$-ième colonne
    (resp. dans le cas (2) selon la $i$-ème ligne).
]

#yproof[
    _Preuve hors-programme, voici tout de même l'idée._

    - Cas 1 : On développe selon la première colonne.

        On note $C_1, ..., C_n$ les colonnes de la matrice $A$,
        et $E_1, ..., E_n$ celles de $I_n$.

        $ &det(A) \
            &= det(sum_(i=1)^n a_(i,1) E_i, C_2, ..., C_n) \
            &= sum_(i=1)^n a_(i,1) det(E_i, C_2, ..., C_n) \
            &= mat(delim: "|", 0, a_(1,2), ..., a_(1,n);
                dots.v, dots.v, , dots.v;
                1, a_(i,2), ..., a_(i,n);
                dots.v, dots.v, , dots.v;
                0, a_(n,2), ..., a_(n,n)) \
            &= (-1)^(i-1) underbrace( mat(delim: "|",
                1, a_(i,2), ..., a_(i,n);
                0, a_(1,2), ..., a_(1,n);
                dots.v, dots.v, , dots.v;
                1, a_(i-1,2), ..., a_(i-1,n);
                1, a_(i+1,2), ..., a_(i+1,n);
                dots.v, dots.v, , dots.v;
                0, a_(n,2), ..., a_(n,n)), B) $
        
        $ det(B) = sum_(sigma in S_n) epsilon(sigma)
                product_(j=1)^n b_(sigma(j), j) $
        si $j=1$ et $sigma(1) != 1$ alors $b_(sigma(i), i) = 0$
        $ &det(B) \
            &= sum_(sigma in S_n \ sigma(1) = 1) epsilon(sigma)
                product_(j=2)^n b_(sigma(j), j) \
            &= sum_(sigma : [|2, n|] -> [|2, n|] \ "bijective") epsilon(sigma)
                product_(j=2)^n b_(sigma(j), j) \
            &= m_(i,j) $
    
    - Cas 2 : on se ramène au as 1 en ramenanant la première colonne
        au début.
]

#yprop(name: [Déterminant de la matrice de Vandermonde])[
    Soit $(a_1, ..., a_n) in KK^n$.
    $ &mat(delim: "|", 1, a_1, a_1^2, ..., a_1^(n-1);
            dots.v, dots.v, dots.v, , dots.v;
            1, a_n, a_n^2, ..., a_n^(n-1)) \
            &= product_(n >= j > i>=1) (a_j - a_i) $
]

#yproof[
    On note
    $ V(a_1,i ..., a_n) =mat(delim: "|", 1, a_1, a_1^2, ..., a_1^(n-1);
            dots.v, dots.v, dots.v, , dots.v;
            1, a_n, a_n^2, ..., a_n^(n-1)) $
    
    Cas 1 : il existe $i != j$ tel que $a_i=a_j$.

        $V(a_1, ..., a_n) = 0$ car il y a 2 lignes identiques et
        $ product_(k> ell) (a_k - a_ell) = 0$.
    
    Cas 2 : pour tout $i!=j$,$a_i!=a_j$.

    Pour $n in NN$, on pose
    $ P_n : &forall (a_1, ..., a_n) in KK^n, \
        &forall i!=j, a_i != a_j ==> \
        &wide V(a_1, ..., a_n) = product_(j>i) (a_j -a_i) $
    
    - Soit $a_1 in KK$.

        $ V(a_1) = 1 $
        $ product_(j>i) (a_j - a_i) = 1 $
    
    - Soit $(a_1, a_2) in KK^2$ avec $a_1 != a_2$.
        $ mat(delim: "|", 1, a_1; 1, a_2) &= a_2 - a_1  \
            &= product_(2>=j>i>=1) (a_j - a_i) $
    
    - Soit $n in NN^*$. On suppose $P_n$ vraie.

        Soit $(a_1, ..., a_(n+1)) in KK^(n+1)$
        deux-à-deux distincts.

        $ &V(a_1, ..., a_(n+1)) \
            &= sum_(j=1)^(n+1) (-1)^(n+1+j) a_(n+1)^(j-1) m_(n+1, j) $
        en développant suivant la dernière ligne,
        et où $m_(n+1,j)$ est le mineur d'indices $n+1,j$.

        On en déduit que $V(a_1, ..., a_(n+1))$
        est un polynôme en $a_(n+1)$ (les $a_1, ..., a_n$
        étant fixés) de degré au plus $n$,
        et le coefficient de degré $n$ vaut
        $ m_(n+1, n+1) &= V(a_1, ..., a_n) \
            &= product_(n>=j>i>=1) (a_j - a_i) != 0 $
        et donc c'est un polynôme de degré $n$.

        De plus, pour $i in [|1, n|]$,
        $ V(a_1, ..., a_n, a_i) = 0 $
        (2 lignes identiques) : on a trouvé $n$ racines de ce polynôme.

        $   &V(a_1, ..., a_(n+1)) \
            &= V(a_1, ..., a_n) product_(i=1)^n (a_(n+1) - a_i) \
            &= product_(n>=j>i>=1) (a_j - a_i) \
                &wide times product_(n+1 >= i >= 1) (a_(n+1) - a_i) \
            &= product_(n+1>=j>i>=1) (a_j - a_i) $
]

#ydef(name: [Cofacteur d'une matrice])[
    Soit $A in cal(M)_n (KK)$ et $(i,j) in [|1,n|]^2$.

    Pour $(i,j) in [|1,n|]^2$, on note $m_(i,j)$ le
    mineur d'indices $i,j$ de $A$.

    Le nombre $(-1)^(i+j) m_(i,j)$ est appelé _cofacteur_
    de $A$ d'indices $(i,j)$.
]

#let com(A) = { $"com"(#A)$ }

#ydef(name: [Comatrice])[
    Soit $A in cal(M)_n (KK)$.

    La _comatrice_ de $A$ est
    $ com(A) = ((-1)^(i+j) m_(i,j))_(1<=i,j<=n) $
]

#yprop(name: [Produit d'une matrice et de la transposée de sa comatrice])[
    Soit $A in cal(M)_n (KK)$.

    $ A (com(A)^T) &= (com(A)^T) A \
        &= det(A) I_n $
]

#yproof[
    Soit $B = (b_(i,j)) = A com(A)^T$.

    Soit $i in [|1, n|]$.
    $ b_(i,j) = sum_(j=1)^n a_(i,j) (-1)^(i+j) m_(i,j) = det(A) $

    Soit $j!=i$.
    $ b_(i,j) = sum_(k=1)^n a_(i,k) (-1)^(k+j) m_(j,k) = det(C) $
    où $C$ est la matrice $A$ dans laquelle on a remplacé
    la ligne $j$ par la ligne $i$
    et donc $C$ a 2 lignes identiques.
]