#import "../../template/ytemplate.typ": *


#ypart[Déterminant d'une matrice (carrée)]


#ydef(name: [Déterminant d'une matrice])[
    Soit $M in cal(M)_n (KK)$
    et $cal(B)$ la base canonique de $KK^n$.

    On note $(x_1, ..., x_n) in KK^n$ tel que
    $ M = "Mat"_cal(B) (x_1, ..., x_n) $

    Le _déterminant_ de $M$ est
    $ det(M) = det_cal(B) (x_1, ..., x_n ) $
]

#yprop(name: [Déterminant d'une matrice diagonale])[

    $ det("diag"(lambda_1, ..., lambda_n)) = product_(i=1)^n lambda_i $
]

#yproof[
    Soit $cal(B) = (e_1, ..., e_n)$ la base canonique de $KK^n$.

    Alors
    $ D = "Mat"_cal(B) (lambda_1 e_1, ..., lambda_n e_n) $

    Donc
    $ det(D) &=  lambda_1 det_cal(B) (e_1, lambda e_2, ... lambda e_n) \
        &dots.v \
        &= lambda_1 ... lambda_n det_cal(B) (cal(B)) \
        &= lambda_1 ... lambda_n $
]

#ynot(name: [Déterminant d'une matrice])[
    Soit $A = (a_(i,j))_(1<=i<=n \ 1<=j <=n)$.

    On note aussi $det(A)$ par
    $ mat(delim: "|",
        a_(1,1), dots, a_(1,n);
        dots.v, dots.down, dots.v;
        a_(n,1), ..., a_(n,n)) $
]

#yprop(name: [Formule du déterminant d'une matrice])[
    Soit $A = (a_(i,j))_(1<=i<=n \ 1<=j <=n)$.
    $ det(A) = sum_(sigma in S_n) epsilon(sigma) product_(i=1)^n a_(sigma(i), i) $
]

#yprop(name: [Lien entre déterminant d'une matrice d'un endomorphisme])[
    Soit $f in cal(L)(E)$,
    $B$ une base de $E$
    et $A = "Mat"_cal(B) (f)$.

    $ det(f) = det(A) $
]

#yproof[
    $ det(A) &= sum_(sigma in S_n) epsilon(sigma) product_(i=1)^n a_(sigma(i), i) \
        &= det_cal(B) (f(cal(B))) \
        &= det(f) $
]

#ycor(name: [Déterminant d'un produit de matrices])[
    Soit $(A,B) in cal(M)_n (KK)^2$.
    $ det(A B) = det(A)det(B) $
]

#yproof[
    Soient $f$ et $g$ els endomorphismes canoniquement
    associés à $A$ et $B$.
    $ det(A B) &= det (f compose g) \
        &= det(f) det(g) \
        &= det(A) det(B) $
]

#ycor(name: [Caractérisation de l'invesibilité d'une matrice par son déterminant])[
    Soit $A in cal(M)_n (KK)$.

    $A$ est inversible ssi elle est de déterminant non nul.
]

#ycor(name: [Déterminant de la matrice inverse])[
    Soit $A in G L_n (KK)$.

    $ det(A^(-1)) = det(A)^(-1) $
]

#yprop(name: [Déterminant du produit par un scalaire d'une matrice])[
    Soit $A in cal(M)_n (KK)$ et $lambda in KK$.

    $ det(lambda A)= lambda^n det(A) $
]

#yprop(name: [Déterminant d'une somme de matrices])[
    En général, $ det(A+B) != det(A) + det(B) $
]

#yprop(name: [Déterminant de la transposée])[
    Soit $A in cal(M)_n (KK)$.
    $ det(A^T) = det(A) $
]

#yproof[
    $ &det (A^T) \
        &= sum_(sigma in S_n) epsilon(sigma) product_(i=1)^n a_(i, sigma(i)) \
        &= sum_(sigma in S_n) epsilon(sigma) product_(k=1)^n a_(sigma^-1 (k), k) \
        &=^* sum_(sigma' in S_n) epsilon(sigma') product_(k=1)^n a_(sigma' (k), k) \
        &= det(A) $
    car
    $ forall sigma in S_n, epsilon(sigma) = epsilon(sigma^(-1)) $
    car la réciproque s'écrit avec le même nombre de transpositions.
]

#yprop(name: [Déterminant après une opération élémentaire sur les colonnes])[
    Soit $A in cal(M)_n (KK)$, $C$ une opération
    élémentaire sur les colonnes et $B$ la matrice obtenue
    en appliquant  $C$ au colonnes de $A$.

    + Si $C = (C_i <-> C_j)$ où $i<j$ alors
        $ det (B) = - det(A) $
    
    + Si $C = (C_i <- lambda C_i)$ où $lambda in KK without{0}$ alors
        $ det(B) = lambda det(A) $
    
    + Si $C = (C_i <- C_i + lambda C_j)$ où $i!=j$ et $lambda in KK$ alors
        $ det(B) = det(A) $
]

#yproof[
    Soit $(x_1, ..., x_n) in (KK^n)^n$ telle que
    $A = "Mat"_cal(B) (x_1, ..., x_n)$ où
    $cal(B)$ est la base canonique de $KK^n$.

    $B = "Mat"_cal(B) (x_1, ..., x_j, ..., x_i, ..., x_n)$

    + $ &det(B) \
            &= det_cal(B) (x_1, ..., x_j, ..., x_i, ..., x_n) \
            &= - det_cal(B) (x_1, ..., x_i, ..., x_j, ..., x_n) \
            &= - det(A) $
    
    + $ &det(B) \
            &= det_cal(B) (x_1, ..., lambda x_i, ..., x_n) \
            &= lambda det_cal(B) (x_1, ..., x_i, ..., x_n) \
            &= lambda det(A) $
    
    + $ &det(B) \
            &= det_cal(B) (x_1, ..., x_i + lambda x_j, ..., x_j, ..., x_n) \
            &= det_cal(B) (x_1, ..., x_i, ..., x_j, ..., x_n) \
                &wide lambda det_cal(B) (x_1, ..., x_j, ..., x_j, ..., x_n) \
            &= det(A) + lambda times 0 $
]

#yprop(name: [Déterminant d'une matrice triangulaire])[
    Soit $A = (a_(i,j))$ une matrice triangulaire
    $ det(A) = product_(i=1)^n a_(i,i) $
]

#yproof[
    $ det(A) &= sum_(sigma in S_n) epsilon(sigma)
            product_(i=1)^n underbrace(a_(sigma(i), i), =0 "si" sigma(i)>i) \
        &= epsilon(id) product_(i=1)^n a_(i,i) \
        &= product_(i=1)^n a_(i,i) $
]

#ybtw[
    Les lignes d'une matrice étant égales aux colonnes
    des sa transposée, on dispose d'un résultat analogue
    aux opérations sur les colonnes mais avec les lignes.
]