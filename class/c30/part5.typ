#import "../../template/ytemplate.typ": *


#ypart[Produit vectoriel (H.-P.)]


#let scal(vec1,vec2) = {$lr(angle.l vec1 mid(|) vec2  angle.r)$}

ytheo(name: [Théorème de représentation de Riesz (H.-P.)])[
    Soit $(E, scal(dot, dot))$ un espace euclidien
    de dimension $n$, et $E^* in cal(L)(E, RR)$.

    Le _crochet de dualité_ est
    $ E^* times E &-> RR \
        (f, a) &|-> f(a) $
    
    On fixe $a in E$.
    $ phi_a : E^* &-> RR \
        f &|-> f(a) $
    $phi_a in cal(L)(E^*, RR) = (E^*)^*$

    Soit $ phi.alt : E &-> (E^*)^* \
        a &|-> phi_a $
    $phi.alt in cal(L)(E, E^(**))$

    Soit $a in E$
    $ psi_a : E &-> RR \
        x &|-> scal(a,x) $
    est linéaire.

    $ forall a in E, psi_a in E^*, 
        Psi: E &-> E^* \
            a &|-> psi_a $
    $Psi$ est linéaire.

    $dim(E^*) = dim(cal(L)(E, RR)) = dim(E) dim(RR )= dim E$

    $ &a in ker(Psi) \
        &<=> Psi(a) = 0_(E^*) \
        &<=> forall x in E, psi_a (x) = 0 \
        &<=> forall x in E, scal(a,x) = 0 \
        &=>  scal(a,a) = 0 \
        &=> a= 0_E $
    
    #rect[$Psi$ est un isomorphisme]

    $ forall f in E^*, exists! a in E, f = psi_a $
]

$dim(E) = 3$

$cal(B) = (e_1, e_2, e_3)$ est un base orthonormée de $E$

$(u, v) in E^2$

$ f: E &-> RR \
    omega &|-> det_cal(B) (u, v, omega) $
linéaire

$f in E^*$
donc
$ exists! a $

(...)