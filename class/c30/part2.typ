#import "../../template/ytemplate.typ": *


#ypart[Déterminant d'un endomorphisme]


#yprop(name: [Déterminant de l'image d'une base par un endomorphisme])[
    Soit $f$ un endomorphisme d'un $KK$-ev $E$
    de dimension $n$, $cal(B) = (e_1, ..., e_n)$
    et $cal(C) = (u_1, ..., u_n)$ deux bases de $E$.

    $ &det_cal(B) (f(e_1), ..., f(e_n)) \
        &= det_cal(C) (f(u_1), ..., f(u_n)) $
]

#yproof[
    $det_cal(B)$ et $det_cal(C)$ sont colinéaires.

    Soit $lambda in KK without {0}$ tel que
    $ det_cal(C) = lambda det_cal(C)$.

    Donc
    $ &det_cal(C) (f(u_1), ..., f(u_n)) \
        &= lambda det_cal(B) (f(u_1), ..., f(u_n)) $
    
    L'application
    $ E^n &-> KK \
        (x_1, ..., x_n) &|-> det_cal(B) (f(x_1), ..., f(x_n)) $
    est $n$-linéaire alternée.

    Elle est donc colinéaire à $det_cal(B)$.

    Soit $mu in KK$ tel que, pour tout $(x_1, ..., x_n) in E^n$,
    $ &det_cal(B) (f(x_1), ..., f(x_n)) \
        &= mu det_cal(B) (x_1, ..., x_n) $
    
    En particulier,
    $ det_cal(B) (f(e_1), ..., f(e_n)) = mu times 1 = mu $

    D'où,
    $ &det_cal(B) (f(u_1), ..., f(u_n)) \
        &= mu det_cal(B) (u_1, ..., u_n) \
        &= det_cal(B) (f(e_1), ..., f(e_n)) det_cal(B) (cal(C)) \
        &= (det_cal(C) (cal(B)))^(-1) det_cal(B) (f(e_1), ..., f(e_n))
         $
    
    D'où,
    $ &det_cal(C) (f(u_1), ..., f(u_n)) \
        &= det_cal(B) (f(e_1), ..., f(e_n)) $
]

#ydef(name: [Déterminant d'un endomorphisme])[
    Soit $f in cal(L)(E)$ ou $E$ est un $KK$-ev de dimension finie.

    Le _déterminant_ de $f$ est $det_cal(B) (f(cal(B)))$
    où $cal(B)$ est n'importe qu'elle base de $E$.

    On le note $det(f)$.
]

#yprop(name: [Écriture du déterminant de l'image d'une famille avec le déterminant d'endomorphisme])[
    Soit $f in cal(L)(E)$ où $E$ est un $KK$-ev
    de dimension $n$, $cal(B) = (e_1, ..., e_n)$ un base de $E$.

    Pour tout $(x_1, ..., x_n) in E^n$,
    $ &det_cal(B) (f(x_1), ..., f(x_n)) \
        &= det(f) det_cal(B) (x_1, ..., x_n) $
]

#yproof[
    L'application
    $ (x_1, ..., x_n) |-> det_cal(B) (f(x_1), ..., f(x_n)) $
    est multilinéaire alternée.
    
    Donc il existe $lambda in KK without {0}$
    tel que, pour tout $(x_1, ..., x_n) in E^n$
    $ &det_cal(B) (f(x_1), ..., f(x_n)) \
        &= lambda det_cal(B) (x_1, ..., x_n) $
    
    En particulier,
    $ &det_cal(B) (f(cal(B))) = lambda det_cal(B) (cal(B)) = lambda $
]

#yprop(name: [Caractérisation de la bijectivité par le déterminant])[
    Soit $f$ un endomorphisme de $E$.

    $f$ est un isomorphisme ssi son déterminant est non nul.
]

#yproof[
    Soit $cal(B) = (e_1, ..., e_n)$ une base de $E$.

    $ &f in cal(G L)(E) \
        &<=> f(e_1, ..., e_n) "est une base de" E \
        &<=> det_cal(B) (f(e_1, ..., e_n)) != 0 \
        &<=> det(f) != 0 $
]

#yprop(name: [Déterminant d'une somme d'endomorphismes])[
    Le déterminant n'étant pas linéaire, en général,
    $ det(f + g) != det(f) + det(g) $
]

#yprop(name: [Déterminant du produit par un scalaire d'un endomorphisme])[
    Soit $f in cal(L)(E)$ et $lambda in KK$.

    $ det(lambda f) = lambda^dim(E) det(f) $
]

#yproof[
    Soit $cal(B) = (e_1, ..., e_n)$.

    $ &det(lambda f) \
        &= det_cal(B) (lambda f(e_1), ..., lambda f(e_n)) \
        &= lambda det_cal(B) (f(e_1), lambda f(e_2), ..., f(e_n)) \
        &= lambda^2 det_cal(B) (f(e_1), f(e_2), lambda f(e_3), ..., lambda f(e_n)) \
        &dots.v \
        &= lambda^n det_cal(B) (f(e_1), ..., f(e_n)) \
        &= lambda^n det(f) $
]

#ytheo(name: [Déterminant d'une composée d'endomorphisme])[
    Soit $(f, g) in cal(L)(E)^2$.
    $ det(f compose g)  = det(f) det(g) $
]

#yproof[
    Soit $cal(B)$ une base de $E$.

    $ det(f compose g) &= det_cal(B) (f(g(cal(B)))) \
        &= det(f) det_cal(B) (g(cal(B))) \
        &= det(f) det(g) $
]

#yprop(name: [Condition nécessaire et suffisante pour qu'une composée soit un automorphisme])[
    Soit $(f,g) in cal(L)(E)^2$.

    $f compose g$ est un isomorphisme (donc un automorphisme) ssi $f$ et $g$ le sont.
]

#yproof[
    $&f compose g in cal(G L)(E) \
        &<=> det(f compose g) != 0 \
        &<=> det(f) det(g) != 0 \
        &<=> cases(det(f) != 0, det(g) != 0) \
        &<=> cases(f in cal(G L)(E), g in cal(G L)(E)) $
]

#yprop(name: [Déterminant de la réciproque])[
    Soit $f in cal(G L)(E)$.
    $ det(f^(-1)) = det(f)^(-1) $
]

#yproof[
    Soit $cal(B)$ une base de $E$.

    $ det(f^(-1)) det(f)
        &= det(f^(-1) compose f) \
        &= deg(id_E) \
        &= det_cal(B) (cal(B)) \
        &= 1 $
]