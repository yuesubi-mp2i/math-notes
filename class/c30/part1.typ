#import "../../template/ytemplate.typ": *


#ypart[Définitions]


#ydef(name: [Forme $n$-linéaire])[
    Soit $E$ un $KK$-ev de dimension $n$
    et $f: E^n -> KK$.

    On dit que $f$ est _$n$-linéaire_ si,
    pour tout $(x_1, ..., x_n) E^n$ et $i in [|1,n|]$,
    l'application
    $ E &-> KK \
        x &|-> f(x_1, ..., x_(i-1), x, x_(i+1), ... x_n) $
    est linéaire.
]

#ydef(name: [Forme antisymétrique])[
    Soit $E$ un $KK$-ev de dimension $n$
    et $f: E^n -> KK$.

    On dit que $f$ est _antisymétrique_ si,
    pour tout $(x_1, ..., x_n) in E^n$ et $i < j$,
    $ &f(x_1, ..., x_i, ..., x_j, ..., x_n) \
        &= -f(x_1, ..., x_j, ..., x_i, ..., x_n) $
]

#yprop(name: [Caractérisation de l'antisymétrie d'une forme linéaire par des permutations])[
    Soit $E$ un $KK$-ev de dimension $n$
    et $f: E^n -> KK$.

    L'application $f$ est antisymétrique
    ssi, pour tout $sigma in S_n$ et $(x_1, .., x_n) in E^n$, 
    $ &f(x_sigma(1), ..., x_sigma(n)) \
        &= epsilon(sigma) f(x_1, ..., x_n) $ 
]

#yproof[
    "$<==$" évident, on prend $sigma = mat(i, j)$.

    "$==>$" On suppose $f$ antisymétrique.

    Soit $sigma in S_n$. On pose $sigma = tau_1 ... tau_p$
    où les $tau_k$ sont des transpositions.

    Soit $(x_1, ..., x_n) in E^n$.
    $ f(x_(tau_p (1)), ..., x_(tau_p (n))) = -f(x_1, ..., x_n) $
    donc
    $ &f(x_(tau_(p-1) t_p (1)), ..., x_(tau_(p-1) tau_p (n))) \
        &= f(x_1, ..., x_n) $
    et ainsi de suite,
    $ &f(x_sigma(1), ..., x_sigma(n)) \
        &= (-1)^p f(x_1, ..., x_n) \
        &= epsilon(sigma) f(x_1, ..., x_n) $
]

#ydef(name: [Forme linéaire alternée])[
    Soit $E$ un $KK$-ev de dimension $n$
    et $f: E^n -> KK$.

    On dit que $f$ est _alternée_
    si, pour tout $(x_1, ..., x_n) in E^n$
    $ &exists i <j, x_i = x_j \
        &=> f(x_1, ..., x_n) = 0 $
]

#yprop(name: [Caractérisation d'une fonction alterné par l'antisymétrie])[
    Soit $E$ un $KK$-ev de dimension $n$
    et $f: E^n -> KK$ une forme $n$-linéaire.

    + Si $f$ est alternée, alors $f$
        est antisymétrie.
    
    + Si $f$ est antisymétrie et $1_KK + 1_KK != 0_KK$,
        alors $f$ est alternée.
]

#yproof[
    1. On suppose $f$ alternée.

    Soit $(x_1, ..., x_n) in E^n$
    et $i < j$.

    Comme $f$ est alternée,
    $ f(&x_1, ..., x_(i-1), x_i + x_j, x_(i+1), ..., \
        &x_(j-1), x_i + x_j, x_(j+1), x_n) = 0 $
    
    $f$ est linéaire par rapport à sa $i$-ème
    et $j$-ème composante donc
    $ 0 =& underbrace(f(x_1, ..., x_i, ..., x_i, ..., x_n), =0) \
        &+ f(x_1, ..., x_i, ..., x_j, ..., x_n) \
        &+ f(x_1, ..., x_j, ..., x_i, ..., x_n) \
        &+ underbrace(f(x_1, ..., x_j, ..., x_j, ..., x_n), =0) $
    donc
    $ &f(x_1, ..., x_j, ..., x_i, ..., x_n) \
        &= - f(x_1, ..., x_i, ..., x_j, ..., x_n) $
    
    ---
    2. On suppose $f$ antisymétrique.

    Soit $(x_1, ..., x_n) in E^n$.
    On suppose $x_i = x_j$ où $i < j$.
    $ &f(x_1, ..., x_j, ..., x_i, ..., x_n) \
        &= - f(x_1, ..., x_i, ..., x_j, ..., x_n) $
    donc
    $ &(1_KK + 1_KK) f(x_1, ..., x_i, ..., x_i, ..., x_n) \
        &= 0 $
    
    Si $1_KK + 1_KK != 0$, alors
    $ f(x_1, ..., x_i, ..., x_i, ..., x_n) = 0 $
]

#ytheo(name: [Ensemble des formes multilinéaires alternées ])[
    L'ensemble des formes $n$-linéaires alternées
    sur un espace vectoriel de dimension $n$ est une 
    droite vectorielle.
]

#yproof[
    _La preuve n'est pas au programme._

    Soit $f: E^n -> KK$ une forme $n$-linéaire
    alternée et $cal(B) = (e_1, ..., e_n)$
    une base de $E$.

    Soit $(x_1, ..., x_n) in E^n$.

    On note $(a_(i,j))$ les coordonnées
    de $x_j$ dans $cal(B)$.
    $ forall j, x_j = sum_(i=1)^n a_(i,j) e_i $

    $ &f(x_1, ..., x_n) \
        &= f(sum_(i=1)^n a_(i,1) e_i, ..., sum_(i=1)^n a_(i,n) e_i) \
        &= sum_((i_1, ..., i_n) in [|1,n|]^n)
            a_(i_1, 1) ... a_(i_n, n) \
            &wide f(e_(i_1), ..., e_(i_n)) \
        &= sum_(sigma in S_n)
            product_(j=1)^n a_(sigma(j), j)
            f(e_sigma(1), ..., e_sigma(n)) \
        &= sum_(sigma in S_n) epsilon(sigma)
            product_(j=1)^n a_(sigma(j), j)
            f(e_1, ..., e_n) \
        &= f(e_1, ..., e_n) sum_(sigma in S_n) epsilon(sigma)
            product_(j=1)^n a_(sigma(j), j) $
    
    Soit
    $ d :&E^n &-> KK \
        &(x_1, ..., x_n) &|-> sum_(sigma in S_n) epsilon(sigma)
            product_(j=1)^n a_(sigma(j), j) $
    $f$ est colinéaire à $d$.
]

#ydef(name: [Déterminant])[
    Soit $E$ un $KK$-ev de dimension $n$
    et $cal(B) = (e_1, ..., e_n)$ une base
    de $E$.

    Le _déterminant dans la base $cal(B)$_
    est la seule forme $n$-linéaire alternée
    vérifiant $f(e_1, ..., e_n) = 1$.

    On la note $det_cal(B)$.
]

#yprop(name: [Formule du déterminant])[
    Pour tout $(x_1, ..., x_n) in E^n$,
    $ det_cal(B) (x_1, ..., x_n)
        = sum_(sigma in S_n) epsilon(sigma)
            product_(j=1)^n a_(sigma(j), j) $
    où $(a_(i,j)) = "Mat"_cal(B) (x_1, ..., x_n)$.
]

#ytheo(name: [Caractérisation d'une base par le déterminant])[
    Soit $cal(B)$ une base de $E$
    et $cal(F) = (x_1, ..., x_n)$ où 
    $n = dim(E)$.

    La famille $cal(F)$ est une base de $E$ ssi
    $ det_cal(B) (x_1, ..., x_n) != 0 $
]

#yproof[
    On suppose que $cal(F)$ n'est pas une base.

    Donc $cal(F)$ n'est pas libre :
    l'un des vecteurs,
    disons $x_1$ est combinaison linéaire
    des autres
    $ x_1 = sum_(j=2)^n lambda_j x_j $
    où $(lambda_2, ..., lambda_n) in KK^(n-1)$.

    $ &det_cal(B) (x_1, ..., x_n) \
        &= sum_(j=2)^n underbrace(det_cal(B) (x_j, x_2, ..., x_n), = 0) \
        &= 0 $
    car le déterminant est alterné.

    On suppose que $cal(F)$ est une base.

    $det_cal(F)$ et $det_cal(B)$ sont colinéaires :

    - Si $det_cal(F) = lambda det_cal(B)$ avec $lambda != 0$,
        alors
        $ 1 = det_cal(F) (cal(F)) = lambda det_cal(B) (cal(F)) $
        et donc $det_cal(B) (cal(F)) != 0$.
    
    - Si $det_cal(F) = 0$,
        $ 1 = det_cal(F) (cal(F)) = 0 $
        une contradiction.
    
    Au passage, on a montré
]

#yprop(name: [Formule de changement de bases du déterminant])[
    Soient $cal(B)$ et $cal(C)$ deux bases d'un $KK$-ev $E$.

    $ det_cal(C) &= det_cal(B) (cal(C))^(-1) det_cal(B) \
        &= det_cal(C) (cal(B)) det_cal(B) $
]

#yproof[
    On sait que $det_cal(B)$ et $det_cal(C)$ sont colinéaires :
    soit $lambda in KK without {0}$ tel que
    $ det_cal(C) = lambda det_cal(B) $

    En particulier,
    $ det_cal(C) (cal(B)) = lambda det_cal(B) (cal(B)) = lambda $
]