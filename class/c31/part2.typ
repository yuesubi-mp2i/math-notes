#import "../../template/ytemplate.typ": *


#ypart[Sous-espaces affines d'un espace vectoriel]


#ydef(name: [Sous-espace affine d'un ev])[
    Soit $E$ un $KK$-ev et $cal(F)$ un partie de $E$.

    On dit que $cal(F)$ est un _sous-espace affine_
    de $E$ si $cal(F)$ est vide ou
    s'il existe $A in cal(F)$ et un sous-espace vectoriel
    $F$ de $E$ tels que
    $ cal(F) = A + F = {A + u | u in F} $

    N.B. en réalité c'est un théorème, mais dans le
    cadre du programme, on le prend comme définition.
]

#yprop(name: [Direction d'un sous-espace affine])[
    Soit $cal(F)$ un sous-espace affine d'un espace
    vectoriel $E$ et $A in cal(F)$.

    Alors il existe un unique $F$ tel que
    $ cal(F) = A + F = {A + u | u in F} $
    il est appellé _direction_ de $cal(F)$, noté $arrow(cal(F))$
    et vaut
    $ arrow(cal(F)) = \{ underbrace(B - A, arrow(A B)) | B in cal(F)\} $

    De plus, 
    $ forall B in cal(F), cal(F) = B + F $
]

#ydef(name: [Dimension d'un sous-espace affine])[
    Soit $cal(F)$ un sous-espace affine d'un espace
    vectoriel $E$.

    La _dimension_ de $cal(F)$ est celle de $arrow(cal(F))$.

    On la note $dim(cal(F))$.
]

#ydef(name: [Droite/hyperplan affine])[
    Soit $cal(F)$ un sous-espace affine d'un espace
    vectoriel $E$.

    - On dit que $cal(F)$ est une _droite (affine)_
        si $dim(cal(F)) = 1$.

    - On dit que $cal(F)$ est un _hyperplan affine_
        si $arrow(cal(F))$ est un hyperplan de $E$.
]

#ydef(name: [Parallèlisme])[
    Soient $cal(F)$ et $cal(G)$ deux sous-espaces
    affines d'un même ev $E$.

    On dit que 

    + $cal(F)$ et $cal(G)$ sont _faiblement parallèles_
        si $arrow(cal(F)) subset arrow(cal(G))$ ou
        $arrow(cal(G)) subset arrow(cal(F))$.

    + $cal(F)$ et $cal(G)$ sont _fortement parallèles_
        si $arrow(cal(F)) = arrow(cal(G))$.
]

#ydef(name: [Repère])[
    Soit $cal(F)$ un sous-espace affine
    d'un ev $E$ de dimension finie.

    Un _repère_ de $cal(F)$ est la donnée d'un
    point $A$ de $cal(F)$ et d'une base
    $cal(B) = (e_1, ..., e_n)$ de $arrow(cal(F))$.
]

#ydef(name: [Coordonnées dans un repère])[
    Soit $cal(F)$ un sous-espace affine
    d'un ev $E$ de dimension finie
    et $cal(R) = (A, e_1, ..., e_n)$ un repère de $cal(F)$.

    Pour tout $M in F$, on appelle _coordonnées_ de
    $M$ dans le repère $cal(R)$ les uniques nombre
    $(lambda_1, ..., lambda_n) in KK^n$ tels que 
    $ M = A + sum_(i=1)^n lambda_i e_i $
    
    N.B. Et donc $arrow(A M) = sum_(i=1)^n lambda_i e_i$.
]

#ytheo(name: [Solutions d'un problème linéaire avec un sous-espace affine])[
    Soit $E, F$ deux $KK$-ev, $f in cal(L)(E, F)$, $y in F$
    et
    $ (cal(P)) space cases( f(x) = y, x in E) $

    L'ensemble des solutions de $(cal(P))$
    est un sous-espace affine de $E$ de direction
    $ker(f)$.
]

#yproof[
    Soit $cal(S)$ l'ensemble des solutions.
    
    Si $cal(S) = emptyset$, c'est bien un sous-espace affine.

    Sinon, soit $x_0 in cal(S)$, et alors
    $ cal(S) &= {x_0 + h | h in ker(f)} \
        &= x_0 + ker(f) $
]