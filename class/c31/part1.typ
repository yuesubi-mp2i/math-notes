#import "../../template/ytemplate.typ": *


#ypart[Espaces affines (H.-P.)]


#ydef(name: [Espace affine])[
    Un _$KK$-espace affine_
    est un triplet $(cal(E), arrow(E), phi)$
    où $KK$ est un corps.

    #set enum(numbering: it => "(EA" + str(it) + ")")

    + $cal(E)$ est un ensemble.

    + $arrow(E)$ un $KK$-espace vectoriel.

    + $phi : cal(E) times arrow(E) &-> cal(E)$
        vérifie

        #set enum(numbering: "(T1)")

        + $forall A in cal(E), phi(A, arrow(0)) = A$

        + $forall A in cal(E), forall (arrow(u), arrow(v)) in arrow(E)^2$, \
            $phi(phi(A, arrow(u)), arrow(v)) $ \
                $wide = phi(A, arrow(u) + arrow(v)) $
        
        + $forall (A,B) in cal(E)^2, exists! arrow(u) in arrow(E)$, \
            $ phi(A, arrow(u)) = B$

    N.B. c'est hors-programme.
]

#ydef(name: [Vocabulaire des espaces affines])[
    Soit $(cal(E), arrow(E), phi)$ un $KK$-espace affine.

    Les éléments de $cal(E)$ sont appelés _points_
    ceux de $arrow(E)$ sont appelés _vecteurs_.

    Pour $arrow(u) in arrow(E)$ l'application
    $ cal(E) &-> cal(E) \
        A &|-> phi(A, arrow(u)) $
    est la _translation_ de vecteur $arrow(u)$.

    N.B. c'est hors-programme.
]

#ynot(name: [Notations pour les translations])[
    Soit $(cal(E), arrow(E), phi)$ un $KK$-espace affine.

    + Au lieu d'écrire $phi(A, arrow(u))$ pour
        $A in cal(E)$ et $arrow(u) in arrow(E)$,
        on écrit $A + arrow(u)$.

    + Pour $(A, B) in cal(E)^2$, on note
        $arrow(A B)$ le seul vecteur tel que
        $ A + arrow(A B) = B $
        On écrit aussi $arrow(A B) = B - A$.
    
    + Soit $arrow(u) in arrow(E)$.
        On note $t_arrow(u)$ l'application
        $ cal(E) &-> cal(E) \
            A &|-> A + arrow(u) $
    
    Ainsi les axiomes de $phi$ devienent
    #set enum(numbering: "(T1)")

    + $forall A in cal(E), A + arrow(0) = A$

    + $forall A in cal(E), forall (arrow(u), arrow(v)) in arrow(E)^2$, \
        $ (A + arrow(u)) + arrow(v) = A + (arrow(u) + arrow(v))$
    
    + $forall (A, B) in cal(E)^2, exists! arrow(u) in arrow(E)$ \
        $ A + arrow(u) = B$

    N.B. c'est hors-programme.
]

#let kea() = {[Soit $(cal(E), arrow(E), phi)$ un $KK$-espace affine]}

#yprop(name: [Relation de Chasles])[
    #kea().
    $ forall (A, B, C) in cal(E)^3, arrow(A B) + arrow(B C) = arrow(A C) $

    N.B. c'est hors-programme.
]

#yproof[
    Soit $(A, B, C) in cal(E)^3$.

    $arrow(A C)$ est le seule élément de $arrow(E)$ tel que
    $ A + arrow(A C) = C $ 
    Or 
    $ &A + (arrow(A B) + arrow(B C)) \
        &=^"(T2)" (A + arrow(A B)) + arrow(B C) \
        &= B + arrow(B C) \
        &= C $
    
    D'où $arrow(A C) = arrow(A B) + arrow(B C)$.
]

#yprop(name: [Translation par le vecteur nul, composé de translations, et inverse d'une translation])[
    #kea().

    + $t_arrow(0) = id_cal(E) $

    + $ forall (arrow(u), arrow(v)) in arrow(E),
        t_arrow(u) compose t_arrow(v) = t_(arrow(u) + arrow(v)) = t_(arrow(v) + arrow(u))$

    + $forall arrow(u) in arrow(E), (t_arrow(u))^(-1) = t_(-arrow(u)) $

    N.B. c'est hors-programme.
]

#yproof[
    + Soit $A in cal(E)$.
        $ t_arrow(0) (A) = A + arrow(0) =^"(T1)" A $

    + Soit $(arrow(u), arrow(v)) in arrow(E)^2$ et $A in cal(E)$.
        $ &t_arrow(u) (t_arrow(v) (A)) \
            &= t_arrow(u) (A + arrow(v)) \
            &= (A + arrow(v)) + arrow(u) \
            &=^"(T2)" A + (arrow(v) + arrow(u)) \
            &= A + (arrow(u) + arrow(v)) \
            &= t_(arrow(u) + arrow(v)) (A) $

    + Soit $arrow(u) in arrow(E)$.
        $ t_arrow(u) compose t_(-arrow(u)) = t_arrow(0) = id $
]

#ydef(name: [Sous-espace affine])[
    #kea() et $F$ un ensemble non vide.

    On dit que $F$ est un _sous-espace affine_ de $cal(E)$ si

    #set enum(numbering: it => [(SEA#it)])

    + $F subset cal(E)$

    + $arrow(F) = { arrow(A B) | (A,B) in F^2} $ \
        est un sous-ev de $arrow(E)$

    + $forall A in F, forall arrow(u) in arrow(F), A + arrow(u) in F$

    Et, par convention, $emptyset$ est un sous-espace affine de $cal(E)$,
    et $arrow(emptyset) = {arrow(0)}$.

    N.B. c'est hors-programme.
]

#ytheo(name: [Un sous-espace affine est un espace affine])[
    #kea() et
    $F$ un sous-espace affine de celui-ci.

    On note
    $ psi: F times arrow(F) &-> F \
        (A, arrow(u)) &|-> A + arrow(u) $
    
    Alors $(F, arrow(F), psi)$ est un espace affine.

    N.B. c'est hors-programme.
]

#yproof[
    #set enum(numbering: it => "(EA" + str(it) + ")")

    + $F$ est un ensemble

    + $arrow(F)$ est un sous ev de $arrow(E)$ donc c'est un $KK$-ev

    + $psi$ est bien a valeur dans $F$ car
        $ forall A in F, forall arrow(u) in arrow(F), A + arrow(u) in F $

        #set enum(numbering: "(T1)")
        + $forall A in F$, \
            $psi(A, arrow(0)) = A + arrow(0) = A$

        + $forall A in F, forall (arrow(u), arrow(v)) in arrow(F)^2$,
            $ &psi( psi(A, arrow(u)), arrow(v)) \
                &= (A + arrow(u)) + arrow(v) \
                &= A + underbrace((arrow(u) + arrow(v)), in arrow(F)) \
                &= psi(A, arrow(u) + arrow(v)) $
        
        + Soit $(A, B) in F^2$.

            On sait que $arrow(A B) in arrow(E)$ 
            est l'unique vecteur $u in arrow(E)$ tel que
            $ A + u = B $

            Or $arrow(A B) in arrow(F)$, donc il est l'unique
            vérifiant la propriété dans $arrow(F)$.
]

#yprop(name: [Espace affine d'un espace vectoriel])[
    Soit $cal(E)$ un $KK$-ev.
    // On pose
    // $ phi : cal(E) times cal(E) &-> cal(E) \
    //     (A, u) &|-> A + u $
    // (c'est le $+$ de $cal(E)$)

    Alors $(cal(E), cal(E), +)$ est un $KK$-espace affine.

    N.B. c'est hors-programme.
]

#yproof[
    + Soit $A in cal(E)$.
        $ A + 0_cal(E) = A $
    
    + Soit $A in cal(E)$ et $(arrow(u), arrow(v)) in cal(E)^2$.

        $ (A + arrow(u)) + arrow(v) = A + (arrow(u) + arrow(v)) $
        car $+$ est associative.
    
    + Soit $(A,B) in cal(E)^2$ et $arrow(u) in cal(E)$.
        $ A + arrow(u) = B <=> arrow(u) = B - A $
        par soustraction de vecteurs.
]

#yprop(name: [Vectorialisé d'un espace affine])[
    #let pp() = {text(fill: aqua, $+$)}
    #let mm() = {text(fill: aqua, $dot$)}

    #kea() non vide et $A in cal(E)$.

    On définit une addition $pp()$ et une multiplication
    externe $mm()$ par
    $ &forall (B, C) in cal(E)^2, \
        &wide B pp() C = A + (arrow(A B) + arrow(A C)) $
    $ &forall B in cal(E), forall lambda in KK, \
        &wide lambda mm() B = A + (lambda arrow(A B)) $
    
    Alors $(cal(E), pp(), mm())$ est un $KK$-ev : c'est le
    _vectorialisé_ de $cal(E)$ au point $A$.

    Son vecteur nul est $A$.

    N.B. c'est hors-programme.
]

#yproof[
    #let pp() = {text(fill: aqua, $+$)}
    #let mm() = {text(fill: aqua, $dot$)}

    -  $(cal(E), pp())$ est un groupe abélien car

        - commutativité : soit $(A,B) in cal(E)^2$
            $ C pp() B &= A + (arrow(A B) + arrow(A C)) \
            &= A + (arrow(A C) + arrow(A B)) \
            &= B pp() C $

        - associativité : soit $(B, C, D) in cal(E)^3$
            $ &B pp() (C pp() D) \
                &= B pp() (A + arrow(A C) + arrow(A D)) \
                &= A + arrow(A B) + arrow(A (A + arrow(A C) + arrow(A D))) \
                &=^* A + arrow(A B) + (arrow(A C) + arrow(A D)) \
                &= A + (arrow(A B) + arrow(A C)) + arrow(A D) \
                &= A + arrow(A (A + arrow(A B) + arrow(A C))) + arrow(A D) \
                &= (A + arrow(A B) + arrow(A C)) pp() D \
                &= (B pp() C) pp() D $
            
            $space^*$ car $arrow(A(A + arrow(u))) = A + arrow(u)$

    (...)
]

#ytheo(name: [Sous-espace affine d'un ev])[
    Soit $cal(E)$ un $KK$-ev (muni de sa structure affine)
    et $F$ une partie de $cal(E)$ non vide.

    $F$ est un sous-espace affine de $cal(E)$ ssi
    il existe $A in F$ et $arrow(F)$ un sous-ev de $cal(E)$ tel que
    $ F = A + arrow(F) = {A + u | u in F } $

    N.B. c'est hors-programme.
]

#yproof[
    "$==>$" On suppose que $F$ est un sous-espace affine
    de $cal(E)$ non vide. Soit $A in F$
    $ forall B in F, B = A + arrow(A B) in A + arrow(F) $
    où $arrow(F) = { arrow(C D) | (C,D) in F^2 }$.

    Donc $F subset A + arrow(F)$ et $arrow(F)$ est un sous-ev de
    $arrow(E) = cal(E)$.

    Soit $arrow(u) in arrow(F)$. Alors $A + arrow(u) in F$
    par définition d'un sous-espace affine. Donc
    $ F = A + arrow(F) $

    "$<==$" Soit $A in F$ et $arrow(F)$ un sous-ev de $cal(E)$ tel que,
    $ F = A + arrow(F) $

    Alors
    $ &{ arrow(C D) | (C, D) in F^2 } \
        & = { arrow((A + arrow(u)) (A + arrow(v))) | (arrow(u), arrow(v)) in arrow(F)^2} \
        &= {A + arrow(v) - A - arrow(u) | (arrow(u), arrow(v)) in arrow(F)^2} \
        &= {arrow(v) - arrow(u) | (arrow(u), arrow(v)) in arrow(F)^2} \
        &= arrow(F) $
    
    Soit $B in F$,
    $arrow(u) in arrow(F)$.

    On pose $B = A + arrow(v), arrow(v) in arrow(F)$.
    $ B + arrow(u) = A + underbrace(arrow(v) + arrow(u), in F) in A + arrow(F) = F $

    Donc $F$ est un sous-espace affine de $cal(E)$.
]