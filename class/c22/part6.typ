#import "../../template/ytemplate.typ": *


#ypart[Racines multiples]


#ydef(name: [Racine de multiplicité $m$])[
Soit $P in bb(K) lr([X])$ et $a in bb(K)$.

On dit que $a$ est une racine
de $P$ de multiplicité $m$ si $lr((X minus a))^m bar.v P$ et
$lr((X minus a))^(m plus 1) divides.not P$.

Lorsque $m eq 1$, on dit que
$a$ est une racine #emph[simple] de $P$.

Lorsque $m + 2$, on dit que $a$ est une racine _double_ de $P$.

]
#yexample[
$i$ et $minus i$ sont racines de multiplicité $2$ de
$P eq X^4 minus 2 X^2 plus 1$ car
$P eq lr((X^2 plus 1))^2 eq lr((X minus i))^2 lr((X plus i))^2$.

]

#ydef(name: [Polynôme dérivé])[
Soit $P eq sum_(k eq 0)^n a_k X^k in bb(K) lr([X])$.

Son #emph[polynôme
dérivé], noté $P prime$ est défini par
$ P prime eq sum_(k eq 1)^n k a_k X^(k minus 1) $

On définit les polynômes dérivés successifs de $P$ par récurrence :
$ P^(lr((k plus 1))) eq lr((P^(lr((k))))) prime $

]

#ydef(name: [Famille de polynômes degrés échelonnés])[
Soient $lr((P_i))_(i in I)$ une famille de polynômes.

On dit qu'elle est
#emph[de degrés échelonnés] si pour tout $i eq.not j$,
$deg lr((P_i)) eq.not deg lr((P_j))$.

]

#yprop(name: [Particularité d'une famille de degrés échelonnés])[
Toute famille de polynômes non nuls de degrés échelonnés est libre.

]

#yproof[Soit $(P_i)$ une famille de polynômes de degrés échelonnés et $(lambda_i)$ une famille presque nulle de scalaires telles que $sum_i lambda_i P_i = 0$.

Comme les degrés sont deux-à-deux distincts, le coefficient associé au $P_i$ de plus haut degré est nécessairement nul (sinon la combinaison de polynômes serait de degré positif ou nul).

Il suffit alors d'itérer ce raisonnement pour obtenir la nullité de tous les coefficients. ]



#ytheo(name: [Formule de Taylor pour les polynômes])[
Soit $P in bb(K) lr([X])$ non nul de degré $n$, et $a in bb(K)$. Alors
$ P eq P lr((a)) plus sum_(k eq 1)^deg(P) frac(P^(lr((k))) lr((a)), k excl) lr((X minus a))^k dot.basic $

]

#yproof[
    
    La famille de polynômes $ ((X-a)^k)_(0 <= k<= n) $ est de degrés échelonnés, donc libre.
    
    Comme elle contient $n+1$ polynômes, $n+1$ étant la dimension de $KK_n[X]$, il s'agit d'une base de cet espace.
    
    On sait donc qu'il existe des coefficients $a_0,...,a_n$ tels que
    $ P = sum_(k=0)^n a_k (X-a)^k $
    
    En calculant les dérivées successives de ce polynôme, on trouve que pour tout $k$,
    $ k! a_k = P^((k))(a) $]

#ycor(name: [Formule de Taylor pour les polynômes en $0$])[
    Pour tout $P in KK[X]$,
    $ P = sum_(k=0)^n (P^((k)) (0))/k! X^k $
    où $n = deg P$. $qed$
]

#yprop(name: [Caractérisation de la multiplicité d'une racine par des dérivées])[
Soit $P in bb(K) lr([X])$ et $a in bb(K)$.

Alors
$a$ est un racine de $P$ de multiplicité $m$ ssi
$ cases(forall k in bracket.l.double 0 comma m minus 1 bracket.r.double comma P^(lr((k))) lr((a)) eq 0,
P^((m)) lr((a)) eq.not 0 dot.basic) $

]

#yproof[D'après la formule de Taylor, 

$ P(X) = sum_(k=0)^n (P^((k))(a))/(k!) (X-a)^k. $

- On en déduit que si $P^((k))(a)=0$ pour tout $k<m$, alors
    $ P = (X-a)^m Q $ avec $Q(a) = P^((m)(a))/(m!) != 0$.

- Si $a$ est racine de $P$ de multiplicité $m$, alors $P = (X-a)^m Q$ avec $Q in KK[X]$.
    
    En appliquant la formule de Taylor au polynôme $Q$ on
    obtient $ P = sum_(k=m)^n (Q^((k-m))(a))/((k-m)!) (X-a)^k $
    et donc pour tout $k<m$, $P^((k))(a)=0$ et $P^((m))(a) = Q(a) != 0$.
]


#ycor(name: [Multiplicité d'une racine pour la dérivée d'un polynôme])[
    (Corollaire de la caractérisation de la multiplicité d'une racine par des dérivées)

    Soit $P in KK[X]$ tel que $deg P > 0$ et $a in CC$.

    Si $a$ est racine de $P$ de multiplicité $m$,
    alors $a$ est une racine de $P'$ de multiplicité $m - 1$. $qed$
]

#yprop(name: [Degré de la dérivé d'un polynôme])[
    Pour $P in KK[X]$,
    - si $deg P > 0$, $deg P' = deg P - 1$
    - sinon $deg P' = 0$.
]

#ytheo(name: [Théorème d'Alembert-Gauss])[
(Aussi appelés théorème fondamental de l'algèbre)

Tout polynôme non constant de $bb(C) lr([X])$ admet au moins une racine
complexe.

]

#ycor(name: [Polynômes irréductible de $CC$])[
(Corollaire du théorème fondamental de l'algèbre)

Les polynômes irréductibles de $bb(C) lr([X])$ sont les polynômes de
degré 1.

]

#yproof[
    Soit $P in CC[X]$ irréductible sur $CC[X]$.

    Par définition, $deg P >= 1$.

    Soit $a$ une racine de $P$.

    Il existe $Q in CC[X]$, tel que $ P = (X - a) Q $

    $P$ est irréductible donc $deg Q = 0$, donc $deg P = 1$.

    Réciproquement, les polynômes de degré $1$ sont irréductibles. $qed$
]

#yprop(name: [Racines conjugés d'un polynôme de $RR[X]$])[
    Soit $P in RR[X]$ et $a in CC$ une racine de $P$.

    Alors $overline(a)$ est une racine de $P$ de même multiplicité que $a$.
]

#yproof[
    On pose $P = sum_(k=0)^n alpha_k X^k$ avec $(a_0, ..., a_n) in RR^(n+1)$.

    $ P(overline(a)) &= sum_(k=0)^n a_k overline(a)^k \
        &= overline(sum_(k=0)^n a_k a^k) \
        &= overline(P(a)) \
        &= 0 $
    
    De même,
    $ forall j in NN, P^((j)) (overline(a)) = overline(P^((j)) (a)) $
    car $forall j in NN, P^((j)) in RR[X]$.

    On note $m$ la multiplicité de $a$.

    Pour $j in [|1, m-1|]$,
    $ P^((j)) (overline(a)) = overline(P^((j)) (a)) = overline(0) = 0 $
    $ P^((m)) (overline(a)) = overline(P^((m)) (a)) != 0 $

    Donc $overline(a)$ est racine de $P$ avec multiplicité $m$.
    $qed$
]

#yprop(name: [Forme dévellopé du polynôme de $CC[X]$ suivant $ (X - a)(X -overline(a)) $])[
    Soit $a in CC without RR$.
    $ &(X - a)(X -overline(a)) \
        &= X^2 - 2 "Re"(a) X + abs(a)^2 in RR[X] $
]

#yproof[
    On développe. $qed$
]

#ycor(name: [Décomposition des polynômes de $RR[X]$ en polynômes irréductibles])[
    Soit $P in RR[X]$ tel que $deg P >= 1$.

    $P$ peut s'écrire comme un produit de polynômes à coefficients
    réels de degré $1$ ou $2$, les facteurs de degré $2$
    n'ayant pas de racines réelles.
]


#ycor(name: [Décomposition des polynômes de $CC[X]$ en polynômes irréductibles])[Tout polynôme de $CC[X]$ non constant peut s'écrire de manière unique à coefficient multiplicatif près comme un produit de polynômes de degré 1.]

#yprop(name: [Polynômes irréductibles de $RR[X]$])[
Les polynômes irréductibles de $bb(R) lr([X])$ sont les polynômes de
degré 1, et les polynômes de degré 2 sans racine réelle (_i.e._ de discriminants strictement négatif).

]

#yproof[
    Soit $P$ un polynôme à coefficients réels de degré au moins 3.
    
    En tant que polynôme à coefficients complexes, $P$ a au moins une racine $a in CC$.
    
    Si $a in RR$, alors on peut factoriser $P$ par $X-a$ dans $RR[X]$ qui n'est donc pas irréductible sur $RR$.
    
    Sinon, $overline(a)$ est aussi une racine de $P$, et donc $P$ se factorise dans $CC[X]$ par $(X-a)(X-overline(a)) in RR[X]$.
    
    Soit donc $Q in CC[X]$ tel que $P = (X^2 - 2"Re"(a)X+|a|^2) Q$.
    
    En conjuguant, on obtient pour tout $x in RR$, $P(x) = (x^2 - 2 "Re"(a) x + |a|^2)overline(Q(x))$ et donc pour tout $x in RR$, $Q(x)=overline(Q(x))$.
    
    On en déduit par la formule de Taylor que $Q in RR[X]$, et donc $P$ n'est pas irréductible.

On suppose à présent $P$ de degré 2 réductible : nécessairement $P$ est le produit de deux polynômes de degré 1 à coefficients réels, ayant chacun une racine réelle, donc $P$ a au moins une racine réelle.

]

#yexample[
Le polynôme $X^4 plus 1$ n'est pas irréductible dans $bb(R) lr([X])$
alors qu'il n'a aucune racine réelle. En effet,
$ X^4 plus 1 eq X^4 plus 2 X^2 plus 1 minus 2 X^2 eq lr((X^2 plus 1))^2 minus 2 X^2 eq lr((X^2 minus sqrt(2) X plus 1)) lr((X^2 plus sqrt(2) X plus 1)) dot.basic $

]

#yprop(name: [Lien entre la divisibilité de polynômes dans $CC[X]$ et dans $RR[X]$])[
    Soit $(A, B) in RR[X]^2$, $A$ et $B$ non nul.

    Si $A divides B$ dans $CC[X]$, alors $A divides B$ dans $RR[X]$.
]

#yproof[
    Soit $Q in CC[X]$ tel que $A Q = B$.
    $ forall x in RR, A(x) Q(x) = B(x) $

    On conjugue
    $ forall x in RR, A(x) overline(Q(x)) = B(x) $

    Par unicité de la division euclidienne,
    $ forall x in RR, Q(x) = overline(Q(x)) $

    Donc $Q in RR[X]$.
]

#ycor(name: [Nombre de racines d'un polynôme])[
Soit $P in bb(K) lr([X])$ non nul, de degré $n$.

Alors $P$ a au maximum
$n$ racines.

]

#yproof[
    Chaque racine permet de factoriser $P$ par un polynôme de degré 1, et on peut factoriser $P$ au plus $n$ fois par un polynôme de degré 1.
]

#yprop(name: [Nombre de racines d'un polynôme de $CC[X]$])[
    Soit $P in CC[X] without {0}$.

    $P$ a $deg P$ racines comptées avec multiplicité.
]

#yproof[
    On note $a_1, ..., a_r$ le racines distinctes
    de $P$ et $m_1, ..., m_r$ leurs multiplicité repectives.
    $ P = "dom" P product_(i=1)^n (X - a_i)^(m_i) $
    D'où,
    $ deg P = sum_(i = 1)^r m_i $
    $qed$
]

#ydef(name: [Polynôme scindé])[
Soit $P in bb(K) lr([X])$.

On dit que $P$ est #emph[scindé] sur $KK$ si $P = 0$ ou $P$ est
un produit de polynômes de degré 1 de $KK[X]$.

]

#yprop(name: [Polynômes scindés de $CC[X]$])[
Tout polynôme de $bb(C) lr([X])$ est scindé.
]

#yprop(name: [Caractérisation des polynômes scindés de $RR[X]$])[
Un polynôme à coefficients réels est scindé sur $bb(R) lr([X])$ si et
  seulement toutes ses racines sont réelles.
]

#ymetho(name: [Exercice ultra-classique : montrer que la dérivé d'un polynôme de $RR[K]$ scindé est aussi scindée])[
    #emph[
        Soit $P in RR[X]$ de degré supérieur ou égal à 2.

        Montrer que si $P$ est scindé à racines simples,
        alors $P'$ aussi.
    ]
    
    On suppose $P$ scindé à racines simples.

    On note $n = deg(P)$.
    
    $P$ a exactement $n$ racines réelles distinctes.
    On les note $x_1 < ... < x_n$.

    $ forall i in [|1, n-1|], P(x_i) = P(x_(i + 1)) = 0 $
    On note $f$ la fonction
    $ RR &-> RR \
        x &|-> P(x) $
    $f$ est de classe $C^1$ sur $RR$.

    D'après le théorème de Rolle, pour tout $i in [|1, n - 1|]$,
    $ exists y_i in ]x_i, x_(i+1)[, f'(y_i) = 0 $

    Donc $y_1, ..., y_(n - 1)$ sont des racines réelles de $P'$,
    elles sont distinctes car
    $ forall i != j, cases(
        y_i in \]x_i\, x_(i+1)\[,
        y_j in \]x_j\, x_(j+1)\[,
        \]x_i\, x_(i+1)\[ sect \]x_j\, x_(j+1)\[) $
    
    Or $deg P' = n - 1$.

    Ainsi $P'$ n'a pas d'autre racine et les $y_i$ sont de multiplicité $1$.
]

#yprop(name: [Relation entre les racines et les coefficient d'un polynôme dans $CC[X]$])[
Soit $P eq display(sum_(k eq 0)^n a_k X^k )in bb(C) lr([X])$ unitaire, non
constant de degré $n$, $x_1 comma dots.h comma x_n$ ses racines
(comptées plusieurs fois).

Alors
pour $k in bracket.l.double 0 comma n minus 1 bracket.r.double comma$
$ med a_k =& lr((minus 1))^(n minus k) \
    &times sum_(0 lt.eq i_1 lt ... lt i_(n minus k) lt.eq n) x_(i_1) ... x_(i_(n minus k)) $

noté en cours :
pour $k in bracket.l.double 1 comma n bracket.r.double comma$
$ med a_k =& lr((minus 1))^(n minus k) \
    &times sum_(0 lt.eq i_1 lt ... lt i_k lt.eq n) x_(i_1) ... x_(i_k) $

En particulier,
$ cases(a_0 eq lr((minus 1))^n product_(i eq 1)^n x_i,
    a_(n minus 1) eq minus sum_(i eq 1)^n x_i) $

//N.B. La fonction
//$ sigma_k = (x_1, ..., x_n) =
//    sum_(0 lt.eq i_1 lt ... lt i_k lt.eq n) x_(i_1) ... x_(i_k)$

]

#yproof[Il suffit de développer
$ (X-x_1)(X-x_2)...(X-x_n) $]

#ybtw[
Soit $P eq a X^2 plus b X plus c in bb(C) lr([X])$, $a eq.not 0$, et
$x_1 comma x_2$ les deux racines de $P$, éventuellement confondues. On a
$x_1 plus x_2 eq frac(minus b, a)$ et $x_1 x_2 eq c / a$.

]
#yexample[
Soit $P eq X^3 plus a_2 X^2 plus a_1 X plus a_0 in bb(C) lr([X])$ et
$x_1 comma x_2 comma x_3$ ses racines (éventuellement confondues). Alors
$x_1 plus x_2 plus x_3 eq minus a_2$ et $x_1 x_2 x_3 eq minus a_0$. On a
aussi $x_1 x_2 plus x_2 x_3 plus x_1 x_3 eq a_1$.

]