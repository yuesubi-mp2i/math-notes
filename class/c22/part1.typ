#import "../../template/ytemplate.typ": *


#ypart[Construction formelle]


Naïvement, un polynôme est une expression de la forme
$a_n X^n plus dots.h plus a_1 X plus a_0$, où
$a_0 comma dots.h comma a_n$ sont des scalaires. Il nous faut donc
définir l'indéterminée $X$ (qui ensuite pourra être remplacée par un
réel ou une matrice), la somme de deux polynômes et le produit de deux
polynômes. En réalité, un polynôme est entièrement déterminé par la
suite de ses coefficients $a_0 comma dots.h comma a_n$, d'où l'idée de
définir un polynôme formellement par ses coefficients.

#ydef(name: [Polynôme])[
On appelle #emph[polynôme à coefficients dans $bb(K)$] toute suite
$lr((a_k))_(k in bb(N)) in bb(K)^(bb(N))$ presque nulle.

_I.e._ une suite pour laquelle il existe un ensemble
#strong[fini] $I subset bb(N)$ tel que pour tout $k in.not I$,
$a_k eq 0$.

Les scalaires $a_k$, $k in bb(N)$ sont alors appelés coefficients du
polynôme.

]

#ydef(name: [Somme de polynômes])[
Soient $lr((a_k))$ et $lr((b_k))$ deux polynômes.

On définit leur #emph[somme], notée $+$, par
$(a_k plus b_k) $.

]

#yprop(name: [Stabilité de la somme de polynômes])[
    La somme de deux polynômes est aussi un polynôme.
]
#yproof[
Soient $lr((a_k))$ et $lr((b_k))$ deux polynômes.

    Si tous les $a_k$ sont nuls pour $k >= n_1$ et les $b_k$ sont nuls pour $k >= n_2$, alors $a_k + b_k = 0$ pour tout $k >= max(n_1,n_2)$.]


#ydef(name: [Produit de polynômes])[
Soient $lr((a_k))$ et $lr((b_k))$ deux polynômes.

On définit leur #emph[produit], noté $times$, par $lr((c_k))$ où
$ forall k in bb(N) comma med c_k eq sum_(i eq 0)^k a_i b_(k minus i) $

]

#yprop(name: [Stabilité du produit de polynômes])[
    Le produit de deux polynômes est aussi un polynôme.
]
#yproof[
    Soient $(N_1, N_2) in NN^2$ tels que
    $ cases(forall n >= N_1 \, a_n = 0,
        forall n >= N_2 \, b_n = 0) $
    où $(a_k)$ et $(b_k)$ sont des polynômes.
    
    Pour $n >= N_1 + N_2$,
    $ &sum_(k=0)^n a_k b_(n-k) \
        &= sum_(k=0)^(N_1 - 1) a_k underbrace(b_(n-k), 0) + sum_(k =N_1)^n underbrace(a_k, 0) b_(n-k) \
        &= 0 $
    $qed$
]

#ydef(name: [Multiplication d'un polynôme par un scalaire])[
    On définit pour $lambda in KK$ et $(a_k)$ un polynôme, la multiplication
    par un scalaire, notée $dot$, par
    $ lambda dot (a_i) = (lambda a_i) $
]


#yprop(name: [Stabilité de la multiplication par un scalaire d'un polynôme])[
    Le produit de la multiplication d'un scalaire et d'un polynôme
    est un polynôme.
]

#yproof[
Soit $lr((a_k))$ un polynôme et $lambda in KK$ un scalaire.

Si tous les $a_k$ sont nuls pour $k >= n_1$, alors $lambda a_k = 0$ pour tout $k >= n_1$.
]

#ynot(name: [Le $X$ pour les polynômes])[

On note $X$ le polynôme
    $ (delta_(n, 1)) = (0, 1, 0, 0, ...) $
]

#yprop(name: [Expression du polynôme $X^n$])[
Pour $n in bb(N)$,
$ X^n &eq (delta_(i,n))_(i in NN) \
    &= \(0, underbrace(0\, ...\, 1, n "nombres"), 0, ...\) $

]

#yproof[Par récurrence sur $n$.]

#yprop(name: [Neutre de la multiplication de polynômes])[
    Le polynôme $(delta_(n, 0)) = (1, 0, 0, ...)$
    est le neutre pour la multiplication.
]

#yexample[Calculons $X^2$ : on note
  $lr((c_k)) eq X^2$ et donc $c_0 eq b_0 times b_0 eq 0$,
  $c_1 eq b_1 times b_0 plus b_0 times b_1 eq 0$,
  $c_2 eq b_2 b_0 plus b_1^2 plus b_0 b_2 eq 1$ et pour tout
  $k gt.eq 3$,
  $c_k eq sum_(i eq 0)^k b_i b_(k minus i) eq 2 b_1 b_(k minus 1) eq 0$.

]



#ynot(name: [Ensemble des polynômes à coefficients dans $KK$, et indéterminée])[
On note $bb(K) lr([X])$ l'ensemble des polynômes à coefficients dans
$bb(K)$.

Le polynôme $X$ est appelé #emph[l'indéterminée].

]

#ynot(name: [Identification d'un scalaire à un polynôme])[
Tout scalaire $x$ peut être identifié (et le sera) au polynôme
$lr((a_k))$ défini par $a_0 eq x$ et pour tout $k gt.eq 1$,
$a_k eq 0$.
  
La somme et le produit de deux scalaires correspond à la
somme et au produit des polynômes associés.
]

#ytheo(name: [Unicité des coefficients d'un polynôme])[
Soit $P in bb(K) lr([X])$ non nul.

Il existe un unique entier $n$ et un
unique $lr((a_0 comma dots.h comma a_n)) in bb(K)^(n plus 1)$ tels que
$ P eq sum_(k eq 0)^n a_k X^k $ et $a_n eq.not 0$.

]

#yproof[
    Par définition, $P$ est une suite presque nulle de scalaires : $P=(b_k)_(k in NN)$.
    
    Soit $(a_k)$ une suite presque nulle.
    
    Alors $ sum_(k=0)^n a_k X^k = (a_0,a_1,...,a_n,0,0,...) $ et donc l'égalité
    $ P = sum_(k=0)^n a_k X^k $
    est vraie si et seulement si $(b_k)=(a_k)$.
    
]

#yprop(name: [Structure de $KK[X]$])[
    $(KK[X], +, times, dot)$ est une $KK$-algèbre commutative, _i.e._

    + $(KK[X], +, times)$ est un anneau commutatif ;

    + $(KK[X], +, dot)$ est un $KK$-ev ;

    + pour $lambda in KK$ et $(P, Q) in KK[X]^2$,
        $ lambda (P Q) = (lambda P) Q = P (lambda Q) $

]

#yproof[

+ On sait que $(KK^NN,+)$ est un groupe abélien et $KK[X]$ en est une partie.

    - *Associativité* : Soient $P=(a_k)$, $Q=(b_k)$ et $R=(c_k)$ trois polynômes. 

        On note $(d_k)$ les coefficients de $P Q$ et $e_k$ ceux de $(P Q) R$ ; $d'_k$ les coefficients de $Q R$ et $e'_k$ ceux de $P (Q R)$.

        $ forall k, e'_k &= sum_(i=0)^k a_i d'_(k-i) \
                 &= sum_(i=0)^k a_i sum_(j=0)^(k-i) b_j c_(k-i-j)\
                 &= sum_(i=0)^k sum_(ell=0)^(k-i) a_i b_(k-i-ell) c_ell\
                 &= sum_(ell=0)^k (sum_(i=0)^(k-ell) a_i b_(k-ell-i)) c_ell \
                 &= sum_(ell=0)^k d_(k-ell) c_ell \
                 &= e_k. $
    
    - *Commutativité* : ...

    - *Neutre multiplicatif* : $X^0$

    - *Distributivité* : ...

+ Par trivialité.

+ En exercice. $qed$
]