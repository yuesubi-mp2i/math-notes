#import "../../template/ytemplate.typ": *


#ypart[Division euclidienne]


=== Division euclidienne

#ytheo(name: [Division euclidienne de polynômes])[
Soient $A$, $B in bb(K) lr([X])$ tels que $B eq.not 0$.

$ exists ! (Q comma R) in bb(K) lr([X])^2,
yt cases(A eq B Q plus R,
 deg lr((R)) lt deg lr((B)) dot.basic) $

$Q$ est appellé le _quotient_ et $R$ le _reste_ de la division
de $A$ par $B$.
]

#yproof[On prouve l'existence par récurrence sur le degré de $A$.]

#yprop(name: [Caractérisation d'une racine par la divisibilité])[
Soit $P in bb(K) lr([X])$ et $a in bb(K)$.
$ P lr((a)) eq 0 arrow.l.r.double X minus a bar.v P dot.basic $

]

#yproof[
- On suppose $P(a)=0$.

    La division euclidienne de $P$ par $X-a$ permet d'écrire $ P = (X-a)Q + R $ avec $deg(R)<1$.
    
    Ainsi $R$ est constant et en évaluant en $a$, on obtient $R=0$, par suite $X-a$ divise $P$.

- La réciproque est immédiate.
  ]