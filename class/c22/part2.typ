#import "../../template/ytemplate.typ": *


#ypart[Degré d'un polynôme]


#ydef(name: [Degré, coefficient dominant et terme dominant])[
- Le #emph[degré] d'un polynôme $P eq lr((a_k))$ non nul est l'entier
    $ deg lr((P)) eq max brace.l k in bb(N) divides a_k eq.not 0 brace.r $
    et son #emph[coefficient dominant] est alors $"dom"lr((P)) eq a_(deg P)$.
    
    Le monôme $a_(deg P) X^(deg P)$ 
    est alors appelé #emph[terme dominant] de $P$. 


- Par convention, le degré du polynôme nul est $minus oo$.

]

#ydef(name: [Polynôme unitaire])[
On dit qu'un polynôme est #emph[unitaire] si $"dom"lr((P)) eq 1$. 
]

#yprop(name: [Degré et coefficient dominant du produit de polynômes])[
Pour $P, Q$ des polynômes,

- $deg lr((P Q)) eq deg lr((P)) + deg lr((Q))$
- $"dom"lr((P Q)) eq "dom"lr((P)) "dom"lr((Q))$
]

#ycor(name: [Particularité de l'anneau des polynômes])[
    (Corollaire du degré et coefficient dominant du produit de polynômes)

    $(KK[X], +, times)$ est (un anneau commutatif) intègre.
    $qed$
]

#yproof[
    Soient $P$ et $Q$ deux polynômes non nuls.
    
    On suppose $P Q = 0$.
    
    Alors $deg(P)+deg(Q)=-oo$,
    une contradiction.
]


#yprop(name: [Dégré de la somme de polynômes])[
Soient $P$ et $Q$ deux polynômes.
$ deg lr((P plus Q)) lt.eq max(deg P comma deg Q) $
]
#yproof[En exercice.]

#yprop(name: [Caractérisation de l'égalité sur le degré d'une somme de polynômes])[
Soient $P$ et $Q$ deux polynômes non nuls.

On a l'égalité
$ &deg lr((P plus Q)) = max(deg P comma deg Q) $
ssi
$ deg P eq.not deg Q  "ou" \
    cases(deg lr((P)) eq deg lr((Q)),
        "dom" lr((P)) plus "dom" lr((Q)) eq.not 0) $

]

#yproof[En exercice.]

#yprop(name: [Caractérisation de l'inégalité stricte sur le degré d'une somme de polynômes])[
Soient $P$ et $Q$ deux polynômes non nuls.

On a l'inégalité
$ &deg lr((P plus Q)) < max(deg P comma deg Q) $
ssi
$ cases(deg lr((P)) eq deg lr((Q)),
    "dom"lr((P)) plus "dom"lr((Q)) eq 0) $

]

#yproof[En exercice.]

#ynot(name: [Ensemble des polynômes de degré au plus $n$])[
Pour tout $n in bb(N)$, on note $bb(K)_n lr([X])$ l'ensemble des
polynômes à coefficients dans $bb(K)$ de degré inférieur ou égal à $n$.

$ KK_n [X] = { P in KK[X] | deg P <= n} $
]

#yprop(name: [Espaces vectoriels des polynômes])[
$bb(K) lr([X])$ est un espace vectoriel, et pour tout $n in bb(N)$,
$bb(K)_n lr([X])$ est un sous-espace vectoriel de $bb(K) lr([X])$.

]

#yproof[On a vu que
$ KK[X]="Vect"_(KK) (X^k | k in NN) $
donc $KK[X]$ est un sous-espace vectoriel de $(KK^NN, +, .)$.

De même, $ KK_n [X] = "Vect"_KK (X^k | 0 <= k <= n) $]

#yprop(name: [Base et dimension de $KK_n [X]$])[
Soit $n in bb(N)^ast.basic$.

La famille
$lr((1 comma X comma dots.h comma X^n))$ est une base de
$bb(K)_n lr([X])$, appelée #emph[base canonique], et donc
$dim lr((bb(K)_n lr([X]))) eq n plus 1$.

]

#yproof[Soit $(a_k)_(0 <= k<= n)$ des entiers tels que
$ sum_k a_k X^k = 0_(KK[X]) $

Par définition même d'un polynôme, tous les $a_k$ sont nuls, donc la famille est libre.]

#yprop(name: [Polynôme inversibles])[
Un polynôme $P in bb(K) [X]$ est inversible si et seulement si
$deg (P) = 0$.

]

#yproof[Les polynômes de degré nul sont de la forme $a 1_(KK[X])$ avec $a in KK without {0}$, donc inversibles (d'inverse $a^(-1) 1_(KK[X])$).

Réciproquement, si $P$ est inversible, il existe $Q in KK[X]$ tel que $P Q = 1$, et donc $deg(P)+deg(Q)=0$, et donc $deg(P)=0$.

]