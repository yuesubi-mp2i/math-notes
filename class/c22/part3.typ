#import "../../template/ytemplate.typ": *


#ypart[Substitutions]

#ytheo(name: [Morphismes d'algèbre des polynômes])[
    Soit $(E, +, times, dot)$ une $KK$-algèbre et $x in E$.

    L'application
    $ f_x : &KK[X] &-> &E \
        &sum_(k = 0)^n a_k X^k &|-> &sum_(k = 0)^n a_k x^k $
    est un morphisme d'algèbre, _i.e._, $f_x$ est à la fois
    une application linéaire et un morphisme d'anneaux. $qed$
]

#ydef(name: [Substitution et fonction polynomiale])[
Soit $P eq display(sum_k a_k X^k) in bb(K) lr([X])$ et $x in E$.

L'élément
$sum_k a_k x^k$ de $E$ est noté $P lr((x))$ et on dit que l'on a
_substitué_ $x$ à $X$.

On obtient de cette façon la _fonction polynomiale_
$ f_P colon &E &-> E \
    &x &arrow.r.bar P lr((x)) $

]

#ydef(name: [Racine d'un polynôme])[
Soit $P in bb(K) lr([X])$ et $a in bb(K)$.

On dit que $a$ est une #emph[racine] de $P$ si $P lr((a)) eq 0$.

]