// Polynômes
#import "../../template/ytemplate.typ": *

#ychap[Polynômes formels]

#include "part1.typ"
#include "part2.typ"
#include "part3.typ"
#include "part4.typ"
#include "part5.typ"
#include "part6.typ"
#include "part7.typ"