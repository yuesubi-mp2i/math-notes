#import "../../template/ytemplate.typ": *


#ypart[Arithmétique dans $KK[X]$]


=== 1. Divisibilité

#ydef(name: [Divisibilité de polynômes])[
Soient $A comma B in bb(K) lr([X])$.

On dit que $A$ #emph[divise] $B$,
situation notée $A bar.v B$, s'il existe $Q in bb(K) lr([X])$ tel que
$B eq A Q$.

On dit alors que $A$ est un _diviseur_ de $B$ et que
$B$ est un _multiple_ de $A$.
]

#yexample[
Le polynôme $X minus i$ divise $X^2 plus 1$ dans $bb(C) lr([X])$ car
$X^2 plus 1 eq lr((X minus i)) lr((X plus i))$.

]

#yprop(name: [Divisibilité d'une combinaison linéaire de polynômes])[
Soit $A, B, C$ trois polynômes.

On suppose que $A$ divise $B$ et $C$.

Alors
- $A$ divise tout polynôme de la forme $lambda B + mu C$ avec $(lambda, mu) in KK^2$.

- Alors $A$ divise tout polynôme de la forme $U B + V C$ avec $(U, V) in KK[X]^2$.]

#yproof[Soit $P$ et $Q$ deux polynômes tels que $A P = B$ et $A Q = C$.

Alors $lambda B + mu C = A (lambda P + mu Q)$.

Et $U B + V C = A (U P + V Q)$.]

#yprop(name: [Caractérisation de la divisibilité de polynômes par le reste de la division euclidienne])[
Soient $A comma B in bb(K) lr([X])$, avec $B$ non nul.

Alors $B bar.v A$ si et seulement
si le reste dans la division de $A$ par $B$ est nul.
]

#ydef(name: [Polynômes irréductible])[
Soit $A in bb(K) lr([X])$.

On dit que $A$ #emph[n'est pas irréductible]
(dans $bb(K) lr([X])$) s'il existe $B comma Q in bb(K) lr([X])$ non
constants tels que $A eq B Q$.

Si ce n'est pas le cas, on dit que $A$
est #emph[irréductible].

*Définition équivalente* : On dit que $A$ est _irréductible_ si il n'est pas constant
et que, pour tout $(P, Q) in KK[X]^2$,
$ P Q = A => deg P = 0 or deg Q = 0 $

]
#yexample[
Le polynôme $X^2 plus 1$ est irréductible dans $bb(R) lr([X])$, mais pas
dans $bb(C) lr([X])$.

]

#ydef(name: [PGCD d'un polynôme])[
Soient $A$ et $B$ deux polynômes dont l'un au moins est non nul. Soit
$D$ un autre polynôme.

On dit que $D$ est un #emph[PGCD] de $A$ et $B$
si $D$ est un diviseur de $A$ et $B$ de degré maximal.

]

#ytheo(name: [PGCD de deux polynôme comme combinaison linéaire])[
Soient $A$ et $B$ deux polynômes non tous les deux nuls et
$D$ un PGCD de $A$ et $B$.

Alors il existe $lr((U comma V)) in bb(K) lr([X])^2$ tel
que $ A U plus B V eq D $

]

#yproof[Soient $A'$ et $B'$ deux polynômes tels que $D A' = A$ et $D B' = B$.

On note $ I = {A' U + B' V | (U, V) in KK[X]^2} $

Il s'agit d'un idéal de $KK[X]$, ce qui signifie que $I$ est un sous-groupe
additif de $KK[X]$ et que pour tout $P in I$ et tout $Q in KK[X]$, $P Q in I$. 

On considère alors un polynôme $N$ non nul de degré minimal dans $I$.

D'après le théorème de division euclidienne,
tout polynôme $P$ de $I$ s'écrit sous la forme $P = N Q + R$ avec $Q$ et $R$ deux polynômes et $deg(R)<deg(N)$.

Or, $P$ et $N$ étant deux éléments de $I$, $R in I$.

On en déduit que $R$ est nul par minimalité du degré de $N$ et donc $I = N KK[X]$.

Comme $A' in I$, $N$ divise $A'$.

De même $N$ divise $B'$.

On en déduit que $N D$ divise $A$ et $B$, et donc $deg(N D) <= deg(D)$, puisque $D$ est de degré maximal parmi les diviseurs communs à $A$ et $B$.

Par suite, $N$ est une constante non nulle et donc $I = KK[X]$.

En particulier, il existe deux polynômes $U$ et $V$ tels que $1 = A' U + B' V$ et donc $ D = A U + B V $

]

#ydef(name: [Polynôme associés])[
    On dit que deux polynômes $P, Q in KK[X]$ sont associés s'il existe
    $lambda in KK without {0}$ tel que
    $P = lambda Q$.
]

#ycor(name: [Les PGCD de polynômes sont associés])[
(Corollaire de l'écriture du PGCD comme combinaison linéaire)

Soient $A$ et $B$ deux polynômes non tous les deux nuls.

Alors tous les PGCD de $A$ et $B$ sont associés.

]
#ynot(name: [PGCD unitaire pour les polynômes])[
On note $A and B$ le seul PGCD de $A$ et $B$ unitaire.

]

#yprop(name: [Caractérisation de l'association de deux polynômes par la divisibilité])[
    Soit $(P, Q) in KK[X]$ deux polynômes non nuls.

    $ cases(P divides Q, Q divides P) <=> P "et" Q "sont associés" $
]

#yproof[
    $ &cases(P divides Q, Q divides P)\
        &<=> cases(exists A in KK[X]\, P = A Q,
            exists B in KK[X]\, Q = B Q) \
        &<=> exists (A, B) in KK[X]^2, cases(P = A Q, Q = B A Q) \
        &<=> exists (A, B) in KK[X]^2, cases(P = A Q, 1 = B A) \
        &<=> exists lambda in KK without {0}, P = lambda Q $
    $qed$
]

#ydef(name: [Polynôme premiers entre eux])[
On dit que deux polynômes sont #emph[premiers entre eux] si
$A and B eq 1$.

]

#ytheo(name: [Théorème de Bézout pour les polynômes])[
    Soit $A, B in KK[X]$ deux polynômes non nuls.
    $ &1 = A and B <=> \
        &exists(U, V) in KK[X]^2, 1 = A U + B V $
]

#ytheo(name: [Théorème de Gauss pour les polynômes])[
Soient $A comma B comma C$ trois polynômes tels que $A and B eq 1$ et
$A bar.v B C$.

Alors $A bar.v C$.

]

#yprop(name: [Algorithme d'Euclide pour les polynômes])[
Soient $A$ et $B$ deux polynômes avec $B eq.not 0$. On pose $R$ le reste
de la division euclidienne de $A$ par $B$.

Alors $A and B eq B and R$.

]