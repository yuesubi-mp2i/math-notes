

 #let problème(pb, notion: "") = [
  #rect(width: 100%, stroke: (left: blue))[
    *Problème #c.display()*. #if notion != "" {[(#notion)]}
    #pb<element>
    ]

   ]




= L'espace vectoriel des polynômes
<lespace-vectoriel-des-polynômes>


= Fonctions polynomiales
<fonctions-polynomiales>

Dans ce paragraphe, $E$ désignera $bb(R)$, ou $bb(C)$, ou
$M_n lr((bb(K)))$, ou $bb(R)^I$, ou $bb(C)^(bb(N))$, ou $bb(K) lr([X])$,
de manière générale, tout ensemble dans lequel on peut additionner et
multiplier comme dans $M_n lr((bb(K)))$ par exemple.


#yprop[
Soient $P$ et $Q$ deux polynômes et $x in E$. Alors
$lr((P plus Q)) lr((x)) eq P lr((x)) plus Q lr((x))$ et
$lr((P Q)) lr((x)) eq P lr((x)) Q lr((x))$.

]

#yproof[C'est une conséquence des propriétés de la multiplication et de l'addition des polynômes formels.]

#yprop[
Si $KK$ est infini, alors l'application $P in bb(K) lr([X]) arrow.r.bar f_P in bb(K)^(bb(K))$ est
injective.

]

#yproof[Cette proposition sera démontrée plus tard…]


#yprop[
Soit $P in bb(R) lr([X])$ et $f colon bb(R) arrow.r bb(R)$ la fonction
polynomiale associée. Alors $f$ est dérivable et $f prime$ est la
fonction polynomiale associée à $P prime$.

]

#yprop[
Soient $P comma Q in bb(K) lr([X])$. On a
$lr((P plus Q)) prime eq P prime plus Q prime$ et
$lr((P Q)) prime eq P prime Q plus P Q prime$.

]


#ydef[
Soient $P comma Q in bb(K) lr([X])$. On définit le polynôme composé
$P lr((Q))$ en substituant $Q$ à $X$.

]

= Arithmétique des polynômes
<arithmétique-des-polynômes>

== PGCD et PPCM
<pgcd-et-ppcm>



#ybtw[Les propositions suivantes se démontrent de la même façon que leur analogue dans $NN$.]




#yprop[
Soient $A$ et $B$ deux polynômes dont l'un au moins est non nul, et $D$
un autre polynôme. Alors $D$ divise à la fois $A$ et $B$ si et seulement
si $D$ divise $A and B$.

]


#ycor[
Soient $A comma B comma C$ trois polynômes tels que $A bar.v C$,
$B bar.v C$ et $A and B eq 1$. Alors $A B bar.v C$.

]
#ydef[
Soient $A comma B comma M$ trois polynômes. On dit que $R$ est un
#emph[PPCM] de $A$ et $B$ si $R$ est un multiple de $A$ et $B$ de degré
minimal. On note $A or B$ le seul PPCM unitaire de $A$ et $B$.

]
#yprop[
Soient $A$ et $B$ deux polynômes dont l'un au moins est non nul. Alors
$lr((A and B)) lr((A or B))$ et $A B$ sont associés.

]
= Racines d'un polynôme
<racines-dun-polynôme>





#ycor[
Si $KK$ est infini, l'application $P in bb(K) lr([X]) arrow.r.bar f_P in bb(K)^(bb(K))$ est
injective.

]

#yproof[On suppose $KK$ infini. L'application $P |-> f_P$ est linéaire, son noyau est constitué des polynômes $P$ tels que pour tout $x in KK$, $P(x)=0$. Un tel polynôme a donc une infinité de racines, il ne peut donc pas être non nul. Donc l'application est injective.]

#ybtw[Avec $KK=ZZ slash 3 ZZ$, les polynômes $X^3$ et $X$ sont distincts mais pour tout $x in ZZ slash 3 ZZ$, $x^3 =x$ d'après le petit théorème de Fermat.]



== Théorème fondamental de l'algèbre
<théorème-fondamental-de-lalgèbre>
~ \


#yproof[Soit $P$ un polynôme de degré au moins 2. Comme $P$ a au moins une racine $a$, il se factorise par $X-a$, et comme $X-a$ n'est pas associé à $P$, $P$ n'est pas irréductible.]






= Relations entre coefficients et racines
<relations-entre-coefficients-et-racines>




= Polynômes interpolateurs de Lagrange



#ybtw[Les polynômes $L_i$ forment une famille génératrice de $KK_(n-1) [X]$ : tout polynôme $P in KK_(n-1) [X]$ s'écrit sous la forme $sum_(i=1)^n P(x_i) L_i$. On en déduit qu'ils forment une base de $KK_(n-1) [X]$.]
