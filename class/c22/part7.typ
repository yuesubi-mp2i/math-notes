#import "../../template/ytemplate.typ": *


#ypart[Interpolation]


#ydef(name: [Polynômes interpolateurs de Lagrange])[
    Les polynômes de $KK[X]$ suivants :
    $ L_i = product_(1 <= j <= n \ j != i) (X - x_j)/(x_i - x_j) $
    sont appelés
    #emph[polynômes interpolateurs de Lagrange] associés à
    $(x_1,...,x_n) in KK^n$.
]

#yprop(name: [Polynômes passant par des points donnés])[

Soient $(x_1,...,x_n) in KK^n$ des scalaires distincts deux-à-deux
et $(y_1,...,y_n) in KK^n$.

Les polynômes
$ P_R =& sum_(i=1)^n y_i product_(1 <= j <= n\ j != i) (X-x_j)/(x_i - x_j) \
    & + product_(k=1)^n (X-x_k) R(X) $
où $R$ est un polynôme quelconque.

Sont les seuls polynômes $P in KK[X]$ tels que
pour tout $i in [|1,n|]$, $P(x_i) = y_i$.


]

#yproof[
On cherche tous les polynômes
$P in KK[X]$ tels que pour tout
$i in bracket.l.double 1,n bracket.r.double$, $P(x_i)=y_i$.

Ce problème est linéaire : on cherche tous les polynômes $P$ tels que $phi(P)=(y_1,...,y_n)$ où $phi : KK[X] -> KK^n, P |-> (P(x_1),...,P(x_n))$, et $phi$ est linéaire. On ne peut pas utiliser d'argument de dimension ici puisque $KK[X]$ est de dimension infinie. On restreint $phi$ à $KK_(n-1) [X]$ : l'application $phi_n : KK_(n-1) [X] -> KK^n, P |-> phi(P)$ est encore linéaire, et $dim(KK_(n-1) [X]) = n = dim(KK^n)$. 

Déterminons le noyau de $phi_n$. Soit $P in KK_(n-1) [X]$. Le polynôme $P$ appartient au noyau de $phi_n$ si et seulement si $x_1,...,x_n$ sont racines de $P$. Comme les $x_i$ sont distincts deux-à-deux et $P$ de degré au plus $n-1$, nécessairement $P=0$ et donc $phi_n$ est injective, donc bijective.

On en déduit qu'il existe un unique polynôme $P$ de degré au plus $n-1$ tel que $P(x_i)=y_i$ pour tout $i in bracket.l.double 1,n bracket.r.double$, et $P = phi_n^(-1)  (y_1,...,y_n)$. Comme $phi_n^(-1)$ est linéaire, $P = sum_(i=1)^n y_i phi_n^(-1)(e_i)$ où $(e_1,...,e_n)$ est la base canonique de $KK^n$.

Soit $i in bracket.l.double 1, n bracket.r.double$ et $L_i = phi_n^(-1)(e_i)$. Par définition, $L_i$ est le seul polynôme de degré au plus $n-1$ tel que 
$ forall j in bracket.l.double 1,n bracket.r.double, L_i (x_j) = cases(1 "si" i=j, 0 "sinon"). $ 

Ainsi, $L_i$ a $n-1$ racines distinctes : les $x_j$ avec $j != i$, donc $L_i$ est associé à $display(product_(1<= j <=n\ j != i) (X-x_j))$. De plus $L_i (x_i) = 1$ donc $L_i = display(product_(1 <= j <= n\ j != i) (X-x_j)/(x_i-x_j))$.

Pour trouver tous les polynômes $Q$ tels que $Q(x_i)=y_i$ quel que soit leur degré, il suffit de remarquer que $Q-P$ appartient au noyau de $phi$, ensemble des polynômes multiples de $(X-x_1)...(X-x_n)$.

Finalement, les solutions du problème sont les polynômes de la forme
$ display(sum_(i=1)^n y_i product_(1 <= j <= n\ j != i) (X-x_j)/(x_i - x_j) + product_(k=1)^n (X-x_k) R(X) ) $
où $R$ est un polynôme quelconque.
]