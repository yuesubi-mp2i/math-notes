#import "../../template/ytemplate.typ": *


#ypart[Congruence]


#ydef(name: [Congruence])[
Soient $n in bb(N)^ast.basic$ et $lr((a comma b)) in bb(Z)^2$.

On dit que $a$ est #emph[congru à $b$ modulo $n$] si $n$ divise $b minus a$.

Dans ce cas, on écrit $a equiv b lr([n])$.

]
#yprop(name: [Caractérisation de la divisibilité par la congruence])[
Pour tout $n in bb(N)^ast.basic$ et $a in bb(Z)$,
$ n bar.v a arrow.l.r.double a equiv 0 lr([n]) $
]
#yprop(name: [Caractérisation de la congruence par les restes de la division euclidienne])[
Soit $n in bb(N)^ast.basic$ et $lr((a comma b)) in bb(Z)^2$.

Alors
$a equiv b lr([n])$ si et seulement si $a$ et $b$ ont même reste dans la
division euclidienne par $n$.

]
#yproof[
- Supposons $a equiv b lr([n])$ et notons $r$ et $r prime$
    les restes respectifs de $a$ et $b$ dans la division par $n$, et $q$ et
    $q prime$ les quotients respectifs.

    Alors
    $ a minus b eq n lr((q minus q prime)) plus lr((r minus r prime)) $
    Donc
    $n$ divise $lr((a minus b)) minus lr((r minus r prime))$.

    Or $n$ divise
    $a minus b$.

    Donc $n$ divise $r minus r prime$.

    Or
    $minus n lt r minus r prime lt n$.

    Le seul entier de cet intervalle
    divisible par $n$ est $0$.

    Donc $r eq r prime$.

- On suppose maintenant que $a$ et $b$ ont le même reste dans la division
    par $n$.
    
    On note $r$ ce reste.
    
    On sait donc que $n$ divise $a minus r$
    et $b minus r$, et donc aussi la différence de ces deux nombres, donc
    $n$ divise $a minus b$, et donc $a equiv b lr([n])$.

]
#ycor(name: [Relation d'équivalence de la congruence])[
Soit $n in bb(N)^ast.basic$.

La relation de congruence modulo $n$ est
une relation d'équivalence sur $bb(Z)$.

Elle a exactement $n$ classes
d'équivalence : ce sont les ensembles $k bb(Z)$ pour
$k in bracket.l.double 0 comma n minus 1 bracket.r.double$.

On note
$bb(Z) slash n bb(Z)$ l'ensemble de ces classes d'équivalence.

]
#yprop(name: [Stabilité des congruences par l'addition])[
Soient $n in bb(N)^ast.basic$, $a_1 comma a_2 comma b_1 comma b_2$
quatre entiers tels que $a_1 equiv b_1 lr([n])$ et
$a_2 equiv b_2 lr([n])$.

Alors
$a_1 plus a_2 equiv b_1 plus b_2 lr([n])$.

]
#yproof[
On sait que $n$ divise $a_1 minus b_1$ et que $n$ divise
aussi $a_2 minus b_2$.

Donc $n$ divise aussi leur somme : $n$ divise
$lr((a_1 plus a_2)) minus lr((b_1 plus b_2))$

]

#ydef(name: [Addition dans $ZZ slash n ZZ$])[
On peut ainsi définir une addition dans $bb(Z) slash n bb(Z)$ en posant
$overline(a) plus overline(b) eq overline(a plus b)$.

N.B. Il faut montrer que la définition de l'addition ne dépend
pas du choix des représentants des classes d'équivalence. C'est
ce qu'affirme la stabilité de la congruence par l'addition.
]
#yprop(name: [Stabilité des congruences par la multiplication])[
Soient $n in bb(N)^ast.basic$, $a_1 comma a_2 comma b_1 comma b_2$
quatre entiers tels que $a_1 equiv b_1 lr([n])$ et
$a_2 equiv b_2 lr([n])$.

Alors $a_1 a_2 equiv b_1 b_2 lr([n])$.

]
#yproof[
On sait que $n$ divise $a_1 minus b_1$ et que $n$ divise
aussi $a_2 minus b_2$.

On pose $q_1$ et $q_2$ deux entiers tels que
$a_1 eq b_1 plus n q_1$ et $a_2 eq b_2 plus n q_2$.

On en déduit que
$ a_1 a_2 eq &b_1 b_2 plus \ &n lr((q_1 b_2 plus q_2 b_1 plus n q_1 q_2)) $
donc $n$ divise $a_1 a_2 minus b_1 b_2$.

]
#ydef(name: [Multiplication dans $ZZ slash n ZZ$])[
On peut ainsi définir une multiplication dans $bb(Z) slash n bb(Z)$ en
posant $overline(a) times overline(b) eq overline(a times b)$.

N.B. Il faut montrer que la définition de la multiplication ne dépend
pas du choix des représentants des classes d'équivalence. C'est
ce qu'affirme la stabilité de la congruence par la multiplication.
]

#ytheo(name: [Structure de $ZZ slash n ZZ$])[
$lr((bb(Z) slash n bb(Z) comma plus comma times))$ est un anneau
commutatif.

]
#yproof[
Il n'y a pas spécialement de difficultés :
$bb(Z) slash n bb(Z)$ hérite des propriétés de $bb(Z)$.

]

#ytheo(name: [Éléments inversibles de $ZZ slash n ZZ$])[
Les éléments inversibles de l’anneau $bb(Z) slash n bb(Z)$ sont les
$overline(a)$ avec $a and n eq 1$.

]
#yproof[
Soit $a$ un entier premier avec $n$.

D’après le théorème
de Bézout, il existe $u$ et $v$ deux entiers tels que
$a u plus n v eq 1$.

En passant dans $bb(Z) slash n bb(Z)$, on obtient
$overline(a) overline(u) eq overline(1)$ et donc $a$ est inversible.

Réciproquement, soit $a$ un entier tel que $overline(a)$ soit inversible
modulo $n$ .

Soit $overline(u)$ l’inverse de $overline(a)$.

Ainsi
$a u equiv 1 lr([n])$ et donc il existe $v$ un entier
$a u minus n v eq 1$.

D’après le théorème de Bézout, $a$ et $n$ sont
premiers entre eux.

]
#ycor(name: [Anneaux $ZZ slash n ZZ$ qui sont des corps])[
$lr((bb(Z) slash n bb(Z) comma plus comma times))$ est un corps si et
seulement si $n$ est premier.

]
#yproof[
$bb(Z) slash n bb(Z)$ est un corps si et seulement si tout
entier non multiple de $n$ est premier avec $n$, si et seulement si $n$
est premier.

]
#ytheo(name: [Petit théorème de Fermat])[
Soient $p$ un nombre premier et $a$ un entier premier avec $p$.

Alors
$a^(p minus 1) equiv 1 lr([p])$
_i.e._ $a^p equiv a [p]$.
]
#yproof[
Pour tout $i in bracket.l.double 1, p-1 bracket.r.double$, on note $x_i = i a$.

On remarque que
$ product_(i=1)^(p-1) x_i = (p-1)! a^(p-1) $

Or, pour tout $i != j$, $overline(x_i) != overline(x_j)$ (dans $ZZ slash p ZZ$) car $overline(x_i)-overline(x_j) = overline(i-j) overline(a) != 0$ car $ZZ slash p ZZ$ est un corps.

On en déduit que l'application $f : i |-> overline(x_i)$ est une injection de $bracket.l.double 1, p-1 bracket.r.double$ dans $ZZ slash p ZZ without {overline(0)}$.

Comme ces deux ensembles sont finis de même cardinal $p-1$, $f$ est bijective et donc
$ product_(i=1)^(p-1) overline(x_i) = product_(j=1)^(p-1) overline(j) = overline((p-1)!) $

On en déduit que
$ overline((p-1)!) overline(a^(p-1)) = overline((p-1)!) $
et comme $overline((p-1)!) != overline(0)$,
$ overline(a^(p-1))=overline(1) $
]

#ytheo(name: [Théorème de Gauss])[
Soient $a comma b comma c$ trois entiers tels que $a bar.v b c$ et
$a and b eq 1$.

Alors $a bar.v c$.

]
#yproof[
On se place dans $bb(Z) slash a bb(Z)$.

Puisque
$a bar.v b c$, $overline(b) times overline(c) eq overline(0)$.

Or $b$ est premier avec
$a$, donc $overline(b)$ est inversible, donc régulier, donc $overline(c) eq overline(0)$,
donc $a$ divise $c$.

]
#ycor(name: [Conséquence qu'un nombre possède deux diviseurs premiers entre eux])[
Soient $a comma b comma c$ trois entiers tels que $a and b eq 1$,
$a bar.v c$ et $b bar.v c$.

Alors $a b bar.v c$.

]
#yproof[
Soit $q$ un entier tel que $b q eq c$.

Ainsi $a$ divise
$b q$.

Comme $a$ et $b$ sont premiers entre eux, $a$ divise $q$, et donc
$a b$ divise $b q eq c$.

]
