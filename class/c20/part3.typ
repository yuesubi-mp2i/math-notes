#import "../../template/ytemplate.typ": *


#ypart[Un peu de théorie des groupes]


#yprop(name: [Sous groupe de $ZZ$ engendré par un entier])[
Soit $n$ un entier et $n bb(Z)$ l'ensemble des multiples de $n$.

$n bb(Z)$ est le sous-groupe de $lr((bb(Z) comma plus))$ engendré par
$n$.

]

#yproof[
$0 in n bb(Z)$.

Soient $lr((a comma b)) in n bb(Z)^2$.

Alors $n$ divise $a$ et $b$, donc $n$ divise $a minus b$, donc
$lr((a minus b)) in n bb(Z)$.

On vient de prouver que $n bb(Z)$ est un
sous-groupe de $lr((bb(Z) comma plus))$.

De plus, $n in n bb(Z)$.

Montrons que c'est le plus petit.

Soit donc $G$ un sous-groupe de $lr((bb(Z) comma plus))$ tel que
$n in G$.

On prouve alors par récurrence immédiate que pour tout
$k in bb(N)$, $k n in G$, puis par stabilité par passage à l'opposé que
$n bb(Z) subset G$.

]

#ynot(name: [Groupe engendré dans $ZZ$])[#ymega_emph[Dans ce cours seulement], nous noterons $<A>$ le plus petit sous-groupe de $ZZ$ contenant la partie $A$.

Ainsi, $<{n}>  = n ZZ$.]

#yprop(name: [Caractérisation de la divisibilité par des ensembles de multiples])[
Soient $a$ et $b$ deux entiers.

Alors $a$ divise $b$ si et seulement si
$b bb(Z) subset a bb(Z)$.

]
#yproof[
$a$ divise $b$ si et seulement si $b in a bb(Z)$ si et
seulement si $<{b}> subset a bb(Z)$ si et seulement si
$b bb(Z) subset a bb(Z)$.

]
#ytheo(name: [Forme d'un sous groupe de $ZZ$])[
Soit $G$ un sous-groupe non nul de $lr((Z comma plus))$.

Alors
$G eq n bb(Z)$ où $n$ est le plus petit élément strictement positif de
$G$.

]
#yproof[
Comme $G$ est non nul, $G sect bb(N)^ast.basic$ est une
partie non vide de $bb(N)$, elle admet donc un plus petit élément que
l'on note $n$.

Par suite, $G$ contient le sous-groupe engendré par $n$,
c'est-à-dire $n bb(Z)$.

Soit maintenant $a in G$. On effectue la division euclidienne de $a$ par
$n$ : $a eq q n plus r$ avec $q in bb(Z)$ et
$r in bracket.l.double 0 comma n minus 1 bracket.r.double$.

Ainsi
$r eq a minus q n$.

Or $a in G$ et $q n in n bb(Z) subset G$, donc
$r in G$.

Comme $r lt n$, $r$ ne peut pas être strictement positif, donc
$r eq 0$, donc $n$ divise $a$, donc $a in n bb(Z)$.

On a bien prouvé par double inclusion que $G eq n bb(Z)$.

]