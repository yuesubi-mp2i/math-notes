#import "../../template/ytemplate.typ": *


#ypart[Division euclidienne]


#ytheo(name: [division euclidienne dans $NN$])[
Soient $a in bb(N)$ et $b in bb(N)^ast.basic$.

Il existe un unique
couple $lr((q comma r)) in bb(N)^2$ tel que
$ cases(a eq b q plus r,
0 lt.eq r lt b) $

Les entiers $q$ et $r$ sont respectivement appelés #emph[quotient] et
#emph[reste] dans la division euclidienne de $a$ par $b$.

]

#yproof[

- Existence.

    Soit
    $ E eq brace.l n in bb(N) divides b bar.v n upright(" et ") n lt.eq a brace.r $

    C'est une partie non vide et majorée de $bb(N)$ donc elle admet un
    plus grand élément de la forme $q b$, avec $q in bb(N)$.
    
    On pose
    $r eq a minus b q$.
    
    Puisque $b q lt.eq a$, on a $r gt.eq 0$.
    
    De plus,
    $lr((q plus 1)) b gt q b$ donc $ lr((q plus 1)) b in.not E $ d'où
    $lr((q plus 1)) b gt a$.
    
    On en déduit que $b gt a minus q b eq r$.

- Unicité.

    Soit $lr((q prime comma r prime))$ un autre couple d'entiers
    tel que $a eq b q prime plus r prime$ avec $0 lt.eq r prime lt b$.
    
    On
    a alors $b lr((q minus q prime)) eq r prime minus r$, donc
    $r prime minus r$ est un diviseur de $b$ compris strictement entre
    $minus b$ et $b$.
    
    La seule possibilité est $r prime minus r eq 0$ et
    donc $q eq q prime$.


]
#ytheo(name: [division euclidienne dans $ZZ$])[
Soient $a in bb(Z)$ et $b in bb(Z)^ast.basic$.

Il existe un unique
couple $lr((q comma r)) in bb(Z) times bb(N)$ tel que
$ cases(a eq b q plus r,
    0 lt.eq r lt lr(|b|)) $

]
#yproof[
L'unicité se prouve de la même façon que dans $NN$.

Pour prouver
l'existence, on effectue la division euclidienne de $lr(|a|)$ par
$lr(|b|)$ : $ lr(|a|) eq lr(|b|) q prime plus r prime $ avec
$q prime in bb(N)$ et
$r prime in bracket.l.double 0 comma lr(|b|) minus 1 bracket.r.double$.

En posant alors $q eq q prime$ ou $minus q prime$ ou
$minus q prime plus 1$ et $r eq r prime$ ou $b minus r prime$ suivant
les cas, on conclut.

]
#ycor(name: [Lien entre divisibilité et division euclidienne])[
(Corollaire de l'existance de la division euclidienne)

Soient $a comma b in bb(N)^ast.basic$.

Alors $b$ divise $a$ si et
seulement si le reste dans la division de $a$ par $b$ est 0. $qed$
]