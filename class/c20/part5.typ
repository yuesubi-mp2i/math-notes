#import "../../template/ytemplate.typ": *


#ypart[Décomposition en facteurs premiers]


#yprop(name: [Nombre minimal de diviseurs premiers])[
Soit $n$ un entier supérieur ou égal à 2.

Alors $n$ a au moins un
diviseur premier.

]
#yproof[
L'ensemble des diviseurs supérieurs strictement à 1 de $n$
est non vide (il contient $n$) donc possède un plus petit élément $p$.

Nécessairement $p$ est premier, car sinon tout diviseur de $p$ autre que
1 et $p$ serait un diviseur de $n$ inférieur à $p$, contredisant ainsi
la minimalité de $p$.

]
#yprop(name: [Nombre de nombre premiers])[
Il existe une infinité de nombres premiers.

]
#yproof[
Supposons qu'il n'y ait qu'un nombre fini de nombres
premiers, disons $N$.

On note alors $p_1 comma dots.h comma p_N$ les $N$
nombres premiers et on forme l'entier $p_1 dots.h p_N plus 1$.

Cet
entier étant supérieur strictement aux $N$ nombres premiers, il n'est
pas premier.

Il admet donc nécessairement un diviseur premier, disons
$p_i$.

Or $p_i$ divise $p_1 dots.h p_N$, donc $p_i$ divise aussi $1$,
une contradiction.

]

#ytheo(name: [Théorème fondamental de l'arithmétique])[
Soit $n$ un entier supérieur ou égal à $2$.

On peut écrire $n$ sous la
forme d'un produit de nombres premiers.

Cette décomposition est unique.

]
#yproof[
Commençons par prouver l'existence par récurrence forte
sur $n$.

- La propriété est vraie pour $n eq 2$ puisque cet entier est premier.

- On suppose la propriété vraie pour tout entier $k lt.eq n$.

    Si
    $n plus 1$ est premier, alors on a déjà une décomposition.
    
    Sinon, il
    existe $p$ et $q$ strictement inférieurs à $n plus 1$ et supérieurs
    strictement à 1 tels que $n plus 1 eq p q$.
    
    D'après l'hypothèse de
    récurrence, on peut décomposer $p$ et $q$ en produit de facteurs
    premiers, et on obtient alors une décomposition pour $n plus 1$ en
    multipliant les deux décompositions.

L'unicité de la décomposition en facteurs premiers se prouve également
par récurrence forte.

L'initialisation ne pose pas de difficulté puisque
$2$ étant premier, il n'admet pas d'autre décomposition que $2 eq 2$.

Soit $n$ un entier supérieur ou égal à $2$ et on suppose que pour tout
$k in bracket.l.double 2 comma n minus 1 bracket.r.double$, $k$ a une
unique décomposition en facteurs premiers.

Supposons que $n$ ait deux décompositions en facteurs premiers
$n eq p_1 dots.h p_r eq q_1 dots.h q_s$. On suppose $s gt.eq r$ par
exemple.

Comme $p_1$ est premier et $p_1$ divise $q_1 dots.h q_s$,
d'après le théorème de Gauss, $p_1$ divise l'un des $q_j$, et puisque
$q_j$ est premier, $p_1 eq q_j$.

En simplifiant alors par $p_1$, on
obtient deux décompositions en facteurs premiers de $n slash p_1$ : si
$n slash p_1 gt.eq 2$, alors on conclut en appliquant l'hypothèse de
récurrence.

Si $n slash p_1 eq 1$, alors $n$ est premier et n'a donc qu'une seule
décomposition.

]
#ybtw[
Pour tout $n in bb(N)^ast.basic$, on note $p_n$ le $n$-ième nombre
premier. Pour tout $a in bb(N)^ast.basic$, on peut alors écrire
$a eq product_(p in P) p^(v_p (a))$ où les entiers $v_p (a)$ peuvent
être nuls, et $n$ choisi assez grand.

]
#ydef(name: [Valuation $p$-adique])[
Soit $p$ un nombre premier et $n in bb(N)$ un entier supérieur ou égal à 2. 

La #emph[valuation $p$-adique de $n$] est l'exposant (éventuellement
nul) de $p$ dans la décomposition en facteurs premiers de $n$. On note
cet entier $v_p lr((n))$.

]
#yprop(name: [Caractérisation de la divisibilité par les valuations $p$-adiques])[
Soient $a$ et $b$ deux entiers supérieurs ou égaux à 2.

Alors $a$ divise
$b$ si et seulement si pour tout nombre premier $p$,
$v_p lr((a)) lt.eq v_p lr((b))$.

]
#yproof[
    $ cases(a = product_(p in P) p^(v_p (a)),
        b = product_(p in P) p^(v_p (b))) $
    
    - "$<==$" On suppose que pour $p in P$, $v_p (a) <= v_p (b)$.

        Alors
        $ b &= product_(p in P) p^(v_p (b)) \
            &= underbrace(product_(p in P) p^(v_p (a)), = a) times
                underbrace(product_(p in P) p^(v_p (b) - v_p (a)), in NN^*) $
        
        Donc $a bar.v b$.
    
    - "$==>$" On suppose $a bar.v b$.

        Soit $k in NN^*$ tel que $a k = b$.

        $ product_(p in P) p^(v_p (a)) = product_(p in P) p^(v_p (b) - v_p (k)) $

        Par unicité de la décomposition en nombre premier,
        pour $p in P$,
        $ v_p (a) = v_p (b) - v_p (k) <= v_p (b) $
        $qed$
]

#ycor(name: [Décomposition en nombre premiers du PGCD])[
(Corollaire de la caractérisation de la divisibilité par les valuations $p$-adiques)

Soient $a$ et $b$ deux entiers, et
$ cases(a eq product_(p in P) p^(v_p (a)),
    b eq product_(p in P) p^(v_p (b))) $
leur décomposition en produit
de facteurs premiers.

Alors
$ a and b eq product_(p in P) p^(min lr((v_p (a) comma v_p (b)))) $

]

#yproof[
    Comme le PGCD divise les deux nombres, 
    sa valuation $p$-adique est inférieure aux deux nombres. $qed$
]

#ycor(name: [Décomposition en nombre premiers du PPCM])[
(Corollaire de la caractérisation de la divisibilité par les valuations $p$-adiques)

Soient $a$ et $b$ deux entiers, et
$ cases(a eq product_(p in P) p^(v_p (a)),
    b eq product_(p in P) p^(v_p (b))) $
leur décomposition en produit
de facteurs premiers.

Alors
$ a or b eq product_(p in P) p^(max lr((v_p (a) comma v_p (b)))) $

]
#yproof[
    Comme les deux nombres divisent le PPCM, 
    leurs valuations $p$-adiques sont inférieure à celle du PPCM. $qed$
]
#ycor(name: [Produit du PGCD et du PPCM])[
(Corollaire de la décomposition en nombres premiers du PGCD et du PPCM)

Soient $a$ et $b$ deux entiers.

Alors
$a b eq lr((a and b)) lr((a or b))$.

]

#yproof[
    Les valuations $p$-adiques de $a$ et $b$ sont toutes deux présentes
    dans l'exposant, car on a le minimum et le maximum des deux valuations.
    $qed$
]
