#import "../../template/ytemplate.typ": *


#ypart[Divisibilité]


L'arithmétique est l'étude des propriétés de $bb(Z)$ vis-à-vis de la
relation de divisibilité.

#ydef(name: [Divisibilité])[
Soient $p$ et $n$ deux entiers relatifs.

On dit que #emph[$p$ divise
$n$], ou que #emph[$p$ est un diviseur de $n$], ou que #emph[$n$ est un
multiple de $p$] s'il existe $k in bb(Z)$ tel que $n eq k p$.

On note
cette situation $p bar.v n$.

]
#yexample[
Pour tout $n in bb(Z)$, on a $1 bar.v n$ et $n bar.v 0$.

]

#yprop(name: [Propriétés élémentaires de la divisibilité])[
Soient $a comma b comma c$ trois entiers.

+ $a bar.v a$.

+ Si $a bar.v b$ et $b bar.v a$ alors $a eq plus.minus b$.

+ Si $a bar.v b$ et $b bar.v c$ alors $a bar.v c$.

]
#yproof[
+ $a eq 1 times a$ donc $a bar.v a$.

+ Si $a$ et $b$ sont nuls, le résultat est clair.

    Supposons à présent que $a eq.not 0$.
  
    Il existe $k comma l in bb(Z)$ tels que $a eq k b$
    et $b eq l a$.
    
    Donc $a eq k l a$.
    
    Comme $a eq.not 0$, on a
    $k l eq 1$,de sorte que $k eq plus.minus 1$ et donc
    $a eq plus.minus b$.

+ Il existe $k comma l in bb(Z)$ tels que $b eq a k$ et $c eq b l$ donc
    $c eq k l a$.
    
    Comme $k l in bb(Z)$ on a bien $a bar.v c$.

]
#yprop(name: [Divisibilité d'une combinaison linéaire])[
Soient $a comma b comma c$ trois entiers. Si $a bar.v b$ et $a bar.v c$,
alors pour tous $u comma v in bb(Z)$, $a bar.v lr((u b plus v c))$.

]
#yproof[ Il existe $k comma l in bb(Z)$ tels que $b eq a k$ et
$c eq a l$ donc
$ u b plus v c &eq u a k plus v a l \ &eq a lr((u k plus v l)) $

Comme
$u k plus v l in bb(Z)$, on a bien $ a bar.v lr((u b plus v c)) $

]

#ycor(name: [Diviseurs commun d'un nombre et son succésseur])[
    (Corollaire de la divisibilité sur une combinaison linéaire)

    Les diviseurs communs de $n$ et $n + 1$ sont $1$ et $-1$.
]

#yproof[
    Soit $d in ZZ$ tel que $d bar.v n$ et $d bar.v n + 1$.

    Donc $d bar.v 1 times (n + 1) + (-1) times n = 1$.

    Donc $d = plus.minus 1$.
]

#yprop(name: [Ordre avec la divisibilité])[
    $(NN, bar.v)$ est un ensemble ordonné. $qed$
]

#ydef(name: [Nombre premier])[
Un entier $p$ est dit #emph[premier] si ses seuls diviseurs sont
$plus.minus 1$ et $plus.minus p$.

]

#ymetho(name: [Crible d'Ératosthène])[
Pour déterminer tous les entiers positifs premiers inférieurs à 100 par
exemple, on peut procéder de la façon suivante.

#let gen_data() = {
    let colors = [red, blue]

    let nums = range(1, 101)
    let is_prime = range(1, 101).map(_ => true)
    is_prime.at(0) = false

    for i in range(2, calc.ceil(calc.sqrt(nums.at(-1)))) {
        let j = 2
        while i*j < nums.len() {
            is_prime.at(i*j - 1) = false
            j += 1
        }
    }

    return nums.zip(is_prime)
}

#figure(align(center, table(
    columns: 10,
    inset: 0.2em,
    stroke: none,
    ..gen_data().map(p => if p.at(1) {rect($#p.at(0)$)} else {math.cancel($#p.at(0)$)})
)),
caption: [Typst flex])

On commence par écrire tous les entiers de 2 à 100. Le premier de ces
entiers est 2, il est premier. Les autres entiers pairs ne peuvent pas
être premiers donc on les supprime du tableau. Le premier entier devient
alors 3 qui est donc premier, on supprime ensuite tous les autres
multiples de 3. Le premier entier restant est alors 5 qui doit donc être
premier, et on supprime alors tous les multiples de 5, et ainsi de
suite.]