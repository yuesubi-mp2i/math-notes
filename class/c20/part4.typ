#import "../../template/ytemplate.typ": *


#ypart[PGCD et PPCM]


#ydef(name: [PGCD])[
Soient $a$ et $b$ deux entiers dont l'un au moins est non nul.

Le
#emph[PGCD] (plus grand commun diviseur) de $a$ et $b$ est le plus grand
de tous les entiers qui divisent à la fois $a$ et $b$.

On le note
$"PGCD"lr((a comma b))$ ou $a and b$.

N.B.
On note $cal(D)$ l'ensemble de tous les diviseurs communs à $a$ et $b$.
La partie $cal(D)$ est majorée par $a$, et non vide puisque
$1 in cal(D)$, donc $cal(D)$ admet un plus grand élément $d$. Ceci
prouve que $a and b$ existe.
]

#yprop(name: [Sous groupe de $ZZ$ engendré par la réunion de deux ensembles de multiples])[
Soient $a$ et $b$ deux entiers dont l'un au moins est non nul.

Alors
$lt a bb(Z) union b bb(Z) gt eq lr((a and b)) bb(Z)$.

]
#yproof[
Soit $d in bb(N)^ast.basic$ tel que
$d bb(Z) eq lt a bb(Z) union b bb(Z) gt$.

Comme
$a bb(Z) subset d bb(Z)$, $d$ divise $a$.

De même $d$ divise $b$.

Donc
$d lt.eq a and b$.

De plus $a and b$ divise $a$ donc $ a bb(Z) subset lr((a and b)) bb(Z) $

De même $b bb(Z) subset lr((a and b)) bb(Z)$.

Donc
$ d bb(Z) subset lr((a and b)) bb(Z) $ et donc $a and b$ divise $d$.

Ainsi $d eq a and b$.

]

#ydef(name: [Nombres premiers entre eux])[
On dit que deux entiers $a$ et $b$ sont #emph[premiers entre eux] si
leur PGCD vaut 1.

]

#yprop(name: [Partiularité du PGCD de deux nombres ayant un diviseur commun])[
Soient $a$ et $b$ deux entiers et $c$ un diviseur commun à $a$ et $b$.

Alors $c$ divise $a and b$.

]
#yproof[
Comme $c$ est un diviseur de $a$,
$a bb(Z) subset c bb(Z)$.

De même $b bb(Z) subset c bb(Z)$.

On en déduit
que $c bb(Z)$ contient le sous-groupe engendré par
$a bb(Z) union b bb(Z)$.

Or
$a bb(Z) union b bb(Z) eq lr((a and b)) bb(Z)$.

Par suite $c$ est un
multiple de $a and b$.

]
#ytheo(name: [Écriture du PGCD comme une combinaison linéaire])[
Soient $a$ et $b$ deux entiers dont l'un au moins est non nul et $d$
leur PGCD. Alors il existe $lr((u comma v)) in bb(Z)^2$ tel que
$d eq a u plus b v$.

]
#yproof[
$d bb(Z) eq lt a bb(Z) union b bb(Z) gt eq a bb(Z) plus b bb(Z)$.

]


#ytheo(name: [Théorème de Bézout])[
Soient $a$ et $b$ deux entiers.

Alors $a$ et $b$ sont premiers entre eux
si et seulement s'il existe $lr((u comma v)) in bb(Z)^2$ tel que
$a u plus b v eq 1$.

]
#yproof[
On pose $d eq a and b$ de sorte que
$d bb(Z) eq a bb(Z) plus b bb(Z)$.

Si $d eq 1$, alors
$1 in a bb(Z) plus b bb(Z)$ donc il existe $u$ et $v$ entiers tels que
$1 eq a u plus b v$.

Réciproquement, s'il existe $u$ et $v$ entiers tels que
$1 eq a u plus b v$, alors $1 in d bb(Z)$ et donc $d$ divise $1$, donc
$d eq 1$.

]
#ytheo(name: [Euclide])[
Soient $a$ et $b$ deux entiers.

On note $r$ le reste de la division de
$a$ par $b$.

Alors $a and b eq b and r$.

]
#yproof[
On écrit $a eq b q plus r$ avec $q in bb(N)$ et
$r in bracket.l.double 0 comma b minus 1 bracket.r.double$.

Soient
$d eq a and b$ et $delta eq b and r$.

D'une part, $d$ divise $a$ et $b$, donc $d$ divise $a minus b q eq r$,
donc $d$ divise à la fois $b$ et $r$.

On en déduit que $d$ divise
$delta$.

D'autre part, $delta$ divise $b$ et $r$, donc $delta$ divise
$b q plus r eq a$, donc $delta$ divise à la fois $a$ et $b$.

On en
déduit que $delta$ divise $d$.

Finalement, $d eq delta$.

]
#ybtw[
Comme $r lt b$, le calcul de $b and r$ est plus aisé que $a and b$. De
plus, on peut itérer cette formule, obtenant des entiers de plus en plus
petits, jusqu'à éventuellement obtenir un reste nul, auquel cas le
calcul du PGCD est particulièrement aisé !

]
#ydef(name: [PPCM])[
Soient $a$ et $b$ deux entiers. Le #emph[PPCM de $a$ et $b$] (Plus Petit
Commun Multiple) est le plus petit entier qui soit à la fois un multiple
de $a$ et un multiple de $b$. On le note $a or b$.

]
#yprop(name: [Intersection de deux ensembles de multiples])[
$a bb(Z) sect b bb(Z) eq lr((a or b)) bb(Z)$.

]
#yproof[
Soit $m in bb(N)^ast.basic$ tel que
$a bb(Z) sect b bb(Z) eq m bb(Z)$.

Comme $m bb(Z) subset a bb(Z)$, $m$
est un multiple de $a$.

De même $m$ est un multiple de $b$.

On en déduit
que $a or b lt.eq m$.

Comme $a$ divise $a or b$, $a bb(Z) supset lr((a or b)) bb(Z)$.

De même
$b bb(Z) supset lr((a or b)) bb(Z)$.

Donc
$a bb(Z) sect b bb(Z) supset lr((a or b)) bb(Z)$ et par suite $m$ divise
$a or b$.

Donc $a or b eq m$.

]
#yprop(name: [Partiularité du PPCM de deux nombres ayant un multiple commun])[
Soient $a$ et $b$ deux entiers et $c$ un multiple commun à $a$ et $b$.
Alors $c$ est un multiple de $a or b$.

]