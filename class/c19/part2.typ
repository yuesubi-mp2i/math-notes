#import "../../template/ytemplate.typ": *


#ypart[Opérations sur la dimension]


#ytheo(name: [Comparaison de la dimension d'un ev et d'un sev])[
Soient $E$ un espace vectoriel de dimension finie, et $F$ un sous-espace
vectoriel de $E$.

Alors :

+ $F$ est de dimension finie

+ $F$ est de dimension finie et $dim lr((F)) lt.eq dim lr((E))$.

+ De plus, si $dim lr((F)) eq dim lr((E))$ alors $F eq E$.

]

#yproof[
    Toute famille libre de $F$ est aussi une famille libre de $E$ donc de cardinal majoré par $dim(E)$. Soit $L$ une famille libre de $F$ de cardinal maximal.
    
    Si $L$ n'engendre pas $F$, alors il existe $x in F$ tel que $L union {x}$ soit libre, ce qui contredit le caractère maximal de $L$.
    
    Ainsi $L$ est une base de $F$ et donc $F$ est de dimension finie et on a trouvé une base $L$ de cardinal inférieur ou égal à la dimension de $E$.]


#yprop(name: [Existance d'un supplémentaire en dimension finie])[
Soit $F$ un sous-espace vectoriel d'un espace $E$ de dimension finie.
Alors $F$ admet un supplémentaire dans $E$.

]

#yproof[Soit $L$ une base de $F$. Comme $L$ est une famille libre de $E$, on peut la compléter en une base $B$ de $E$. On constate alors que $"Vect"(B without L)$ est un supplémentaire de $F$.]

#yprop(name: [Dimension du produit cartésien])[
Soient $E$ et $F$ deux espaces vectoriels de dimension finie.

Alors
$E times F$ est de dimension finie et
$ dim lr((E times F)) eq dim lr((E)) plus dim lr((F)) $

On peut étendre à un produit cartésien de $n$ facteurs.
]

#yproof[Soit $B = (e_1,...,e_n)$ une base de $E$ et $C=(f_1,...,f_p)$ une base de $F$.

Alors
$ (&(e_1,0_F),...,(e_n, 0_F), \
    &(0_E, f_1),...,(0_E, f_p)) $ est une base de $E times F$.

On le prouve pour un produit cartésien de $n$ facteurs, par récurrence.
]


#yprop(name: [Formule de Grassmann])[
Soient $E$ un espace vectoriel de dimension finie et $F$ et $G$ deux
sous-espaces vectoriels de $E$. Alors
$ dim lr((F plus G)) eq
    yt dim F plus dim G minus dim lr((F sect G)) $

]

#yproof[Soit $(u_1,...,u_p)$ une base de $F sect G$.

Il s'agit d'une famille libre de $F$, on la complète en une base $ (u_1,...,u_p, v_1,...,v_q) $ de $F$.

De même, on complète $(u_1,...,u_p)$ en une base $(u_1,...,u_p, w_1,...,w_r)$ de $G$.

On vérifie alors que $ (u_1,...,u_p,v_1,...,v_q, w_1,...,w_r) $ est une base de $F+G$ et donc 

$ &dim(F+G) \ &= p+q+r \ &= (p+q)+(p+r)-p \
 &= dim(F)+dim(G)-dim(F sect G) $
]


#ycor(name: [Caractérisation du supplémentaire en dimension finie])[
Soient $F$ et $G$ deux sous-espaces vectoriels d'un espace de dimension
finie $E$.

$F$ et $G$ sont supplémentaires si et seulement si $F sect G = {0_E}$
et $dim F plus dim G eq dim E$.

]

#yproof[Si $F$ et $G$ sont supplémentaires, alors $F sect G = {0_E}$ car la somme
est directe et
$ &dim(E) = dim(F+G) \
    &=dim(F)+dim(G)-dim(F sect G) \
    &= dim(F)+dim(G) $

Réciproquement, si $F sect G = {0_E}$ alors la somme est directe et
$ dim(F+G)=dim(F)+dim(G) $ et donc si $ dim(F)+dim(G)=dim(E) $ alors $F+G$ a la même dimension que $E$ et donc $F+G = E$.
]


#ybtw[
L'égalité $dim F plus dim G eq dim E$ ne suffit pas pour prouver que $F$
et $G$ sont supplémentaires : considérer une droite $D$ incluse dans un
plan $P$ d'un espace $E$ de dimension $3$. Ces deux espaces ne sont pas
supplémentaires, et pourtant
$dim D plus dim P eq 1 plus 2 eq 3 eq dim E$.

]