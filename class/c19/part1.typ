#import "../../template/ytemplate.typ": *


#ypart[Dimension d'un espace vectoriel]


=== 1. Théorème de la base extraite

#ydef(name: [Dimension finie])[
Soit $E$ un espace vectoriel. On dit que $E$ est de #emph[dimension
finie] s'il possède une famille génératrice de cardinal fini (i.e. avec
un nombre fini d'éléments).
$ exists n in& NN, exists (u_i) in E^n, \
    &E = "Vect"(u_i | i in [|1, p|]) $
Sinon il est dit de #emph[dimension
infinie].
]
#yexample[
L'espace vectoriel $bb(R)^n$ est de dimension finie car sa base
canonique (qui est une famille génératrice) possède un nombre fini de
vecteurs.

]
#ybtw[
+ Toute famille d'un espace vectoriel $E$ qui contient une famille génératrice de $E$ est elle-même une famille génératrice de $E$.

+ Soit $lr((e_i))_(i in I)$ une famille génératrice d'un espace vectoriel
  $E$ et $i_0 in I$ tel que $e_(i_0)$ est combinaison linéaire des
  vecteurs de $lr((e_i))_(i eq.not i_0)$. Alors $lr((e_i))_(i eq.not i_0)$
  engendre $E$.
]
#ytheo(name: [Théorème de la base extraite])[
Soit $E$ un espace vectoriel non nul, de dimension finie. De toute
famille génératrice on peut extraire une base.

]

#yproof[
    On procède par récurrence sur le cardinal d'une famille génératrice.
    
    La preuve est comme un algorithme :
    - invariant : la famille est génératrice
    - variant : la taille de la famille

    On pose, pour $n in NN^*$,
    $ P(n) : &"si " cal(G) " est une famille" \
        &"génératrice de " E "de taille" n "," \
        &"alors on peut en extraire" \
        &"une base" cal(B) "de" E $

    - _Inititalisation_ : On suppose que $E$ a une famille génératrice $cal(G)$
        de 1 vecteur.

        Soit $u in E$ tel que $E = "Vect"(u)$.

        Or $u != 0$ par hypothèse, donc $(u)$ est libre.

    - _Hérédité_ : Soit $n in NN^*$. On suppose que $P(n)$ est vraie.

        On suppose que $E$ à une famille génératrice $cal(G)$ de $n + 1$
        vecteurs.

        - Si $cal(G)$ est libre, c'est une base.

        - Sinon : soit $u$ un vecteur de $cal(G)$ qui s'exprime comme combinaison
            linéaire des autres.

            Ainsi $G without {u}$ est génératrice de taille $n$.

            Donc par hypothèse de récurrence on peut donc en extraire une base.
]


#ycor(name: [Particularité d'un ev non nul de dimension finie])[
(Corollaire du théorème de la base extraite)

Si $E$ est un espace vectoriel, non réduit à $brace.l 0_E brace.r$, de
dimension finie alors $E$ possède au moins une base.
]

#yproof[
    Soit $cal(G)$ une famille génératrice de $E$.

    On peut compléter $cal(G)$ en une base de $E$. $qed$
]


=== 2. Théorème de la base incomplète


#ybtw[
+ Toute sous-famille d'une famille libre d'un espace vectoriel $E$ est
  encore libre.
+ Soit $lr((e_i))_(i in I)$ une famille libre d'un espace vectoriel $E$ et
  $u in E$ un vecteur qui n'est pas combinaison linéaire des $e_i$,
  $i in I$. Alors la famille $lr((e_i))_(i in I) union brace.l u brace.r$
  est encore libre.
]

#ytheo(name: [Base incomplète])[
Soient $E$ un espace vectoriel non nul de dimension finie, et
$frak(L) = lr((e_1 comma dots.h comma e_k))$ une famille libre de $E$.

Alors on peut compléter la famille libre
$frak(L)$ en une base
$cal(B) = lr((e_1 dots.h comma e_k comma e_(k plus 1) comma dots.h comma e_n))$
de $E$.

(En lui ajoutant des vecteurs d'une famille génératrice $cal(G)$ finie)
]

#yproof[
    Par récurrence descendante sur le cardinal de $abs(cal(G) without frak(L))$.

    On note $N = abs(cal(G))$.
    On pose, pour $n in NN$,
    $ P(n) : &"pour toute famille libre" frak(L) \
        &"telle que" abs(cal(G) without frak(L)) = n "," \
        &frak(L) "peut être complètée en" \
        &"base de E" $
    
    - _Inititalisation_ : On suppose qu'on a une famille libre telle que
        $abs(cal(G) without frak(L)) = 0$.

        Donc $E subset "Vect"(frak(L))$.
        Donc $frak(L)$ est une base.
    
    - _Hérédité_ : Soit $n in NN$. On suppose que $P(n)$ est vraie.

        Soit une famille libre $frak(L)$ telle que
        $abs(cal(G) without frak(L)) = n + 1$.

        - Si $"Vect"(frak(L)) = E$, alors $frak(L)$ est une base.

        - Sinon : il existe $u in cal(G)$ tel que $u in.not "Vect"(frak(L))$ :
            car sinon $cal(G) subset "Vect"(frak(L))$ et donc
            $ E = "Vect"(cal(G)) subset "Vect"(frak(L)) subset E $

            On pose $frak(L)' = frak(L) union {u}$, c'est une famille
            libre de $E$.
            $ abs(cal(G) without frak(L)') = abs(cal(G) without frak(L)) - 1 = n $

            Donc par hypothèse de récurrence,
            $frak(L')$ peut être complètée en une base de $E$ (en lui ajoutant
            des vecteurs de $cal(G)$).
]

#ybtw[
Les théorèmes de la base extraite et de la base incomplète sont encore
vrais en dimension infinie si on admet l'axiome du choix.

]

#yprop(name: [Théorème de la base extraite, et de la base incomplète en dimension infinie (H.-P.)])[
    Soient $frak(L)$ une famille libre de $E$ et $cal(G)$ une famille
    génératrice de $E$ telles que $frak(L) subset cal(G)$.

    Alors il existe une base $cal(B)$ de $E$ telle que $frak(L) subset cal(B) subset cal(G)$.
]

#yproof[
    On considère l'ensemble

    $A = { frak(L)' "famille de" E | cases(frak(L)' "est libre", frak(L) subset frak(L)' subset cal(G)) }$

    $A != emptyset$ car $frak(L) in A$
    
    Soit $(frak(L)_n)_(n in NN)$ une suite croissante d'éléments de
    $A$, au sens de l'inclusion.
    $ forall n in NN, cases(frak(L)_n subset frak(L)_(n+1), frak(L)_n in A) $

    On pose $frak(L)' = union.big_(n in NN) frak(L)_n$. C'est la borne supérieure de $A$.

    Nécessairement $frak(L) subset frak(L)' subset cal(G)$.

    $dots.v$
]


=== 3. Dimension d'un espace vectoriel

#ylemme(name: [Particularité d'une base incluse dans une autre])[
Soient $B_1$ et $B_2$ deux bases de $E$ telles que $B_1 subset B_2$.
Alors $B_1 eq B_2$.

]

#yproof[On suppose $B_2 != B_1$.

On considère alors un vecteur $x$ de $B_2$ n'appartenant pas à $B_1$.

Comme $B_1$ engendre $E$, $x$ est combinaison linéaire des vecteurs de $B_1$, donc $B_1 union {x}$ n'est pas libre.

Or il s'agit d'une sous-famille de la famille libre $B_2$, une contradiction.

Ainsi $B_2 = B_1$.]

#ylemme(name: [Lemme d'échange])[
Soient $B_1$ et $B_2$ deux bases distinctes de $E$.

Alors, pour tout
$a in B_1 backslash B_2$, il existe $b in B_2 backslash B_1$ tel que
$(B_1 backslash brace.l a brace.r) union brace.l b brace.r$ est encore une
base de $E$.

]

#yproof[
    Soit $a in B_1 without B_2$. Alors $C_1 = B_1 without {a}$ est une famille libre, et ce n'est pas une base car $C_1 subset.neq B_1$.
    
    Supposons que pour tout $b in B_2 without B_1$, $C_1 union {b}$ ne soit pas une base de $E$.
    
    Remarquons que c'est aussi le cas pour tous les vecteurs de $B_2 sect B_1$ puisque ces derniers appartiennent à $C_1$. 

Soit $b in B_2$. Comme $B_1$ est une base de $E$, on peut écrire $b$ comme une combinaison linéaire des vecteurs de $C_1 union {a}$.

On distingue deux cas :
- Si la composante de $b$ suivant $a$ est non nulle, alors on peut exprimer $a$ comme combinaison linéaire de vecteurs de $C_1 union {b}$, et donc $C_1 union {b}$ engendre $E$. Comme ce n'est pas une base, alors $C_1 union {b}$ n'est pas libre, et comme $C_1$ est libre, nécessairement $b$ est combinaison linéaire des vecteurs de $C_1$.
- Si la composante de $b$ suivant $a$ est nulle, alors $b$ est combinaison linéaire des vecteurs de $C_1$. 
Ainsi, tout vecteur de $B_2$ est combinaison linéaire des vecteurs de $C_1$, donc $C_1$ engendre $E$.

Or $C_1$ est libre, donc $C_1$ est une base de $E$, une contradiction.
]

#ytheo(name: [Théorème de la dimension])[
Soit $E$ un espace vectoriel non réduit à $brace.l 0_E brace.r$ de
dimension finie.

Alors toutes les bases de $E$ sont finies et ont le même nombre de vecteurs.

]

#yproof[
    Nous utiliserons deux lemmes :
    - pour toutes bases telles que $B_1 subset B_2$, on a $B_1 = B_2$
    - le lemme d'échange d'échange

    Soient $B_1$ et $B_2$ deux bases de $E$.
    
    En itérant le lemme d'échange, on construit des bases $C_k$ de $E$ de même cardinal que $B_1$ (échanger deux vecteurs ne change pas le cardinal) telles que $|C_k sect B_2|$ croît strictement avec $k$, si bien qu'on arrive à une base $C$ contenue dans $B_2$.
    
    On en déduit que $C = B_2$ et $|B_1| = |C|$.
]

#ydef(name: [Dimension d'un ev])[
    Soite $E$ un $KK$-ev de dimension finie.

    La #emph[dimension] de $E$ est le nombre de vecteurs de toute base de $E$,
    elle est notée $dim lr((E))$ ou $dim_KK (E)$.
]

#ydef(name: [Dimension de l'ev nul])[
Par convention, $dim lr((brace.l 0_E brace.r)) eq 0$.
]

#ydef(name: [Noms des evs de dimensions 1 et 2])[
+ Un espace vectoriel de dimension 1 est appelé #emph[droite
  vectorielle].

+ Un espace vectoriel de dimension 2 est appelé #emph[plan vectoriel].

]
#yexample[
+ La base canonique de $bb(R)^n$ est constituée de $n$ vecteurs :
  $bb(R)^n$ est donc de dimension $n$.

+ $dim lr((bb(R)_n lr([X]))) eq n plus 1$.

]
#yexample[
$bb(R)^D$ (où $D$ est un ensemble non vide quelconque), $bb(R) lr([X])$
sont des espaces vectoriels de dimension infinie.

]



#yprop(name: [Propriétés sur le cardinal des familles libres et génératrices])[
Soit $E$ un espace vectoriel de dimension finie $n$.
#set enum(numbering: "A.1.")

+ 
  + Toute famille libre possède au plus $n$ éléments.

  + Toute famille libre de $n$ vecteurs est une base de $E$.

  + Toute famille de $p$ vecteurs avec $p gt n$ n'est pas libre, elle
    est donc liée.

+ 
  + Toute famille génératrice possède au moins $n$ vecteurs.

  + Toute famille génératrice de $n$ vecteurs est une base de $E$.

  + Toute famille de $p$ vecteurs avec $p lt n$ n'est pas génératrice.

]

#yproof[
  + Soit $L$ une famille libre.
    
    D'après le théorème de la base incomplète, on peut compléter $L$ en une base $B$ de $E$.
    
    Comme $L subset B$, $|L| <= |B| = n$ et si $|L|=n$ alors $L = B$. 
  
  + C'est le même raisonnement en utilisant le théorème de la base extraite cette fois.
  ]



#ydef(name: [Rang d'une famille d'ev])[
On appelle #emph[rang] d'une famille de vecteurs
$lr((e_1 comma dots.h comma e_p))$ la dimension de l'espace
$"Vect" lr((e_1 comma dots.h comma e_p))$.

]
#yprop(name: [Caractérisation d'une famille libre par le rang])[
Une famille finie à $p$ éléments est libre si et seulement si elle est
de rang $p$.

]