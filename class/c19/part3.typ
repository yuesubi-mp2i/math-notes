#import "../../template/ytemplate.typ": *


#ypart[Théorème du rang]


=== 1. Rang d'une application linéaire

#ydef(name: [Rang d'une application linéaire])[
Soit $f colon E arrow.r F$ une application linéaire. On appelle
  #emph[rang] de $f$ la dimension de $"Im" lr((f))$.

]
#yprop(name: [Rang à partir de l'image d'une base])[
Soit $E$ un espace vectoriel de dimension finie, $(e_1,...,e_n)$ une famille génératrice de $E$ 
et $f colon E arrow.r F$ une application linéaire.

Alors $"rg"lr((f)) = "rg"(f(e_1),...,f(e_n))$.

]

#yproof[La famille $(f(e_1),...,f(e_n))$ engendre $"Im"(f)$.]

=== 2. Théorème du rang

On suppose dans ce paragraphe que $E$ est de dimension finie.

#yprop(name: [Caractérisation d'un isomorphisme par l'image d'une base])[
Soit $f colon E arrow.r F$ une application linéaire, et
$lr((e_1 comma dots.h comma e_n))$ une base de $E$.

Alors $f$ est un
isomorphisme si et seulement si
$(f lr((e_1)) comma dots.h comma f lr((e_n)))$ est une base de
$F$.

]

#yproof[On sait déjà que $(f(e_1),...,f(e_n))$ engendre $"Im"(f)$, donc $f$ est surjective si et seulement si $(f(e_1),...,f(e_n))$ est une famille génératrice de $F$.

De plus, on vérifie sans peine que $(f(e_1),...,f(e_n))$ est libre si et seulement si $"Ker"(f) = {0_E}$ donc si et seulement si $f$ est injective.
]

#ycor(name: [Dimension des l'ensembles de départ et arrivée d'un isomorphisme])[
(Corollaire de la caractérisation d'un isomorphisme par l'image d'une base)

Soit $f colon E arrow.r F$ un isomorphisme. Alors
$dim lr((E)) eq dim lr((F))$.

]

#yproof[
  Comme $f$ est isomorphique, l'image d'une base de $E$ est une base de $F$
  donc ils ont la même dimension.
]

#ytheo(name: [Théorème du rang])[
Soit $f in cal(L) lr((E comma F))$, avec #emph[$E$ de dimension finie.]
Alors $ dim lr((E)) eq dim lr(("Ker" f)) plus "rg"lr((f)). $

]


#yproof[Soit $U$ un supplémentaire de $"Ker"(f)$ dans $E$ et $g : u in U |-> f(u) in "Im"(f)$.

On vérifie que $g$ est un isomorphisme de $U$ dans $"Im"(f)$.

Ainsi $ &dim("Im"(f)) \ &= dim(U) \ &= dim(E)-dim("Ker"(f)) $]


=== 3. Caractérisation d'un isomorphisme

#yprop(name: [Caractérisation de l'injectivité et la surjectivité par le rang])[
Soit $f in cal(L) lr((E comma F))$, avec #emph[$E$ et $F$ de dimensions
finies.] Alors :

+ $f$ est injective si et seulement si $"rg"lr((f)) eq dim lr((E))$.

+ $f$ est surjective si et seulement si $"rg"lr((f)) eq dim lr((F))$.

]

#yproof[
  + Avec le théorème du rang
    $ dim E = "rg" f &<=> ker f = {0} \
      &<=> f "injective" $
  
  + On remarque que $"Im" f subset F$ donc
    $ "rg" f = dim F ==> "Im" f = F $
    car une base de $"Im" f$ est dans $F$.

    Ainsi,
    $ "rg" f = dim F &<=> "Im" f = F \
      &<=> f "surjective" $
]

#ytheo(name: [Conséquence que la dimension de l'ensemble de depart et d'arrivée d'une application linéaire est la même])[
Soit $f colon E arrow.r F$ une application linéaire telle que $E$ et $F$
sont de même dimension finie. Alors on a les équivalences :

$ f "est injective" &<=> f "est surjective" \
  &<=> f "est bijective" $
]

#yproof[
  Imédiat par les résultats sur la surjectivité et l'injectivité selon le rang.
]