#import "../../template/ytemplate.typ": *


#ypart[Formes linéaires et hyperplans]


#ydef(name: [Forme linéaire])[Une #emph[forme linéaire] sur le $KK$-espace vectoriel $E$ est une application linéaire de $E$ dans $KK$. ]

#ydef(name: [Espace dual (H.-P.)])[L'espace vectoriel des formes linéaires est noté $E^*$ et appelé #emph[espace dual] de $E$.]

#yprop(name: [Surjectivité d'une forme linéaire])[Toute forme linéaire non nulle est surjective.]

#yproof[Soit $f in E^*$ : $"rg"(f)<= dim(KK)=1$, donc $"rg"(f)={0,1}$, donc $f$ est nulle ou surjective.]

#ydef(name: [Hyperplan])[On appelle #emph[hyperplan] de $E$ le noyau d'une forme linéaire non nulle.]

#yprop(name: [Dimension d'un hyperplan])[Si $E$ est de dimension finie $n$, alors tout hyperplan est de dimension $n-1$.]

#yproof[C'est une application du théorème du rang.]

#ytheo(name: [Supplémentaire d'un hyperplan])[Soit $H$ un hyperplan de $E$ et $u in E without H$.

Alors $E = H plus.circle "Vect"(u)$.]

#yproof[Par définition, $H$ est le noyau d'une forme linéaire $f$. Comme $u in.not H$, $f(u) != 0$.

On pose $a = f(u)$.

- Soit $x in E$.

    On pose $v = x - a^(-1)f(x)u$ (on rappelle que $f(x)$ est un scalaire et que $KK$ est un corps). 
    
    Par linéarité de $f$, $f(v) = f(x) - a^(-1) f(x) f(u) = f(x)-f(x) = 0$ donc $v in H$.

    Ainsi $x = v + a^(-1) f(x) u in H + "Vect"(u)$.

- Soit $x in H sect "Vect"(u)$. On écrit $x = lambda u$ avec $lambda in KK$.

    Ainsi $f(x) = lambda f(u)$.
    
    Comme $x in H$, $f(x)=0$ et comme $f(u) != 0$, $lambda = 0$, et donc $x=0$.
    
    Ainsi la somme $H + "Vect"(u)$ est directe.
]

#ytheo(name: [Supplémentaire d'une droite])[Tout supplémentaire d'une droite est un hyperplan.]

#yproof[Soit $D="Vect"(u)$ une droite vectorielle et $H$ un supplémentaire de $D$.

On note alors $p$ la projection sur $D$ parallèlement à $H$, et $f : lambda u |-> lambda$.

On vérifie que $f$ est linéaire et $H = "Ker" (f compose p)$.]

#ycor(name: [Particularité d'un sev de dimension $dim(E) - 1$ de l'ev $E$])[
(Corollaire de "tout supplémentaire d'une droite est un hyperplan")

Si $E$ est de dimension finie $n$, tout sous-espace de dimension $n-1$ est un hyperplan.]

#yproof[Soit $H$ un sous-espace de $E$ de dimension $n-1$.

Comme $n-1<n$, il existe un vecteur $u in E without H$.

Ainsi $H sect "Vect"(u) = {0_E}$ et
$ &dim(H)+dim("Vect"(u)) \ &= n-1+1 \ &=n $
donc $H plus.circle "Vect"(u) = E$.]
