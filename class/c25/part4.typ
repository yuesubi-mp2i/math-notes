#import "../../template/ytemplate.typ": *


#ypart[Chaine de Markov]


#ydef(name: [Chaine de Markov])[
    On considère un _système_ $S$, pouvant se trouver dans une état $A_i$,
    $i in I$, et ce système évolue dans le temps, passant d'un état $A_i$
    à un état $A_j$ etc. de façon _aléatoire_, la transition d'un état
    à un autre ne dépend pas de l'historique des états passés.

    On peut les représenter avec un graphe, chaqu'une des arrêtes ayant
    une probabilité.

    On note $X_n$ l'état du système à l'étape $n$,
    et on note $p_(i,j)$ la probabilité de transition
    de l'état $A_i$ à l'état $A_j$.

    Pour tout $i$,
    $ P(X_n = A_i)
        yt= sum_j P(X_n = A_i | X_(n-1) A_j)
            yttt P(X_(n-1) = A_j)
        yt = sum_j p_(j,i) P(X_(n-1) = A_j) $

    On pose $ U_n = vec(P(X_n = A_1), P(X_n = A_2), dots.v, P(X_n = A_k))$
    et $Q = (p_(i,j))$.

    D'où, $U_n = Q^T U_(n-1)$
    et $U_n = (Q^T)^n U_0$.
]

#ytheo(name: [Théorème ergodique])[
    Soit $cal(C)$ une chaîne de Markov irréductible
    ($forall i!=j$, on peut passer de $A_i$ à $A_j$ en
    a un nombre fini d'étapes) ayant un nombre fini d'états
    et apériodique (pour tout état, le PGCD des longueurs
    des chemins qui commencent et se terminent en cet état vaut 1).

    Alors la suite $((Q^T)^n)$ converge vers une matrice dont toutes
    les colonnes sont égales. $qed$

    N.B. c'est hors-programme.
]