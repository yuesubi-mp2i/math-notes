#import "../../template/ytemplate.typ": *


#ypart[Familles de variables aléatoires]


#ynot(name: [Notations pour $P(X in A, Y in B)$ et $P(X = x, Y = y)$])[
    $X, Y$ deux variables aléatoires sur $(Omega, P)$.

    + On note $P(X in A, Y in B)$ à la place de
        $P((X in A) sect (Y in B))$.

    + On note $P(X = x, Y = y)$ à la place de
        $P((X = x) sect (Y = y))$.
]

#yprop(name: [Décomposition de $P(X=x)$ en fonction de la probabilité deux variables aléatoires])[
    Soient $X ,Y$ deux variables définies sur $(Omega, P)$.

    Pour tout $x in X(Omega)$,
    $ P(X = x) = sum_(y in Y(Omega)) P(X=x, Y=y) $

    Pour tout $y in Y(Omega)$,
    $ P(Y = y) = sum_(x in X(Omega)) P(X=x, Y=y) $

    De plus, si pour tout $y$, $P(Y=y) != 0$, alors
    pour tout $x in X(Omega)$,
    $ &P(X=x) = \
        &sum_(y in Y(Omega)) P(Y=y) P_((Y=y)) (X = x) $
    de même pour $P(Y = y)$ si on peut conditionner par $X=x$.
]

#yproof[
    Formule de probabilités totales. $qed$
]

#ynot(name: [Probabilités conditionnelles $P(B|A)$])[
    Au lieu d'écrire $P_A (B)$ on peut aussi écrire
    $P(B | A)$.
]

#yprop(name: [Formule de transfert avec une fonction à plusieurs variables])[
    Soient $X, Y$ deux variables aléatoires définies sur $(Omega, P)$,
    et $f: X(Omega) times Y(Omega) -> RR$.

    Alors
    $ E(f(X, Y))
        yt = sum_(x in X(Omega)) sum_(y in Y(Omega)) f(x,y)
            yttt P(X=x,Y=y) $
]

#yproof[
    C'est la fonction de transfert appliquée à la variable
    $ Z :& Omega &-> X(Omega) times Y(Omega) \
        & omega &|-> (X(omega), Y(omega)) $
]

#ytheo(name: [Loi faible des grands nombres])[
    Soit $(X_n)_(n in NN)$ une suite de variables aléatoires
    indépendantes (i.e. toute sous-famille finie est indépendante)
    définies sur le même espace probabilisé $(Omega, P)$
    et suivant toues la même loi.

    On note $mu$ leur espérence commune et $sigma$ leur
    écart-type commun.

    Pour tout $epsilon > 0$,
    $ P(abs( 1/n sum_(k=1)^n X_k - mu) >= epsilon) -->_(n->+oo) 0 $

    N.B. Hors-programme en MP2I.
]

#yproof[
    On pose $S_n = 1/n sum_(k=1)^n X_k$.

    Par linéarité de l'espérence :
    $ E(S_n) = 1/n sum_(k=1)^n E(X_k) = mu $

    Les variables $X_1, ..., X_n$ sont mutuellement indépendantes donc
    $ V(sum_(k=1)^n X_k) = sum_(k=1)^n V(X_k) = n sigma^2 $
    donc
    $ V(S_n) = 1/n^2 n sigma^2 = sigma^2/n $

    D'après l'inégalité de Bienaymé-Tchebichev,
    pour tout $epsilon > 0$,
    $ 0 <= P(abs(S_n - mu) >= epsilon) <= sigma^2/(n epsilon^2) -->_(n->+oo) 0 $
]