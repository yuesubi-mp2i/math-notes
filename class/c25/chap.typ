// Probabilités conditionnelles
#import "../../template/ytemplate.typ": *

#ychap[Probabilités conditionnelles]

#include "part1.typ"
#include "part2.typ"
#include "part3.typ"
#include "part4.typ"