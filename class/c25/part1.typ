#import "../../template/ytemplate.typ": *


#ypart[Définition]


#yprop(name: [La probabilité conditionnelle est bien une probabilité])[
    Soit $(Omega, P)$ un espace probabilisé fini et $A in cal(P)(Omega)$ un
    évènement pour lequel $P(A) != 0$.

    Alors l'application
    $ P_A: &cal(P)(Omega) &-> [0,1] \
        &B &|-> (P(A sect B))/P(A) $
    est une probabilité.
]

#yproof[
    Il suffit de vérifier les trois propriétés des probabilités.

    + Soit $B in cal(P)(Omega)$.
        $ &A sect B subset A \
            &=> 0 <=P(A sect B) <= P(A) \
            &=> 0 <= P_A (B) <= 1 $
    
    + $P(Omega) = P(Omega sect A)/P(A) = P(A)/P(A) = 1$

    + Soient $B_1, B_2$ des évènement incompatibles.
        $ &P_A (B_1 union B_2) \
            &= P(A sect (B_1 union B_2))/P(A) \
            &= P((A sect B_1) union (A sect B_2))/P(A) \
            &= P(A sect B_1)/P(A) + P(A sect B_2)/P(A) \
            &= P_A (B_1) + P_A (B_2) $
]

#ydef(name: [Probabilité conditionnelle])[
    Soit $(Omega, P)$ un espace probabilisé fini et $A in cal(P)(Omega)$
    un évènement de probabilité non nulle.

    La probabilité 
    $ P_A: &cal(P)(Omega) &-> [0,1] \
        &B &|-> (P(A sect B))/P(A) $
    est appelée _probabilité (conditionnelle) sachant $A$_.
]

#ytheo(name: [Formule des probabilités composées])[
    Soient $A_1, ..., A_n, B$ des évènement des l'espace probabilisé
    $(Omega, P)$ tels que $P(sect.big_(i=1)^n A_i) != 0$.

    Alors
    $ &P(B sect A_1 sect ... sect A_n) \
        &= P(A_1) P_(A_1) (A_2) times ... times P_(sect.big_(i=1)^n A_i) (B) \
        &= P_(sect.big_(i=1)^n A_i) (B) times product_(k=1)^n P_(sect.big_(i=1)^(k-1) A_i) (A_k) $
]

#yproof[
    On remarque que pour $k in [|1, n|]$,
    $ &sect.big_(i=1)^k A_i supset sect.big_(i=1)^n A_i \
        &=> P(sect.big_(i=1)^k A_i) >= P(sect.big_(i=1)^n A_i) != 0 $
    donc on peut parler de $P_(sect.big_(i=1)^k A_i)$.

    Le membre de droite est un produit téléscopique.
    $ P_(sect.big_(i=1)^n A_i) (B) times
            product_(k=1)^n P_(sect.big_(i=1)^(k-1) A_i) (A_k)
        yt = P(sect.big_(i=1)^n A_i sect B)/P(sect.big_(i=1)^n A_i) times
            ytt product_(k=1)^n P(sect.big_(i=1)^(k-1) A_i sect A_k)/P(sect.big_(i=1)^(k-1) A_i)
        yt = P(sect.big_(i=1)^n A_i sect B)/P(sect.big_(i=1)^n A_i) times
            ytt product_(k=1)^n P(sect.big_(i=1)^k A_i)/P(sect.big_(i=1)^(k-1) A_i) \
        yt = P(sect.big_(i=1)^n A_i sect B) $
]

#ytheo(name: [Formule des probabilités totales])[
    Soit $(A_1, ..., A_n)$ un système complet d'évènement de probabilités
    non nulles.

    Pour tout évènement $B$,
    $ P(B) = sum_(i=1)^n P(A_i) P_(A_i) (B) $
]

#yproof[
    On remarque que
    $ B &= B sect Omega = B sect (union.dot.big_(i=1)^n A_i) \
        &= union.dot.big_(i=1)^n (B sect A_i) $
    d'où,
    $ P(B) &= P(union.dot.big_(i=1)^n (B sect A_i)) \
        &= sum_(i=1)^n P(B sect A_i) \
        &= sum_(i=1)^n P(A_i) P(B sect A_i)/P(A_i) \
        &= sum_(i=1)^n P(A_i) P_(A_i) (B) $
]

#ytheo(name: [Cas particulier de la formule de Bayes])[
    Soit $(Omega, P)$ un espace probabilisé fini et $A in cal(P)(Omega)$
    un évènement de probabilité non nulle.  // tel que $P(A) in ]0, 1[$.

    Pour tout évènement $B in cal(P)(Omega)$ de probabilité non nulle,
    $ P_B (A) = (P_A (B) P(A))/(P(B)) $
    ($= (P_A (B) P(A))/(P_A (B) P(A) + P_overline(A) (B) P(overline(A)))$).
    $qed$
]

#ytheo(name: [Formule de Bayes])[
    Soit $(Omega, P)$ un espace probabilisé fini,
    $(A_k)_(1<=k<=n)$ un système complet d'évènement de probabilité
    non nulle, $B in cal(P)(Omega)$ une évènement de probabilité non nulle.

    Pour $k in [|1, n|]$,
    $ P_B (A_k) &= (P_A_k (B) P(A_k))/P(B) \
        &= (P_A_k (B) P(A_k))/(sum_(i=1)^n P(A_i) P_(A_i) (B)) $

    N.B. il peut être plus pratique de travailler avec l'opposé du
    logarithme des probabilités (porte le nom d'_entropie_),
    vu qu'on a que des produits dans la formule.
]

#yproof[
    On simplifie juste le membre de droite.
    $ &(P_A_k (B) P(A_k))/P(B) \
        &= (P(A_k sect B) P(A_k))/(P(B) P(A_k)) \
        &= P_B (A_k) $
    $qed$
]

#ybtw[
    N.B. l'opposé du logarithme d'une proba est appelé entropie.
]