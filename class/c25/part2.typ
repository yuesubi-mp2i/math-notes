#import "../../template/ytemplate.typ": *


#ypart[Indépendance]


#ydef(name: [Évènements indépendants])[
    Soit $(Omega, P)$ une espace probabilisé, $(A, B) in cal(P)(Omega)$.

    On dit que $A$ et $ B$ sont _indépendants_ si
    $ P(A sect B) = P(A) P(B) $
]

#yprop(name: [Caractérisation de l'indépendance par les probabilités conditionnelles])[
    Soit $(Omega, P)$ un espace probabilisé, $A in cal(P)(Omega)$ tel que
    $P(A) != 0$, et $B in cal(P)(Omega)$.

    Les évènement $A$ et $B$ sont indépendants ssi
    $P_A (B) = P(B)$. $qed$
]

#yprop(name: [Indépendance de l'évènement complémentaire])[
    Soit $(Omega, P)$ un espace probabilisé, $A$ et $B$ deux évènements
    indépendants.

    Alors $overline(A)$ et $B$ sont indépendants.
]

#yproof[
    $ P(overline(A)) P(B) &= (1 - P(A)) P(B) \
        &= P(B) - P(A sect B) \
        &= P(B without (A sect B)) \
        &= P(B sect overline(A)) $
]

#ydef(name: [Indépendance deux-à-deux et indépendance mutuelle])[
    Soit $(Omega, P)$ un espace probabilisé et
    $(A_i)_(1<=i<=n)$ une famille finie d'évènements.

    + On dit que les $A_i$ ($1<=i<=n$) sont _indépendants deux-à-deux_ si
        pour tout $i != j$,
        $ P(A_i sect A_j) = P(A_i) P(A_j) $
    
    + On dit que les évènements $(A_i)_(1<=i<=n)$ sont
        _(mutuellement) indépendants_ si pour toute partie $J$ de $[|1, n|]$
        $ P(sect.big_(j in J) A_j) = product_(j in J) P(A_j) $
]

#yprop(name: [Mutuelle indépendance d'évènements complémentaires])[
    Soit $(Omega, P)$ un espace probabilisé et
    $(A_i)_(1<=i<=n)$ une famille finie d'évènements mutuellement indépendants.

    Pour tout $i in [|1, n|]$, on considère
    $B_i in {A_i, overline(A_i)}$.

    Alors $(B_i)_(1<=i<=n)$ est une famille
    d'évènements mutuellement indépendants. $qed$
]

#ydef(name: [Variable aléatoires indépendantes])[
    Soient $X, Y$ deux variables aléatoires définies sur le même
    espace probabilisé fini $(Omega, P)$.

    On dit que $X$ et $Y$ sont _indépendantes_ si
    pour tout $A in cal(P)(X(Omega))$ et $B in cal(P)(Y(Omega))$,
    $ &P((X in A) sect (Y in B)) \
        &= P(X in A) P(Y in B) $

    Dans ce cas on écrit $X tack.t.double Y$.
]

#yprop(name: [Caractérisation des variables indépendentes par la probabilité sur les singletons])[
    Soient $X, Y$ deux variables aléatoires définies sur le même
    espace probabilisé fini $(Omega, P)$.

    Les variables $X$ et $Y$ sont indépendantes ssi
    pour tout $x in X(Omega)$ et $y in Y(Omega)$,
    $ &P((X = x) sect (Y = y)) \
        &= P(X = x) P(Y = y) $
]

#yproof[
    "$==>$" évident : ${x} in cal(P)(X(Omega))$ et
        ${y} in cal(P)(Y(Omega))$
    
    "$<==$" Soient $A in cal(P)(X(Omega))$ et
    $B in cal(P)(Y(Omega))$.

    $ P((X in A) sect (Y in B))
        yt  = P([union.big.dot_(x in A) (X = x)] sect
            yttt [union.big.dot_(y in B) (Y = y)])
        yt = P(union.big.dot_(x in A)union.big.dot_(y in B) ((X=x) sect
            yttt (Y=y)))
        yt = sum_(x in A) sum_(y in B) P((X=x) sect (Y=y))
        yt = sum_(x in A) sum_(y in B) P(X=x) P(Y=y)
        yt = sum_(x in A) P(X=x) sum_(y in B) P(Y=y)
        yt = P(X in A) P(Y in B) $
]

#ydef(name: [Variables aléatoires deux-à-deux indépendantes et mutuellement indépendantes])[
    Soit $(X_i)_(1<=i<=n)$ une famille de variables aléatoires définies
    sur le même espace probabilisé $(Omega, P)$.

    On dit que les $X_i$ sont

    + _deux-à-deux indépendantes_ si
        $forall i != j, X_i tack.t.double X_j $
    
    + _mutuellement indépendantes_ si
        pour tout $J subset [|1,n|]$ et toute famille
        $(A_j)_(j in J) in product_(j in J) cal(P)(X_j (Omega))$,
        $ &P(sect.big_(j in J) (X_j in A_j)) \
            &= product_(j in J) P(X_j in A_j) $
]

#ylemme(name: [Lemme des coalitions])[
    Soit $(X_i)_(1<=i<=n)$ une famille de variables aléatoires
    définies sur le même espace probabilisé $(Omega, P)$ mutuellement
    indépendantes.

    Soit $k in [|1, n|]$,
    $ f: product_(i=1)^k X_i (Omega) -> E$ et
    $ g: product_(i=k+1)^n X_i (Omega) -> E$
    deux applications.

    Alors les variables $f(X_1, ..., X_k)$ et
    $g(X_(k+1), ..., X_n)$ sont indépendantes.
    
    N.B. on rappelle que $f(X_1, ..., X_k)$ désigne $omega |-> f(X_1 (omega), ..., X_k (omega))$. $qed$
]

#yproof[
    Pas au programme.
]

#yprop(name: [Espérence du produit de variables réelles])[
    Soit $X$ et $Y$ deux variables indépendantes
    définies sur le même espace probabilisé $(Omega, P)$
    à valeurs réelles. Alors
    $ E(X Y) = E(X) E(Y) $

    Attention ! la réciproque est fausse.
]

#yproof[
    $X Y = f(Z)$ où
    $ Z: &Omega &-> X(Omega) times Y(Omega) \
        &omega &|-> (X(omega), Y(omega)) $
    et
    $ f: &X(Omega) times Y(Omega) &-> RR \
        &(x, y) |-> x y $

    D'après la formule de transfert,
    $ E(X Y)
        yt = E(f(Z))
        yt = sum_((x,y) in X(Omega) times Y(Omega)) P(Z = (x,y))
            ytt times f(x, y)
        yt = sum_((x,y) in X(Omega) times Y(Omega)) f (x, y)
            ytt times P(X = x, Y = y)
        yt = sum_(x in X(Omega)) sum_(y in Y(Omega)) x y
            ytt times P(X = x) P(Y = y)
        yt = sum_(x in X(Omega)) x P(X=x)
            ytt times sum_(y in Y(Omega)) y P(Y=y)
        yt = E(X) E(Y) $
    $qed$
]

#ydef(name: [Variables aléatoires non corrélées])[
    Soient $X$ et $Y$ deux variables réelles définies sur un même
    espace probabilisé fini.

    On dit que $X$ et $Y$ sont _non corrélées_ si
    $ E(X Y) = E(X) E(Y) $
]

#yprop(name: [Variance d'une somme de variables non corrélées])[
    Soit $X$ et $Y$ deux variables non corrélées à valeurs réelles.

    Alors $V(X+Y) = V(X) + V(Y)$.
]

#yproof[
    En utilisant Koenig-Huygens,
    $ V(X + Y)
        yt= E((X+Y)^2) - E(X+Y)^2 
        yt= E(X^2+Y^2+2 X Y)
            ytt - (E(X) + E(Y))^2 
        yt= E(X^2+Y^2+2 X Y)
            ytt - E(X)^2 - E(Y)^2
            ytt - 2 E(X) E(Y)
        yt= E(X^2)+E(Y^2)
            ytt +2 E(X Y)
            ytt - E(X)^2 - E(Y)^2
            ytt - 2 E(X) E(Y)
        yt= V(X) + V(Y)
            ytt +2 E(X) E(Y)
            ytt - 2 E(X) E(Y)
        yt= V(X) + V(Y) $
]

#ydef(name: [Covariance])[
    Soient $X$ et $Y$ deux variables réelles définies sur un même
    espace probabilisé fini.

    La _covarience_ de $X$ et $Y$ est
    $ "cov"(X, Y) = E(X Y) - E(X) E(Y) $
]

#yprop(name: [Variance de la somme])[
    Soit $X$ et $Y$ deux variables à valeurs réelles.
    $ &V(X+Y) \
        &= V(X) + V(Y) + 2 "cov"(X,Y) $
]

#yproof[
    En utilisant Koenig-Huygens.
]

#yprop(name: [Variance d'une "grande" somme])[
    Soit $(X_i)_(1<=i<=n)$ une famille de variables aléatoires réelles.
    $ &V(sum_(i=1)^n X_i) \
        &= sum_(i=1)^n V(X_i) + 2 sum_(i<j) "cov"(X_i, X_j) $
    $qed$
]

#ycor(name: [Variance d'une "grande" somme de variables non corrélées])[
    Soit $(X_i)_(1<=i<=n)$ une famille de variables aléatoires réelles
    non corrélées.

    $ V(sum_(i=1)^n X_i) = sum_(i=1)^n V(X_i) $
    $qed$
]

#yproof[
    C'est un corollaire de
    $ &V(sum_(i=1)^n X_i) \
        &= sum_(i=1)^n V(X_i) + 2 sum_(i<j) "cov"(X_i, X_j) $
]

#ycor(name: [Variance d'une binomiale])[
    Si $X tilde cal(B)(n,p)$, alors
    $ V(X) = n p (1 - p) $
]

#yproof[
    On note $X_i$ les variables telles que $X = sum_(i=1)^n X_i$.

    Les $X_i$ suivent des loi de Bernoulli de paramètre $p$
    et les $X_i$ sont indépendants.
]