// Sommes, produits, intégrales (apkg)
#import "../../template/ytemplate.typ": *

#ychap[Sommes et produits finis, intégrales d'une fonction continue]

Cours dans le premier et le second cahier (de la page 64 du premier
à la page 16 du second).