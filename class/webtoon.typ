#import "../template/ytemplate.typ": *


#set page(
    width: 1080pt,
    height: auto,
    margin: (x: 20pt, top: 25pt, bottom: 40pt)
)

#set text(
    size: 62pt,
    lang: "fr"
)


#rect(width: 100%, height: 2400pt - 65pt, inset: 20pt, stroke: none)[
    #align(center + horizon)[
        #text(size: 2em, smallcaps[
            Notes de maths \
            MP2I
        ])

        #text(size: 1.5em)[
            $floral$ #underline(stroke: 0.06em + rgb("#8f3f71"),
                link(<summary>)[Sommaire]
            )
        ]
    ]

    #align(right + bottom)[Clemenceau 2023-2024]
]

#pagebreak()

#show outline: set heading(numbering: none)
#outline(title: [Chapitres], depth: 1) <summary>


#let prev_heading_loc(level: int, loc: location) = {
    query(
        heading.where(level: level)
            .before(loc),
        loc
    ).last().location()
}

#let next_heading_loc(level: int, loc: location) = {
    let arr = query(
        heading.where(level: level)
            .after(loc, inclusive: false),
        loc
    )

    if arr.len() == 0 {
        none
    } else {
        arr.first().location()
    }
}


#set heading(numbering: "1.1")

#show heading: it => {
    if it.level == 1 and it.numbering != none {
        pagebreak()
        let num = numbering(
            it.numbering,
            ..counter(heading).at(it.location())
        )

        align(center, link(<summary>)[
            $floral$ Chapitre #num \
            #it.body
        ])
    } else if it.level == 2 {
        pagebreak()

        let num = numbering(
            it.numbering,
            ..counter(heading)
                .at(it.location())
                .slice(1)
        )

        let chap_loc = prev_heading_loc(level: 1, loc: it.location())

        align(center)[
            #link(chap_loc)[§#num #it.body]
        ]

    } else if it.level == 4 {
        pagebreak()
        let part_loc = prev_heading_loc(level: 2, loc: it.location())

        link(part_loc, it.body)
    } else {
        it.body
    }
}

#show <chap>: it => {
    it

    show par: set block(spacing: 0em)

    show outline.entry: elem => {
        set text(size: 62pt)

        par(
            first-line-indent: 0em,
            hanging-indent: 1em,
            elem
        )
    }


    let entries = heading.where(level: 2).after(it.location()) 
    let next = next_heading_loc(level: 1, loc: it.location())

    if next != none {
        entries = entries.before(next)
    }

    outline(
        title: none,
        target: entries
    )
}

#show <part>: it => {
    it

    show par: set block(spacing: 0em)

    show outline.entry: elem => {
        set text(size: 62pt)

        par(
            first-line-indent: 0em,
            hanging-indent: 1em,
            link(elem.element.location(), elem.element.body)
        )
    }

    let entries = heading.where(level: 4).after(it.location()) 
    let next = next_heading_loc(level: 2, loc: it.location())

    if next != none {
        entries = entries.before(next)
    }

    outline(
        title: none,
        target: entries
    )
}


#show: doc => yconf_math(doc)

#include "complete.typ"


#pagebreak()

#align(center)[
    $floral$ #smallcaps[Fin] 42
]