#import "../template/ytemplate.typ": *

#let _end_of_page(loc) = {
    let footers = query(selector(<__footer__>).after(loc), loc)

    if footers == () {
        return loc
    } else {
        return footers.first().location()
    }
}

#set page(
    header: locate(loc => {
        let chapters = query(
            heading.where(level: 1)
                .before(_end_of_page(loc)),
            loc
        )
        let parts = query(
            heading.where(level: 2)
                .before(_end_of_page(loc)),
            loc
        )

        if chapters != () {
            chapters.last().body
        }
        h(1fr)
        if parts != () {
            parts.last().body
        }
    }),
    footer: [
        #align(
            center,
            counter(page).display(
                "1 / 1",
                both: true
            )
        )
        <__footer__>
    ]
)

#align(center, text(size: 24pt)[*Notes de maths de MP2I*])

#outline(title: [Sommaire], depth: 2)

#pagebreak()


#show: ychapconf

#include "complete.typ"