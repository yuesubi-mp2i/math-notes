#import "../../template/ytemplate.typ": *


#ypart[Dérivées partielles]


#ydef(name: [Dérivée partielle])[
    Soient $D$ un ouvert de $RR^2$ et une fonction
    $f : D → RR$. Soit un point $(a, b) ∈ D$.

    + Si
        $ lim_(h -> 0) (f(a+h, b) - f(a, b))/h $
        existe et est finie, alors ce nombre réel est noté $∂_1 f(a, b)$
        ou $(diff f)/(∂x) (a, b)$ et est appelé _la première dérivée
        partielle de $f$ en $(a, b)$_.

    + Si
        $ lim_(k -> 0) (f(a, b + k) - f(a, b))/k $
        existe et est finie, alors ce nombre réel est noté
        $∂_2 f(a, b)$ ou $(∂f)/(∂y) (a, b)$ et est appelé
        _la deuxième dérivée partielle de $f$ en $(a, b)$_.

    + Soit $i ∈ [|1, 2|]$. Si $∂_i f (a, b)$ existe pour tout
        $(a, b) ∈ D$, alors la fonction $∂_i f : D → RR$ est appelée
        _la $i$-ème dérivée partielle de $f$_.
]

#ydef(name: [Fonction de deux variables de classe $C^1$])[
    Soient $D$ un ouvert de $RR^2$ et une fonction
    $f : D → RR$.

    On dit que la fonction $f$ est de classe $C^1$ sur $D$
    si les 2 dérivées partielles $∂_1 f$ et $∂_2 f$ existent
    et sont continues sur $D$.
]