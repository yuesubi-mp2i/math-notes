#import "../../template/ytemplate.typ": *


#ypart[Le gradiant]


#ydef(name: [Le gradient])[
    On définit _le gradient de $f$ en $(a, b)$_,
    noté $nabla f(a, b)$, par
    $ nabla f(a, b) := (diff_1 f(a, b), diff_2 f(a, b)) $
    on le note aussi $arrow(nabla) f(a, b)$
    ou $arrow("grad") f(a, b)$.
]

#ylemme(name: [Règle de la chaîne])[
    Soient deux fonctions $f : D → RR$ définie sur
    un ouvert $D$ de $RR^2$ et
    $ M : &I → RR^2 \ &t |-> M(x(t), y(t)) $
    définie sur un intervalle $I$ de $RR$.

    Si $M(I) ⊂ D$ et si les fonctions $f$ et $M$
    sont de classe $C^1$, alors la fonction
    $f ◦ M$ est de
    classe $C^1$ et, pour tout $t ∈ I$,
    $ (f ◦ M )' (t)
        yt = x'(t) ∂_1 f(x(t), y(t))
            ytt + y'(t) ∂_2 f(x(t), y(t))
        yt = M'(t) dot ∇f (M (t)) $
]

#yproof[
    Pour tout $t ∈ I$,
    $ cases(x(t + u) = x(t) + u x'(t) + u ε_1(u),
        y(t + u) = y(t) + u y'(t) + u ε_2(u)) $
    car $x$ et $y$ sont de classe $C_1$ sur $I$.

    D'après la formule de Taylor & Young, pour tout $(a, b) ∈ D$,
    $ f (a + h, b + k)
        yt = f (a, b) + h ∂_1 f (a, b)
            ytt + k ∂_2 f (a, b) + ||(h, k)||ε(h, k) $
    car $f$ est de classe $C^1$ sur $D$.

    D'où
    $ f ◦ M (t + u) =
        yt f ◦ M (t) + u x'(t) ∂_1 f (x(t), y(t))
        yt + y'(t) ∂_2 f (x(t), y(t)) + u ε_3(u) $

    Par suite
    $ (f ◦ M (t + u) - f ◦ M (t))/u
        yt -->_(u→0) x'(t) ∂_1 f(x(t), y(t))
            ytt + y'(t) ∂_2 f(x(t), y(t)) $

    Donc $f ◦ M$ est dérivable.
    
    Et cette dérivée est continue car les fonctions $x'$, $y'$,
    $∂_1 f$ et $∂_2 f$ sont continues.

    Enfin, on peut réécrire $(f ◦ M )'(t)$ comme un produit scalaire :
    $ (f ◦ M )'(t) =
        yt (x'(t), y'(t)) times
        yt (∂_1 f(x(t), y(t)), ∂_2 f(x(t), y(t))) $
]