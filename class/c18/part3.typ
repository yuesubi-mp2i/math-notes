#import "../../template/ytemplate.typ": *


#ypart[Continuité]


#ydef(name: [Continuité pour une fonction de deux variables])[
    Soient $D ⊂ RR^2$ un ouvert du plan et
    $f: D -> RR$ une fonction
    définie sur $D$. Soit $(a, b) ∈ D$.

    + On dit que _$f$ est continue en $(a, b)$_ si
        $ f(x, y) -->_((x,y)->(a,b)) f (a, b) $
        autrement dit :
        $ ∀ε > 0, ∃r > 0, ∀(x, y) ∈ D,
            yt ||(x - a, y - b)|| < r
            yt => abs(f (x, y) - f (a, b)) < ε $

    + On dit que _$f$ est continue sur $D$_ si $f$ est continue
        en tout point de $D$.
]