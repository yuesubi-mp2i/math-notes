#import "../../template/ytemplate.typ": *


#ypart[Ouverts]


#ydef(name: [Boule et partie ouverte])[
    Soient $r > 0$ un réel strictement positif,
    $(a, b) ∈ RR^2$ un point du plan
    et $A ⊂ RR^2$ une partie du plan.

    + On appelle _boule de centre $(a, b)$ et de rayon $r$_,
        et on note $cal(B)((a, b), r)$, la partie de $RR^2$ définie par
        $ (x, y) ∈ cal(B)((a, b), r)
            yt <=> ||(x - a, y - b)|| < r $

    + On dit que $A$ est _un ouvert ou une partie ouverte de $RR^2$_ si
        $ ∀(a, b) ∈ A, ∃r > 0,
            yt cal(B)((a, b), r) ⊂ A. $
]