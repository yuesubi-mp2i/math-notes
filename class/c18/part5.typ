#import "../../template/ytemplate.typ": *


#ypart[Formule de Taylor & Young]


#ytheo(name: [Formule de Taylor & Young (fonctions à deux variables)])[
    Soient $D$ un ouvert de $RR^2$ et une fonction $f : D → RR$.
    
    Si $f$ est de classe $C^1$, alors, pour tout $(a, b) ∈ D$,
    $ f (a + h, b + k)
        yt = f(a, b) + h ∂_1 f(a, b)
            ytt + k ∂_2 f(a, b) + ||(h, k)||ε(h, k)
        yt = f(a, b) + (h, k) dot nabla f(a,b)
            ytt + o(h, k) $
    où $ε(h, k) -->_((h,k)→(0,0)) 0$.
]

#yproof[
    $ f (a + h, b + k) - f (a, b)
        yt = f (a + h, b + k) - f (a, b + k)
            ytt + f (a, b + k) - f (a, b) $

    On applique le théorème des accroissements finis à chaque différence :
    $ f (a + h, b + k) - f (a, b) =
        yt h ∂_1 f (a + θ_1 h, b + k)
        yt + k ∂_2f (a, b + θ_2k) $
    où $θ_1$ et $θ_2$ appartiennent à $]0, 1[$.
    
    Or chaque dérivée partielle est continue (car $f$ est de classe $C^1$),
    d'où :
    $ f (a + h, b + k) - f (a, b) =
        yt h [∂_1 f (a, b) + ε_1(h, k)]
        yt + k [∂_2 f (a, b) + ε_ 2(h, k)] $
    où $ε_1(h, k)$ et $ε_2(h, k)$ tendent vers zéro quand $(h, k) → (0, 0)$.

    D'où
    $ f (a + h) - f (a, b) =
        yt h ∂_1f (a, b) + k ∂_2f (a, b)
        yt + h ε_1(h, k) + k ε_2(h, k) $

    Or
    $ abs(h ε_1(h, k) + k ε_2(h, k))
        yt <= abs(h ε_1(h, k)) + abs(k ε_2(h, k))
        yt <= sqrt(h^2 + k^2) times
            ytt underbrace((|ε_1(h, k)| + |ε_2(h, k)|), -->_((h,k)→(0,0)) 0) $
]

#ycor(name: [Condition suffisante pour qu'une fonction de deux variables soit continue])[
    (Corollaire de la formule de Taylor & Young)

    Toute fonction de classe $C^1$ est continue.
]

#yproof[
    $ (a + h, b + k) - f (a, b)
        yt = h∂1f (a, b) + k∂2f (a, b)
            ytt + ||(h, k)||ε(h, k)
        yt -->_((h,k)→(0,0)) 0 $
    $qed$
]