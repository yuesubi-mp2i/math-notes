#import "../../template/ytemplate.typ": *


#ypart[Courbes de niveau]


#ydef(name: [Courbe de niveau])[
    Soient $D ⊂ RR^2$ une partie du plan et $f : D -> RR$
    une fonction définie sur $D$.
    
    Pour chaque réel $K$, la courbe de niveau $K$ de la fonction $f$
    est l'ensemble des points $(x, y) ∈ D$ tels que $f(x, y) = K$.
]