#import "../../template/ytemplate.typ": *


#ypart[Bonus]


#ydef(name: [Dérivée selon un vecteur])[
    Soient un ouvert $D in RR^2$, un point $a in D$ et un vecteur $v in RR^2$.

    On dit qu'une fonction _$f: D -> RR$ est dérivable en le point $a$
    suivant le vecteur $v$_ si la limite
    $ lim_(t -> 0) (f(a + t v) - f(a))/t $
    existe et est finie.
]