#import "../../template/ytemplate.typ": *


#ypart[Les extrema locaux voire globaux]


#ydef(name: [Minimum local et global pour les fonctions de deux variables])[
    Soient une partie $D ⊂ RR^2$, un point $(a, b) ∈ D$ et
    une fonction $f : D → RR$.
    
    On dit que la fonction $f$ possède :

    + un _minimum global en (a, b)_ si
        $ ∀(x, y) ∈ D, f (x, y) ≥ f (a, b)) $

    + un _minimum local en (a, b)_ si
        $ ∃ε > 0, ∀(x, y) ∈ D,
            yt ||(x - a, y - b)|| ≤ ε
            yt => f (x, y) ≥ f (a, b) $
]

#yprop(name: [Une condition nécessaire d'extremum local sur un ouvert])[
    Soient un ouvert $D ⊂ RR^2$, un point $(a, b) ∈ D$
    et une fonction $f : D → RR$ de classe $C^1$.

    Si $f$ possède un extremum local en $(a, b)$,
    alors $∇f (a, b) = (0, 0)$.
]

#yproof[
    Soient les fonctions
    $ cases(f_1 : t |-> f(t, b), f_2 : t |-> f(a, t)) $

    L'ensemble $D$ est un ouvert, d'où il existe $ε > 0$
    tel que la fonction $f_1$ est définie sur $]a_1 - ε, a_1 + ε[ ⊂ RR$.
    
    La fonction $f_1$ est dérivable en $a$ (car $f$ est de classe $C^1$)
    et elle possède un extremum local en a, donc sa dérivée est nulle en $a$ :
    $f'_1(a) = 0$.

    Or $f'_1 (a) = ∂_1 f(a, b)$.
    
    De même avec la fonction $f_2$.
    $qed$
]