#import "../../template/ytemplate.typ": *


#ypart[Dénombrement]


#yexample[
    #cetz.canvas(length: 5em, {
        import cetz: *

        plot.plot({

        })
    })

    On place 4 point, non alignés 3 à 3.
    On trace les droites qui passent par 2 de ces points.
    On suppose que 2 droites ne sont jamais parallèles,
    et si 3 droites sont concourantes, leut point
    d'intersection est l'un des 4 points de départ.

    Combien a-t-on formé de triangles ?
]

#ydef(name: [$p$-liste])[
    Soit $E$ un ensemble à $n$ éléments et $p in NN^*$.

    Une *$p$-liste de $E$* est un élement de $E^p$.

    (synonyme de $p$-uplet)
]

#yprop(name: [Nombre de $p$-listes])[
    Soit $E$ un ensemble à $n$ éléments et $p in NN^*$.
    $ abs(E)^p = n^p $
]

#yproof[
    $abs(E^n) = abs(E)^n$
    $qed$
]

#yexample[
    Vous avez oublié votre code PIN mais vous saveé qu'il
    est constitué de $8$ chiffres.

    Combien y-a-t-il de possibilités ?

    Il y a $10^8$ 8-listes de $[|0, 9|]$.
]

#ybtw(name: [Principe multiplicatif])[
    arbres

    À chaque étape le nombre de branche de chaque neuds
    d'un même range doit être le même.

    On peut alors multiplier l'arité de chaque rang.
]

#yexample[
    Combien y a-t-il de codes à 8 chiffres,
    les chiffres étant 2 à 2 disticts ?

    $ 10 times 9 times 8 times 7 = 10!/6! $
]

#ybtw[
    Les termes $p$-arrangement et $p$-uplet.
]

#ydef(name: [Arrangement])[
    Un *$p$-arrangement* de $E$ est un $p$-uplet
    $(x_1, ..., x_p) in E^p$ tel que
    $ forall i != j, x_i != x_j $
]

#yprop(name: [Nombre d'arrangements])[
    Soit $E$ de cardinal $n$ et $p <= n$.

    Il y a $n!/(n - p)!$ $p$-arrangements de $E$.

    On le note parfois
    $ A^p_n = n!/(n-p)! $
]

#yproof[
    Soit $E$ un ensemble de cardinal $n$, $p <= n$.

    On pose
    $ cal(A)^p_n = { (x_1, ..., x_p) in E^p
        yt | forall i != j, x_i != x_j } $

    - _Initialisation_ :
        $ abs(cal(A)^1_n) &= abs({ x in E }) \
            &= abs(E) \
            &= n \
            &= n!/(n-1)! $

    - _Hérédité_ : Soit $p in [|1, n - 1|]$ tel que
        $ abs(cal(A)^p_n) = n!/(n - p)! $

        On pose,
        $ f: &cal(A)^(p+1)_n --> cal(A)^p_n \
            &(x_1, ..., x_(p+1)) |-> (x_1, ..., x_p) $

        Soit $(x_1, ..., x_p) in cal(A)^p_n$.
        $ abs(f^(-1) ({(x_1, ..., x_p)}))
            yt = abs({(x_1, ..., x_p, a) in E^(p+1)
                ytt | a in.not {x_1, ..., x_p}})
            yt = n - p $
        
        D'après le principe des bergers,
        $ abs(cal(A)^(p+1)_n) &= (n - p) abs(cal(A)^p_n) \
            &= (n - p) n!/(n - p)! \
            &= n!/(n - p - 1)! \
            &= n!/(n - (p+1))! $
]

#ydef(name: [Permutation])[
    Une *permutation* de $E$ (avec $abs(E) = n$) est
    un $n$-arrangement de $E$.
]

#yprop(name: [Nombre de permutations])[
    Il y a $n!$ permutations de $E$ si $abs(E) = n$.
]

#yexample[
    Combien y a-t-il d'anagrammes du mot COURS ?

    Il y en a $5! = 120$.
]

#yexample[
    Combien y a-t-il d'anagrammes du mot
    MATHEMATIQUES ?

    Chaque anagrammes correpond à 16 permutations.

    Il y a donc $13!/16$ anagrammes.
]

#ydef(name: [Combinaison])[
    Une *$p$-combinaison* de $E$ est un sous-ensemble
    de $E$ de cardinal $p$.

    Pour former une $p$-combinaison, il faut choisir
    $p$ éléments distincts de $E$, l'ordre dans lequel
    on a choisi ces éléments n'ayant pas d'importance.
]

#ylemme(name: [Nombre de combinaisons])[
    Soit $n = abs(E)$.

    Il y a $binom(n, p)$ $p$-combinaisons de $E$.

    $ binom(n, p) = n!/(p! (n - p)!) = A^p_n/n! $
]

#yproof[
    On note $cal(A)_n^p$ l'ensemble des $p$-arrangements de $E$
    (où $abs(E) = n$), et $cal(C)_n^p$ celui des
    $p$-combinaisons de $E$.

    On pose
    $ f: &cal(A)^p_n --> cal(C)^p_n \
        &(x_1, ..., x_p) |-> {x_1, ..., x_p} $
    
    Soit ${x_1, ..., x_p} in cal(C)^p_n$.
    $ f^(-1) ({{x_1, ..., x_p}})
        yt = { (sigma(x_1), ..., sigma(x_p))
            ytt | sigma: {x_1, ..., x_p}
                yttt --> {x_1, ..., x_p}
                yttt "bijective"} $
    donc $abs(f^(-1) ({{x_1, ..., x_p}})) = p!$.

    D'après le principe des bergers :
    $ abs(cal(A^p_n)) = p! abs(cal(C)^p_n) $
    donc
    $ abs(cal(C)^p_n) = n!/((n-p)! p!) = binom(n, p) $
]

#yexample[
    Combien y a-t-il d'anagrammes du mot
    MATHEMATIQUES ?

    On considère les 13 emplacements pour mettre
    les lettres.

    $ binom(13, 2) binom(11, 2) binom(9, 2) dot 7 dot binom(6, 2) dot
        yt 4 dot 3 dot 2 dot 1 $
    
    Car $binom(13, 2)$ correspond au nombres de façon de mettre
    M dans les 13 emplacements.
]

#yexample[
    + Combien y a-t-il de choix de 2 délégués dans
        une classe de 45 ?
        $ binom(45, 2) = (45 dot 44)/2 = 22 dot 45 = 990 $
    
    + Combien y a-t-il de podiums dans une compétition
        avec $n$ ?
        $ n!/(n - 3)! = n (n - 1) (n - 2) $
    
    + Combien y a-t-il de sous-ensembles de $E$
        (où $abs(E) = n$)
        $ sum_(k=0)^n binom(n, k) = (1 + 1)^2 = 2^n $
    
    + Combien y a-t-il de couples $(X, Y) in cal(P)(E)^2$
        tels que $X subset Y$ ?

        On pose $n = abs(E)$.

        - Méthode 1
            + On forme $Y$.

                Il y a $2^n$ possibilités pour former $Y$.

            + On forme $X$.

                On pose $k = abs(Y)$.

                Il y a $2^k$ possibilités pour former $X$.
            
            Donc
            $ sum_(k=0)^n binom(n, k) 2^k = (2 + 1)^n = 3^n $
        
        - Méthode 2
            + On forme $X$.
                Il y a $2^n$ possibilités pour former $X$.

            + On forme $Y without X$.

                On pose $k = abs(X)$.

                $ sum_(k=0)^n binom(n, k) 2^(n - k) = (1 + 2)^n = 3^n $

        - Méthode 3

            Pour chaque $x in E$, on a trois possibilités
            - On met $x$ dans $E$
            - On met $x$ dans $Y without E$
    
    + Combien y a-t-il de configurations de 5 cartes
        issues d'un jeu de 32 avec
        - (a) 1 paire et 3 cartes de valeurs distinctes exactement,
        - (b) 2 paires (pas de carré) exactement,
        - (c) au moins 1 paire ?

        (a) $8 dot binom(4, 2) times binom(7, 3) dot 4^3$
        - On choisi la paire :
            - Il y a 8 chiffres possibles
            - Avec les 4 cartes de mêmes chiffres, on en
                choisi 2 parmis 4
        - On choisi les 3 cartes :
            - On choisi les chiffres pour les 3 cartes de
                valeurs distinctes. Comme 1 chiffre est
                déjà pris, on en choisi 3 parmis 7
            - On choisi la couleurs de cartes choisies,
                chaqu'une à 4 couleurs possibles
        
        (b) $binom(8, 2) binom(4, 2)^2 dot 6 dot 4$

        (c) $binom(32, 5) - binom(8, 5) dot 4^5$
        Utilisation du complémentaire
    
    + Exos de $n$ points non alignés, qui ne forment pas
        des droites parallèles

        Il faut compter le nombre de droites
        Or une droite passe seulement par deux points
        $binom(n, 2)$ droites

        $binom(6, 3)$ configuration de 3 droites

        Parmis ces configuraions, il y a 4 configurations
        concourantes.

        Il y a $binom(6, 3) - 4 = 6!/4! - 4 = 16$
        triangles

        Avec $n$ points au lieu de 4.
        $ binom((n (n - 1))/2, 3) - n binom(n - 1, 3) $
]