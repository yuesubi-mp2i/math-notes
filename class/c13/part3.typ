#import "../../template/ytemplate.typ": *


#ypart[Preuves combinatoires]


#yexample[
    $ forall n in NN^*, forall k in [|0, n|],
        yt binom(n, k) = binom(n, n-k) $
    
    #underline[Preuve] :
    Soient $n in NN^*$ et $k in [|0, n|]$.
    $binom(n, k)$ est le nombre de façons de choisir
    $k$ entiers parmi $[|1, n|]$, et donc le nombre
    de façons de choisir $n - k$ entiers que l'on ne
    garde pas.
    Donc $binom(n, k) = binom(n, n - k)$.

    On note $cal(A)_k = {A subset [|1, n|] | abs(A) = k }$, et
    $ f: &cal(A)_k --> cal(A)_(n-k) \
        &A |-> [|1, n|] without A $
    $f$ est bijective.

    D'où,
    $ binom(n, k) = abs(cal(A)_k) = abs(cal(A)_(n-k)) = binom(n, n - k) $
]

#yexample[
    Soient $n in NN^*$ et $k in [|1, n|]$.
    $ binom(n+1, k) = binom(n, k) + binom(n, k - 1) $

    On note $cal(C)_k (E)$ l'ensemble des $k$-combinaisons d'un
    ensemble $E$.
    $ cal(C)_k ([|1, n + 1|]) = cal(A) union cal(B) $
    où
    $ cal(A) = { X in cal(C)_k ([|1, n+1|])
        yt | n + 1 in.not X } $
    et
    $ cal(B) = { X in cal(C)_k ([|1, n+1|])
        yt | n + 1 in X } $
    
    On remarque que $cal(A) sect cal(B) = emptyset$ et
    $cal(A) = cal(C)_k ([|1, n|])$.

    D'où, $abs(cal(A)) = binom(n, k)$.

    Soit
    $ f: &cal(B) --> cal(C)_(k-1) ([|1, n|]) \
        &B |-> B without {n + 1} $
    $f$ est bijective
    $ abs(cal(B)) = binom(n, k-1) $

    Donc
    $ binom(n+1, k) &= abs(cal(C)_k ([|1, n+1|])) \
        &= abs(cal(A)) + abs(cal(B)) \
        &= binom(n, k) + binom(n, k-1) $
]

#yexample[
    Monter que
    $ forall (a,b) in CC^2, forall n in NN^*,
        yt (a + b)^n = sum_(k+0)^n binom(n, k) a^k b^(n-k) $
    
    $ (a_1 + a_2)^n
        yt = product_(j=1)^n sum_(i_j=1)^2 a_(i_j)
        yt = sum_((i_1, ..., i_n) in {1,2}^n) product_(j=1)^n a_(i_j)
        yt (*)
        yt = sum_(K subset [|1, n|]) a_1^abs(K) a_2^(n - abs(K))
        yt ("on regroupe suivant le" \ yt "cardinal de " K)
        yt = sum_(k=0)^n sum_(K subset [|1, n|] \ abs(K) = k) a_1^k a_2^(n - k)
        yt = sum_(k=0)^n binom(n, k) a_1^k a_2^(n-k) $
    
    $(*)
    $
    ...
]

#yexample[
    forule du trinôme
    ...
]