#import "../../template/ytemplate.typ": *

#ydate(2024, 1, 10)

#ypart[ Cardinal ]


#ydef(name: [Cardinal d'un ensemble])[
    Soit $E$ un ensemble non vide et $n in NN^*$.

    On dit que *$E$ est de cardinal $n$*
    s'il existe une bijection de $E$ dans $[|1, n|]$.
]

#ydef(name: [Ensemble fini ou infini])[
    On dit que $E$ est *fini* s'il existe $n in NN$
    tel que $E$ soit de cardinal $n$, sinon on dit que
    $E$ est *infini*.
]

#ytheo(name: [Bijection entre deux ensembles d'entiers jusqu'à des rangs différent])[
    Soit $(n, p) in NN_*^2$ avec $n != p$. 

    Il existe pas de bijection de $[|1, n|]$
    dans $[|1, p|]$.
    $qed$
    // peut être prouvé par récurrence
]

#ybtw[
    Pour trouver le cardinal d'un ensemble,
    on peut essayer de construire une bijection de
    $E$ dans $[|1, n|]$ ("compter sur les doigts").
]

#ynot(name: [Cardinal d'un ensemble])[
    Soit $E$ un ensemble.

    Son cardinal est noté
    $ abs(E) = hash E = "Card"(E) in NN union {+oo} $
]

Dans la suite du cours, tous les ensembles considérés sont finis.

#yprop(name: [Cardinaux des ensembles, finis, de départ et d'arrivée d'une bijection])[
    Soit $E$ et $F$ deux ensembles finis et
    $f: E --> F$ un bijection.

    Alors $abs(E) = abs(F)$.
]

#yproof[
    On note $n = abs(F) in NN$.

    // dessin
    // E --f--> F
    //  \       |
    //    \     | phi
    //      \   |
    //     [|1, n|]
    #align(center, cetz.canvas(length: 3em, {
        import cetz.draw: *

        content((-1, 1), [$E$])
        content((1, 1), [$F$])
        content((1, -1), [$[|1, n|]$])
        
        line((-0.5, 1), (0.5, 1), mark: (end: ">"))
        line((1, 0.5), (1, -0.5), mark: (end: ">"))
        line((-0.5, 0.5), (0.5, -0.5), mark: (end: ">"))

        content((0, 1), [$f$])
        content((1, 0), [$phi$])
        content((0, 0), [$phi compose f$])
    }))

    Soit $phi: F --> [|1, n|]$ une bijection.

    Alors $phi compose f$ est une bijection
    de $E$ dans $[|1, n|]$.

    Donc $abs(E) = n$.
    $qed$
]

#yprop(name: [Parties d'une ensemble fini])[
    Soient $E$ un ensemble et $F in cal(P)(E)$.

    Si $E$ est fini, alors $F$ aussi et $abs(F) <= abs(E)$.
]

#yproof[
    Soient $n = abs(E)$, $i: F --> E,  x |-> x$
    et $phi: E --> [|1, n|]$ bijective.

    // dessin
    // F --i--> E
    //  \       |
    //    \     | phi
    //      \   |
    //     [|1, n|]
    #align(center, cetz.canvas(length: 3em, {
        import cetz.draw: *

        content((-1, 1), [$F$])
        content((1, 1), [$E$])
        content((1, -1), [$[|1, n|]$])
        
        line((-0.5, 1), (0.5, 1), mark: (end: ">"))
        line((1, 0.5), (1, -0.5), mark: (end: ">"))
        line((-0.5, 0.5), (0.5, -0.5), mark: (end: ">"))

        content((0, 1), [$i$])
        content((1, 0), [$phi$])
        content((0, 0), [$phi compose i$])
    }))

    Donc $phi compose i: F --> [|1, n|]$ est injective.

    #pad(left: 1em)[
        #underline[Lemme] : Toute partie de $[|1, n|]$
        est finie et de cardinal $<= n$.
    ]

    Donc
    $ &F --> phi compose i (F) \
        &x |-> phi compose i (x) $
    est bijective, et $phi compose i (F)$ est
    une partie de $[|1, n|]$ et donc
    $ abs(F) = abs(phi compose i (F)) <= n $
    $qed$
]

Avec la même preuve, on a aussi :

#yprop(name: [Ensemble injectif dans une ensemble fini])[
    Soient $E$ un ensemble fini, $F$ un ensemble,
    $i: F --> E$ une injection.
    
    Alors $F$ est fini et $abs(F) <= abs(E)$.

    Si $abs(F) = abs(E)$ alors $i$ est bijective.
    $qed$
]

#yprop(name: [Ensemble d'arrivé d'une surjection sur un ensemble fini])[
    Soient $E$ un ensemble fini et $F$ un ensemble,
    $s: E --> F$ une surjection.

    Alors $F$ est fini et
    $ abs(E) >= abs(F) $

    Si $abs(E) = abs(F)$, alors $s$ est bijective.
]

#yproof[
    Soit
    $ i: &F --> E \
        &x |-> "l'un des antécédents \n de x par s" $
    
    Alors $s compose i = id_F$.

    Comme $id_F$ est injective, $i$ est injective.
    $qed$
]

#ybtw[
    Ces propositions sont aussi connues sous le nom de
    "principe des tiroirs" (pigeon-hole principle).

    On considère $p$ objets à ranger dans $n$ tiroirs.
    Si $p > n$ alors au moins un tiroir contient 2
    objets au moins.
    
    En effet : soit $f: O -- T$ où $O$ est l'ensemble
    des objets et $T$ celui des tiroirs.
    Comme $abs(O) > abs(T)$, $f$ n'est pas injective.
    $ exists (x, y) in O^2, cases(x != y, f(x) = f(y)) $
]

#yprop(name: [Principe additif])[
    // principe additif
    Soient $E$ un ensemble fini et $(A, B) in cal(P)(E)^2$
    tel que $A union B != emptyset$.

    Alors, $abs(A union B) = abs(A) + abs(B)$.
]

#yproof[
    Soit $n = abs(A union B)$. 

    Soient $a = abs(A)$, $b = abs(B)$, $f: A --> [|1, a|]$
    et $g: B --> [|1, b|]$ deux bijections.

    On vérifie que
    $ h: &[|1, b|] --> [|a + 1, a + b|] \
        &k |-> a + b $
    est bijective.

    Soit
    $ phi: &A union B --> [|1, a + b|] \
        &k |-> cases(f(k) "si" k in A, h(g(k)) "si" k in B) $
    
    On pose
    $ psi: &[|1, a + b|] --> A union B \
        & b |-> cases(f^(-1)(b) "si" b <= a,
                g^(-1) compose h^(-1) (b) "si" b > a) $
    
    On vérifie que
    $ cases(phi compose psi = id_[|1, a + b|],
        ... ) $
    
    ...
]

#yprop(name: [Formule du crible (corrolaire du principe additif)])[
    Soit $(A, B) in cal(P)(E)^2$ où $E$ est fini.

    Alors, $abs(A union B) = abs(A) + abs(B) - abs(A sect B)$.
]

#yproof[
    On pose
    $cases(A' = A without (A union ))$

    ...

    ...

    // venn diagram

    $ (*) <=> abs(A union B) = abs(A') + abs(B') + abs(C) $
    car $C sect B') != emptyset$.

    $ abs(A') + abs(B') + abs(C)
        yt = abs(1') + abs(C) + abs(B') + abs(C) - abs(C)
        yt = abs(A' union C) + abs(B' union C) - abs(C)
        yt = abs(A) + abs(B) - abs(C) $
    $qed$
]

#yprop(name: [Cardinal d'ensembles disjoins finis])[
    Soient $E$ un ensemble fini, et $(A_i) in cal(P)(E)^[|1, n|]$
    telle que
    $ forall i != j, A_i sect A_j = emptyset $

    Alors
    $ abs(union.big_(i=1)^n A_i) = sum_(i=1)^n abs(A_i) $
]

#yproof[
    par récurrence sur $n$.
    $qed$
]

#yexercise[
    Soit $E$ un ensemble fini et $(A_i) in cal(P)(E)^[|1, n|]$.

    Montrer que
    $ abs(union.big_(i=1)^n A_i) = sum_(k=1)^n (-1)^(k + 1)
        yt sum_(1 <= i_1 < i_2 < ... < i_k < n)
            abs(sect.big_(j=1)^k A_(i j)) $
    
    $n=2$ : $abs(A_1 union A_2) = sum_(i_1 = 1)^2 abs(A_i)$

    $n=3$
]

#yprop(name: [Lemme des bergers])[
    Soit $f: E --> F$ une application telle qu'il
    existe $p in NN^*$ tel que
    $ forall y in F, abs(f^(-1) {y}) = p $

    Alors $abs(E) = p abs(F)$.
]

#yproof[
    On définit une relation $tilde$ sur $E$ :
    $ a tilde b <=> f(a) = f(b) $
    Il s'agit d'une relation d'équivalence.

    Soit $R$ un système de représentant de $tilde$.
    $ E = union_(x in R) overline(x) $
    et cette réunion est disjointe.

    Donc
    $ abs(E) = sum_(x in R) abs(overline(x)) $

    Soit $x in R$. On pose $y = f(x)$.
    $ overline(x) &= { a in E | a tilde x } \
        &= { a in E | f(a) = y } \
        &= f^(-1) ({y}) $
    donc $abs(overline(x)) = p$.

    Donc $abs(E) = sum_(x in R) p = p abs(R)$.

    // dessin ens arriv et dep de f

    Soit $phi: R --> F, x |-> f(x)$.

    Montrons que $phi$ est bijective.

    - Soit $(x,y) in R^2$. On suppose $phi(x) = phi(y)$.

        Alors $f(x) = f(y)$, donc $x tilde y$, donc $x = y$.
    
    - Soit $alpha in F$.

        $abs(f^(-1) ({alpha})) = p$ donc il existe
        $beta in E$ tel que $f(beta) = alpha$.

        Soit $x in R$ un représentant de $overline(beta)$
        et alors $phi(x) = f(x) = f(beta) = alpha$.

    Ainsi $phi$ est bijective et donc $abs(R) = abs(F)$.
    $qed$
]

#yprop(name: [Cardinal d'un couple d'ensembles finis])[
    Soient $E$ et $F$ deux ensembles finis.

    Alors $E times F$ est fini et
    $ abs(E times F) = abs(E) times abs(F) $
]

#yproof[
    Soit $p: E times F --> F, (x, y) |-> y$.

    $ forall y in F, p^(-1) ({y}) = { (x, y) | x in E } $

    On fixe $y in F$.
    $ &E --> {(x, y) | x in E} \
        &x |-> (x, y) $
    est bijective donc $abs({(x,y) | x in E}) = abs(E)$
    et donc
    $ forall y in F, abs(p^(-1) ({y})) = abs(E) $

    D'après le principe des bergers,
    $ abs(E times F) = abs(E) abs(F) $
    $qed$
]

#yprop(name: [Cardinal d'un produit cartésien d'ensembles finis (corrolaire du cardinal d'un couple)])[

    Soit $E_1, ..., E_n$ des ensembles finis.

    Alors $E_1 times ... times E_n$ est fini
    et donc $abs(E_1 times ... times E_n) = product_(i=1)^n abs(E_i)$.
    $qed$
]

#yprop(name: [Cardinal de l'ensemble des familles finies à valeurs finies])[
    Soient $E$ et $F$ deux ensembles finis.

    Alors $F^E$ est fini et
    $ abs(F^E) = abs(F)^abs(E) $
]

#yproof[
    Soit $n = abs(E)$.

    On notes $x_1, ..., x_n$ les éléments de $E$.

    On définit
    $ phi: &F^E --> F^n \
        &f |-> (f(x_1), f(x_2), ..., f(x_n)) $$$
    $phi$ est bijecive.

    Donc
    $ abs(F^E) =
        abs(underbrace(F^n, underbrace(F times ... times F, n "fois")))
        = abs(F)^n $
    $qed$
]

#ybtw[
    On a défini une permutation de $E$ ($abs(E) = n$)
    comme un $n$-arrangement.

    Il y a donc $n!/(n-n)! = n!$ permutations.

    Une définition équivalent est :
    on appelle *permutation* toute bijection
    de $E$ dans $E$.

    En effet, un arrangement $(x_1, ..., x_n)$ de
    $E$ (avec $abs(E) = n$) correspond à la bijection
    $g = f compose phi$

    #align(center, cetz.canvas(length: 3em, {
        import cetz.draw: *

        content((-1, 1), [$E$])
        content((1, 1), [$[|1, n|]$])
        content((1, -1), [$E$])
        
        line((-0.5, 1), (0.5, 1), mark: (end: ">"))
        line((1, 0.5), (1, -0.5), mark: (end: ">"))
        line((-0.5, 0.5), (0.5, -0.5), mark: (end: ">"))

        content((0, 1), [$phi$])
        content((1, 0), [$f$])
        content((0, 0), [$g$])
    }))

    avec $phi: E --> [|1, n|]$ est une numérotation de
    $E$, et
    $ f: &[|1, n|] --> E \
        &i |-> x_i $
]

#yexample[
    On pose $E = {1, 2, 3, 4}$.

    L'arrangement $(4, 3, 1, 2)$ correspond
    à la bijection
    $ &1 |-> 4 \
        &2 |-> 3 \
        &3 |-> 1 \
        &4 |-> 2 $
    
    Réciproquement, la bijection
    $ &1 |-> 1 \
        &2 |-> 3 \
        &3 |-> 4 \
        &4 |-> 2 $
    correspond à l'arrangement $(1, 3, 4, 2)$.
]

#yexample[
    On pose $E = {triangle, square, penta, hexa}$.

    $ g: &triangle |-> penta \
        &hexa |-> square \
        &square |-> hexa \
        &penta |-> triangle $
    
    $ phi: &E --> [|1, 4|] \
        &triangle |-> 1 \
        &square |-> 2 \
        &penta |-> 3 \
        &hexa |-> 4 $
    
    $ f: &1 |-> penta \
        &2 |-> hexa \
        &3 |-> triangle \
        &4 |-> square $
]