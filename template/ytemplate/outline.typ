#let yconf_numering(doc) = {
    set heading(numbering: "1.1")

    show heading: it => block(
        if it.level == 1 {
            [Chapitre ] + numbering(
                it.numbering,
                ..counter(heading).at(it.location())
            ) + [ : ]
        } else if it.level == 2 {
            numbering(
                it.numbering,
                ..counter(heading).at(it.location()).slice(1)
            ) + [. ]
        } + it.body
    )
    doc
}


#let ychap(name) = [ #heading(level: 1, name) <chap> ]

#let ypart(name) = [ #heading(level: 2, name) <part> ]

#let ydate(y, m, d) = {
    let weekday = datetime(year: y, month: m, day: d).weekday()
    let weekday_kanji = if (weekday == none) {
        "?"
    } else {
        ("月", "火", "水", "木", "金", "土", "日").at(weekday - 1)
    }

    [#y 年#m 月#d 日 (#weekday_kanji)]
}