#let yconf_math(doc) = {
    show math.equation.where(block: true): mathblock => {
        set align(left)
        pad(left: 8%, mathblock)
    }
    doc
}

#import "@preview/riesketcher:0.2.1": riesketcher
#import "@preview/cetz:0.2.2": canvas


#let ymega_emph(doc) = text(gradient.linear(..color.map.rainbow), weight: "black")[#underline[#emph[#doc]]]


#let _y_tab_trans_circ = text(fill: rgb("#00000000"), sym.circle.filled.tiny)
#let _y_tab = {
    _y_tab_trans_circ * 2
    sym.circle.filled.tiny
    _y_tab_trans_circ * 2
}

#let yt = $\ #_y_tab$
#let ytt = $yt #_y_tab$
#let yttt = $ytt #_y_tab$
#let ytttt = $yttt #_y_tab$
#let yttttt = $ytttt #_y_tab$

#let ytttttt = $yttttt #_y_tab$
#let yttttttt = $ytttttt #_y_tab$
#let ytttttttt = $yttttttt #_y_tab$
#let yttttttttt = $ytttttttt #_y_tab$
#let ytttttttttt = $yttttttttt #_y_tab$