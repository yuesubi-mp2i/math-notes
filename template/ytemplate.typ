#import "@preview/cetz:0.1.2"

#import "ytemplate/blocks.typ": *
#import "ytemplate/math_layout.typ": *
#import "ytemplate/theme.typ": theme
#import "ytemplate/outline.typ": *


#let yfg = theme.fg
#let ybg = theme.bg


#let ychapconf(doc) = {
    // set page(fill: theme.bg)
    // set text(fill: theme.fg)
    
    columns(
        2, 
        yconf_math(
            yconf_numering(doc)
        )
    )
}