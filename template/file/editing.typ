#set page(
    width: 1080pt,
    height: auto,
    margin: (x: 20pt, y: 20pt),
    fill: rgb("#282828")
)

#set text(
    size: 62pt,
    fill: rgb("#ebdbb2")
)

#import "../template/ytemplate.typ": yconf_math
#show: yconf_math