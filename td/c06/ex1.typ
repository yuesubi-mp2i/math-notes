Déterminer les solutions réelles des équations différentielles suivantes :
- $(E_1) : y' - y = x^2$
- $(E_2) : y' = (ln x)3y$
- $(E_3) : x y' ln x = (3 ln x + 1)y$
- $(E_4) : x y' - y = x^2 e^x$
- $(E_5) : x y' - y = x$
- $(E_6) : y' sin x - y cos x = sin x - x cos x$
- $(E_7) : y' = e^(x+y)$


== Solutions de $E_1$

#show regex("(variation de la constante)"): word => {
    [ #sym.quote.angle.l.double #word #sym.quote.angle.r.double ]
}

Soit $y$ solution de $E_1$.

Par la variation de la constante, il existe une solution, disons $y_0$, de
$E_1$, telle que
$ y_0 = lambda(x) e^x $
où $lambda$ est dérivable sur $RR$.

$ y_0 &"solution de" E_1 \
    &<=> lambda'(x) e^x + cancel(lambda(x) e^x) - cancel(lambda(x) e^x) = x^2 \
    &<=> lambda'(x) = x^2 e^(-x) \
    &<=> lambda(x) = integral x^2 e^(-x) dif x \
    &<=> lambda(x) = -x^2 e^(-x) - integral x (-e^(-x)) dif x \
    &<=> lambda(x) = -x^2 e^(-x) - x e^(-x) + integral e^(-x) dif x \
    &<=> lambda(x) = -x^2 e^(-x) - x e^(-x) -e^(-x) $