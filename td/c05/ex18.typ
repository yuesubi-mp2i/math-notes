= Exercice 18

== Question 1
Pour $n in NN$.

$ 0 <= abs(integral_0^1 x^n/(1 + x) dif x)
    &= integral_0^1 x^n/(1+x) dif x \
    &<= integral x^n dif x \
    &<= 1/(n+1) \
    &-->_(n -> oo) 0 $

Donc $I_n -->_(n -> oo) 0$.


== Question 2
Soit $n in NN$.$$

$ I_n + I_(n+1) &= integral_0^1 (x^n + x^(n+1))/(1 + x) dif x \
    &= integral_0^1 x^n dif x \
    &= 1/(n+1) $


== Question 3
Soit $n in NN^*$.

$ sum_(k=1)^n (-1)^(k+1)/k
    &= sum_(k=1)^n (-1)^(k+1) (I_(k -1) + I_k) \
    &= sum_(k=1)^n (-1)^(k-1) I_(k-1) - sum_(k=1)^n (-1)^k I_k \
    &= sum_(k=1)^n [ (-1)^(k-1) I_(k-1) - (-1)^k I_k ] \
    &= I_0 - (-1)^n I_n $

$forall n, abs((-1)^n I_n) = I_n -->_(n -> oo) 0$

$ lim_(n -> oo) sum_(k=1)^n (-1)^(k + 1)/k
    &= I_0 \
    &= integral_0^1 (dif x)/(1 + x) \
    &= ln(2) $