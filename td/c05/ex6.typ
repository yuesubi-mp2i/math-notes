= Exercice 6

On pose pour $k in NN$, $theta_k = 2^k n$ et $z_k = e^( i theta_k)$.

== Brouillon
Soit $k in NN$.

$ product_(j=0)^(k-1) 1/cos(theta_j) =
    product_(j=0)^(k-1) 2/(z_j + overline(z_j)) $

+ $2/(z_0 + overline(z_0)) = 2 (z_0 - overline(z_0))/(z_0^2 - overline(z_0)^2)$

+ ...

Conjecture :
$product_(j=0)^(k-1) 1/cos(theta_j) =
    2^k (z_0 - overline(z_0))/(z_0^(2^k) + overline(z_0)^(2^k))$

On cherche un formule explicite pour
$ sum_(k=1)^n (z_0 - overline(z_0))/(z_0^(2^k) - overline(z_0)^(2^k)) $

Conjecture :
$ sum_(k=1)^n 1/2^k product_(j=1)^(k-1) 1/cos(theta_j)
    &= (z_0^(2^n -1) - overline(z_0)^(2^n -1))/(z^(2^n) - overline(z)^(2^n)) $