= Exercice 5

Soit $ n >= 1$.

$ product_(k=1) ^n (4k -2) &= 2^n product_(k=1)^n (2k - 1) \
    &= 2^n ((2n-1)!)/(2^(n-1) (n-1)!) \
    &= 2 ((2n - 1)!)/((n-1)!) \
    &= ((2n)!)/(n(n-1)!) \
    &= (2n)!/n $

$ product_(k=1)^n (n +k) &= (n +1)(n+2) dots (n + n) \
    &= product_(j = n+1)^(2n) j \
    &= (2n)!/n! $
