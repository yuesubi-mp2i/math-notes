#!/usr/bin/python3

from mn import *


def main():
    args = Args.parse()

    if isinstance(args, Args.Anki):
        run.anki(args)
    elif isinstance(args, Args.Apkg):
        run.apkg(args)
    elif isinstance(args, Args.Class):
        run.class_(args)
    elif isinstance(args, Args.Complete):
        run.complete(args)
    elif isinstance(args, Args.Draft):
        run.draft(args)
    elif isinstance(args, Args.Td):
        run.td(args)
    else:
        raise NotImplementedError(args)


if __name__ == "__main__":
    main()