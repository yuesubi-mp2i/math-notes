{ pkgs ? import <nixpkgs> { } }:
with pkgs;
mkShell {
  buildInputs = [
    typst

    (python3.withPackages (python-pkgs: with python-pkgs; [
      genanki
    ]))
  ];
}
