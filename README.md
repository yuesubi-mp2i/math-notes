# MP2I Math Notes
My math class notes of the year.
All the notes are written in [Typst](https://typst.app).

__CHECK OUT the generated PDF :__
- ✨ [math_notes.pdf](https://gitlab.com/api/v4/projects/51473655/jobs/artifacts/main/raw/math_notes.pdf?job=normal-pdf) ✨
- ✨ [math_notes_webtoon.pdf](https://gitlab.com/api/v4/projects/51473655/jobs/artifacts/main/raw/math_notes_webtoon.pdf?job=webtoon-pdf) ✨

__Get the Anki decks :__ [BROWSE](/anki-index.md)


## How to use (generating the deck)
Warning : the chapters 1 to 5 (included) are not generated with this script,
so the anki decks are empty ! Get those decks directy from me instead.

+ Be on Linux
+ Install dependencies :
    - `typst` (so that it is accessible via the terminal)
    - `python` a recent version of python 3
    - `genanki` a python library for generating anki decks

Instructions
- generate the 14th deck `python mn.py apkg 14`
- edit a the 1st paragraph of chapter 30 `python mn.py class 30 1`
- the name of the deck in Anki depends on the first line of the `chap.typ` file
    in each chapter folder


## How Anki generation works
- Scans, with a custom parser, the source for blocks of code
- Creates a file with the code of said block and compiles it to svg
- Packs up every card in a deck