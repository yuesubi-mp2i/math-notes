= Voiture et pompes à essense

== Énoncé
_On considère un circuit automobile, disposant de $n$ pompes à essance.
La quantité totale de carburant correcond exactement à la quantité
nécessaire pour parcourir une fois le circuit en entier._

_Le véhicule a un réservoir vide au départ.
Montrer qu'il est possible de choisir la position de départ de la voiture
pour effectuer le circuit._

== Solution

Soit $n in NN^*$ le nombre de pompes à essenses.

On note, pour $i in [|1, n|]$, $e_i in RR^+$
le carburant de la $i$-ème pompe à essense.

On note, pour $i in [|1, n|]$, $d_i in RR^+$
la distance entre la $i$-ème pompe et la suivante.

On sait par l'énoncé que
$ sum_(i=1)^n e_i = sum_(i=1)^n d_i $

On suppose qu'il n'existe pas de position de départ
qui permet de parcourir tout le circuit, i.e.
$ forall a in [|1, n|], exists b in [|a, n+a|], $
$ sum_(i=a)^(min(b, n)) (e_i &- d_i) \
    &+ sum_(i=0)^(max(0, b-n)) (e_i - d_i) < 0 $

Soit $a in [|1, n|]$.
On considère un tel $b in [|a, n+a|]$.
$ sum_(i=a)^(min(b, n)) e_i &- sum_(i=a)^(min(b, n)) d_i \
    &+ sum_(i=0)^(max(0, b-n)) e_i \
    &- sum_(i=0)^(max(0, b-n)) d_i < 0 $

- Cas 1, on suppose $b <= n$ :

    $ sum_(i=a)^b e_i - sum_(i=a)^b d_i < 0 $
    donc
    $ sum_(i in [|1, a-1|] union [|b+1, n|]) (e_i - d_i) > 0 $

On pose pour 