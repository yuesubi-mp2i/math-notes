#set heading(numbering: "1.1")

#show heading: it => block(
    if it.level == 1 {
        [Chapter ] + numbering(
            it.numbering,
            ..counter(heading).at(it.location())
        ) + [ : ]
    } else {
        numbering(
            it.numbering,
            ..counter(heading).at(it.location()).slice(1)
        ) + [. ]
    } + it.body
)


= Naïve set theory
== Sets
== Functions and applications

= Natural numbers
== Construction
== Some other thing