#show link: underline

== Énoncé

#link("https://discord.com/channels/856613808159326279/936365267775287356/1169643242468425820"
)[_Message d'Ywan_]

_Le polynôme $P(x)$ est de degré $n >= 5$, à coefficients entiers et possède
$n$ racines entières distinctes $x_1, dots, x_n$._

_Touvez toutes les racines entières du polynôme $P compose P(x)$ en fonction de
$x_1, dots, x_n$ sachant que $x_1 = 0$._


== Brouillon
$P(x) = lambda x product_(i=2)^n (x - x_i)$

Comme les coefficients sont entiers, pour $ i in [|1, n|]$,
$lambda (x - x_i) in ZZ$.




$ P &compose P(x) \
    &= lambda x product_(i=2)^n (lambda x product_(j=2)^n (x - x_j) - x_i) \
    &= lambda^(n+1) x^(n+1) product_(i=2)^n (product_(j=2)^n (x - x_j) - x_i) \
    $


#lorem(50)