#show math.equation.where(block: true): mathblock => {
    set align(left)
    pad(left: 8pt, mathblock)
}

#let _y_math_spacing = 64pt
#let _y_tab = {
    h(_y_math_spacing / 2)
    style(styles => {
        
        sym.circle.filled.tiny
    })
    h(_y_math_spacing / 2)
}

#let yt = $\ #_y_tab$
#let ytt = $yt #_y_tab$
#let yttt = $ytt #_y_tab$


= Preuve de la primitivation d'un DL

Soit $f: I --> RR$ continue sur l'intervalle $I$, et admettant un $"DL"_n (a)$
tel que ...

Alors $F$ admet un $"DL"_(n+1) (a)$
$ F(x) =_a F(a) + sum_(k=0)^n (alpha_k (x-a)^(k+1))/(k + 1)
    yt + o((x-a)^(n+1))
    ytt "hello world"
    yttt "3 tabs" $

== Preuve
