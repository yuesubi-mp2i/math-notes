= Inégalité triangulaire (somme)
Soit $(a_i) in CC^I$, $I$ fini.

$ abs(sum_(i in I) a_i) <= sum_(i in I) abs(a_i) $


= Preuve
On note $EE$, l'ensemble des ensembles finis.

On pose pour $n in NN$ le prédicat :
$ P(n): &forall I in EE, |I| = n, forall (a_i) in CC^I, \
    &abs(sum_(i in I) a_i) <= sum_(i in I) abs(a_i) $

*Initialisation* : 
Pour $n = 0$, le seul ensemble de cardinal nul est $emptyset$.
- D'une part, $abs(sum_(i in emptyset) a_i) = 0$
- D'autre part, $sum_(i in emptyset) abs(a_i) = 0$
Donc $P(0)$ est vraie.

*Hérédité* :
Soit un rang $n in NN$ tel que $P(n)$ est vraie.

Soient $J in EE$ de cardinal $n + 1$, tel que $I subset J$,
où $I in EE$ de cardinal $n$.

Soient $(b_j) in CC^J$ et $(a_i) in CC^I$, tel que pour $i in I$, $b_i = a_i$.

On note $beta$ le seul élément de $J without I$.

Selon l'hypothèse de récurrence, on a
$ abs(sum_(i in I) a_i) <= sum_(i in I) abs(a_i) $
donc
$ abs(sum_(i in I) a_i + beta)
    <= abs(sum_(i in I) a_i) + abs(beta)
    <= sum_(j in J) abs(a_j) $
donc
$ abs(sum_(j in J) a_j) <= sum_(j in J) abs(a_j) $

donc $P(n + 1)$ est vraie.

Ainsi, pour tout $n$, $P(n)$ est vraie, et donc pour tout ensemble $I$ fini et
toute famille de complexes indexés sur $I$, l'inégalité triangulaire est vraie.
$square$